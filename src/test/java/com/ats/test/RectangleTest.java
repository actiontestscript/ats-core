package com.ats.test;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Vector;

import org.junit.jupiter.api.Test;

import com.ats.data.Rectangle;

public class RectangleTest {

	@Test
	public void rectContainsTrue() {

		Vector<Rectangle[]> rects = new Vector<>();
		rects.add(new Rectangle[]{new Rectangle(0, 0, 1, 1), new Rectangle(0, 0, 1, 1)});
		rects.add(new Rectangle[]{new Rectangle(-1, -1, 1, 1), new Rectangle(0, 0, 0, 0)});
		rects.add(new Rectangle[]{new Rectangle(0, 0, 1, 1), new Rectangle(1, 1, 0, 0)});
		rects.add(new Rectangle[]{new Rectangle(1, 1, 1, 1), new Rectangle(1, 1, 1, 1)});
		rects.add(new Rectangle[]{new Rectangle(0, 0, 2, 2), new Rectangle(1, 1, 1, 1)});
		rects.add(new Rectangle[]{new Rectangle(0, 0, 10, 10), new Rectangle(0, 0, 10, 10)});
		rects.add(new Rectangle[]{new Rectangle(0, 0, 10, 10), new Rectangle(1, 1, 9, 9)});
		rects.add(new Rectangle[]{new Rectangle(-10, -10, 10, 10), new Rectangle(-10, -10, 10, 10)});
		rects.add(new Rectangle[]{new Rectangle(-10, -10, 10, 10), new Rectangle(-9, -9, 9, 9)});
		rects.add(new Rectangle[]{new Rectangle(0.01, 0.01, 1.01, 1.01), new Rectangle(0.01, 0.01, 1.01, 1.01)});
		rects.add(new Rectangle[]{new Rectangle(0.01, 0.01, 1.01, 1.01), new Rectangle(0.02, 0.02, 1.0, 1.0)});
		rects.add(new Rectangle[]{new Rectangle(0.01, 0.01, 10.01, 10.01), new Rectangle(0.01, 0.01, 10.0, 10.0)});
		rects.add(new Rectangle[]{new Rectangle(0.01, 0.01, 10.01, 10.01), new Rectangle(0.01, 0.01, 10.01, 10.01)});
		rects.add(new Rectangle[]{new Rectangle(-10.01, -10.01, 10.01, 10.01), new Rectangle(-10.01, -10.01, 10.01, 10.01)});
		rects.add(new Rectangle[]{new Rectangle(-10.01, -10.01, 10.01, 10.01), new Rectangle(-10.01, -10.01, 10.01, 10.01)});
		rects.add(new Rectangle[]{new Rectangle(10, 10, 5, 5), new Rectangle(10.01, 10.01, 4.99, 4.99)});

		rects.add(new Rectangle[]{new Rectangle(10, 10, 20, 20), new Rectangle(10, 10, 19, 19)});
		rects.add(new Rectangle[]{new Rectangle(-10, -10, 20, 20), new Rectangle(-9, -9, 19, 19)});

		for (Rectangle[] r : rects) {
			if(!r[0].contains(r[1])) {
				fail("Not contains");
			}
		}

	}

	@Test
	public void rectContainsFalse() {

		Vector<Rectangle[]> rects = new Vector<>();
		rects.add(new Rectangle[]{new Rectangle(-1, -1, 1, 1), new Rectangle(0, 0, 0, 1)});
		rects.add(new Rectangle[]{new Rectangle(0, 0, 1, 1), new Rectangle(1, 1, 1, 1)});
		rects.add(new Rectangle[]{new Rectangle(-10, -10, 5, 5), new Rectangle(-11, -11, 20, 20)});
		rects.add(new Rectangle[]{new Rectangle(10, 10, 5, 5), new Rectangle(9, 9, 5, 5)});
		rects.add(new Rectangle[]{new Rectangle(10, 10, 5, 5), new Rectangle(9.99, 9.99, 4.99, 4.99)});
		rects.add(new Rectangle[]{new Rectangle(10, 10, 5, 5), new Rectangle(10.02, 10.01, 4.99, 4.99)});

		for (Rectangle[] r : rects) {
			if(r[0].contains(r[1])) {
				fail("Contains but should not");
			}
		}
	}
}