package com.ats.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import com.ats.tools.telemetry.Collector;
import com.ats.tools.telemetry.CollectorBuilder;

import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.sdk.common.CompletableResultCode;
import io.opentelemetry.sdk.metrics.InstrumentType;
import io.opentelemetry.sdk.metrics.data.AggregationTemporality;
import io.opentelemetry.sdk.metrics.data.MetricData;
import io.opentelemetry.sdk.metrics.export.MetricExporter;
import io.opentelemetry.sdk.metrics.internal.data.ImmutableLongPointData;
import io.opentelemetry.sdk.metrics.internal.data.ImmutableMetricData;

public class CollectorTest {
	
	@Test
	public void testExportWhenClose() throws InterruptedException, IOException {
		CollectorBuilder collectorBuilder = Collector.builder();
		MockMetricExporter metricExporter = getMetricExporterMock();
		collectorBuilder.withMetricExporter(metricExporter);
		Collector collector = collectorBuilder.build();
		collector.addCount("c1", 1);
		collector.addCount("c2",2);
		collector.close();
		Object[] array = metricExporter.exportedCollection.toArray();
		assertEquals(2, array.length);
	}

	@Test
	public void testCounterCumul() {
		CollectorBuilder collectorBuilder = Collector.builder();
		MockMetricExporter metricExporter = getMetricExporterMock();
		collectorBuilder.withMetricExporter(metricExporter);
		Collector collector = collectorBuilder.build();
		collector.addCount("c1", 1);
		collector.addCount("c1",3);
		collector.close();
		Object[] array = metricExporter.exportedCollection.toArray();
		long value = ((ImmutableLongPointData)(((ImmutableMetricData)array[0]).getData().getPoints().toArray()[0])).getValue();
		assertEquals(1, array.length);
		assertEquals(4, value);
	}

	@Test
	public void testCounterAttribut() {
		CollectorBuilder collectorBuilder = Collector.builder();
		MockMetricExporter metricExporter = getMetricExporterMock();
		collectorBuilder.withMetricExporter(metricExporter);
		collectorBuilder.withResources(Collections.singletonMap("attribute1", "value1"));
		Collector collector = collectorBuilder.build();
		collector.addCount("c1", 1);
		collector.close();
		Object[] array = metricExporter.exportedCollection.toArray();
		ImmutableMetricData immutableMetricData = (ImmutableMetricData)array[0];
		assertEquals(1, array.length);
		assertEquals("value1", immutableMetricData.getResource().getAttribute(AttributeKey.stringKey("attribute1")));
	}
	public MockMetricExporter getMetricExporterMock() {
		return new MockMetricExporter();
	}

	private class MockMetricExporter implements MetricExporter {
		public Collection<MetricData> exportedCollection;
		@Override
		public CompletableResultCode export(Collection<MetricData> collection) {
			exportedCollection = collection;
			return CompletableResultCode.ofSuccess();
		}

		@Override
		public CompletableResultCode flush() {
			return CompletableResultCode.ofSuccess();
		}

		@Override
		public CompletableResultCode shutdown() {
			return CompletableResultCode.ofSuccess();
		}

		@Override
		public AggregationTemporality getAggregationTemporality(InstrumentType instrumentType) {
			return AggregationTemporality.CUMULATIVE;
		}
	}
}
