package com.ats.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import com.ats.crypto.Password;
import com.ats.crypto.Passwords;
import com.ats.executor.ActionTestScript;
import com.ats.generator.variables.CalculatedValue;

public class PasswordsTest {


	@TempDir
	File tempFolder;

	/*
	 * @Before public void setUp() { }
	 *
	 * @After public void tearDown() { }
	 */

	@Test
	public void createPasswordsFile() throws IOException {

		final String[] names = new String[] {"passw1", "passw2", "specials", "numbers", "name1"};
		final String[] values = new String[] {"cryptedValue", "", "é&#')@à'", "1235456789", "very_long_password_value_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"};

		final File folder = new File(tempFolder, "tc");
		final Passwords setPasswords = new Passwords(folder.toPath());
		for(int i = 0; i< names.length; i++) {
			setPasswords.setPassword(names[i], values[i]);
		}

		final Passwords readPasswords = new Passwords(folder);
		for(int i = 0; i< names.length; i++) {
			assertEquals(readPasswords.getPassword(names[i]), values[i]);
		}

		folder.deleteOnExit();
	}

	@Test
	public void checkPasswordValue() throws IOException {

		final String passwordName = "passw1";
		String passwordValue = UUID.randomUUID().toString();

		final ActionTestScript script = new ActionTestScript(new File(tempFolder, "tc"));

		final Passwords passwords = script.getPasswords();
		passwords.setPassword(passwordName, passwordValue);

		Password pass = new Password(script, passwordName);

		assertEquals(pass.getValue(), passwordValue);
		assertEquals(script.getPassword(passwordName), passwordValue);

		passwordValue = UUID.randomUUID().toString();
		passwords.setPassword(passwordName, passwordValue);

		assertEquals(pass.getValue(), passwordValue);
		assertEquals(script.getPassword(passwordName), passwordValue);
	}

	@Test
	public void passwordInVariable() throws IOException {

		final String passwordName = "passw1";
		String passwordValue = UUID.randomUUID().toString();

		final ActionTestScript script = new ActionTestScript(new File(tempFolder, "tc"));

		final Passwords passwords = script.getPasswords();
		passwords.setPassword(passwordName, passwordValue);

		CalculatedValue calc1 = new CalculatedValue(script, "$pass(passw1)");
		script.createVariable("var1", calc1, null);

		CalculatedValue calc2 = new CalculatedValue(script, "$var(var1)");

		assertEquals(calc2.getCalculatedText(script, false).get(0).getData(), passwordValue);

	}
}