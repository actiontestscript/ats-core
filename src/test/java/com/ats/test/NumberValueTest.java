package com.ats.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import com.ats.executor.ActionTestScript;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.Variable;
import com.ats.generator.variables.transform.NumericTransformer;

public class NumberValueTest {

	@TempDir
	File tempFolder;

	/*
	 * @Before public void setUp() { }
	 *
	 * @After public void tearDown() { }
	 */

	@Test
	public void transformValues() throws IOException {

		boolean useComma = false;

		final ArrayList<Object[]> items = new ArrayList<>();

		items.add(new Object[] {"20.10", "20.1", -1, useComma});
		items.add(new Object[] {"20.1000", "20.1", -1, useComma});
		items.add(new Object[] {"1 020.1", "1020.1", -1, useComma});
		items.add(new Object[] {"1 020.12", "1020.12", -1, useComma});
		items.add(new Object[] {"1 020.123", "1020.123", -1, useComma});
		items.add(new Object[] {"1  020.1", "1020.1", -1, useComma});
		items.add(new Object[] {"1 100 020.1", "1100020.1", -1, useComma});
		items.add(new Object[] {"1,100,020.10", "1100020.1", -1, useComma});
		items.add(new Object[] {"20", "20", -1, useComma});
		items.add(new Object[] {"20", "20", -1, useComma});
		items.add(new Object[] {"1 020", "1020", -1, useComma});
		items.add(new Object[] {"1  020", "1020", -1, useComma});
		items.add(new Object[] {"1 100 020", "1100020", -1, useComma});
		items.add(new Object[] {"1,100,020", "1100020", -1, useComma});

		items.add(new Object[] {"20.10", "20", 0, useComma});
		items.add(new Object[] {"20.1000", "20", 0, useComma});
		items.add(new Object[] {"1 020.1", "1020", 0, useComma});
		items.add(new Object[] {"1 020.123", "1020", 0, useComma});
		items.add(new Object[] {"1 020.5", "1021", 0, useComma});
		items.add(new Object[] {"1  020.1", "1020", 0, useComma});
		items.add(new Object[] {"1 100 020.1", "1100020", 0, useComma});
		items.add(new Object[] {"1,100,020.5", "1100021", 0, useComma});
		items.add(new Object[] {"1,100,020.10", "1100020", 0, useComma});
		items.add(new Object[] {"20", "20", 0, useComma});
		items.add(new Object[] {"20", "20", 0, useComma});
		items.add(new Object[] {"1 020", "1020", 0, useComma});
		items.add(new Object[] {"1  020", "1020", 0, useComma});
		items.add(new Object[] {"1 100 020", "1100020", 0, useComma});
		items.add(new Object[] {"1,100,020", "1100020", 0, useComma});

		items.add(new Object[] {"20.1", "20.1000", 4, useComma});
		items.add(new Object[] {"20.10", "20.1000", 4, useComma});
		items.add(new Object[] {"20.1000", "20.1000", 4, useComma});
		items.add(new Object[] {"1 020.1", "1020.1000", 4, useComma});
		items.add(new Object[] {"1  020.1", "1020.1000", 4, useComma});
		items.add(new Object[] {"1 100 020.1", "1100020.1000", 4, useComma});
		items.add(new Object[] {"1,100,020.10", "1100020.1000", 4, useComma});
		items.add(new Object[] {"20", "20.0000", 4, useComma});
		items.add(new Object[] {"20", "20.0000", 4, useComma});
		items.add(new Object[] {"1 020", "1020.0000", 4, useComma});
		items.add(new Object[] {"1  020", "1020.0000", 4, useComma});
		items.add(new Object[] {"1 100 020", "1100020.0000", 4, useComma});
		items.add(new Object[] {"1,100,020", "1100020.0000", 4, useComma});
		items.add(new Object[] {"100,100,020", "100100020.0000", 4, useComma});

		items.add(new Object[] {"20.100015", "20.10002", 5, useComma});
		items.add(new Object[] {"20.100014", "20.10001", 5, useComma});
		items.add(new Object[] {"20.1000149", "20.10001", 5, useComma});

		//-------------------------------------------------------------------------
		// calculation with dot decimal separator
		//-------------------------------------------------------------------------

		items.add(new Object[] {"20.10+0.9", "21", -1, useComma});
		items.add(new Object[] {"1+2", "3", -1, useComma});
		items.add(new Object[] {"3*4", "12", -1, useComma});
		items.add(new Object[] {"3*4.0", "12", -1, useComma});
		items.add(new Object[] {"5/2", "2.5", -1, useComma});
		items.add(new Object[] {"10/3", "3.3333", 4, useComma});
		items.add(new Object[] {"5-2.789", "2.2110", 4, useComma});
		items.add(new Object[] {"5-2.789", "2.211", -1, useComma});
		items.add(new Object[] {"5-2.789", "2", 0, useComma});
		items.add(new Object[] {"2.45^4", "36.030006250000014", -1, useComma});
		items.add(new Object[] {"sin(45.23)", "0.94826", 5, useComma});

		items.add(new Object[] {"1>2", "0", -1, useComma});
		items.add(new Object[] {"1<2", "1", -1, useComma});
		items.add(new Object[] {"1.00001>1.000001", "1", -1, useComma});
		items.add(new Object[] {"1.00001>=1.000001", "1", -1, useComma});
		items.add(new Object[] {"1.00001>=1.00001", "1", -1, useComma});
		items.add(new Object[] {"1.00001>1.00001", "0", -1, useComma});
		items.add(new Object[] {"1.00001<=1.00002", "1", -1, useComma});

		//-------------------------------------------------------------------------
		// comma decimal separator
		//-------------------------------------------------------------------------

		useComma = true;

		items.add(new Object[] {"20,10", "20,1", -1 , useComma});
		items.add(new Object[] {"20,1000", "20,1", -1 , useComma});
		items.add(new Object[] {"1 020,1", "1020,1", -1 , useComma});
		items.add(new Object[] {"1 020,12", "1020,12", -1 , useComma});
		items.add(new Object[] {"1 020,123", "1020,123", -1 , useComma});
		items.add(new Object[] {"1  020,1", "1020,1", -1 , useComma});
		items.add(new Object[] {"1 100 020,1", "1100020,1", -1 , useComma});
		items.add(new Object[] {"1100020,10", "1100020,1", -1 , useComma});
		items.add(new Object[] {"20", "20", -1 , useComma});
		items.add(new Object[] {"  20", "20", -1 , useComma});
		items.add(new Object[] {"1 020", "1020", -1 , useComma});
		items.add(new Object[] {"1  020", "1020", -1 , useComma});
		items.add(new Object[] {"1 100 020", "1100020", -1 , useComma});
		items.add(new Object[] {"1100020", "1100020", -1 , useComma});

		items.add(new Object[] {"20,10", "20", 0 , useComma});
		items.add(new Object[] {"20,1000", "20", 0 , useComma});
		items.add(new Object[] {"1 020,1", "1020", 0 , useComma});
		items.add(new Object[] {"1 020,123", "1020", 0 , useComma});
		items.add(new Object[] {"1 020,5", "1021", 0 , useComma});
		items.add(new Object[] {"1  020,1", "1020", 0 , useComma});
		items.add(new Object[] {"1 100 020,1", "1100020", 0 , useComma});
		items.add(new Object[] {"1100020,5", "1100021", 0 , useComma});
		items.add(new Object[] {"1100020,10", "1100020", 0 , useComma});
		items.add(new Object[] {"20", "20", 0 , useComma});
		items.add(new Object[] {"20", "20", 0 , useComma});
		items.add(new Object[] {"1 020", "1020", 0 , useComma});
		items.add(new Object[] {"1  020", "1020", 0 , useComma});
		items.add(new Object[] {"1 100 020", "1100020", 0 , useComma});
		items.add(new Object[] {"1100020", "1100020", 0 , useComma});

		items.add(new Object[] {"20,1", "20,1000", 4 , useComma});
		items.add(new Object[] {"20,10", "20,1000", 4 , useComma});
		items.add(new Object[] {"20,1000", "20,1000", 4 , useComma});
		items.add(new Object[] {"1 020,1", "1020,1000", 4 , useComma});
		items.add(new Object[] {"1  020,1", "1020,1000", 4 , useComma});
		items.add(new Object[] {"1 100 020,1", "1100020,1000", 4 , useComma});
		items.add(new Object[] {"1100020,10", "1100020,1000", 4 , useComma});
		items.add(new Object[] {"  20", "20,0000", 4 , useComma});
		items.add(new Object[] {"20", "20,0000", 4 , useComma});
		items.add(new Object[] {"1 020", "1020,0000", 4 , useComma});
		items.add(new Object[] {"1  020", "1020,0000", 4 , useComma});
		items.add(new Object[] {"1 100 020", "1100020,0000", 4 , useComma});
		items.add(new Object[] {"1100020", "1100020,0000", 4 , useComma});

		items.add(new Object[] {"20,100015", "20,10002", 5 , useComma});
		items.add(new Object[] {"20,100014", "20,10001", 5 , useComma});
		items.add(new Object[] {"20,1000149", "20,10001", 5 , useComma});

		items.add(new Object[] {"1>2", "0", -1, useComma});
		items.add(new Object[] {"1<2", "1", -1, useComma});
		items.add(new Object[] {"1,00001>1,000001", "1", -1, useComma});
		items.add(new Object[] {"1,00001>=1,000001", "1", -1, useComma});
		items.add(new Object[] {"1,00001>=1,00001", "1", -1, useComma});
		items.add(new Object[] {"1,00001>1.00001", "0", -1, useComma}); // mix dot and comma
		items.add(new Object[] {"1,00001<=1.00002", "1", -1, useComma}); // mix dot and comma

		//-------------------------------------------------------------------------
		// calculation with comma decimal separator
		//-------------------------------------------------------------------------

		items.add(new Object[] {"20,10+0,9", "21", -1, useComma});
		items.add(new Object[] {"20,10+0.9 + 1.01", "22,01", -1, useComma});
		items.add(new Object[] {"1+2", "3", -1, useComma});
		items.add(new Object[] {"3*4", "12", -1, useComma});
		items.add(new Object[] {"3*4,0", "12", -1, useComma});
		items.add(new Object[] {"5/2", "2,5", -1, useComma});
		items.add(new Object[] {"10/3", "3,3333", 4, useComma});
		items.add(new Object[] {"5-2,789", "2,2110", 4, useComma});
		items.add(new Object[] {"5-2,789", "2,211", -1, useComma});
		items.add(new Object[] {"5-2,789", "2", 0, useComma});
		items.add(new Object[] {"2.45^4", "36,03000625", -1, useComma});
		items.add(new Object[] {"sin(45.23)", "0,70994", 5, useComma});

		//-------------------------------------------------------------------------
		//-------------------------------------------------------------------------

		final ActionTestScript script = new ActionTestScript(new File(tempFolder, "tc"));
		int loop = 0;
		for(Object[] item : items) {
			Variable v = script.createVariable("transformedVariable" + loop, new CalculatedValue(script, item[0].toString()), new NumericTransformer(item[2], item[3]));
			assertEquals(v.getCalculatedValue(), item[1].toString());
		}
	}
}
