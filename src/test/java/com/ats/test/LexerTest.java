package com.ats.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.ats.generator.GeneratorReport;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.generator.parsers.Lexer;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.Variable;
import com.ats.script.AtsScript;
import com.ats.script.actions.Action;
import com.ats.script.actions.ActionAssertCount;
import com.ats.script.actions.ActionAssertProperty;
import com.ats.script.actions.ActionAssertValue;
import com.ats.script.actions.ActionCallscript;
import com.ats.script.actions.ActionChannelClose;
import com.ats.script.actions.ActionChannelStart;
import com.ats.script.actions.ActionChannelSwitch;
import com.ats.script.actions.ActionComment;
import com.ats.script.actions.ActionGotoUrl;
import com.ats.script.actions.ActionMouse;
import com.ats.script.actions.ActionMouseKey;
import com.ats.script.actions.ActionMouseScroll;
import com.ats.script.actions.ActionProperty;
import com.ats.script.actions.ActionScripting;
import com.ats.script.actions.ActionSelect;
import com.ats.script.actions.ActionText;
import com.ats.script.actions.ActionWindowResize;
import com.ats.script.actions.ActionWindowState;
import com.ats.script.actions.ActionWindowSwitch;
import com.ats.script.actions.condition.ExecuteOptions;

public class LexerTest {

	@Test
	public void createActionTest() {
		Lexer lexer = new Lexer(new GeneratorReport());
		AtsScript script = new AtsScript(lexer);

		String[] data = new String[] {
				"channel-start -> myFirstChannel -> chrome",
				"comment -> step -> <i>comment ...</i>",
				"comment-step -> <i>comment ...</i>",
				"comment -> <i>comment ...</i>",
				"comment -> script -> <i>comment ...</i>",
				"comment-script -> <i>comment ...</i>",
				"goto-url -> google.com",
				"keyboard -> automated testing$key(ENTER) -> INPUT [id = lst-ib]",
				"click -> A [text = Test automation - Wikipedia]",
				"scroll -> 300",
				"scroll -> 0 -> A [text = Graphical user interface testing]",
				"over -> IMG [src =~ .*Test_Automation_Interface.png]",
				"channel-close -> myFirstChannel",
				"channel-switch -> newChannel",
				"subscript -> ScriptATS",
				"callscript -> ScriptATS",
				"check-property -> id = test",
				"check-count -> 1 -> INPUT [name = q]",
				"check-value -> test = azerty",
				"property -> text => propertyVar -> INPUT [name = q]",
				"window-resize -> x = 0, y = 0, width = 0, height = 0",
				"window-state -> restore",
				"window-switch [try = 2] -> 1",
				"select -> index = 0 -> div",
				"scripting -> alert(\"test\")"/*,
				"neoload-start",
				"neoload-stop",
				"neoload-container -> myContainer",
				"neoload-record -> pause"*/  // remove Neoload from ats for the moment
			};

		AtsScript expectedScript = new AtsScript(lexer);

		for (int i = 0; i <= 1; i++) {
			expectedScript.addAction(new ActionChannelStart(expectedScript, new ExecuteOptions(), "myFirstChannel", new CalculatedValue(script, "chrome"), Collections.emptyList()), i == 0);
			expectedScript.addAction(new ActionComment(expectedScript, "comment", new ArrayList<>(Arrays.asList("step", "<i>comment ...</i>"))), i == 0);
			expectedScript.addAction(new ActionComment(expectedScript, "comment-step", new ArrayList<>(List.of("<i>comment ...</i>"))), i == 0);
			expectedScript.addAction(new ActionComment(expectedScript, "comment", new ArrayList<>(List.of("<i>comment ...</i>"))), i == 0);
			expectedScript.addAction(new ActionComment(expectedScript, "comment", new ArrayList<>(Arrays.asList("script", "<i>comment ...</i>"))), i == 0);
			expectedScript.addAction(new ActionComment(expectedScript, "comment-script", new ArrayList<>(List.of("<i>comment ...</i>"))), i == 0);
			expectedScript.addAction(new ActionGotoUrl(expectedScript, new ExecuteOptions(), 0, new CalculatedValue(script, "google.com")), i == 0);
			expectedScript.addAction(new ActionText(expectedScript, new ExecuteOptions(), 0, "automated testing$key(ENTER)", new ArrayList<>(List.of("INPUT [id = lst-ib]"))), i == 0);
			expectedScript.addAction(new ActionMouseKey(expectedScript, new ExecuteOptions(), "click", 0, new ArrayList<>(List.of("A [text = Test automation - Wikipedia]"))), i == 0);
			expectedScript.addAction(new ActionMouseScroll(expectedScript, new ExecuteOptions(), "300", 0, new ArrayList<>()), i == 0);
			expectedScript.addAction(new ActionMouseScroll(expectedScript, new ExecuteOptions(), "0", 0, new ArrayList<>(List.of("A [text = Graphical user interface testing]"))), i == 0);
			expectedScript.addAction(new ActionMouse(expectedScript, new ExecuteOptions(), Mouse.OVER, 0, new ArrayList<>(List.of("IMG [src =~ .*Test_Automation_Interface.png]"))), i == 0);
			expectedScript.addAction(new ActionChannelClose(expectedScript, new ExecuteOptions(), "myFirstChannel"), i == 0);
			expectedScript.addAction(new ActionChannelSwitch(expectedScript, new ExecuteOptions(), "newChannel"), i == 0);
			expectedScript.addAction(new ActionCallscript(expectedScript, new ExecuteOptions(), "ScriptATS", new String[0], new String[0], null, null), i == 0);
			expectedScript.addAction(new ActionCallscript(expectedScript, new ExecuteOptions(), "ScriptATS", new String[0], new String[0], null, null), i == 0);
			expectedScript.addAction(new ActionAssertProperty(expectedScript, new ExecuteOptions(), 0, "id = test", new ArrayList<>()), i == 0);
			expectedScript.addAction(new ActionAssertCount(expectedScript, new ExecuteOptions(), 0, "1", new ArrayList<>(List.of("INPUT [name = q]"))), i == 0);
			expectedScript.addAction(new ActionAssertValue(expectedScript, new ExecuteOptions(), 0, "test = azerty"), i == 0);
			expectedScript.addAction(new ActionProperty(expectedScript, new ExecuteOptions(), 0, "text", new Variable("propertyVar",new CalculatedValue(script, "")), new ArrayList<>(List.of("INPUT [name = q]"))), i == 0);
			expectedScript.addAction(new ActionWindowResize(expectedScript, new ExecuteOptions(), "x = 0, y = 0, width = 0, height = 0"), i == 0);
			expectedScript.addAction(new ActionWindowState(expectedScript, new ExecuteOptions(), "restore"), i == 0);
			expectedScript.addAction(new ActionWindowSwitch(expectedScript, new ExecuteOptions(expectedScript, new String[] {"try = 2"}), new CalculatedValue(script, "1"), ActionWindowSwitch.SWITCH_INDEX, false), i == 0);
			expectedScript.addAction(new ActionSelect(expectedScript, new ExecuteOptions(), "index = 0", 0, new ArrayList<>(List.of("div"))), i == 0);
			expectedScript.addAction(new ActionScripting(expectedScript, new ExecuteOptions(), 0, "alert(\"test\")", null, new ArrayList<>()), i == 0);
			//expectedScript.addAction(new ActionNeoloadStart((Script) expectedScript, "", null), i == 0);
			//expectedScript.addAction(new ActionNeoloadStop((Script) expectedScript, "", null, ""), i == 0);
			//expectedScript.addAction(new ActionNeoloadContainer(expectedScript, "myContainer"), i == 0);
			//expectedScript.addAction(new ActionNeoloadRecord(expectedScript, "pause"), i == 0);
		}

		for (String element : data) {
			lexer.createAction(script, element, true);
		}

		for (String element : data) {
			lexer.createAction(script, element, false);
		}

		List<Action> allActions = script.getActions();
		for (int i = 0; i < allActions.size(); i++) {
			assertEquals(expectedScript.getActions().get(i).getJavaCode().toString(), allActions.get(i).getJavaCode().toString());
		}
	}
}