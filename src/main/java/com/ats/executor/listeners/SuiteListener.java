/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.listeners;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.TestListenerAdapter;

public class SuiteListener extends TestListenerAdapter implements ISuiteListener {

	@Override
	public void onStart(ISuite suite) {
		ISuiteListener.super.onStart(suite);
				
		//System.out.println("*********************************************************************");
		//System.out.println("suite started  -*--> " + suite.getName() + "     tests: " + suite.getXmlSuite().getTests().size());
		//System.out.println("*********************************************************************");
	}

	@Override
	public void onFinish(ISuite suite) {
		ISuiteListener.super.onFinish(suite);
		
		//System.out.println("*********************************************************************");
		//System.out.println("suite finished   " + suite.getName());
		//System.out.println("*********************************************************************");
	}
}