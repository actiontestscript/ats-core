/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor;

import static org.testng.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.TestListenerAdapter;
import org.testng.TestRunner;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import com.ats.AtsSingleton;
import com.ats.crypto.Passwords;
import com.ats.element.ParameterElement;
import com.ats.element.SearchedElement;
import com.ats.element.test.TestElement;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.IDriverEngine;
import com.ats.executor.listeners.ExecutionListener;
import com.ats.executor.listeners.SuiteListener;
import com.ats.executor.listeners.TestListener;
import com.ats.executor.results.SuiteResultCollection;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.MouseDirectionData;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.generator.objects.mouse.MouseKey;
import com.ats.generator.objects.mouse.MouseScroll;
import com.ats.generator.objects.mouse.MouseSwipe;
import com.ats.generator.variables.ApiData;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.RandomStringValue;
import com.ats.generator.variables.TableSplit;
import com.ats.generator.variables.Variable;
import com.ats.generator.variables.parameter.Parameter;
import com.ats.generator.variables.parameter.ParameterList;
import com.ats.generator.variables.transform.DateTransformer;
import com.ats.generator.variables.transform.EvalTransformer;
import com.ats.generator.variables.transform.NumericTransformer;
import com.ats.generator.variables.transform.RegexpTransformer;
import com.ats.generator.variables.transform.TimeTransformer;
import com.ats.generator.variables.transform.Transformer;
import com.ats.learning.AtsLearning;
import com.ats.learning.AtsLearningOff;
import com.ats.learning.IAtsLearning;
import com.ats.recorder.IVisualRecorder;
import com.ats.recorder.TestError;
import com.ats.recorder.VisualRecorder;
import com.ats.recorder.VisualRecorderNull;
import com.ats.script.Project;
import com.ats.script.Script;
import com.ats.script.ScriptHeader;
import com.ats.script.actions.Action;
import com.ats.script.actions.ActionCallscript;
import com.ats.script.actions.ActionChannelStart;
import com.ats.script.actions.ActionComment;
import com.ats.script.actions.ActionCondition;
import com.ats.script.actions.ActionExecute;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.Utils;
import com.ats.tools.logger.ExecutionLogger;
import com.ats.tools.logger.levels.AtsFailError;
import com.ats.tools.report.AtsReport;
import com.ats.tools.report.AtsReporterListener;
import com.ats.tools.report.SuitesReportItem;
import com.ats.tools.report.utils.ReportImageFormat;
import com.ats.tools.telemetry.Collector;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;

@Listeners({ExecutionListener.class, SuiteListener.class, TestListener.class, AtsReporterListener.class})
public class ActionTestScript extends Script{

	public static final String MAIN_TEST_FUNCTION = "testMain";
	public static final String TEST_STOPPABLE = "com.ats.test.stoppable";
	public static final String SUITE_PARAMETERS = "parameters.txt";
	private static final String COLUMN_TEST_ID = "$ats_test_id";
	private static final String COLUMN_TEST_DESCRIPTION = "$ats_test_description";
	private static final String COLUMN_TEST_PREREQUISITE = "$ats_test_prerequisite";
	private static final String COLUMN_TEST_EXTERNAL = "$ats_test_external_id";

	private static final String JSON_RESULT_FILE = "ats-results.json";

	private static final String SUITE_TERMINATED = "suite terminated";
	private static final String AS_A_SUITE_TERMINATED = "callscript 'as a suite' terminated";

	//------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------

	private int verbosity = 0;
	private Map<String, String> testParameters;
	private Path suiteOutputPath;

	private ActionTestScript topScript;
	private String dateOrder = "";

	private String[] retVals;

	private Project projectData;
	
	private boolean xmlPic = false;

	private static final List<String> trueList = Arrays.asList(new String[] {"on", "true", "1", "yes", "y"});

	protected ScriptHeader getScriptHeader() {return scriptHeader;}

	private ScriptStatus status = new ScriptStatus();

	public ActionTestScript() {
		super(null);
		init(new Passwords());
	}

	public ActionTestScript(File assetsFolder) {
		super(null);
		init(new Passwords(assetsFolder.toPath()));
		initShadowScript();
	}

	public ActionTestScript(ExecutionLogger logger) {
		super(logger);
		init(null);
	}

	public ActionTestScript(ActionTestScript topScript) {
		setTopScript(topScript);
	}

	public void setTopScript(ActionTestScript topScript, String scriptName) {
		setTopScript(topScript);
		setTestName(scriptName);
		setTopLogger(topScript.getLogger());
		setOutputPath(topScript.getSuiteOutputPath());
		initShadowScript(topScript.getShadowScript());

		AtsSingleton.getInstance().addCalledScript(this, this.getCanonicalName());
	}

	public void setTopScript(ActionTestScript topScript) {
		init(topScript, topScript.getPasswords());
	}

	private void init(Passwords passwords) {
		init(this, passwords);
	}

	private void init(ActionTestScript topScript, Passwords passwords) {

		this.topScript = topScript;
		this.scriptHeader = topScript.getScriptHeader();
		this.passwords = passwords;

		java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(Level.OFF);
		java.util.logging.Logger.getLogger(Actions.class.getName()).setLevel(Level.OFF);
		java.util.logging.Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);
	}

	public String[] getReturnValues() {
		return retVals;
	}

	public Passwords getPasswords() {
		return passwords;
	}

	public void updateTestName(String name) {
		setTestName(name);
	}

	public void scriptFail(String message) {
		fail(message);
	}

	public ScriptStatus getStatus() {
		return status;
	}

	public int getVerbosity() {
		return verbosity;
	}

	public Map<String, String> getTestParameters() {
		return testParameters;
	}

	public Path getSuiteOutputPath() {
		return suiteOutputPath;
	}

	public void setOutputPath(Path value) {
		this.suiteOutputPath = value;
	}

	public ScriptHeader getHeader() {
		return getScriptHeader();
	}

	//----------------------------------------------------------------------------------------------------------
	// TestNG management
	//----------------------------------------------------------------------------------------------------------

	private long startTime;

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() {

		startTime = System.currentTimeMillis();
		ITestResult itr = Reporter.getCurrentTestResult();

		final SuitesReportItem currentsuite = new SuitesReportItem(scriptHeader.getProjectUuid(), scriptHeader.getProjectId(), (TestRunner) itr.getTestContext());

		setTopLogger(new ExecutionLogger(System.out, AtsSingleton.getInstance().getLogLevel(), scriptHeader.getLogsType()));

		if(AtsSingleton.getInstance().getAtsPropertiesFilePath() != null) {
			getLogger().sendInfo("using '.atsPropertiesFile'", AtsSingleton.getInstance().getAtsPropertiesFilePath());
		}
		
		getLogger().sendScriptSuite(currentsuite.getStartLog());

		ExecutionListener.startSuite(currentsuite);

		if(System.getProperty("xstats") != null && System.getProperty("xstats").equalsIgnoreCase("true")) {
			TimerTask timerTask = new ExecutionStatistics();
			Timer timer = new Timer(true);
			timer.scheduleAtFixedRate(timerTask, 0, 500);
		}
	}

	@AfterSuite(alwaysRun = true)
	public void afterSuite() {

		ITestResult itr = Reporter.getCurrentTestResult();
		ITestContext ctx = itr.getTestContext();

		int testsPassed = 0;
		int testsFailed = 0;

		String logLabel = null;
		boolean nativeSuite = true;

		final int testsCount = ctx.getSuite().getXmlSuite().getTests().size();

		if(testListener != null) {
			logLabel = SUITE_TERMINATED;
		}else {
			logLabel = AS_A_SUITE_TERMINATED;
			nativeSuite = false;
		}
		
		final Map<String, ISuiteResult> result = ctx.getSuite().getResults();

		Iterator<Map.Entry<String, ISuiteResult>> iterator = result.entrySet().iterator();
		while (iterator.hasNext()) {
			final Map.Entry<String, ISuiteResult> entry = iterator.next();
			final ISuiteResult sr = entry.getValue();

			testsPassed += sr.getTestContext().getPassedTests().size();
			testsFailed += sr.getTestContext().getFailedTests().size();
		}
		
		//-------------------------------------------------------------------------------------------------------------------------
		// save suites results json
		//-------------------------------------------------------------------------------------------------------------------------

		final Path jsonResultsPath = Paths.get(ctx.getOutputDirectory()).getParent().resolve(JSON_RESULT_FILE);

		final int testsFiltered = testsCount - testsPassed - testsFailed;

		final String suiteName = ctx.getSuite().getName();
		final JsonObject logs = new JsonObject();
		logs.addProperty("name", suiteName);
		logs.addProperty("passed", testsPassed);
		logs.addProperty("failed", testsFailed);
		logs.addProperty("filtered", testsFiltered);

		try {

			saveJsonData(
					jsonResultsPath,
					ctx.getSuite().getName(),
					testsCount,
					testsPassed,
					testsFailed,
					testsFiltered,
					nativeSuite,
					startTime);

			getLogger().sendInfo("save ATS results", jsonResultsPath.toString());
		} catch (JsonIOException | IOException e) {
			e.printStackTrace();
		}

		getLogger().sendScriptSuite(logLabel + ExecutionLogger.RIGHT_ARROW_LOG + logs.toString());

		//-------------------------------------------------------------------------------------------------------------------------
		// OpenTelemetry
		//-------------------------------------------------------------------------------------------------------------------------

		if(Collector.isOpenTelemetryEnabled()) {

			getLogger().sendScriptSuite("OpenTelemetry is enabled, collecting data results ...");

			Collector collector = Collector.getDefault();
			collector.addCount("tests-count", testsPassed + testsFailed);
			collector.addCount("tests-passed", testsPassed);
			collector.addCount("tests-failed", testsFailed);

			collector.close();
		}
	}

	private void saveJsonData(Path path, String name, int count, int passed, int failed, int filtered, boolean nativeSuite, long started) throws JsonIOException, IOException {

		final Gson gson = new Gson();
		SuiteResultCollection collection = null;

		try {
			collection = gson.fromJson(new BufferedReader(new FileReader(path.toString())), SuiteResultCollection.class);
		} catch (FileNotFoundException e) {
			collection = new SuiteResultCollection();
		}

		collection.addSuiteResult(name, count, passed, failed, filtered, nativeSuite, started);

		try(FileWriter writer = new FileWriter(path.toString())) {
			gson.toJson(collection, writer);
			writer.flush();
			writer.close();
		}
	}

	private TestListenerAdapter testListener;
	public void setTestList(List<ITestListener> value) {
		if(value.size() > 0) {
			testListener = (TestListenerAdapter)value.get(0);
		}
	}

	private IAtsLearning atsLearning = new AtsLearningOff();

	@BeforeClass(alwaysRun = true)
	public void beforeClass(ITestContext ctx) {

		super.started = System.currentTimeMillis();
		final TestRunner runner = (TestRunner) ctx;
		final String suiteName = ctx.getSuite().getName();

		verbosity = ctx.getSuite().getXmlSuite().getVerbose();

		final SuitesReportItem currentSuite = new SuitesReportItem(runner);
		setTopLogger(new ExecutionLogger(System.out, AtsSingleton.getInstance().getLogLevel(), scriptHeader.getLogsType()));
		setDateOrder(currentSuite.getDateOrder());

		if (currentSuite.isSubScriptIteration()) {
			setTestName(this.getClass().getName() + "-" + ctx.getName());
			testParameters = runner.getTest().getLocalParameters();
		} else {
			testParameters = ctx.getSuite().getXmlSuite().getParameters();
			setTestName(this.getClass().getName());
		}

		suiteOutputPath = Paths.get(runner.getOutputDirectory());
		AtsSingleton.getInstance().addCalledScript(this, getCanonicalName());

		if(AtsSingleton.getInstance().getAtsError() != null) {
			getLogger().sendInfo("ActionTestScript properties file found, but an error occured !", AtsSingleton.getInstance().getAtsError());
		}

		status = new ScriptStatus(getTestName(), suiteName);

		if ("true".equalsIgnoreCase(runner.getTest().getParameter("check.mode"))) {
			throw new SkipException("check mode : " + getTestName());
		} else {

			final Map<String, String> params = runner.getTest().getAllParameters();
			setTestExecutionVariables(params);

			//-----------------------------------------------------------
			// check report output specified
			//-----------------------------------------------------------

			final String atsReportEnv = getEnvironmentValue(AtsReport.ATS_REPORT_ENV, "").toLowerCase(Locale.ROOT);
			final String cmdATSReport = getEnvironmentValue(AtsReport.ATS_REPORT, "").toLowerCase(Locale.ROOT);
			int devReport = 0;
			// specific env variable with different writing
			{
				if (!atsReportEnv.isEmpty()) {
					try {
						int reportNum = Integer.parseInt(atsReportEnv);
						if (reportNum > 0 && reportNum < 4) {
							devReport = Utils.string2Int(atsReportEnv, 0);
						}
					} catch (NumberFormatException e) {
						getLogger().sendError("parameter can not be interpreted as number", "-1");
					}

				}
				if (!cmdATSReport.isEmpty()) {
					try {
						int reportNum = Integer.parseInt(cmdATSReport);
						if (reportNum > 0 && reportNum < 4) {
							devReport = Utils.string2Int(cmdATSReport, 0);
						}
					} catch (NumberFormatException e) {
						getLogger().sendError("parameter can not be interpreted as number", "-1");
					}
				}
			}
			
			xmlPic = devReport > 2;

			System.setProperty(AtsReport.ATS_REPORT, Integer.toString(devReport));
			int mgtReport = trueList.indexOf(getEnvironmentValue(AtsReport.MGT_REPORT,"").toLowerCase(Locale.ROOT)) > -1 ? 1 : 0;
			System.setProperty(AtsReport.MGT_REPORT, Integer.toString(mgtReport));

			int validReport = trueList.indexOf(getEnvironmentValue(AtsReport.VALID_REPORT,"").toLowerCase(Locale.ROOT)) > -1 ? 1 : 0;
			System.setProperty(AtsReport.VALID_REPORT, Integer.toString(validReport));

			boolean xml = trueList.indexOf(getEnvironmentValue("xml.report", "").toLowerCase(Locale.ROOT)) > -1 ? true : false;
			int visualQuality = currentSuite.getVisualQuality();

			if (((devReport + mgtReport + validReport) != 0) || currentSuite.isReporting()) {
				if (visualQuality == 0) {
					visualQuality = 3;
				}
				xml = true;
			}

			final JsonObject logs = new JsonObject();
			logs.addProperty("name", getTestName());
			logs.addProperty("suite", suiteName);
			logs.addProperty("xmlReport", xml);
			logs.addProperty("visualQuality", visualQuality);

			ScriptHeader header = null;

			if (currentSuite.isSubScriptIteration()) {

				params.remove(SuitesReportItem.CALLSCRIPT_ITERATION);
				params.remove(SuitesReportItem.CALLSCRIPT_PARAMETER_FILE);

				final int iteration = Utils.string2Int(params.remove(SuitesReportItem.ITERATION_PROPERTY));
				logs.addProperty("iteration", iteration);

				final ParameterList parameterList = new ParameterList(iteration);

				header = getScriptHeader();

				int i = 0;

				// find numerical param
				boolean numParamExist = true;
				while(numParamExist){
					if(params.containsKey("#"+ i)){
						parameterList.addParameter(new Parameter(i, new CalculatedValue(params.get("#"+ i))));
						params.remove("#" + i);
						i++;
					}else {
						numParamExist = false;
					}
				}

				for (Entry<String, String> param : params.entrySet()) {
					parameterList.addParameter(new Parameter(i, param.getKey(), param.getValue()));

					if (COLUMN_TEST_ID.equals(param.getKey())) {
						header.setId(param.getValue());
					} else if (COLUMN_TEST_DESCRIPTION.equals(param.getKey())) {
						header.setDescription(param.getValue());
					} else if (COLUMN_TEST_EXTERNAL.equals(param.getKey())) {
						header.setExternalId(param.getValue());
					} else if (COLUMN_TEST_PREREQUISITE.equals(param.getKey())) {
						header.setPrerequisite(param.getValue());
					}

					i++;
				}

				setParameterList(parameterList);

			} else {
				final Properties parametersProperties = new Properties();

				final JsonObject parametersObject = new JsonObject();
				for (Entry<String, String> param : params.entrySet()) {

					final String k = param.getKey();
					final String v = getEnvironmentValue(k, param.getValue());

					parametersProperties.put(k, v);

					final JsonObject elem = new JsonObject();
					elem.addProperty(k, v);

					parametersObject.addProperty(k, v);
				}

				logs.add("parameters", parametersObject);

				try {
					parametersProperties.store(new FileOutputStream(Paths.get(runner.getOutputDirectory(), SUITE_PARAMETERS).toFile()), null);
				} catch (IOException e) {}
			}

			final StringBuilder sb = new StringBuilder(ScriptStatus.TEST_STARTED).append(ExecutionLogger.RIGHT_ARROW_LOG).append(logs.toString());
			getHeader().sendPreprocessing(getLogger(), suiteName);
			getLogger().sendScriptTest(sb.toString());

			final File output = new File(runner.getOutputDirectory());

			if (visualQuality > 0 || xml) {

				if (header == null) {
					header = getScriptHeader();
				}
				header.setName(getTestName());

				if (!output.exists()) {
					output.mkdirs();
				}

				final ReportImageFormat imgFormat = new ReportImageFormat();
				
				initRecorder(output,
						header,
						xml,
						currentSuite.isAtsvHtml(),
						visualQuality,
						imgFormat);
			}

			initShadowScript(header, suiteName);

			//-------------------------------------------------------------------------------------------------------------------------
			// ATS Learning
			//-------------------------------------------------------------------------------------------------------------------------

			topScript.setAtsLearning(new AtsLearning(suiteOutputPath));

			//-----------------------------------------------------------
			//-----------------------------------------------------------

			if ("true".equals(System.getProperty(TEST_STOPPABLE))) {
				getLogger().sendScriptInfo("this script can be stopped if 'q' is sent to 'System.in' stream");
				new StopExecutionThread(this, System.in).start();
			}
		}
	}

	private void setAtsLearning(AtsLearning value) {
		atsLearning = value;
	}

	public void saveAtsLearning(TestElement testElement, IDriverEngine engine, SearchedElement searchedElement) {
		atsLearning.saveAtsLearning(testElement, engine, searchedElement);
	}

	private void closeAtsLearning() {
		atsLearning.terminate();
	}

	@AfterClass(alwaysRun = true)
	public void afterClass(ITestContext ctx) {
		final TestRunner runner = (TestRunner) ctx;

		getTopScript().closeAtsLearning();

		status.endLogs(this, runner);
	}

	@AfterMethod(alwaysRun = true)
	public void cleanup() {

		//AtsSingleton.getInstance().closeChannels(new ActionStatus(), getChannels());

		if(status.getCallscriptStack() != null) {
			getLogger().sendScriptError("Callscripts error", status.getCallscriptStack());
		}

		stopRecorder();
		AtsSingleton.getInstance().closeChannels(new ActionStatus(), getChannels());

		getTopScript().closeAtsLearning();
	}

	//----------------------------------------------------------------------------------------------------------
	// Channel management
	//----------------------------------------------------------------------------------------------------------

	private List<String> channels = new ArrayList<>();
	public void startChannel(String name) {
		topScript.channels.add(name);
	}

	public List<String> getChannels() {
		return topScript.channels;
	}

	//----------------------------------------------------------------------------------------------------------
	// Call script
	//----------------------------------------------------------------------------------------------------------

	private ParameterElement parameterElement = null;
	public ParameterElement getParameterElement() {
		return parameterElement;
	}

	public ActionTestScript getTopScript() {
		return topScript;
	}

	public String getCanonicalName() {
		return this.getClass().getCanonicalName();
	}

	public void initCalledScript(
			ActionCallscript action,
			ActionTestScript atsCaller,
			String testName,
			int line,
			ParameterList parameters,
			List<Variable> variables,
			int iter,
			int iterCount,
			String scriptName,
			String type,
			String path) {

		parameterElement = null;
		if(parameters != null) {
			parameterElement = parameters.getElement();
		}

		iteration = iter;
		iterationsCount = iterCount;
		csvAbsoluteFilePath = path;

		setTestName(getClass().getName());

		final JsonObject log = action.getConditionLogs();

		log.addProperty("called", scriptName);
		log.addProperty("iteration", iteration + "/" + iterationsCount);
		log.addProperty("type", type);

		if (csvAbsoluteFilePath != null) {
			log.addProperty("url", csvAbsoluteFilePath);
		}

		if (parameters != null) {
			setParameterList(parameters);
			if (parameters.getList().size() > 0) {

				final JsonArray parametersArray = new JsonArray();
				for (Parameter p : parameters.getList()) {
					final JsonObject jso = new JsonObject();
					jso.addProperty(p.getName(), p.getCalculated());
					parametersArray.add(jso);
				}

				final JsonObject parametersData = new JsonObject();
				if(parameterElement != null) {
					parametersData.addProperty("element", parameterElement.getTag());
					parametersData.add("values", parametersArray);
					log.add("parameters", parametersData);
				}else {
					log.add("parameters", parametersArray);
				}
			}
		}

		if (variables != null) {
			setVariables(variables);
		}

		setTestExecutionVariables(topScript.getTestExecutionVariables());
		topScript.getLogger().sendScriptInfo(ActionCallscript.getScriptLog(testName, line, log));
	}

	//----------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------

	public void setDateOrder(String value) {
		if(topScript != null) {
			topScript.dateOrder = value;
		}
	}

	public String getDateOrder() {
		if(topScript != null) {
			return topScript.dateOrder;
		}
		return "";
	}
	
	public String getTopScriptName() {
		if(topScript != null) {
			return topScript.getTestName();
		}
		return getTestName();
	}

	public void setProjectData(Project value) {
		projectData = value;
		projectData.synchronize();
		passwords = new Passwords(projectData.getAssetsFolderPath().toFile());
	}

	//----------------------------------------------------------------------------------------------------------
	// Generated methods
	//----------------------------------------------------------------------------------------------------------

	public static final String JAVA_VAR_FUNCTION_NAME = "var";

	public Variable var(String name, CalculatedValue value) {
		return createVariable(name, value, null, false);
	}

	public Variable var(String name) {
		return createVariable(name, new CalculatedValue(""), null, false);
	}

	public Variable var(String name, Transformer transformer) {
		return createVariable(name, new CalculatedValue(""), transformer, false);
	}

	public Variable var(String name, CalculatedValue value, Transformer transformer) {
		return createVariable(name, value, transformer, false);
	}

	public static final String JAVA_GLOBAL_VAR_FUNCTION_NAME = "gvar";

	public String gvar(String varPath) {
		return topScript.getGlobalVariableValue(varPath);
	}

	public void addSummary(ActionComment action) {
		getTopScript().summaryStack(action.getComment().getCalculated());
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_VALUE_FUNCTION_NAME = "clv";

	public CalculatedValue clv(Object... data) {
		return new CalculatedValue(this, data);
	}

	//---------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_SPLIT_FUNCTION_NAME = "split";

	public TableSplit split(String row, String col, CalculatedValue value) {
		return new TableSplit(row, col, value);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_PARAM_FUNCTION_NAME = "prm";

	public String prm(String name) {
		return getParameterValue(name);
	}

	public String prm(String name, String defaultValue) {
		return getParameterValue(name, defaultValue);
	}

	public String prm(int index) {
		return getParameterValue(index);
	}

	public String prm(int index, String defaultValue) {
		return getParameterValue(index, defaultValue);
	}

	public CalculatedValue[] prm(CalculatedValue... values) {
		return values;
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_RETURNS_FUNCTION_NAME = "rtn";

	public void rtn(CalculatedValue... values) {

		int i = 0;
		retVals = new String[values.length];

		for (CalculatedValue calc : values) {
			retVals[i] = calc.getCalculated();
			i++;
		}

		updateVariables();
	}

	//---------------------------------------------------------------------------------------------

	public void rtn(String... values) {

		int i = 0;
		retVals = new String[values.length];

		for (String value : values) {
			retVals[i] = value;
			i++;
		}

		updateVariables();
	}

	private void updateVariables() {
		List<Variable> variables = getVariables();

		int index = 0;
		for (String value : retVals) {
			if (variables.size() < index + 1) {
				break;
			}
			variables.get(index).setData(value);
			index++;
		}
	}

	//---------------------------------------------------------------------------------------------

	public void ats_return(String... values) {
		retVals = values;
		updateVariables();
	}

	public void ats_return(Object... values) {
		retVals = Arrays.stream(values).map(Object::toString).toArray(String[]::new);
		updateVariables();
	}

	public void returnValues(String... values) {
		retVals = values;
		updateVariables();
	}

	public void returnValues(Object... values) {
		retVals = Arrays.stream(values).map(Object::toString).toArray(String[]::new);
		updateVariables();
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_RNDSTRING_FUNCTION_NAME = "rds";

	public String rds(int len) {
		return new RandomStringValue(len, null).exec();
	}

	public String rds(int len, String type) {
		return new RandomStringValue(len, type).exec();
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_ENV_FUNCTION_NAME = "env";

	public String env(String name) {
		return getEnvironmentValue(name, "");
	}

	public String env(String name, String defaultValue) {
		return getEnvironmentValue(name, defaultValue);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_PROJECT_VARIABLE_FUNCTION_NAME = "prj";

	public String prj(String name) {
		return getProjectVariableValue(name);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_SYSTEM_FUNCTION_NAME = "sys";

	public String sys(String name) {
		return AtsSingleton.getInstance().getSystemValue(name);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_PROPERTY_FUNCTION_NAME = "prp";

	public CalculatedProperty prp(String type, String name, CalculatedValue value) {
		return new CalculatedProperty(type, name, value);
	}

	public ApiData prp(String name, CalculatedValue value) {
		return new ApiData(name, value);
	}

	//---------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------

	public static final String JAVA_UUID_FUNCTION_NAME = "uid";

	public String uid() {
		return getUuidValue();
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_TODAY_FUNCTION_NAME = "td";

	public String td() {
		return getTodayValue();
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_LAST_ACTION_DURATION_FUNCTION_NAME = "lad";

	public String lad() {
		return String.valueOf(getLastActionDuration());
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_NOW_FUNCTION_NAME = "nw";

	public String nw() {
		return getNowValue();
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_ITERATION_FUNCTION_NAME = "itr";

	public int itr() {
		return getIteration();
	}

	public static final String JAVA_ITERATION_COUNT_FUNCTION_NAME = "itc";

	public int itc() {
		return getIterationsCount();
	}

	//---------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------

	public static final String JAVA_ELEMENT_FUNCTION_NAME = "el";

	public SearchedElement el(SearchedElement parent, CalculatedValue index, boolean shadow, String tagName, CalculatedProperty... properties) {
		return new SearchedElement(parent, index, shadow, tagName, properties);
	}

	public SearchedElement el(CalculatedValue index, boolean shadow, String tagName, CalculatedProperty... properties) {
		return new SearchedElement(null, index, shadow, tagName, properties);
	}

	public SearchedElement el(SearchedElement parent, CalculatedValue index, boolean shadow, String tagName) {
		return new SearchedElement(parent, index, shadow, tagName);
	}

	public SearchedElement el(CalculatedValue index, boolean shadow, String tagName) {
		return new SearchedElement(null, index, shadow, tagName);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_REGEX_FUNCTION_NAME = "rx";

	public RegexpTransformer rx(String patt, int[] group) {
		return new RegexpTransformer(patt, group);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_DATE_FUNCTION_NAME = "dt";

	public DateTransformer dt(String... data) {
		final DateTransformer dtr = new DateTransformer(data);
		dtr.setDateOrder(getDateOrder());
		return dtr;
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_TIME_FUNCTION_NAME = "tm";

	public TimeTransformer tm(String... data) {
		return new TimeTransformer(data);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_NUMERIC_FUNCTION_NAME = "nm";

	public NumericTransformer nm(int dp, boolean comma) {
		return new NumericTransformer(dp, comma);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_SCRIPTING_FUNCTION_NAME = "sc";

	public EvalTransformer sc(String code) {
		return new EvalTransformer(code);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_POS_FUNCTION_NAME = "ps";

	public MouseDirectionData ps(Cartesian cart, CalculatedValue value) {
		return new MouseDirectionData(cart, value);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_MOUSE_FUNCTION_NAME = "ms";

	public Mouse ms(String type) {
		return new Mouse(type);
	}

	public Mouse ms(String type, MouseDirectionData hpos, MouseDirectionData vpos) {
		return new Mouse(type, hpos, vpos);
	}

	public MouseKey ms(String type, Keys key, MouseDirectionData hpos, MouseDirectionData vpos) {
		return new MouseKey(type, key, hpos, vpos);
	}

	public MouseKey ms(String type, Keys key) {
		return new MouseKey(type, key);
	}

	public MouseScroll ms(int scroll, MouseDirectionData hpos, MouseDirectionData vpos) {
		return new MouseScroll(scroll, hpos, vpos);
	}

	public MouseScroll ms(int scroll) {
		return new MouseScroll(scroll);
	}

	public MouseSwipe ms(MouseDirectionData hdir, MouseDirectionData vdir, MouseDirectionData hpos, MouseDirectionData vpos) {
		return new MouseSwipe(hdir, vdir, hpos, vpos);
	}

	public MouseSwipe ms(MouseDirectionData hdir, MouseDirectionData vdir) {
		return new MouseSwipe(hdir, vdir);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_EMBEDED_FUNCTION_NAME = "emb";

	public String emb(String relativePath) {
		return getAssetsUrl(relativePath);
	}

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_ATS_FUNCTION_NAME = "ats";

	public String ats(String key) {
		return getTopScript().getSpecialValue(key);
	}

	//----------------------------------------------------------------------------------------------------------
	// Actions
	//----------------------------------------------------------------------------------------------------------

	public static final String JAVA_CONDITION_FUNCTION = "opt";

	public ExecuteOptions opt(CalculatedValue code) {
		return new ExecuteOptions(code);
	}

	public ExecuteOptions opt() {
		return new ExecuteOptions();
	}

	/*public JsonObject getConditionLogs() {
		JsonObject result = new JsonObject();
		if (condition != null) {
			result = condition.getLog(result);
			condition = null;
		}
		return result;
	}*/

	//---------------------------------------------------------------------------------------------

	public static final String JAVA_EXECUTE_FUNCTION_NAME = "exec";

	public void exec(int line, Action action) {
		if(action instanceof ActionCondition) {
			exec(line, (ActionCondition)action);
		}
	}

	public void exec(int line, ActionComment action) {
		action.execute(this, getTestName(), line, 0);
		if (action.isPassed()) {
			getLogger().sendAction(action, getTestName(), line);
		}
	}
	
	public void exec(int line, ActionCondition action) {
		if(action.getCondition().isPassed()) {
			action.execute(this, getTestName(), line, 0);
			getTopScript().actionFinished(getTestName(), line, action);
		}
	}

	public void exec(int line, ActionCallscript action) {
		if(action.getCondition().isPassed()) {
			try {
				action.execute(this, getTestName(), line, 0);
				action.getStatus().setMessage(action.getCalledScriptLogs());

				if(action.isPassed()) {
					getTopScript().actionFinished(getTestName(), line, action);
				}else{
					actionFinishedFail(action.getStatus(), action, line, getTestName(), TestError.TestErrorStatus.FAIL_STOP);
					getTopScript().getRecorder().updateSummaryFail(getTestName(), line, "subString", action.getStatus().getFailMessage(), TestError.TestErrorStatus.FAIL_STOP);
				}

			}catch(AssertionError e) {
				final ActionStatus ast = action.getStatus();
				ast.setError(ActionStatus.JAVA_EXCEPTION, e.getMessage());

				actionFinishedFail(ast, action, line, getTestName(), TestError.TestErrorStatus.FAIL_STOP);

				getTopScript().getRecorder().updateSummaryFail(getTestName(), line, ast.getChannelApplication(), ast.getFailMessage(), TestError.TestErrorStatus.FAIL_STOP);

				scriptFail(ast.getFailMessage());
			}
		}
	}

	public void exec(int line, ActionExecute action) {
		if(action.getCondition().isPassed()) {
			action.execute(this, getTestName(), line, 0);
			getTopScript().actionFinished(getTestName(), line, action, action.getStopPolicy());
		}
	}

	public static Date getIsoDate(String value) {
		return Utils.parseDateFormat(value);
	}

	//---------------------------------------------------------------------------------------------

	public void startChannelFailed(String testName, int testLine, String appName, String failMessage) {
		failedAt(ActionChannelStart.class.getSimpleName(), testName, testLine, appName, ActionStatus.CHANNEL_START_ERROR, failMessage);
		getRecorder().updateSummaryFail(testName, testLine, appName, failMessage, TestError.TestErrorStatus.FAIL_STOP);
	}

	public void callScriptFailed(String testName, int testLine, int errorNum, String failMessage) {

		failedAt(ActionCallscript.class.getSimpleName(), testName, testLine, "", errorNum, failMessage);
		getRecorder().updateSummaryFail(testName, testLine, "", failMessage, TestError.TestErrorStatus.FAIL_STOP);


		status.addErrorStack(failMessage);
		final StringBuilder sb =
				new StringBuilder(testName)
				.append(":")
				.append(testLine);

		status.addErrorStack(sb.toString());

		//getTopScript().getRecorder().update(errorNum, 0);

		getLogger().sendScriptError(failMessage, testName + ":" + testLine);

		scriptFail(failMessage);
	}

	public void failedAt(String action, String testName, int line, String app, int errorCode, String errorMessage) {
		status.failedAt(action, testName, line, app, errorCode, errorMessage);
		getLogger().sendScriptFail(action, testName, line, app, errorMessage);
	}

	public void actionFinished(String testName, int line, ActionCondition action) {
		actionFinished(testName, line, action, ActionExecute.TEST_STOP_FAIL);
	}

	public void actionFinished(String testName, int line, ActionCondition action, int stopPolicy) {

		if(!action.getCondition().isPassed()) {
			return;
		}

		if(!(action instanceof ActionCallscript)) {
			status.addAction();
		}

		/*if(action instanceof ActionCondition && !((ActionCondition)action).getCondition().isPassed()) {
			return;
		}

		if(!(action instanceof ActionCallscript) && !(action instanceof ActionComment)) {
			status.addAction();
		}*/

		final ActionStatus actionStatus = action.getStatus();

		if (actionStatus.isPassed()) {
			getLogger().sendAction(action, testName, line);
		} else {
			if (stopPolicy == ActionExecute.TEST_STOP_FAIL) {
				if (actionFinishedFail(actionStatus, action, line, testName, TestError.TestErrorStatus.FAIL_STOP)) {
					AtsSingleton.getInstance().getCurrentChannel().addShadowActionError(action, line, TestError.TestErrorStatus.FAIL_STOP);
					throw new AtsFailError(action, testName, line, status.getErrorScript(), status.getErrorInfo(), getLogger());
				}
			} else {
				getLogger().sendAction(action, testName, line);
				if (stopPolicy == ActionExecute.TEST_CONTINUE_FAIL) {
					actionFinishedFail(actionStatus, action, line, testName, TestError.TestErrorStatus.FAIL_CONTINUE);
				} else if (stopPolicy == ActionExecute.TEST_CONTINUE_PASS) {
					actionFinishedFailPass(actionStatus, line, testName, action.getScript().getTimeLine());
				}
			}
		}
	}

	private void actionFinishedFailPass(ActionStatus actionStatus, int line, String testName, long timeline) {
		this.getRecorder().updateSummaryFailPass(testName, line, actionStatus.getFailMessage(), TestError.TestErrorStatus.FAIL_PASS);
	}

	private boolean actionFinishedFail(ActionStatus actionStatus, Action action, int line, String testName, TestError.TestErrorStatus testErrorStatus) {
		if (status.isSuiteExecution()) {
			final String app = actionStatus.getChannelApplication();
			final String errorMessage = actionStatus.getFailMessage();

			getTopScript().failedAt(action.getClass().getSimpleName(), testName, line, app, actionStatus.getCode(), errorMessage);

			this.getRecorder().updateSummaryFail(testName, line, app, errorMessage, testErrorStatus);
			return true;
		}

		return false;
	}

	public Channel getCurrentChannel() {
		return AtsSingleton.getInstance().getCurrentChannel(this);
	}

	//-----------------------------------------------------------------------------------------------------------
	//  - Drag drop management
	//-----------------------------------------------------------------------------------------------------------

	private boolean dragWithDesktop = false;

	public void startDrag() {
		topScript.dragWithDesktop = AtsSingleton.getInstance().isDesktopChannel();
	}

	public boolean isDesktopDragDrop() {
		return topScript.dragWithDesktop;
	}

	public void endDrag() {
		topScript.dragWithDesktop = false;
	}

	//-----------------------------------------------------------------------------------------------------------
	//  - Animation recorder
	//-----------------------------------------------------------------------------------------------------------

	private IVisualRecorder recorder = new VisualRecorderNull();
	
	public void initRecorder(File output, ScriptHeader header, boolean xml,	boolean atsvHtml, int visualQuality, ReportImageFormat imgFormat) {
		setRecorder(new VisualRecorder(
				this,
				output,
				header,
				xml,
				atsvHtml,
				visualQuality,
				logger,
				imgFormat,
				xmlPic));
	}
	
	private void initRecorder(ScriptHeader info, Project projectData, boolean xml, boolean atsvHtml, int quality, ReportImageFormat reportImageFormat, boolean xmlPic2) {
		setRecorder(new VisualRecorder(
				this, 
				info, 
				projectData, 
				xml, 
				atsvHtml, 
				quality,
				new ReportImageFormat(), 
				xmlPic));
	}

	public IVisualRecorder getRecorder() {
		return topScript.recorder;
	}

	public void setRecorder(IVisualRecorder value) {
		if ((value instanceof VisualRecorderNull && this.recorder instanceof VisualRecorder) || (value instanceof VisualRecorder && this.recorder instanceof VisualRecorderNull)) {
			this.topScript.recorder.terminate();
			this.topScript.recorder = value;
		}
	}

	public void startRecorder(ScriptHeader info, int quality, boolean xml, boolean atsvHtml) {
		topScript.initRecorder(info, projectData, xml, atsvHtml, quality, new ReportImageFormat(), xmlPic);
	}

	public void startRecorder(Channel channel, SystemDriver systemDriver) {
		systemDriver.startVisualRecord(channel, getScriptHeader(), 10, 0);
	}

	public void stopRecorder() {
		topScript.setRecorder(new VisualRecorderNull());
	}
}