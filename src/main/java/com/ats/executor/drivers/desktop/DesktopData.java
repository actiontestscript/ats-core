/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.executor.drivers.desktop;

import com.ats.generator.variables.CalculatedProperty;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DesktopData {

	private String name = "";
	private String value = "";

	public DesktopData() {}

	public DesktopData(JsonNode node) {
		jsonDeserialize(node);
	}
	public DesktopData(String name, String value) {
		this.name = name;
		this.value = value;
	}

	//-------------------------------------------------------------------------------------------------
	// getter and setter for serialization
	//-------------------------------------------------------------------------------------------------

	@JsonSetter("data")
	public void setData(String data) {
		final String[] array = data.split("\n",2);

		if(array.length > 0) {
			name = array[0];
			if(array.length > 1) {
				value = array[1];
			}
		}
	}
	@JsonGetter("data")
	public String getData() {
		return null;
	}

	//-------------------------------------------------------------------------------------------------

	public CalculatedProperty getCalculatedProperty() {
		return new CalculatedProperty(name, value);
	}

	@JsonGetter("name")
	public String getName() {
		return name;
	}

	@JsonGetter("value")
	public String getValue() {
		return value;
	}

	public void jsonDeserialize(JsonNode node){
		if(node instanceof NullNode || node.get("error") != null) {	return ;}
		setData(node.get("data").asText());
	}
}