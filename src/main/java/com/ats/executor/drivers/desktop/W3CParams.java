/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.desktop;

import java.util.Map;

import com.ats.AtsSingleton;
import com.ats.executor.drivers.DriverInfo;
import com.ats.executor.drivers.IDriverInfo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class W3CParams {

	private static final String NO_SESSION_ID = "no-session-id";

	public SystemDriver.HttpMethod method;
	public StringBuilder  url = new StringBuilder();

	public W3CParams() {}

	private final Map<Integer, String> commandTypeMap = Map.of(1, "Recorder");

	private final Map<Integer, String> commandSTypeMap =
		Map.ofEntries(
			Map.entry(0, "Stop"),
			Map.entry(1, "Screenshot"),
			Map.entry(2, "Start"),
			Map.entry(3, "Create"),
			Map.entry(4, "Image"),
			Map.entry(5, "Value"),
			Map.entry(6, "Data"),
			Map.entry(7, "Status"),
			Map.entry(8, "Element"),
			Map.entry(9, "Position"),
			Map.entry(10, "Download"),
			Map.entry(11, "ImageMobile"),
			Map.entry(12, "CreateMobile"),
			Map.entry(13, "ScreenshotMobile"),
			Map.entry(14, "Summary"));

	public ObjectNode get(Integer type, Integer subType, String data) throws Exception {
		String strType = commandTypeMap.get(type);
		String strSType = commandSTypeMap.get(subType);

		if ((strType != null) && (strSType != null)) {
			return get(strType, strSType, data);
		}
		return null;
	}

	private IDriverInfo getDriverInfo() {
		if(AtsSingleton.getInstance().getCurrentChannel().getDriverEngine() != null) {
			return AtsSingleton.getInstance().getCurrentChannel().getDriverEngine().getDriverInfo();
		}
		return null;
	}

	public ObjectNode get(String type, String subType, String data) throws Exception {

		String sessionId = NO_SESSION_ID;
		if (getDriverInfo() != null) {
			sessionId = getDriverInfo().getSessionId();
		}

		ObjectNode result = null;
		url.append("session")
		.append("/")
		.append(sessionId)
		.append("/")
		.append("ats")
		.append("/")
		.append(type.toLowerCase())
		.append("/")
		.append(subType.toLowerCase());

		method = SystemDriver.HttpMethod.POST;

		switch(type.toLowerCase()) {
		case "recorder":
			switch(subType.toLowerCase()) {
			case "image":
				result = convertToImage(data);
				break;

			case "start":
				result = convertToStart(data);
				break;

			case "create":
				result = convertToCreate(data);
				break;

			case "createmobile":
				result = convertToCreateMobile(data);
				break;

			case "imagemobile":
				result = convertToImageMobile(data);
				break;

			case "value":
				result = convertToValue(data);
				break;

			case "data":
				result = convertToData(data);
				break;

			case "status":
				result = convertToStatus(data);
				break;

			case "element":
				result = convertToElement(data);
				break;

			case "position":
				result = convertToPosition(data);
				break;

			case "summary":
				result = convertToSummary(data);
				break;

			case "screenshot":
				result = convertToScreenShot(data);
				break;

			case "screenshotmobile":
				result = convertToScreenShotMobile(data);
				break;

			case "stop":
				result = convertToStop(data);
				break;
			}
			break;
		}

		if (result != null) {
			if (getDriverInfo() != null) {
				JsonNode newNode = getDriverInfo().toJson();
				if ((newNode != null) && (newNode.has(DriverInfo.DRIVER_INFO) && newNode.get(DriverInfo.DRIVER_INFO).has(DriverInfo.SESSION_ID))) {
					result.set(DriverInfo.DRIVER_INFO, newNode.get(DriverInfo.DRIVER_INFO));
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode rootNode = mapper.createObjectNode();
			rootNode.set("value", result);
			result = rootNode;
		}

		return result;
	}

	public DesktopResponse getDesktopResponse(JsonNode result) {
		DesktopResponse response = new DesktopResponse();
		if (result.has("error")) {
			if ((result.get("error").has("message"))) {
				response.setErrorCode(400); //Integer.parseInt(result.get("error").get("message").toString());
				response.setErrorMessage(result.get("error").get("message").toString());
				//response.errorMessage = result.get("error").get("stacktrace").toString();
			} else {
				response.setErrorCode(400);
				response.setErrorMessage("unknow error");
			}
		} else {
			response.setErrorCode(200);
			response.setErrorMessage("");
		}
		return response;
	}

	private ObjectNode convertToImage(String data) throws Exception {
		try {
			final String[] parts = data.split("\n");
			final JsonNodeFactory factory = JsonNodeFactory.instance;
			final ArrayNode screenRect = factory.arrayNode();
			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			for (int i = 0; i < 4; i++) {
				screenRect.add(Integer.parseInt(parts[i]));
			}

			postData.set("screenRect", screenRect);
			postData.put("isRef", Boolean.parseBoolean(parts[4]));

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/image action");
		}
	}

	private ObjectNode convertToImageMobile(String data) throws Exception {
		try {
			final String[] parts = data.split("\n");
			final JsonNodeFactory factory = JsonNodeFactory.instance;
			final ArrayNode screenRect = factory.arrayNode();
			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			for (int i = 0; i < 4; i++) {
				screenRect.add(Integer.parseInt(parts[i]));
			}

			postData.set("screenRect", screenRect);
			postData.put("isRef", Boolean.parseBoolean(parts[4]));
			postData.put("url", parts[5]);

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/image/mobile action");
		}
	}

	private ObjectNode convertToStart(String data) throws Exception {
		try {
			final String[] parts = data.split("\n");

			final ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("id", parts[0]);
			postData.put("fullName", parts[1]);
			postData.put("description", parts[2]);
			postData.put("author", parts[3]);
			postData.put("groups", parts[4]);
			postData.put("preRequisites", parts[5]);
			postData.put("externalId", parts[6]);
			postData.put("videoQuality", Integer.parseInt(parts[7]));
			postData.put("started", parts[8]);

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/start action");
		}
	}

	private ObjectNode convertToCreate(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");
			final JsonNodeFactory factory = JsonNodeFactory.instance;

			final ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			final ArrayNode screenRect = factory.arrayNode();
			for (int i = 0; i < 4; i++) {
				screenRect.add(Integer.parseInt(parts[i+5]));
			}

			postData.put("actionType", parts[0]);
			postData.put("line", Integer.parseInt(parts[1]));
			postData.put("script", parts[2]);
			postData.put("timeLine", Long.parseLong(parts[3]));
			postData.put("channelName", parts[4]);
			/*
			postData.put("channelDimensionX", Integer.parseInt(parts[5]));
			postData.put("channelDimensionY", Integer.parseInt(parts[6]));
			postData.put("channelDimensionWidth", Integer.parseInt(parts[7]));
			postData.put("channelDimensionHeight", Integer.parseInt(parts[8]));

			 */
			postData.set("channelDimension", screenRect);
			postData.put("sync", Boolean.parseBoolean(parts[9]));
			postData.put("stop", Boolean.parseBoolean(parts[10]));

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/create action");
		}
	}

	private ObjectNode convertToCreateMobile(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");
			final JsonNodeFactory factory = JsonNodeFactory.instance;
			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			final ArrayNode screenRect = factory.arrayNode();
			for (int i = 0; i < 4; i++) {
				screenRect.add(Integer.parseInt(parts[i+5]));
			}

			postData.put("actionType", parts[0]);
			postData.put("line", Integer.parseInt(parts[1]));
			postData.put("script", parts[2]);
			postData.put("timeline", Long.parseLong(parts[3]));
			postData.put("channelName", parts[4]);
			postData.set("channelDimension", screenRect);
			postData.put("url", parts[9]);
			postData.put("sync", Boolean.parseBoolean(parts[10]));
			postData.put("stop", Boolean.parseBoolean(parts[11]));

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/create/mobile action");
		}
	}

	private ObjectNode convertToValue(String data) throws Exception {
		try
		{
			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("v", data);

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/value action");
		}
	}

	private ObjectNode convertToData(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");

			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			if (parts.length > 0) {
				postData.put("v1", parts[0]);
				if (parts.length > 1) {
					postData.put("v2", parts[1]);
				} else {
					postData.put("v2", "");
				}
			} else {
				postData.put("v1", "");
				postData.put("v2", "");
			}

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/data action");
		}
	}

	private ObjectNode convertToStatus(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");

			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("error", Integer.parseInt(parts[0]));
			postData.put("duration", Long.parseLong(parts[1]));

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/status action");
		}
	}

	private ObjectNode convertToElement(String data) throws Exception {
		try
		{
			final JsonNodeFactory factory = JsonNodeFactory.instance;
			final String[] parts = data.split("\n");

			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			final ArrayNode screenRect = factory.arrayNode();
			for (int i = 0; i < 4; i++) {
				screenRect.add(Integer.parseInt(parts[i]));
			}

			postData.set("elementBound", screenRect);
			postData.put("searchDuration", Long.parseLong(parts[4]));
			postData.put("numElements", Integer.parseInt(parts[5]));
			postData.put("searchCriterias", parts[6]);

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/element action");
		}
	}

	private ObjectNode convertToPosition(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");

			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("hpos", parts[0]);
			postData.put("hposValue", Integer.parseInt(parts[1]));
			postData.put("vpos", parts[2]);
			postData.put("vposValue", Integer.parseInt(parts[3]));

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/position action");
		}
	}

	private ObjectNode convertToSummary(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");

			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("passed", Boolean.parseBoolean(parts[0]));
			postData.put("actions", Integer.parseInt(parts[1]));
			postData.put("suiteName", parts[2]);
			postData.put("testName", parts[3]);
			postData.put("data", parts[4]);
			if (parts.length > 5) {
				postData.put("errorScript", parts[5]);
				postData.put("errorLine", Integer.parseInt(parts[6]));
				postData.put("errorMessage", parts[7]);
			}

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/summary action");
		}
	}

	private ObjectNode convertToScreenShot(String data) throws Exception {
		try
		{
			final String[] parts = data.split("\n");

			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("x", Integer.parseInt(parts[0]));
			postData.put("y", Integer.parseInt(parts[1]));
			postData.put("w", Integer.parseInt(parts[2]));
			postData.put("h", Integer.parseInt(parts[3]));

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/screenshot action");
		}
	}

	private ObjectNode convertToScreenShotMobile(String data) throws Exception {
		try
		{
			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			postData.put("uri", data);

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/screenshot/mobile action");
		}
	}

	private ObjectNode convertToStop(String data) throws Exception {
		try
		{
			ObjectMapper mapper = new ObjectMapper();
			final ObjectNode postData = mapper.createObjectNode();

			return postData;
		}
		catch (Exception e) {
			throw new Exception("cannot convert " + data + " string to json for the recorder/stop action");
		}
	}
}
