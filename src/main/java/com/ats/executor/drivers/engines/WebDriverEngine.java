/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnsupportedCommandException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.Select;

import com.ats.AtsSingleton;
import com.ats.data.Dimension;
import com.ats.data.Point;
import com.ats.data.Rectangle;
import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsManager;
import com.ats.driver.AtsRemoteWebDriver;
import com.ats.element.AtsBaseElement;
import com.ats.element.AtsElement;
import com.ats.element.DialogBox;
import com.ats.element.FoundElement;
import com.ats.element.test.TestElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.SendKeyData;
import com.ats.executor.SendTextException;
import com.ats.executor.TestBound;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.DriverManager;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.DesktopWindow;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.generator.ATS;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.objects.MouseDirectionData;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.transform.DateTransformer;
import com.ats.script.actions.ActionApi;
import com.ats.script.actions.ActionChannelStart;
import com.ats.script.actions.ActionGotoUrl;
import com.ats.script.actions.ActionSelect;
import com.ats.script.actions.ActionWindowState;
import com.ats.script.actions.ActionWindowSwitch;
import com.ats.tools.ResourceContent;
import com.ats.tools.Utils;
import com.ats.tools.logger.MessageCode;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;

@SuppressWarnings("unchecked")
public class WebDriverEngine extends DriverEngine implements IDriverEngine {

	protected static final String WEB_ELEMENT_REF = "element-6066-11e4-a52e-4f735466cecf";

	private final static int DEFAULT_WAIT = 150;
	private final static int DEFAULT_PROPERTY_WAIT = 200;

	private final static String ANGULAR_OPTION = "mat-option";

	//-----------------------------------------------------------------------------------------------------------------------------
	// Javascript static code
	//-----------------------------------------------------------------------------------------------------------------------------

	//protected static final String JS_AUTO_SCROLL = "var e=arguments[0];e.scrollIntoView();var r=e.getBoundingClientRect();var result=[r.left+0.0001, r.top+0.0001]";
	//protected static final String JS_AUTO_SCROLL_CALC = "var e=arguments[0];var r=e.getBoundingClientRect();var top=r.top + window.pageYOffset;window.scrollTo(0, top-(window.innerHeight / 2));r=e.getBoundingClientRect();var result=[r.left+0.0001, r.top+0.0001]";
	//protected static final String JS_AUTO_SCROLL_MOZ = "var e=arguments[0];e.scrollIntoView({behavior:'auto',block:'center',inline:'center'});var r=e.getBoundingClientRect();var result=[r.left+0.0001, r.top+0.0001]";
	//protected String JS_SCROLL_IF_NEEDED = "var e=arguments[0], result=[], r=e.getBoundingClientRect(), top0=r.top, left0=r.left;if(r.top < 0 || r.left < 0 || r.bottom > (window.innerHeight || document.documentElement.clientHeight) || r.right > (window.innerWidth || document.documentElement.clientWidth)) {e.scrollIntoView({behavior:'instant',block:'center',inline:'nearest'});r=e.getBoundingClientRect();result=[r.left+0.0001, r.top+0.0001, left0+0.0001, top0+0.0001];}";

	protected String JS_SCROLL_IF_NEEDED = "var e=arguments[0], result=[], r=e.getBoundingClientRect(), top0=r.top, left0=r.left; e.scrollIntoView({behavior:'instant',block:'center',inline:'nearest'});r=e.getBoundingClientRect();if(r.left!=left0 || r.top!=top0) {result=[r.left+0.0001, r.top+0.0001];}";
	protected String JS_ELEMENT_FROM_POINT = "var result=null;var parent=document;if(arguments[0]!=null){parent=arguments[0].shadowRoot};var e=parent.elementFromPoint(arguments[1],arguments[2]);if(e && e!=arguments[0]){var r=e.getBoundingClientRect();result=[e, e.tagName, e.getAttribute('inputmode')=='numeric', e.getAttribute('type')=='password', r.x+0.0001, r.y+0.0001, r.width+0.0001, r.height+0.0001, r.left+0.0001, r.top+0.0001, 0.0001, 0.0001, {},e.shadowRoot!=null];};";
	protected String JS_ELEMENT_PARENTS = ResourceContent.getParentElementJavaScript();
	protected final String JS_ELEMENT_PARENTS_HTML = ResourceContent.getParentElementHtmlCode();

	protected static final String JS_ELEMENT_SCROLL = "var e=arguments[0];var d=arguments[1];e.scrollTop += d;var r=e.getBoundingClientRect();var result=[r.left+0.0001, r.top+0.0001]";
	protected static final String JS_WINDOW_SCROLL = "window.scrollBy(0,arguments[0]);var result=[0.0001, 0.0001]";
	protected static final String JS_ELEMENT_FROM_RECT = "let parent=document, x1=arguments[1],y1=arguments[2],w=arguments[3],h=arguments[4];if(arguments[0]!=null){parent=arguments[0].shadowRoot};let x2=x1+w,y2=y1+h;var e=parent.elementFromPoint(x1+(w/2), y1+(h/2)),result=null;while(e != null && e!=arguments[0]){var r=e.getBoundingClientRect();if(x1 >= r.x && x2 <= r.x+r.width && y1 >= r.y && y2 <= r.y+r.height){result=[e,e.tagName,e.getAttribute('inputmode')=='numeric',e.getAttribute('type')=='password',r.x+0.0001,r.y+0.0001,r.width+0.0001,r.height+0.0001,r.left+0.0001,r.top+0.0001,0.0001,0.0001,{},e.shadowRoot!=null];e=null;}else{e=e.parentElement;}};";
	protected static final String JS_ELEMENT_BOUNDING = "var rect=arguments[0].getBoundingClientRect();var result=[rect.left+0.0001, rect.top+0.0001];";
	protected static final String JS_MIDDLE_CLICK = "var evt=new MouseEvent('click', {bubbles: true,cancelable: true,view: window, button: 1}),result={};arguments[0].dispatchEvent(evt);";
	protected static final String JS_ELEMENT_CSS = "var result={};var o=getComputedStyle(arguments[0]);for(var i=0, len=o.length; i < len; i++){result[o[i]]=o.getPropertyValue(o[i]);};";

	protected static final String JS_SEARCH_ELEMENT = ResourceContent.getSearchElementsJavaScript();
	protected static final String JS_SEARCH_SHADOW_ELEMENT = ResourceContent.getSearchShadowElementsJavaScript();
	protected static final String JS_ELEMENT_AUTOSCROLL = ResourceContent.getScrollElementJavaScript();
	protected static final String JS_ELEMENT_ATTRIBUTES = ResourceContent.getElementAttributesJavaScript();
	protected static final String JS_ELEMENT_TEXT_DATA = ResourceContent.getElementTextDataJavaScript();
	protected static final String JS_DOCUMENT_SIZE = ResourceContent.getDocumentSizeJavaScript();
	protected static final String JS_ELEMENT_HTML = ResourceContent.getElementKeysAndAttributesJavaScript();
	protected static final String JS_ELEMENT_FUNCTIONS = ResourceContent.getElementFunctionsJavaScript();

	//-----------------------------------------------------------------------------------------------------------------------------

	protected Double initElementX = 0.0;
	protected Double initElementY = 0.0;

	protected Actions actions;

	private LinkedList<RemoteWebElement> shadowList;

	protected String searchElementScript = JS_SEARCH_ELEMENT;

	public WebDriverEngine(
			Channel channel,
			IDriverInfo driverInfo,
			SystemDriver systemDriver,
			ApplicationProperties props,
			int defaultWait,
			int defaultPropertyWait,
			boolean enableLearning) {

		super(channel, driverInfo, systemDriver, props, defaultWait, defaultPropertyWait, enableLearning);
	}

	public WebDriverEngine(
			Channel channel,
			IDriverInfo driverInfo,
			SystemDriver systemDriver,
			ApplicationProperties props,
			boolean enableLearning) {

		this(channel, driverInfo, systemDriver, props, DEFAULT_WAIT, DEFAULT_PROPERTY_WAIT, enableLearning);
	}

	public WebDriverEngine(
			Channel channel,
			SystemDriver desktopDriver,
			IDriverInfo driverInfo,
			String application,
			ApplicationProperties props,
			int defaultWait,
			int defaultCheck,
			boolean enableLearning) {

		super(channel, driverInfo, desktopDriver, props, defaultWait, defaultCheck, enableLearning);
	}

	protected boolean isHeadless() {
		return getDriverInfo().isHeadless();
	}

	private AtsRemoteWebDriver getRemoteDriver(ActionStatus status, Capabilities cap) {

		String errorMessage = "";
		AtsRemoteWebDriver rwd = null;

		try{
			rwd = getDriverInfo().getAtsRemoteWebDriver(cap);
			return rwd;
		}catch(InvalidArgumentException ex0) {

			if(rwd != null) {
				rwd.close();
			}
			//if(ex0.getMessage().contains("--user-data-dir")) {
			status.setTechnicalError(ActionStatus.CHANNEL_START_ERROR, "Unable to start remote driver:\n" + ex0.getMessage());
			return null;
			//}

		}catch(SessionNotCreatedException ex0){

			rwd = null;
			channel.sleep(500);
			errorMessage = ex0.getMessage();

		}catch(UnsupportedCommandException ex1){

			status.setTechnicalError(ActionStatus.CHANNEL_START_ERROR, "Unable to create Selenium remote session:\n" + ex1.getMessage());
			return null;

		}catch(Exception ex2){

			if(rwd != null) {
				rwd.close();
			}
			channel.sleep(500);
			errorMessage = ex2.getMessage();

		}

		if(getDriverInfo().getDriverLoopback() != null) {
			try{ // last chance to start local remote driver
				return new AtsRemoteWebDriver(getDriverInfo().getDriverLoopback().toURL(), cap);
			}catch(Exception ex){
				errorMessage += "\n" + ex.getMessage();
			}
		}

		status.setTechnicalError(ActionStatus.CHANNEL_START_ERROR, "Unable to start remote driver:\n" + errorMessage);
		return null;
	}

	protected void launchDriver(ActionStatus status, MutableCapabilities cap) {

		final AtsManager ats = AtsSingleton.getInstance().getAts();

		final int maxTrySearch = ats.getMaxTrySearch();
		final int maxTryProperty = ats.getMaxTryProperty();

		final int scriptTimeout = ats.getScriptTimeOut();
		final int pageLoadTimeout = ats.getPageloadTimeOut();
		final int watchdog = ats.getWatchDogTimeOut();

		if(channel.getPerformance() == ActionChannelStart.PERF) {
			cap.setCapability(CapabilityType.PROXY, channel.startAtsProxy(ats));
		}else if(channel.getPerformance() == ActionChannelStart.NEOLOAD) {
			channel.setNeoloadDesignApi(ats.getNeoloadDesignApi());
			cap.setCapability(CapabilityType.PROXY, ats.getNeoloadProxy().getValue());
		}

		cap.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, PageLoadStrategy.NONE);

		driver = getRemoteDriver(status, cap);
		if(driver == null){
			getDriverInfo().close();
			driver = null;
			return;
		}

		/* for Opera windows */
		setWindowHandles(getDriverWindowsList());
		setMainWindowHandle(driver.getWindowHandle());
		setBlackListWindowHandles();
		/* end opera windows */

		status.setNoError();
		status.startDuration();

		actions = new Actions(driver);

		driver.manage().timeouts().scriptTimeout(Duration.ofSeconds(scriptTimeout));
		try {
			driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(pageLoadTimeout));
		}catch(InvalidArgumentException e) {} // needed for old versions of Firefox

		if(getDriverInfo().isGrid()) {

			driver.navigate().to("chrome://version/");

		}else {


			String applicationVersion = null;
			String driverVersion = getDriverInfo().getDriverVersion();
			String profileDir = "";

			final Map<String, ?> infos = driver.getCapabilities().asMap();
			for (Map.Entry<String, ?> entry : infos.entrySet()){
				if("browserVersion".equals(entry.getKey()) || "version".equals(entry.getKey())){
					applicationVersion = entry.getValue().toString();
				}else if(DriverManager.CHROME_BROWSER.equals(entry.getKey())) {
					Map<String, String> chromeData = (Map<String, String>) entry.getValue();
					driverVersion = getDriverVersion(chromeData.get("chromedriverVersion"));
					profileDir = chromeData.get("userDataDir");
				}else if(DriverManager.OPERA_BROWSER.equals(entry.getKey())) {
					Map<String, String> operaData = (Map<String, String>) entry.getValue();
					driverVersion = getDriverVersion(operaData.get("operadriverVersion"));
					profileDir = operaData.get("userDataDir");
				}else if(DriverManager.MSEDGE_BROWSER.equals(entry.getKey())) {
					Map<String, String> msedgeData = (Map<String, String>) entry.getValue();
					driverVersion = getDriverVersion(msedgeData.get("msedgedriverVersion"));
					profileDir = msedgeData.get("userDataDir");
				}else if("moz:geckodriverVersion".equals(entry.getKey())) {
					driverVersion = entry.getValue().toString();
				}else if("moz:profile".equals(entry.getKey())) {
					profileDir = entry.getValue().toString();
				}
			}

			final String titleUid = UUID.randomUUID().toString();

			final StringBuilder sb = getSystemDriver().getLocalhostAndPort()
					.append("/start?tt=").append(titleUid)
					.append("&av=").append(ATS.getAtsVersion())
					.append("&et=").append(maxTrySearch)
					.append("&pt=").append(maxTryProperty)
					.append("&js=").append(scriptTimeout)
					.append("&to=").append(pageLoadTimeout)
					.append("&wd=").append(watchdog)
					.append("&dv=").append(driverVersion)
					.append("&bn=").append(channel.getApplication())
					.append("&bv=").append(applicationVersion)
					.append("&px=").append(channel.getDimension().getX().intValue())
					.append("&py=").append(channel.getDimension().getY().intValue())
					.append("&pw=").append(channel.getDimension().getWidth().intValue())
					.append("&ph=").append(channel.getDimension().getHeight().intValue())
					.append("&wa=").append(getActionWait())
					.append("&dc=").append(getPropertyWait())
					.append("&pd=").append(profileDir);

			final String startUrl = sb.toString();
			driver.navigate().to(startUrl);

			int maxTry = 30;
			String winTitle = driver.getTitle();
			while(maxTry > 0 && !titleUid.equals(winTitle)) {
				channel.sleep(500);
				driver.navigate().to(startUrl);
				winTitle = driver.getTitle();
				maxTry--;
			}

			if(maxTry == 0) {
				status.setTechnicalError(ActionStatus.CHANNEL_START_ERROR, "Unable to start web channel driver:\ntimeout when loading start page");
			}else {

				final String osVersion = getSystemDriver().getOsName() + " (" + getSystemDriver().getOsVersion() +")";

				final DesktopWindow window = getSystemDriver().getWindowByTitle(titleUid, getDriverInfo().getName());

				getSystemDriver().setEngine(new SystemDriverEngine(channel, window, isAtsLearningEnabled()));
				channel.setApplicationData(Channel.HTML, osVersion,	applicationVersion,	driverVersion, window.getPid(),	getBrowserIcon(window));

				getDriverInfo().setSessionId(driver.getSessionId().toString());

				final Dimension channelSize = channel.getDimension().getSize();
				try{
					if(channelSize.getWidth() == -1 && channelSize.getWidth() == -1) {
						final ArrayList<Object> screen = (ArrayList<Object>)runJavaScript("var result=[screen.width, screen.height]");
						channel.setFullScreenSize((long) screen.get(0), (long) screen.get(1));
					}
					driver.setWindowSize(channel.getDimension().getSize());
					driver.setWindowPosition(channel.getDimension().getPoint());
				}catch(Exception ex){}

				try{
					channel.setZoom((double)runJavaScript("var result=window.devicePixelRatio/100")*10000);
				}catch(Exception ex){}
			}
		}
	}

	private byte[] getBrowserIcon(DesktopWindow window) {
		try {
			return Resources.toByteArray(ResourceContent.class.getResource("/icons/" + getDriverInfo().getName() + ".png"));
		}catch (Exception e) {}
		return window.getAppIcon();
	}

	private String getDriverVersion(String value) {
		if(value != null) {
			return value.replaceFirst("\\(.*\\)", "").trim();
		}
		return null;
	}

	@Override
	public void waitAfterAction(ActionStatus status) {
		actionWait();
	}

	protected String[] getWindowsHandle(int index, int tries) {
		Set<String> list = getDriverWindowsList();
		int maxTry = 1 + tries;
		while(index >= list.size() && maxTry > 0) {
			channel.sleep(1000);
			list = getDriverWindowsList();
			maxTry--;
		}
		return list.toArray(new String[list.size()]);
	}

	protected Set<String> getDriverWindowsList(){
		getWindowsData();
		return windowsHandleList;
	}

	protected Set<String> getWindowsList(){
		try {
			return driver.getWindowHandles();
		}catch (WebDriverException e) {
			return Collections.<String>emptySet();
		}
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	@Override
	public void scroll(int delta) {
		runJavaScript(JS_WINDOW_SCROLL, delta);
	}

	@Override
	public void scroll(FoundElement element, int delta) {
		if(delta == 0) {
			scroll(element);
		}else {
			final ArrayList<Double> newPosition =  (ArrayList<Double>) runJavaScript(JS_ELEMENT_SCROLL, element.getValue(), delta);
			updatePosition(newPosition, element);
		}
	}

	@Override
	public void scroll(FoundElement element) {
		updatePosition((ArrayList<Double>) runJavaScript(JS_SCROLL_IF_NEEDED, element.getValue()), element);
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	private void updatePosition(ArrayList<Double> position, FoundElement element) {
		if(position != null && position.size() > 1) {
			element.updatePosition(position.get(0), position.get(1), channel, 0.0, 0.0);
			channel.sleep(500);
		}
	}

	@Override
	public FoundElement getElementFromPoint(Boolean syscomp, Double x, Double y){

		if(syscomp) {
			return getSystemDriver().getElementFromPoint(x, y);
		}else {

			switchToDefaultContent(false);

			x -= channel.getSubDimension().getX();
			y -= channel.getSubDimension().getY();

			shadowList = new LinkedList<>();
			return loadElement(new ArrayList<>(), null, x, y, initElementX, initElementY);
		}
	}

	protected FoundElement loadElement(ArrayList<AtsElement> iframes, RemoteWebElement shadowRoot, Double x, Double y, Double offsetX, Double offsetY) {

		final ArrayList<Object> objectData = (ArrayList<Object>)runJavaScript(JS_ELEMENT_FROM_POINT, shadowRoot, x - offsetX, y - offsetY);

		if(objectData != null){

			final AtsElement element = new AtsElement(this, objectData, false);

			if(element.isIframe()){

				//FoundElement frm = new FoundElement(element);
				//switchToFrame(frm.getValue());

				iframes.add(0, element);
				switchToFrame(element.getElement());

				offsetX += element.getX();
				offsetY += element.getY();

				return loadElement(iframes, null, x, y, offsetX, offsetY);

			}else if(element.isShadowRoot()) {

				return getShadowElements(iframes, element.getElement(), x, y);

			} else {
				return new FoundElement(element, iframes, channel, offsetX, offsetY);
			}

		} else {
			return null;
		}
	}

	private FoundElement getShadowElements(ArrayList<AtsElement> iframes, RemoteWebElement element, Double x, Double y) {
		shadowList.add(0, element);
		return loadElement(iframes, element, x, y, 0D, 0D);
	}

	private FoundElement getShadowElements(ArrayList<AtsElement> iframes, RemoteWebElement element, Double x, Double y, Double w, Double h) {
		shadowList.add(0, element);
		return loadElement(iframes, element, x, y, w, h, 0D, 0D);
	}

	@Override
	public FoundElement getElementFromRect(Boolean syscomp, Double x, Double y, Double w, Double h){

		if(syscomp) {
			return getSystemDriver().getElementFromRect(x, y, w, y);
		}else {

			switchToDefaultContent(false);

			x = x - channel.getSubDimension().getX() - channel.getDimension().getX();
			y = y - channel.getSubDimension().getY() - channel.getDimension().getY();

			return loadElement(new ArrayList<>(), null, x, y, w, h, initElementX, initElementY);
		}
	}

	private FoundElement loadElement(ArrayList<AtsElement> iframes, RemoteWebElement shadow, Double x, Double y, Double w, Double h, Double offsetX, Double offsetY) {

		final ArrayList<Object> objectData = (ArrayList<Object>)runJavaScript(JS_ELEMENT_FROM_RECT, shadow, x - offsetX, y - offsetY, w, h);

		if(objectData != null){

			final AtsElement element = new AtsElement(this, objectData, false);

			if(element.isIframe()){

				//iframes.add(0, element);
				//FoundElement frm = new FoundElement(element);
				//switchToFrame(frm.getValue());

				iframes.add(0, element);
				switchToFrame(element.getElement());

				offsetX += element.getX();
				offsetY += element.getY();

				return loadElement(iframes, shadow, x, y, w, h, offsetX, offsetY);

			}else if(element.isShadowRoot()) {

				return getShadowElements(iframes, element.getElement(), x, y, w, h);

			} else {
				return new FoundElement(element, iframes, channel, offsetX, offsetY);
			}

		} else {
			return null;
		}
	}

	@Override
	public void loadParents(FoundElement hoverElement){
		if(hoverElement.isDesktop()){
			hoverElement.setParent(getSystemDriver().getTestElementParent(hoverElement.getId(), channel));
		}else{
			hoverElement.setParent(getTestElementParent(hoverElement));
		}
	}

	@Override
	public String getParentsDomCode(WebElement element) {

		final StringBuilder html = new StringBuilder();
		final StringBuilder closeHtml = new StringBuilder();

		while(iframesDom != null && iframesDom.size() > 0) {
			final String iframeDom = iframesDom.remove(iframesDom.size()-1);
			final int lastIndex = iframeDom.indexOf("</iframe>");
			if(lastIndex > -1) {
				html.append(iframeDom.substring(0, lastIndex));
				closeHtml.append(iframeDom.substring(lastIndex));
			}
		}

		return html.append((String) runJavaScript(JS_ELEMENT_PARENTS_HTML, element))
				.append(closeHtml).toString();
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	@Override
	public WebElement getRootElement(Channel cnl) {

		int maxTry = 20;
		WebElement body = getHtmlView();

		while(body == null && maxTry > 0) {
			cnl.sleep(200);
			body = getHtmlView();
			maxTry--;
		}

		return body;
	}

	@Override
	public String getTitle() {
		return driver.getTitle();
	}

	@Override
	public String getCookies() {
		final StringJoiner joiner = new StringJoiner("\n");
		for(Cookie cookie : driver.manage().getCookies()){
			joiner.add(cookie.getName() + "=" + cookie.getValue());
		}
		return joiner.toString();
	}

	@Override
	public String getHeaders(ActionStatus status) {
		try {
			final Object obj = driver.executeScript("var req = new XMLHttpRequest();req.open('HEAD', document.location, false);req.send(null);var result = req.getAllResponseHeaders();delete req;return result;");
			return obj.toString();
		}catch(InvalidSelectorException e) {
			status.setError(ActionStatus.JAVASCRIPT_ERROR, e.getStackTrace().toString());
		}catch(Exception e) {
			status.setError(ActionStatus.JAVASCRIPT_ERROR, e.getMessage());
		}
		return "";
	}

	private WebElement getHtmlView() {
		return (WebElement)driver.executeScript("return window.document.getElementsByTagName(\"html\")[0];");
	}

	protected RemoteWebElement getWebElement(FoundElement element) {
		return element.getRemoteWebElement(driver);
	}

	@Override
	public String getAttribute(ActionStatus status, FoundElement element, String attributeName, int maxTry) {

		String result = getAttribute(status, element, attributeName);
		if(result != null && doubleCheckAttribute(status, result, element, attributeName)) {
			return result;
		}

		return null;
	}

	@Override
	public List<String[]> loadSelectOptions(TestElement element) {
		final ArrayList<String[]> result = new ArrayList<>();

		if(element.isAngularSelect()) {
			element.getWebElement().sendKeys(Keys.TAB);
			element.click(channel.newActionStatus(), new MouseDirection());

			final List<FoundElement> options = findMatSelectOptions(element);
			if(options != null && options.size() > 0) {
				options.stream().forEachOrdered(e -> result.add(new String[]{e.getValue().getText(), e.getValue().getText()}));
			}

		}else {
			final List<FoundElement> options = findSelectOptions(null, element);
			if(options != null && options.size() > 0) {
				options.stream().forEachOrdered(e -> result.add(new String[]{e.getValue().getDomAttribute("value"), e.getValue().getDomProperty("text")}));
			}
		}
		return result;
	}

	@Override
	public List<FoundElement> findSelectOptions(TestBound dimension, TestElement element) {
		return listElementsFound(runJavaScript(searchElementScript, element.getWebElement(), AtsElement.OPTION, new String[0], 0), Objects::nonNull, false);
	}

	public List<FoundElement> findMatSelectOptions(TestElement element) {
		switchToDefaultContent(false);
		return findElements(false, element, ANGULAR_OPTION, new String[0], new String[0], Objects::nonNull, null);
	}

	@Override
	public void selectOptionsItem(ActionStatus status, TestElement element, CalculatedProperty selectProperty, boolean keepSelect) {

		if(element.isAngularSelect()) {

			element.getWebElement().sendKeys(Keys.TAB);
			element.click(status, new MouseDirection());

			final List<FoundElement> items = findMatSelectOptions(element);

			if(items != null && items.size() > 0) {
				if(ActionSelect.SELECT_INDEX.equals(selectProperty.getName())){
					final int index = Utils.string2Int(selectProperty.getValue().getCalculated());
					if(items.size() > index) {
						try {
							items.get(index).getValue().click();
						}catch (Exception e) {
							status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, e.getMessage());
						}
					}else {
						status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, "index not found, max length options: " + items.size());
					}
				}else {
					final String searchedValue = selectProperty.getValue().getCalculated();
					try {
						if(selectProperty.isRegexp()) {
							items.stream().filter(e -> e.getValue().getText().matches(searchedValue)).findFirst().get().getValue().click();
						}else {
							items.stream().filter(e -> e.getValue().getText().equals(searchedValue)).findFirst().get().getValue().click();
						}
					}catch (NoSuchElementException e) {
						status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, "mat-option not found: " + searchedValue);
					}
				}
			}

		}else {
			final List<FoundElement> items = findSelectOptions(null, element);

			if(items != null && items.size() > 0) {

				boolean expanded = false;

				final WebElement webElem = element.getFoundElement().getValue();
				String tagName = getElementTag(webElem);

				if(!AtsElement.SELECT.equalsIgnoreCase(tagName)) {
					status.setError(ActionStatus.NOT_SELECT_ELEMENT, "element is not a 'select' element: " + tagName);
					return;
				}

				final Select select = new Select(webElem);
				if(select.isMultiple()) {
					if(!keepSelect) {
						select.deselectAll();
					}
				}else {
					expanded = true;
					element.click(status, new MouseDirection());
				}

				if(ActionSelect.SELECT_INDEX.equals(selectProperty.getName())){

					int index = 0;
					String calculated = selectProperty.getValue().getCalculated();
					try {
						index = Integer.parseInt(calculated);
					}catch (NumberFormatException e) {
						channel.sendWarningLog("select using index", "'" + calculated + "' cannot be parsed as integer, using default value 0");
					}

					if(items.size() > index) {
						try {
							items.get(index).getValue().click();
						}catch (Exception e) {

							WebElement we = items.get(index).getValue();
							tagName = getElementTag(we);

							if(AtsElement.OPTION.equalsIgnoreCase(tagName)) {
								we = we.findElement(By.xpath("./.."));
							}

							tagName = getElementTag(we);
							if(AtsElement.SELECT.equalsIgnoreCase(tagName)){
								new Select(we).selectByIndex(index);
							}else {
								status.setError(ActionStatus.NOT_SELECT_ELEMENT, "element is not a 'select' element: " + tagName);
							}
						}
					}else {
						status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, "index not found, max length options: " + items.size());
					}

				}else{

					final String attribute = selectProperty.getName();
					final String searchedValue = selectProperty.getValue().getCalculated();

					Optional<FoundElement> foundOption = null;

					if(selectProperty.isRegexp()) {
						foundOption = items.stream().filter(e -> e.getDomAttributeOrProperty(attribute).matches(searchedValue)).findFirst();
					}else {
						foundOption = items.stream().filter(e -> e.getDomAttributeOrProperty(attribute).equals(searchedValue)).findFirst();
					}

					if(foundOption.isEmpty()) {
						status.setError(ActionStatus.OBJECT_NOT_FOUND, "option not found: " + searchedValue);
					}else {
						try {
							foundOption.get().getValue().click();
						}catch (NoSuchElementException e) {
							status.setError(ActionStatus.OBJECT_NOT_INTERACTABLE, "option not found: " + searchedValue);
						}
					}
				}

				if(expanded) {
					/*try {
						element.click(status, new MouseDirection());
					}catch(Exception e) {}*/
					try {
						driver.executeScript("arguments[0].blur();", element.getWebElement());
					}catch(Exception e) {}
				}
			}
		}
	}

	private boolean doubleCheckAttribute(ActionStatus status, String verify, FoundElement element, String attributeName) {
		channel.sleep(getPropertyWait());

		final String current = getAttribute(status, element, attributeName);
		return current != null && current.equals(verify);
	}

	@Override
	public void setSysProperty(String propertyName, String propertyValue) { }

	private String getAttribute(ActionStatus status, FoundElement element, String attributeName) {

		final RemoteWebElement elem = getWebElement(element);
		String result = elem.getAttribute(attributeName);

		if(result == null) {

			for (CalculatedProperty calc : getAttributes(element, false)) {
				if(attributeName.equals(calc.getName())) {
					return calc.getValue().getCalculated();
				}
			}
			result = getCssAttributeValueByName(element, attributeName);

			if(result == null) {
				final Object obj = executeJavaScript(status, attributeName, true);
				if(obj != null) {
					result = obj.toString();
				}
			}
		}
		return result;
	}

	private String getCssAttributeValueByName(FoundElement element, String name) {
		return foundAttributeValue(name, getCssAttributes(element));
	}

	private String foundAttributeValue(String name, CalculatedProperty[] properties) {
		final Stream<CalculatedProperty> stream = Arrays.stream(properties);
		final Optional<CalculatedProperty> calc = stream.parallel().filter(c -> c.getName().equals(name)).findFirst();
		if(calc.isPresent()) {
			return calc.get().getValue().getCalculated();
		}
		return null;
	}

	@Override
	public CalculatedProperty[] getAttributes(FoundElement element, boolean reload){
		if(element.isDesktop()){
			return getSystemDriver().getElementAttributes(element.getId());
		}else {
			return getAttributes(getWebElement(element));
		}
	}

	@Override
	public String getTextData(FoundElement element){
		if(element.isDesktop()){
			return getSystemDriver().getTextData(element.getId());
		}else {
			final Object result = runJavaScript(JS_ELEMENT_TEXT_DATA, getWebElement(element));
			if(result != null && result instanceof Map){
				return ((Map<String, Object>)result).get("text").toString();
			}
		}
		return "";
	}

	@Override
	public String getSelectedText(TestElement element) {

		List<FoundElement> items = null;
		if(element.isAngularSelect()) {
			items = findMatSelectOptions(element);
		}else {
			items = findSelectOptions(null, element);
		}

		final StringJoiner sj = new StringJoiner("|");
		items.stream().filter(e -> e.isSelected()).forEach(e -> sj.add(e.getTextContent()));

		return sj.toString();
	}

	@Override
	public CalculatedProperty[] getCssAttributes(FoundElement element){
		return getCssAttributes(getWebElement(element));
	}

	@Override
	public CalculatedProperty[] getHtmlAttributes(FoundElement element){
		return getHtmlAttributes(getWebElement(element));
	}

	@Override
	public CalculatedProperty[] getFunctions(FoundElement element) {
		final Object result = runJavaScript(JS_ELEMENT_FUNCTIONS, getWebElement(element));
		if(result != null && result instanceof Map){
			return ((Map<String, Object>)result).entrySet().stream().parallel().filter(e -> !(e.getValue() instanceof Map)).map(e -> new CalculatedProperty(e.getKey(), getFunctionSignature((long)e.getValue()))).toArray(c -> new CalculatedProperty[c]);
		}
		return new CalculatedProperty[0];
	}

	private String getFunctionSignature(long length) {

		final StringJoiner sj = new StringJoiner(", ");
		for(int i=0; i<length; i++) {
			sj.add("'param" + i + "'");
		}

		return sj.toString();
	}

	private CalculatedProperty[] getCssAttributes(RemoteWebElement element){
		return getAttributesList(element, JS_ELEMENT_CSS);
	}

	private CalculatedProperty[] getHtmlAttributes(RemoteWebElement element){

		final Object result = runJavaScript(JS_ELEMENT_HTML, element);
		if(result != null && result instanceof ArrayList){
			final ArrayList<CalculatedProperty> props = new ArrayList<CalculatedProperty>();
			((ArrayList<String>)result).forEach(s -> addProperty(props, s, element));
			return props.toArray(new CalculatedProperty[props.size()]);
		}

		return new CalculatedProperty[0];
	}

	private void addProperty(ArrayList<CalculatedProperty> props, String attribute, RemoteWebElement element) {
		String attributeValue = element.getDomAttribute(attribute);
		if(attributeValue == null) {
			attributeValue = element.getDomProperty(attribute);
		}

		props.add(
				new CalculatedProperty(
						attribute, 
						attributeValue
						));
	}

	protected CalculatedProperty[] getAttributes(RemoteWebElement element){
		return getAttributesList(element, JS_ELEMENT_ATTRIBUTES);
	}

	private CalculatedProperty[] getAttributesList(RemoteWebElement element, String script) {
		final Object result = runJavaScript(script, element);
		if(result != null && result instanceof Map){
			return ((Map<String, Object>)result).entrySet().stream().parallel().filter(e -> !(e.getValue() instanceof Map)).map(e -> new CalculatedProperty(e.getKey(), e.getValue().toString())).toArray(c -> new CalculatedProperty[c]);
		}
		return new CalculatedProperty[0];
	}

	public FoundElement getTestElementParent(FoundElement element){

		final ArrayList<ArrayList<Object>> listElements = (ArrayList<ArrayList<Object>>) runJavaScript(JS_ELEMENT_PARENTS, element.getValue());

		FoundElement found = null;

		if(listElements != null && listElements.size() > 0){
			found = new FoundElement(
					channel,
					element.getIframes(),
					listElements.stream().map(e -> new AtsElement(this, e, false)).collect(Collectors.toCollection(ArrayList::new)),
					initElementX,
					initElementY);
		}

		if(shadowList != null && shadowList.size() > 0) {

			switchToDefaultContent(false);
			FoundElement shadow = new FoundElement(this, shadowList.pop());
			while(shadowList.size() > 0) {
				shadow.addShadowParent((new FoundElement(this, shadowList.pop())));
			}

			if(found == null) {
				found = shadow;
			}else {
				found.addShadowParent(shadow);
			}
		}

		return found;
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	@Override
	public void updateDimensions() {
		final ArrayList<Double> response = (ArrayList<Double>) runJavaScript(JS_DOCUMENT_SIZE);
		if(response != null && response.size() == 8) {
			if(isHeadless()) {
				channel.getDimension().update(response.get(4), response.get(5), response.get(6), response.get(7));
			}else {
				channel.getDimension().update(response.get(0), response.get(1), response.get(2), response.get(3));
			}
			channel.getSubDimension().update(response.get(4), response.get(5), response.get(6), response.get(7));
		}
	}

	@Override
	public synchronized void close() {

		if(!isHeadless() && channel.isDesktopDriverEnabled()) {
			getSystemDriver().closeDialogBoxes(channel.getProcessId());
		}

		closeWindows();
		getDriverInfo().close();
	}

	@Override
	public void quit() {
	}

	protected void closeWindows() {
		channel.sendLog(MessageCode.ACTION_IN_PROGRESS, "close web browser", channel.getApplication());
		if(driver != null){
			try {
				driver.getWindowHandles().stream().sorted(Collections.reverseOrder()).forEach(s -> closeWindowHandler(s));
				driver.quit();
			}catch (Exception e) {}
			driver = null;
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Mouse position by browser
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	protected double getCartesianOffset(double value, MouseDirectionData direction, Cartesian cart1, Cartesian cart2,	Cartesian cart3) {
		return super.getCartesianOffset(value, direction, cart1, cart2, cart3) - value/2;
	}

	@Override
	public void mouseMoveToElement(FoundElement element) {
		try {
			move(element, 0, 0);
		}catch(ElementNotInteractableException e0) {
			throw e0;
		}catch (Exception e) {}

	}

	@Override
	public void mouseMoveToElement(ActionStatus status, FoundElement foundElement, MouseDirection position, boolean withDesktop, int offsetX, int offsetY) {

		channel.waitBeforeMouseMoveToElement(this);

		if(withDesktop) {
			desktopMoveToElement(foundElement, position,offsetX ,offsetY);
		}else {
			int maxTry = 10;
			while(maxTry > 0) {
				status.setNoError();
				try {
					scrollAndMove(foundElement, position, offsetX, offsetY);
					maxTry = 0;
				}catch(StaleElementReferenceException e0) {
					throw e0;
				}catch(JavascriptException e1) {
					switchToDefaultContent(false);
					status.setException(ActionStatus.JAVASCRIPT_ERROR, e1);
					throw e1;
				}catch(MoveTargetOutOfBoundsException e2) {
					driver.executeScript("arguments[0].scrollIntoView();", foundElement.getValue());
					maxTry = 0;
				}catch(ElementNotInteractableException e5) {
					maxTry = 0;
					throw e5;
				}catch(WebDriverException e3) {
					status.setException(ActionStatus.WEB_DRIVER_ERROR, e3);
					channel.sleep(500);
					maxTry--;
				}
			}
		}
	}

	private void scrollAndMove(FoundElement element, MouseDirection position, int offsetX, int offsetY) {
		scroll(element);
		channel.sleep(100);

		final Rectangle rect = element.getRectangle();
		move(element, ((int)getOffsetX(rect, position)) + offsetX, ((int)getOffsetY(rect, position)) + offsetY);
	}

	protected void move(FoundElement element, double offsetX, double offsetY) {
		try {
			actions.moveToElement(element.getValue(), (int)offsetX, (int)offsetY).perform();
		}catch (JavascriptException e) {
			if(!e.getMessage().contains("elementsFromPoint")){
				throw e;
			}
		}
	}

	@Override
	public void mouseClick(ActionStatus status, FoundElement element, MouseDirection position, int offsetX, int offsetY) {
		final Rectangle rect = element.getRectangle();
		try {
			click(element, getOffsetX(rect, position) + offsetX, getOffsetY(rect, position) + offsetY);
			status.setPassed(true);
		}catch(StaleElementReferenceException e1) {
			throw e1;
		}catch(MoveTargetOutOfBoundsException e) {
			driver.executeScript("arguments[0].click();", element.getValue());
		}catch (Exception e) {
			status.setException(ActionStatus.OBJECT_NOT_INTERACTABLE, e);
		}
	}

	protected void click(FoundElement element, double offsetX, double offsetY) {
		actions.moveToElement(element.getValue(), (int)offsetX, (int)offsetY)
		.click()
		.build()
		.perform();
	}

	@Override
	public void drag(ActionStatus status, FoundElement element, MouseDirection position, int offsetX, int offsetY, boolean offset) {

		final Rectangle rect = element.getRectangle();

		try {

			final Actions a = actions
					.moveToElement(element.getValue(), ((int)getOffsetX(rect, position)) + offsetX, ((int)getOffsetY(rect, position)) + offsetY)
					.pause(Duration.ofMillis(200))
					.clickAndHold(element.getValue())
					.pause(Duration.ofMillis(200));

			if(offset) {
				a.moveByOffset(20, -20).pause(Duration.ofMillis(200));
			}

			a.build().perform();

			status.setPassed(true);

		}catch(StaleElementReferenceException e1) {
			throw e1;
		}catch (Exception e) {
			status.setException(ActionStatus.OBJECT_NOT_INTERACTABLE, e);
		}
	}

	@Override
	public void drop(FoundElement element, MouseDirection md, boolean desktopDragDrop) {
		if(desktopDragDrop) {
			getSystemDriver().mouseRelease();
		}else {
			actions
			.moveToElement(element.getValue())
			.pause(Duration.ofMillis(200))
			.release(element.getValue())
			.build().perform();

			actions.release().perform();
			actions.moveToElement(element.getValue()).perform();
		}
	}

	@Override
	public void swipe(ActionStatus status, FoundElement element, MouseDirection position, MouseDirection direction) {
		drag(status, element, position, 0, 0, false);
		moveByOffset(direction.getHorizontalDirection(), direction.getVerticalDirection());
		actions.release().perform();
	}

	@Override
	public void keyDown(Keys key) {
		actions.keyDown(key).perform();
	}

	@Override
	public void keyUp(Keys key) {
		actions.keyUp(key).perform();
	}

	@Override
	public void moveByOffset(int hDirection, int vDirection) {
		actions.moveByOffset(hDirection, vDirection).perform();
	}

	@Override
	public void doubleClick() {
		actions.doubleClick().perform();
	}

	@Override
	public void rightClick() {
		actions.contextClick().perform();
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Iframes management
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public boolean switchToDefaultContent(boolean dialog) {

		if(driver != null) {
			if(dialog) {
				try {

					driver
					.switchTo()
					.alert();

					return true;
				}catch(NoAlertPresentException | NoSuchWindowException e) {}
			}

			try {

				driver
				.switchTo()
				.defaultContent();

				return true;
			}catch (WebDriverException e) {}
		}
		return false;
	}

	@Override
	public void switchToFrameId(String id) {
		final RemoteWebElement rwe = new RemoteWebElement();
		rwe.setId(id);
		rwe.setParent(driver);
		switchToFrame(rwe);
	}

	protected void switchToFrame(WebElement we) {
		driver.switchTo().frame(we);
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Window management
	//-----------------------------------------------------------------------------------------------------------------------------------

	private Set<String> windowsHandleList = Collections.<String>emptySet();

	@Override
	public List<String[]> getWindowsData() {

		final ArrayList<String[]> list = new ArrayList<String[]>();
		windowsHandleList = new LinkedHashSet<String>();

		final String currentWin = driver.getWindowHandle();

		for (String handle : getWindowsList()) {
			driver.switchTo().window(handle);
			if(!driver.getCurrentUrl().startsWith("chrome-extension://")) {
				list.add(new String[] {driver.getTitle(), driver.getCurrentUrl()});
				windowsHandleList.add(handle);
			}
		}

		driver.switchTo().window(currentWin);

		return ImmutableList.copyOf(list);
	}

	protected boolean switchToWindowHandle(String handle) {
		try {
			driver.switchTo().window(handle);
			channel.sleep(500);
			return switchToDefaultContent(false);
		}catch(NoSuchWindowException ex) {
			return false;
		}
	}

	protected void switchToWindowIndex(String[] wins, int index, boolean refresh) {

		int maxTry = 10;
		boolean switched = switchToWindowHandle(wins[index]);

		while(!switched && maxTry > 0) {
			channel.sleep(1000);
			wins = getWindowsHandle(index, 0);
			switched = switchToWindowHandle(wins[index]);
		}

		if(switched) {
			currentWindow = index;
			if(refresh) {
				channel.sleep(300);
				driver.navigate().refresh();
			}
		}
	}

	@Override
	public void setWindowToFront() {
		if(!isHeadless() && channel.isDesktopDriverEnabled()) {
			channel.toFront();
			List<String> winsList = new ArrayList<>(Arrays.asList(getWindowsHandle(0, 0)));
			winsList.removeAll(getBlackListWindowHandles());
			final String[] wins = winsList.toArray(new String[0]);
			//			final String[] wins = getWindowsHandle(0, 0);
			if(wins.length> currentWindow) {
				driver.switchTo().window(wins[currentWindow]);
				try {
					driver.setWindowSize(channel.getDimension().getSize());
				}catch(Exception e) {}
			}
		}
	}

	@Override
	public void newWindow(ActionStatus status, WindowType type, CalculatedValue url) {
		driver.switchTo().newWindow(type);
		finalizeSwitch(driver.getWindowHandle(), getDriverWindowsList().size()-1, false);

		if(url != null && !url.getCalculated().isBlank()) {
			goToUrl(status, url.getCalculated());
		}
	}

	@Override
	public boolean switchWindow(ActionStatus status, String type, String data, boolean regexp, int tries, boolean refresh) {
		channel.waitBeforeSwitchWindow(this);

		if(ActionWindowSwitch.SWITCH_INDEX.equals(type)) {

			return switchWindow(status, Utils.string2Int(data) , tries, refresh);

		}else if(ActionWindowSwitch.SWITCH_NAME.equals(type)) {

			final String currentWin = driver.getWindowHandle();

			int idxLoop = 0;

			if(regexp) {

				final Pattern pattern = Pattern.compile(data, Pattern.CASE_INSENSITIVE);

				for (String handle : getDriverWindowsList()) {
					driver.switchTo().window(handle);

					final Matcher matcher = pattern.matcher(driver.getTitle());
					if(matcher.find()) {
						finalizeSwitch(driver.getWindowHandle(), idxLoop, refresh);
						return true;
					}

					idxLoop++;
				}

			}else {
				for (String handle : getDriverWindowsList()) {
					driver.switchTo().window(handle);
					final String title = driver.getTitle();
					if(title.equals(data)){
						finalizeSwitch(driver.getWindowHandle(), idxLoop, refresh);
						return true;
					}
					idxLoop++;
				}
			}

			driver.switchTo().window(currentWin);
			status.setError(ActionStatus.WINDOW_NOT_FOUND, "cannot switch to window with title matching '" + data + "'");

		}else if(ActionWindowSwitch.SWITCH_URL.equals(type)) {

			final String currentWin = driver.getWindowHandle();
			int idxLoop = 0;

			if(regexp) {

				final Pattern pattern = Pattern.compile(data, Pattern.CASE_INSENSITIVE);

				for (String handle : getDriverWindowsList()) {
					driver.switchTo().window(handle);

					final Matcher matcher = pattern.matcher(driver.getCurrentUrl());
					if(matcher.find()) {
						finalizeSwitch(driver.getWindowHandle(), idxLoop, refresh);
						return true;
					}

					idxLoop++;
				}

			}else {
				for (String handle : getDriverWindowsList()) {
					driver.switchTo().window(handle);
					if(driver.getCurrentUrl().equals(data)){
						finalizeSwitch(driver.getWindowHandle(), idxLoop, refresh);
						return true;
					}
					idxLoop++;
				}
			}

			driver.switchTo().window(currentWin);
			status.setError(ActionStatus.WINDOW_NOT_FOUND, "cannot switch to window with url matching '" + data + "'");
		}

		return false;
	}

	private void finalizeSwitch(String handle, int idx, boolean refresh) {

		currentWindow = idx;
		getSystemDriver().updateWindowHandle(channel);
		switchToDefaultContent(false);

		if(refresh) {
			channel.sleep(200);
			driver.navigate().refresh();
		}
	}

	public boolean switchWindow(ActionStatus status, int index, int tries, boolean refresh) {

		channel.waitBeforeSwitchWindow(this);

		if(index >= 0) {
			final String[] wins = getWindowsHandle(index, tries);
			if(wins.length > index) {
				switchToWindowIndex(wins, index, refresh);
				channel.cleanHandle();
				getSystemDriver().updateWindowHandle(channel);
			}else {
				status.setError(ActionStatus.WINDOW_NOT_FOUND, "cannot switch to window index '" + index + "', only " + wins.length + " window(s) found");
			}
		}
		return true;
	}

	@Override
	protected void setPosition(Point pt) {
		channel.sleep(500);
		driver.setWindowPosition(pt);
	}

	@Override
	protected void setSize(Dimension dim) {
		channel.sleep(500);
		driver.setWindowSize(dim);
	}

	protected void closeWindowHandler(String windowHandle) {
		switchToWindowHandle(windowHandle);

		try {
			Alert alert = driver.switchTo().alert();
			alert.dismiss();
		}catch(Exception e) {}

		closeCurrentWindow();
	}

	protected void closeCurrentWindow() {
		driver.close();
		channel.sleep(200);
	}

	@Override
	public void closeWindow(ActionStatus status) {
		final String[] list = getWindowsHandle(0, 0);
		if(list.length > 1) {
			if(currentWindow < list.length) {
				closeWindowHandler(list[currentWindow]);
				currentWindow = 0;
			}
			switchToWindowHandle(list[0]);
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	//
	//-----------------------------------------------------------------------------------------------------------------------------------

	@Override
	public String getCurrentHandle() {
		return driver.getWindowHandle();
	}

	@Override
	public Object executeScript(ActionStatus status, String javaScript, Object... params) {
		final Object result = runJavaScript(status, "var result={};" + javaScript + ";", params);
		if(status.isPassed() && result != null) {
			status.setMessage(result.toString());
		}
		return result;
	}

	@Override
	public Object executeJavaScript(ActionStatus status, String javaScript, boolean returnValue) {
		try {
			if(returnValue) {
				final Object result = driver.executeAsyncScript("var callback=arguments[arguments.length-1];var result=" + javaScript + ";callback(result);");
				status.setMessage(result.toString());
				return result;
			}else {
				driver.executeScript(javaScript);
			}
			status.setPassed(true);
		}catch(StaleElementReferenceException e0) {
			throw e0;
		}catch(Exception e1) {
			status.setException(ActionStatus.JAVASCRIPT_ERROR, e1);
		}
		return null;
	}

	@Override
	public Object executeJavaScript(ActionStatus status, String javaScript, TestElement element) {
		return executeJavaScript(status, javaScript, element.getWebElement());
	}

	public Object executeJavaScript(ActionStatus status, String javaScript, WebElement element) {
		final Object result = runJavaScript(status, "var e=arguments[0];var result=e." + javaScript.replaceAll("this", "e") + ";", element);
		if(status.isPassed() && result != null) {
			status.setMessage(result.toString());
		}
		return result;
	}

	//TODO remove this default method and add actionstatus
	public Object runJavaScript(String javaScript, Object ... params) {
		return runJavaScript(channel.newActionStatus(), javaScript, params);
	}

	public Object runJavaScriptResult(String javaScript) {
		try {
			return driver.executeAsyncScript("var result=" + javaScript + ";arguments[arguments.length-1](result);");
		}catch(Exception e) {
			return e.getMessage();
		}
	}

	protected Object runJavaScript(ActionStatus status, String javaScript, Object ... params) {
		status.setPassed(true);
		try {
			return driver.executeAsyncScript(javaScript + ";arguments[arguments.length-1](result);", params);
		}catch(StaleElementReferenceException e0) {
			status.setPassed(false);
			throw e0;
		}catch(JavascriptException e1) {
			if(!e1.getMessage().contains("document unloaded while waiting for result")) {
				status.setException(ActionStatus.JAVASCRIPT_ERROR, e1);
			}
		}catch(Exception e2) {
			status.setException(ActionStatus.JAVASCRIPT_ERROR, e2);
		}
		return null;
	}

	public List<Double> getBoundingClientRect(RemoteWebElement element) {
		try {
			return (List<Double>) driver.executeAsyncScript("var callback=arguments[arguments.length-1], rect=arguments[0].getBoundingClientRect();var result=[rect.x+0.0001, rect.y+0.0001, rect.width+0.0001, rect.height+0.0001];callback(result);", element);
		}catch (Exception e) {
			return List.of(0D, 0D, 1D, 1D);
		}
	}

	private String getTagName(WebElement element) {
		try {
			return element.getTagName();
		}catch(UnreachableBrowserException e) {
			channel.sleep(200);
		}

		return null;
	}

	public String getElementTag(WebElement element) {

		int maxTry = 10;
		String tagName = getTagName(element);

		while(tagName == null && maxTry > 0) {
			tagName = getTagName(element);
			maxTry--;
		}

		return tagName;
	}

	@Override
	public String getColName(WebElement element) {

		final String tagName = getElementTag(element);

		if(AtsElement.TD.equalsIgnoreCase(tagName)) {
			WebElement parent = element.findElement(By.xpath("./.."));

			int index = 0;
			final List<WebElement> nestedElements = parent.findElements(By.tagName(AtsElement.TD));
			for(WebElement td : nestedElements) {
				if(td.equals(element)) {
					break;
				}
				index++;
			}

			while (parent != null) {
				if(AtsElement.TABLE.equalsIgnoreCase(parent.getTagName())) {
					List<WebElement> list = parent.findElements(By.tagName(AtsElement.TH));
					if(list.size() > index) {
						final WebElement header = list.get(index);
						return header.getDomProperty("innerText");
					}
					break;
				}
				parent = parent.findElement(By.xpath("./.."));
			}
		}
		return null;
	}

	//---------------------------------------------------------------------------------------------------------------------
	//
	//---------------------------------------------------------------------------------------------------------------------

	@Override
	public void goToUrl(ActionStatus status, String url) {

		channel.waitBeforeGotoUrl(this);

		if(ActionGotoUrl.REFRESH.equals(url)) {
			driver.navigate().refresh();
		}else if(ActionGotoUrl.NEXT.equals(url)) {
			driver.navigate().forward();
		}else if(ActionGotoUrl.BACK.equals(url)) {
			driver.navigate().back();
		}else {
			switchToDefaultContent(false);
			if(!url.contains("://") && !url.startsWith("https://") && !url.startsWith("http://") && !url.startsWith("file://") ) {
				url = "http://" + url;
			}
			loadUrl(status, url);
		}

		status.setPassed(true);
		status.setData(url);

		actionWait();
	}

	protected void loadUrl(ActionStatus status, String url) {
		if(driver == null) {
			status.setError(ActionStatus.UNREACHABLE_GOTO_URL, "driver is null ! (channel may be closed)");
		}else {
			driver.get(url);
		}
	}

	private ArrayList<String> iframesDom;
	private WebElement iframe = null;
	private double offsetIframeX = 0.0;
	private double offsetIframeY = 0.0;

	@Override
	public List<FoundElement> findElements(
			boolean sysComp,
			TestElement testObject,
			String tagName,
			String[] attributes,
			String[] attributesValues,
			Predicate<AtsBaseElement> predicate,
			WebElement startElement) {

		String script = searchElementScript;
		iframesDom = new ArrayList<String>();

		if(testObject.getParent() != null){
			if(testObject.getParent().isIframe()) {

				iframe = testObject.getParent().getWebElement();

				if(isAtsLearningEnabled()) {
					iframesDom.add((String) runJavaScript(JS_ELEMENT_PARENTS_HTML, iframe));
				}

				try {

					final org.openqa.selenium.Point pt = iframe.getLocation();

					offsetIframeX += pt.getX();
					offsetIframeY += pt.getY();

					switchToFrame(iframe);

				}catch(WebDriverException e) {
					return Collections.<FoundElement>emptyList();
				}
			}else if(testObject.getParent().isShadowRoot()) {

				script = JS_SEARCH_SHADOW_ELEMENT;
				startElement = testObject.getParent().getWebElement();

			}else if(startElement == null) {
				startElement = testObject.getParent().getWebElement();
			}

		}else {
			if(iframe != null) {
				iframe = null;
				offsetIframeX = 0.0;
				offsetIframeY = 0.0;
			}

			if(!switchToDefaultContent(false)) {
				return Collections.<FoundElement>emptyList();
			}
		}

		channel.waitBeforeSearchElement(this);

		return listElementsFound(runJavaScript(script, startElement, tagName, attributes, attributes.length), predicate, Arrays.asList(attributes).contains(TestElement.ATS_COL_NAME));
	}

	private List<FoundElement> listElementsFound(Object maps, Predicate<AtsBaseElement> predicate, boolean colName){
		final List<List<Object>> objects = (List<List<Object>>) maps;
		if(objects != null && objects.size() > 0){

			final List<AtsElement> elements = objects.parallelStream().filter(Objects::nonNull).map(e -> new AtsElement(this, e, colName)).collect(Collectors.toCollection(ArrayList::new));

			final Stream<FoundElement> st = elements.parallelStream().
					filter(predicate).
					map(e -> new FoundElement(this, e, channel, initElementX + offsetIframeX, initElementY + offsetIframeY, false));

			return st.collect(Collectors.toCollection(ArrayList::new));
		}
		return Collections.<FoundElement>emptyList();
	}

	@Override
	public void middleClick(ActionStatus status, MouseDirection position, TestElement element) {
		runJavaScript(status, JS_MIDDLE_CLICK, element.getWebElement());
	}

	protected void middleClickSimulation(ActionStatus status, MouseDirection position, TestElement element) {
		element.click(status, position, Keys.CONTROL);
	}

	@Override
	public DialogBox switchToAlert() {
		channel.sleep(500);
		return new DialogBox(driver.switchTo().alert());
	}

	@Override
	public void clearText(ActionStatus status, TestElement te, MouseDirection md) {

		if(!te.isPreElement()){
			te.click(status, md);
		}

		if(status.isPassed()) {
			final FoundElement element = te.getFoundElement();

			try {
				executeScript(status, "arguments[0].value='';", element.getValue());
				status.setMessage("");
				return;
			}catch (StaleElementReferenceException e) {}

			try {
				element.getValue().clear();
				status.setMessage("");
				return;
			}catch(Exception e) {}

		}

		status.setError(ActionStatus.ENTER_TEXT_FAIL, "clear text failed on this element");
	}

	@Override
	public void sendTextData(ActionStatus status, TestElement element, ArrayList<SendKeyData> textActionList, int waitChar, ActionTestScript topScript) {

		channel.waitBeforeEnterText(this);

		final WebElement we = element.getWebElement();

		if(element.isPreElement()){
			final StringBuilder seq = new StringBuilder();
			for(SendKeyData sequence : textActionList) {
				seq.append(getSequenceData(sequence));
			}
			executeScript(status, "arguments[0].textContent='" + seq.toString() + "';", we);

		}else if(element.isMuiDateTimeInput()) {

			final StringBuilder seq = new StringBuilder();
			for(SendKeyData sequence : textActionList) {
				seq.append(getCharSequenceData(sequence));
			}

			driver.executeScript("navigator.clipboard.writeText(arguments[0]);", seq.toString());
			driver.executeScript("arguments[0].click();", we);
			driver.executeScript("arguments[0].focus();", we);
			channel.sleep(200);
			we.sendKeys(Keys.CONTROL+ "v");

		}else {

			//executeScript(status, "result={size:window.getComputedStyle(arguments[0], null).getPropertyValue('font-size'), family:window.getComputedStyle(arguments[0], null).getPropertyValue('font-family'), weight:window.getComputedStyle(arguments[0], null).getPropertyValue('font-weight')};", we);

			if(element.isInputDate()) {

				if(textActionList.size() > 0) {

					String dateOrder = System.getProperty("ats.date.order");
					if(dateOrder == null) {
						dateOrder = topScript.getDateOrder();
					}

					final DateTransformer dtr = new DateTransformer();
					dtr.setDateOrder(dateOrder);
					final String localDate = dtr.format(textActionList.get(0).getData());

					if(localDate != null && dtr.getError() == null) {
						final Object verif = executeJavaScript(status, "value='" + localDate + "'", we);
						if(verif != null && verif.toString().equals(localDate)) {
							return;	
						}
					}
				}
			}

			try {

				if(waitChar > 0) {
					String value = "";
					for(SendKeyData sequence : textActionList) {
						if(sequence.getDownKey() != null) {
							we.sendKeys(sequence.getDownKey(), sequence.getData());
						}else {
							if(!sequence.getData().isEmpty()) {

								value += getCharSequenceData(sequence);

								value = value.replace("\\\\", "\\");
								executeJavaScript(status, "value=\"" + value.replace("\\", "\"+String.fromCharCode(92)+\"") + "\"", we);

								channel.sleep(waitChar);

							}else if(sequence.getSpecialKey() != null) {
								we.sendKeys(sequence.getSpecialKey());
							}
						}
					}
				}else {
					for(SendKeyData sequence : textActionList) {

						if(sequence.getSpecialKeyString() != null){
							channel.sleep(20);
							channel.updateVisualAction(false);
						}

						we.sendKeys(getSequenceData(sequence));
					}
				}

			}catch(StaleElementReferenceException e) {
				throw new SendTextException();
			}
		}
	}

	protected CharSequence getCharSequenceData(SendKeyData seq) {
		return seq.getSequenceWeb(false);
	}

	protected CharSequence getSequenceData(SendKeyData seq) {
		return seq.getSequenceWeb(true);
	}

	@Override
	public void refreshElementMapLocation() {
		getSystemDriver().refreshElementMapLocation(channel);
	}

	@Override
	public String getSource() {
		return driver.getPageSource();
	}

	@Override
	public void api(ActionStatus status, ActionApi api) {}

	@Override
	public void buttonClick(ActionStatus status, String id) {}

	@Override
	public void tap(int count, FoundElement element) {}

	@Override
	public void press(int duration, ArrayList<String> paths, FoundElement element) {}

	@Override
	public void windowState(ActionStatus status, Channel channel, String state) {
		if(ActionWindowState.MAXIMIZE.equals(state)) {

			final List<Double> screenSize = (List<Double>) runJavaScript(status, "result=[screen.width+0.0001, screen.height+0.0001];");
			setPosition(new Point(0, 0));
			setSize(new Dimension(screenSize.get(0).intValue(), screenSize.get(1).intValue()));

		}else {
			getSystemDriver().windowState(status, channel, state);
		}
	}

	@Override
	public int getNumWindows() {
		try {
			return getDriverWindowsList().size();
		}catch (WebDriverException e) {
			return 1;
		}
	}

	@Override
	public String getUrl() {
		return driver.getCurrentUrl();
	}

	@Override
	public Rectangle getBoundRect(TestElement element) {
		int maxTry = 10;
		org.openqa.selenium.Rectangle rect = null;
		while(rect == null && maxTry > 0) {
			try {
				rect = element.getWebElement().getRect();
				return new Rectangle(rect.x, rect.y, rect.width, rect.height);
			}catch(Exception e) {}

			maxTry--;
			channel.sleep(100);
		}
		return new Rectangle(0, 0, 1, 1);
	}

	@Override
	public byte[] getScreenshot(WebElement element, TestBound bound) {
		if(element == null) {
			return driver.getScreenshotAs(OutputType.BYTES);
		}
		return element.getScreenshotAs(OutputType.BYTES);
	}
}