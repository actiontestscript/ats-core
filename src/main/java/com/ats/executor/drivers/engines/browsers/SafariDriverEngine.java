/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.executor.drivers.engines.browsers;

import com.ats.driver.ApplicationProperties;
import com.ats.executor.ActionStatus;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.DriverManager;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.WebDriverEngine;
import com.ats.executor.drivers.engines.browsers.capabilities.SafariOptions;

public class SafariDriverEngine extends WebDriverEngine {

	public SafariDriverEngine(Channel channel, ActionStatus status, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, boolean enableLearning) {
		super(channel, driverInfo, systemDriver, props, enableLearning);

		launchDriver(status, 
				new SafariOptions(
				new BrowserArgumentsParser(
						driverInfo,
						channel.getArguments(),
						props,
						DriverManager.SAFARI_BROWSER,
						applicationPath,
						systemDriver),
				props,
				systemDriver));
	}
}