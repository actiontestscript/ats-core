/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines.browsers.capabilities;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsProxy;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.browsers.BrowserArgumentsParser;

public class FirefoxOptions extends org.openqa.selenium.firefox.FirefoxOptions {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, Object> options = new HashMap<>();

	public FirefoxOptions(BrowserArgumentsParser browserArguments, ApplicationProperties props, SystemDriver systemDriver, AtsProxy proxy) {
		super();

		if(proxy.enabled()) {
			setProxy(proxy.getValue());
		}else {
			addArguments("--proxy-auto-detect");
		}
		
		if(browserArguments.getBinaryPath() != null) {
			setBinary(browserArguments.getBinaryPath());
		}

		final String X11_ENABLED = System.getenv("X11_ENABLED");
		if("true".equalsIgnoreCase(X11_ENABLED)) {
			addArguments("--user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.3282.119 Safari/537.36");
		}else if(browserArguments.isHeadless() || !systemDriver.isInteractive()) {
			addArguments("-headless");
		}

		if(browserArguments.isIncognito()) {
			addArguments("-private");
		}

		if(browserArguments.getUserDataPath() != null) {
			final File dir = new File(browserArguments.getUserDataPath());
			if(!dir.exists()) {
				dir.mkdirs();
			}
			addArguments("-profile", browserArguments.getUserDataPath());
		}

		for(String opt : browserArguments.getMoreOptions()) {
			addArguments(opt);
		}

		if(browserArguments.getLang() != null) {
			addPreference("intl.locale.requested", browserArguments.getLang());
		}

		final Set<String> excludedOptions = new HashSet<>(Arrays.asList(props.getExcludedOptions()));

		super.asMap().forEach((k, v) -> {
			if(!excludedOptions.contains(k)) {
				options.put(k, v);
			}
		});
	}

	@Override
	public Map<String, Object> asMap() {
		return options;
	}
}