/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.executor.drivers.engines.desktop;

import com.ats.driver.ApplicationProperties;
import com.ats.executor.ActionStatus;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.SystemDriverEngine;

public class ExplorerDriverEngine extends SystemDriverEngine {

	private final static int DEFAULT_WAIT = 100;

	public ExplorerDriverEngine(
			Channel channel,
			ActionStatus status,
			IDriverInfo driverInfo,
			SystemDriver desktopDriver,
			ApplicationProperties props,
			boolean enableLearning) {

		super(channel, driverInfo, desktopDriver, props, DEFAULT_WAIT, enableLearning);

		int maxTry = 10;
		while(maxTry > 0) {

			window = desktopDriver.openExplorerWindow();

			if(window != null) {

				channel.setApplicationData(Channel.DESKTOP_EXPLORER, desktopDriver.getOsFullName(), window.getHandle(), window.getAppIcon());

				desktopDriver.moveWindow(channel, channel.getDimension().getPoint());
				desktopDriver.resizeWindow(channel, channel.getDimension().getSize());

				maxTry = 0;

			}else {
				channel.sleep(300);
				maxTry--;
			}
		}
	}

	@Override
	public void close() {
		getSystemDriver().closeExplorerWindow(window.getHandle());
	}

	@Override
	public void closeWindow(ActionStatus status) {
		super.closeWindow(status);
	}
}