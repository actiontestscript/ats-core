/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines.browsers.capabilities;

import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsProxy;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.browsers.BrowserArgumentsParser;

@SuppressWarnings("serial")
public class ChromeOptions extends ChromiumOptions {
	public ChromeOptions(BrowserArgumentsParser browserArgs, ApplicationProperties props, SystemDriver systemDriver, AtsProxy proxy) {
		super(browserArgs, props, "goog:chromeOptions", "--incognito", false, systemDriver, proxy);
	}
}