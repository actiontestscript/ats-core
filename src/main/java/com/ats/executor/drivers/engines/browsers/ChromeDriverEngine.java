/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines.browsers;

import com.ats.data.Dimension;
import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsProxy;
import com.ats.element.SearchedElement;
import com.ats.element.test.TestElementSystem;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.DriverManager;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.browsers.capabilities.ChromeOptions;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;

public class ChromeDriverEngine extends ChromiumBasedDriverEngine {

	public ChromeDriverEngine(Channel channel, ActionStatus status, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, AtsProxy proxy, boolean enableLearning) {
		super(channel, status, DriverManager.CHROME_BROWSER, driverInfo, systemDriver, props, enableLearning);

		launchDriver(
				status,
				new ChromeOptions(
						new BrowserArgumentsParser(
								driverInfo,
								channel.getArguments(),
								props,
								DriverManager.CHROME_BROWSER,
								applicationPath,
								systemDriver),
						props,
						systemDriver,
						proxy));
	}

	@Override
	public void started(ActionTestScript script, ActionStatus status) {
		if(!isHeadless() && channel.isDesktopDriverEnabled()) {
			try {
				final TestElementSystem closeInfobarButton =
						new TestElementSystem(script, channel, 1, p -> p == 1, new SearchedElement(
								new SearchedElement(
										new SearchedElement(new CalculatedValue(0), false, "syscomp",
												new CalculatedProperty[] {}), new CalculatedValue(0), false, "Group",
										new CalculatedProperty[] {new CalculatedProperty("ClassName", "InfoBarContainerView")}), new CalculatedValue(0), false, "Button",
								new CalculatedProperty[] {}));

				if(closeInfobarButton.getCount() == 1) {
					closeInfobarButton.executeScript(status, "Invoke()", false);
					channel.sleep(100);

					final Dimension dim = driver.getWindowSize();
					final Dimension dimPlus = driver.getWindowSize().update(1);

					driver.setWindowSize(dimPlus);
					driver.setWindowSize(dim);
				}
			}catch(Exception e) {}
		}
	}
}