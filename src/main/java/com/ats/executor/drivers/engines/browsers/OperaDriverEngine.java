/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers.engines.browsers;

import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsProxy;
import com.ats.element.FoundElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.DriverManager;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.WebDriverEngine;
import com.ats.executor.drivers.engines.browsers.capabilities.OperaOptions;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.objects.MouseDirectionData;
import com.ats.generator.variables.CalculatedValue;

public class OperaDriverEngine extends WebDriverEngine {

	public OperaDriverEngine(Channel channel, ActionStatus status, IDriverInfo driverInfo, SystemDriver systemDriver, ApplicationProperties props, AtsProxy proxy, boolean enableLearning) {
		super(channel, driverInfo, systemDriver, props, enableLearning);

		initElementX = 20.0;

		final OperaOptions options = new OperaOptions(
				new BrowserArgumentsParser(
						driverInfo,
						channel.getArguments(),
						props,
						DriverManager.OPERA_BROWSER,
						applicationPath,
						systemDriver),
				props,
				systemDriver,
				proxy);

		launchDriver(status, options);
	}

	private boolean isLink = false;

	@Override
	public void mouseClick(ActionStatus status, FoundElement element, MouseDirection position, int offsetX,	int offsetY) {
		isLink = element.getTag().toUpperCase().equals("A");
		super.mouseClick(status, element, position, offsetX, offsetY);
	}

	@Override
	protected void click(FoundElement element, double offsetX, double offsetY) {
		actions.moveToElement(element.getValue(), (int)offsetX, (int)offsetY).perform();
		actions.click(element.getValue()).perform();
		/*try {
			runJavaScript("arguments[0].click();", element.getValue());
		}catch(Exception e) {}*/
	}

	@Override
	protected double getCartesianOffset(double value, MouseDirectionData direction, Cartesian cart1, Cartesian cart2,	Cartesian cart3) {
		if(isLink && direction == null) {
			direction = new MouseDirectionData(Cartesian.TOP, new CalculatedValue("10"));
		}
		return super.getCartesianOffset(value, direction, cart1, cart2, cart3) + value/2;
	}

	@Override
	protected void closeWindows() {
		if(driver != null){
			try {
				driver.quit();
			}catch (Exception e) {}

			driver = null;
		}
	}
}