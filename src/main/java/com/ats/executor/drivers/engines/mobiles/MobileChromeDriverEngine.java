package com.ats.executor.drivers.engines.mobiles;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import com.ats.driver.ApplicationProperties;
import com.ats.driver.AtsRemoteWebDriver;
import com.ats.element.FoundElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.channels.Channel;
import com.ats.executor.channels.MobileChromeChannel;
import com.ats.executor.drivers.IDriverInfo;
import com.ats.executor.drivers.desktop.SystemDriver;
import com.ats.executor.drivers.engines.MobileDriverEngine;
import com.ats.executor.drivers.engines.WebDriverEngine;
import com.ats.executor.drivers.engines.browsers.BrowserArgumentsParser;
import com.ats.generator.ATS;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.CharStreams;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class MobileChromeDriverEngine extends WebDriverEngine {

	private String token;
	private final String userAgent;
	private final ObjectMapper jsonMapper = new ObjectMapper();
	private final OkHttpClient client;

	private final MobileDriverEngine mobileDriver;

	public MobileChromeDriverEngine(
			MobileChromeChannel channel,
			ActionStatus status,
			IDriverInfo driverInfo,
			SystemDriver systemDriver,
			MobileDriverEngine mobileDriver,
			ApplicationProperties props,
			boolean enableLearning) {

		super(channel,driverInfo, systemDriver, props, 200, 300, enableLearning);

		this.mobileDriver = mobileDriver;

		this.userAgent = "AtsMobileDriver/" + ATS.getAtsVersion() + "," + System.getProperty("user.name") + ",";
		this.client = new Builder().cache(null).connectTimeout(30, TimeUnit.SECONDS).writeTimeout(50, TimeUnit.SECONDS).readTimeout(50, TimeUnit.SECONDS).build();

		final String endPoint = props.getName().substring(Channel.MOBILE_CHROME.length() + 3);
		this.applicationPath = "http://" + endPoint;

		if (mobileDriver.chromeDriver == null || mobileDriver.chromeDriver.isEmpty()) {
			status.setError(ActionStatus.CHANNEL_START_ERROR, "chromedriver address not set to the driver");
			return;
		}

		if(mobileDriver.serialNumber == null || mobileDriver.serialNumber.isEmpty()) {
			status.setError(ActionStatus.CHANNEL_START_ERROR, "serialNumber not set to the driver");
			return;
		}

		// check driver version
		final String compatibleDriverVersion = getDriverVersion(mobileDriver.chromeVersion);
		if (compatibleDriverVersion == null || !compatibleDriverVersion.equals(mobileDriver.chromeDriverVersion)) {
			status.setError(ActionStatus.CHANNEL_START_ERROR, "This version of ChromeDriver (" + mobileDriver.chromeDriverVersion +") does not support this version of Chrome application (" + mobileDriver.chromeVersion + ")");
			return;
		}

		channel.setApplicationData(Channel.MOBILE_CHROME, "0", "\u00e0".getBytes(), "chrome-mobile");

		final BrowserArgumentsParser browserArguments = new BrowserArgumentsParser(driverInfo, channel.getArguments(), props, Channel.MOBILE_CHROME, applicationPath, systemDriver);

		final ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("androidPackage", "com.android.chrome");
		options.setExperimentalOption("androidDeviceSerial", mobileDriver.serialNumber);

		if (browserArguments.isHeadless()) {
			options.addArguments("--headless");
			options.addArguments("--disable-gpu");
		}

		if (browserArguments.isIncognito()) {
			options.addArguments("--incognito");
		}

		try {
			driver = new AtsRemoteWebDriver(new URI("http://" + mobileDriver.chromeDriver).toURL(), options);
		} catch (MalformedURLException | URISyntaxException e) {
			status.setError(ActionStatus.CHANNEL_START_ERROR, "remote driver url error -> " + mobileDriver.chromeDriver);
			return;
		}

		actions = new Actions(driver);
		try {
			driver.navigate().to("http://" + mobileDriver.statusPage);
		} catch (Exception ignored) {}

		refreshElementMapLocation();
	}

	@Override
	public synchronized void close() {
		closeWindows();
		final JsonNode jsonObject = executeRequest(MobileDriverEngine.DRIVER, MobileDriverEngine.STOP);
		if (jsonObject != null) {
			this.token = null;
		}
	}

	public JsonNode executeRequest(String type, String ... data) {
		return MobileDriverEngine.executeRequestToMobile(
				applicationPath + "/" + type,
				token,
				userAgent,
				client,
				jsonMapper,
				data);
	}

	private String getDriverVersion(String chromeVersion) {
		final String cleanVersion = chromeVersion.substring(0, chromeVersion.lastIndexOf('.'));

		final Request request = new Request.Builder().url("https://chromedriver.storage.googleapis.com/LATEST_RELEASE_" + cleanVersion).build();

		try {
			final Response response = client.newCall(request).execute();
			final String responseData = CharStreams.toString(new InputStreamReader(Objects.requireNonNull(response.body()).byteStream(), StandardCharsets.UTF_8));
			response.close();

			return responseData;
		} catch (IOException e) {
			return null;
		}
	}

	public MobileDriverEngine getMobileDesktopDriver() {
		return mobileDriver;
	}

	@Override
	public FoundElement getElementFromPoint(Boolean isSystemElement, Double x, Double y) {
		if (isSystemElement) {
			return mobileDriver.getElementFromPoint(true, x, y);
		} else {
			switchToDefaultContent(false);

			x -= channel.getSubDimension().getX();
			y -= channel.getSubDimension().getY();

			return loadElement(new ArrayList<>(), null, x, y, initElementX, initElementY);
		}
	}

	@Override
	public void loadParents(FoundElement hoverElement) {
		if (hoverElement.isMobile()) {
			mobileDriver.loadParents(hoverElement);
		} else {
			hoverElement.setParent(getTestElementParent(hoverElement));
		}
	}

	@Override
	public void mouseClick(ActionStatus status, FoundElement element, MouseDirection position, int offsetX, int offsetY) {
		if (element.isMobile()) {
			mobileDriver.mouseClick(status, element, position, offsetX, offsetY);
		} else {
			super.mouseClick(status, element, position, offsetX, offsetY);
		}
	}

	@Override
	public CalculatedProperty[] getAttributes(FoundElement element, boolean reload){
		if (element.isMobile()) {
			return mobileDriver.getAttributes(element, reload);
		} else {
			return getAttributes(getWebElement(element));
		}
	}
}