package com.ats.executor.drivers.engines.desktop;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.openqa.selenium.NoAlertPresentException;

import com.ats.element.DialogBox;
import com.ats.element.FoundElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.TestBound;
import com.ats.executor.drivers.engines.SystemDriverEngine;
import com.ats.generator.objects.MouseDirection;
import com.ats.generator.variables.CalculatedProperty;

public class DesktopAlert extends DialogBox {

	private FoundElement dialog;
	private SystemDriverEngine engine;

	public DesktopAlert(SystemDriverEngine engine, TestBound dimension) {
		super(engine, dimension);

		waitBox = 200;
		Long Pid = engine.getChannel().getProcessId();
		final List<FoundElement> elements = engine.getSystemDriver().getDialogBox(dimension, Pid);
		if(elements.size() > 0) {
			this.dialog = elements.get(0);
			this.engine = engine;
		}else {
			throw new NoAlertPresentException();
		}
	}

	@Override
	public void dismiss(ActionStatus status) {
		if(dialog != null) {

			FoundElement closebutton = null;

			List<FoundElement> buttons = dialog.getChildren().stream().filter(p -> p.getTag().equals("Button")).collect(Collectors.toList());
			if(buttons.size() > 1) {
				closebutton = searchButtonDismiss(buttons);
			}else {
				List<FoundElement> title = dialog.getChildren().stream().filter(p -> p.getTag().equals("TitleBar")).collect(Collectors.toList());
				if(title.size() > 0) {
					FoundElement close = title.get(0).getChildren().get(0);
					if("Button".equals(close.getTag())){
						closebutton = close;
					}
				}
			}

			if(closebutton != null) {
				engine.mouseMoveToElement(status, closebutton, new MouseDirection(), false, 0, 0);
				engine.getSystemDriver().mouseClick();
			}else {
				//TODO click on close button of the window
			}
		}
	}

	private FoundElement searchButtonDismiss(List<FoundElement> buttons) {
		if (buttons.size() > 1) {
			for (FoundElement elem : buttons) {
				Map<String, String> attrs = elem.getAttribute();
				if (attrs != null && attrs.size() > 0 && attrs.containsKey("Text")) {
					String attrTxt = attrs.get("Text").toLowerCase();
					if (attrTxt.equalsIgnoreCase("no") ||
						attrTxt.equalsIgnoreCase("cancel") ||
						attrTxt.equalsIgnoreCase("non") ||
						attrTxt.equalsIgnoreCase("annuler")) {
						return elem;
					}
				}
			}
		}
		return buttons.get(1);
	}

	@Override
	public void accept(ActionStatus status) {
		if(dialog != null) {
			List<FoundElement> buttons = dialog.getChildren().stream().filter(p -> p.getTag().equals("Button")).collect(Collectors.toList());
			if(buttons.size() > 0) {
				FoundElement AcceptButton = searchButtonAccept(buttons);
				engine.mouseMoveToElement(status, AcceptButton, new MouseDirection(), false, 0, 0);
				engine.getSystemDriver().mouseClick();
			}else {
				//TODO execute enter key ?
			}
		}
	}

	private FoundElement searchButtonAccept(List<FoundElement> buttons) {
		if (buttons.size() > 1) {
			for (FoundElement elem : buttons) {
				Map<String, String> attrs = elem.getAttribute();
				if (attrs != null && attrs.size() > 0 && attrs.containsKey("Text")) {
					String attrTxt = attrs.get("Text").toLowerCase();
					if (attrTxt == "yes" || attrTxt == "oui") {
						return elem;
					}
				}
			}
		}
		return buttons.get(0);
	}

	@Override
	public void defaultButton(ActionStatus status) {
		if(dialog != null) {
			engine.mouseMoveToElement(status, dialog, new MouseDirection(), false, 0, 0);
			//engine.sendTextData(status, dialog, textActionList);
		}
	}

	@Override
	public String getText() {
		if(dialog != null) {
			List<FoundElement> labels = dialog.getChildren().stream().filter(p -> p.getTag().equals("Text")).collect(Collectors.toList());
			if(labels.size() > 0) {
				final StringJoiner joiner = new StringJoiner("\n");
				labels.forEach(l -> joiner.add(engine.getAttribute(null, l, "Name", 1)));
				return joiner.toString();
			}
		}
		return "";
	}

	@Override
	public void sendKeys(String text) {
		if(dialog != null) {
			List<FoundElement> edits = dialog.getChildren().stream().filter(p -> p.getTag().equals("Edit")).collect(Collectors.toList());
			if(edits.size() > 0) {
				engine.getSystemDriver().executeScript(null, "SetValue(" + text + ")", edits.get(0));
			}
		}
	}

	@Override
	public String getTitle() {
		if(dialog != null) {
			List<FoundElement> title = dialog.getChildren().stream().filter(p -> p.getTag().equals("TitleBar")).collect(Collectors.toList());
			if(title.size() > 0) {
				return engine.getAttribute(null, title.get(0), "Value", 1);
			}
		}
		return "";
	}

	@Override
	public void clickButtonAtIndex(int index, ActionStatus status) {
		if(dialog != null) {
			List<FoundElement> buttons = dialog.getChildren().stream().filter(p -> p.getTag().equals("Button")).collect(Collectors.toList());
			if (buttons.size() > index) {
				engine.mouseMoveToElement(status, buttons.get(index), new MouseDirection(), false, 0, 0);
				engine.getSystemDriver().mouseClick();
			}
			else {
				status.setPassed(false);
				status.setMessage("Index not found, the maximum is " + (buttons.size()-1));
			}
		}
	}

	@Override
	public CalculatedProperty[] getAttributes() {
		return new CalculatedProperty[] {
				new CalculatedProperty("text", getText()),
				new CalculatedProperty("title", getTitle())};
	}
}