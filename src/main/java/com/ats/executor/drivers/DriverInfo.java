package com.ats.executor.drivers;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;

import com.ats.driver.AtsRemoteWebDriver;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.tools.Utils;
import com.ats.tools.logger.ExecutionLogger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonSyntaxException;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;

public class DriverInfo implements IDriverInfo {

	public static final String ATS_GRID_ENDPOINT = "ats-grid-endpoint";
	public static final String ATS_GRID_NODENAME = "ats-grid-nodename";
	public static final String ATS_GRID_USERNAME = "ats-grid-username";
	public static final String ATS_GRID_ENVIRONMENT = "ats-grid-environment";
	
	public final static String DRIVER_INFO = "driverInfo";
	public final static String SESSION_ID = "sessionId";

	private final static String SHUTDOWN = "/shutdown";
	private final static String STOP = "/stop";

	public final static String ATS_DRIVER_VERSION = "ats-driver-version";
	public final static String ATS_DRIVER_PORT = "ats-driver-port";
	public final static String ATS_AGENT_PORT = "ats-agent-port";
	public final static String ATS_AGENT_OUTPUT = "ats-agent-output";
	public final static String ATS_AGENT_WARNING = "ats-agent-warning";
	public final static String ATS_AGENT_ERROR = "ats-agent-error";
	public final static String ATS_AGENT_INFO = "ats-agent-info";
	//	public final static String ATS_AGENT_CONNECTED = "ats-agent-connected";

	protected String name;

	private String driverId;
	private URI driverServerUri;

	private String applicationPath;

	private String screenshotUrl;
	private String sessionId;
	private boolean headless;
	private boolean grid = false;

	private String uuid;

	protected String driverVersion = null;
	protected int port = -1;

	protected ActionTestScript script;
	
	/*
	protected Boolean agentIsConnected = false;
	protected Boolean agentIsConnectedUpdate = false;
	 */
	
	public DriverInfo(String name, ActionTestScript script) {
		this.name = name;
		this.script = script;
		this.uuid = UUID.randomUUID().toString();
	}

	public DriverInfo(ActionStatus status, String name, URI driverUri, ActionTestScript script) {
		this(name, script);
		this.driverServerUri = driverUri;

		if(driverUri == null) {
			status.setError(0, "driver url is null !");
		}else if(!isAlive()) {
			status.setError(0, "host is not reachable : " + driverUri.getHost() + ":" + driverUri.getPort());
		}else { // remote driver is listening
			final int port = getAgentPort();
			if(port > 0) {

				String hostName = "";
				try {
					hostName += InetAddress.getLocalHost().getHostName();
				}catch(Exception ex) {}

				WebSocket ws = HttpClient
						.newHttpClient()
						.newWebSocketBuilder()
						.buildAsync(URI.create("ws://" + driverUri.getHost() + ":" + port), new WebSocketClient(this))
						.join();

				final StringJoiner sj = new StringJoiner("\t");
				sj.add(System.getProperty("user.name"));
				sj.add(hostName);
				sj.add(uuid);

				ws.sendText(sj.toString(), true);
				/*
				setAgentIsConnectedUpdate(false);
				ws.sendText("isConnected", true);
				int maxTry = 10;
				while(!getAgentIsConnectedUpdate() && maxTry > 0) {
					maxTry--;
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {}
				}
				if(!getAgentIsConnectedUpdate() ){
					status.setError(0, "systemdriver is not ready : " + driverUri.getHost() + ":" + driverUri.getPort());
				}
				else if(!getAgentIsConnected()) {
					status.setError(0, "systemdriver is already used : " + driverUri.getHost() + ":" + driverUri.getPort());
				}
				else{
					status.setNoError();
				}
				 */
				status.setNoError();
			}else {
				status.setError(0, "systemdriver is not ready : " + driverUri.getHost() + ":" + driverUri.getPort());
			}
		}
	}

	public DriverInfo(String name, RemoteDriverInfo remoteDriver, ActionTestScript script) {
		this(name, script);
		this.driverId = remoteDriver.getId();
		this.driverServerUri = remoteDriver.getDriverUri();
		this.applicationPath = remoteDriver.getApplicationPath();
		setDriverVersion(remoteDriver.getDriverVersion());
	}
	
	public DriverInfo(String name, String uri, ActionTestScript script) {

		this.name = name;
		this.grid = true;
		this.script = script;
		this.headless = true;

		String driverUriString = uri;
		String driverPort = "4444";
				
		if(uri.contains(":")) {
			String[] data = uri.split(":");
			driverUriString = data[0];
			driverPort = data[1];
		}
		
		driverUriString = "http://" + driverUriString.replace("http://", "") + ":" + driverPort;
				
		try {
			setDriverServerUri(new URI(driverUriString));
		} catch (URISyntaxException e) {
			//driverServerUri = new URI(r;
		}
	}	
	
	@Override
	public boolean isGrid() {
		return grid;
	}

	@Override
	public AtsRemoteWebDriver getAtsRemoteWebDriver(Capabilities cap) throws MalformedURLException {
		
		AtsRemoteWebDriver rwd = null;
		if(grid) {
			rwd = new AtsRemoteWebDriver(driverServerUri.resolve("/wd/hub").toURL(), getCapabilities(cap));
			setSessionId(rwd.getSessionId().toString());
		}else {
			rwd = new AtsRemoteWebDriver(getDriverServerUri().toURL(), cap);
		}
		
		return rwd;
	}
	
	public Capabilities getCapabilities(Capabilities cap) {
					
		String userName = System.getProperty(ATS_GRID_USERNAME);
		if(userName == null) {
			userName = System.getenv("USERNAME");
		}
		
		String environment = System.getProperty(ATS_GRID_ENVIRONMENT);
		String nodeName = System.getProperty(ATS_GRID_NODENAME);
		if(nodeName == null) {
			nodeName = ATS_GRID_NODENAME;
		}

		final StringJoiner join = new StringJoiner(",");

		if (environment != null) {
			join.add(environment.toLowerCase());
		}

		if (userName != null) {
			join.add(userName);
		}
		
		Map<String, Object> capabilities = new HashMap<String, Object>(5);
		capabilities.put("se:name", script.getTopScriptName());
		capabilities.put("se:tags", join.toString());
		capabilities.put("se:recordVideo", true);
		capabilities.put("se:screenResolution", "1920x1920");
		capabilities.put("cga:nodeName", nodeName);
		
		if(DriverManager.FIREFOX_BROWSER.equals(name)) {
			FirefoxOptions options = new FirefoxOptions();
			for (Map.Entry<String, Object> entry : capabilities.entrySet()) {
				options.setCapability(entry.getKey(), entry.getValue());
			}
			return cap.merge(options);
			
		}else if(DriverManager.MSEDGE_BROWSER.equals(name)) {
			EdgeOptions options = new EdgeOptions();
			for (Map.Entry<String, Object> entry : capabilities.entrySet()) {
				options.setCapability(entry.getKey(), entry.getValue());
			}
			return cap.merge(options);
			
		}else if(DriverManager.IE_BROWSER.equals(name)) {
			InternetExplorerOptions options = new InternetExplorerOptions();
			for (Map.Entry<String, Object> entry : capabilities.entrySet()) {
				options.setCapability(entry.getKey(), entry.getValue());
			}
			return cap.merge(options);
			
		}else {
			ChromeOptions options = new ChromeOptions();
			for (Map.Entry<String, Object> entry : capabilities.entrySet()) {
				options.setCapability(entry.getKey(), entry.getValue());
			}
			return cap.merge(options);
		}
	}

	private static class WebSocketClient implements WebSocket.Listener {

		private DriverInfo driverInfo;

		public WebSocketClient(DriverInfo driverInfo) {
			this.driverInfo = driverInfo;
		}

		@Override
		public void onOpen(WebSocket webSocket) {
			WebSocket.Listener.super.onOpen(webSocket);
		}

		@Override
		public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
			driverInfo.output(data.toString());
			return WebSocket.Listener.super.onText(webSocket, data, last);
		}

		@Override
		public void onError(WebSocket webSocket, Throwable error) {
			System.out.println("WsAgent error -> " + webSocket.toString());
			WebSocket.Listener.super.onError(webSocket, error);
		}
	}

	public void output(String line) {

		final String[] logs = line.split("\\|");

		if(logs.length > 1) {

			final String logType = logs[0];

			if(ATS_AGENT_INFO.equals(logType)) {
				script.getLogger().sendDriverLog(logs[1]);
			}else if(ATS_AGENT_WARNING.equals(logType)) {
				script.getLogger().sendDriverWarning(logs[1]);
			}else if(ATS_AGENT_PORT.equals(logType)) {
				// do nothing
			}else if(ATS_DRIVER_PORT.equals(logType)) {
				port = Utils.string2Int(logs[1], -1);
			}else if(ATS_AGENT_ERROR.equals(logType)) {
				script.getLogger().sendDriverError(logs[1]);
			}else if(ATS_AGENT_OUTPUT.equals(logType)) {
				script.getLogger().sendDriverOutput(logs[1]);
			}else if(ATS_DRIVER_VERSION.equals(logType)) {
				setDriverVersion(logs[1]);
			}
			/*
			else if(ATS_AGENT_CONNECTED.equals(logType)) {
				boolean isConnected = Boolean.parseBoolean(logs[1]);
				setAgentIsConnected(isConnected);
				setAgentIsConnectedUpdate(true);
			}
			 */
			else {
				script.getLogger().sendDriverOutput(logs[1]);
			}
		}else {
			script.getLogger().sendDriverOutput(logs[0]);
		}
	}

	@Override
	public void sendLogsInfo(ExecutionLogger logger) {
		script.getLogger().sendInfo("remote-driver version", name + ": " + driverVersion);
	}

	//-------------------------------------------------------------------------------------------------------
	// send commands
	//-------------------------------------------------------------------------------------------------------

	protected void sendShutdown() {
		sendCommand(SHUTDOWN);
	}

	protected void sendStop() {
		sendCommand(STOP);
	}

	private void sendCommand(String command) {
		final OkHttpClient client = new Builder().cache(null).connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS).build();

		final Request.Builder requestBuilder = new Request.Builder();
		requestBuilder.url(getDriverServerUri().resolve(command).toString());

		final Request request = requestBuilder
				.addHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF8")
				.get()
				.build();

		try {
			final Response resp = client.newCall(request).execute();

			@SuppressWarnings("unused")
			final int responseCode = resp.code();

			resp.close();
		} catch (JsonSyntaxException | IOException e) {}
	}

	//-------------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------

	protected void setDriverVersion(String value) {
		if(this.driverVersion == null) {
			final String[] version = value.split(".");
			if(version.length > 2) {
				final StringJoiner sj = new StringJoiner(".");
				sj.add(version[0]).add(version[1]).add(version[2]);
				this.driverVersion = sj.toString();
			}else {
				this.driverVersion = value;
			}
			this.sendLogsInfo(script.getLogger());
		}
	}
	/*
	public void setAgentIsConnected(boolean value) {
		this.agentIsConnected = value;
	}

	public boolean getAgentIsConnected() {
		return agentIsConnected;
	}

	public void setAgentIsConnectedUpdate(boolean value) {
		this.agentIsConnectedUpdate = value;
	}

	public boolean getAgentIsConnectedUpdate() {
		return agentIsConnectedUpdate;
	}
	 */

	@Override
	public boolean isHeadless() {
		return headless;
	}

	@Override
	public void setHeadless(boolean value) {
		if(!grid) {
			this.headless = value;
		}
	}

	@Override
	public StringBuilder getDriverSessionUrl() {
		return getDriverHostAndPort().append("/session/").append(sessionId).append("/");
	}

	@Override
	public void setSessionId(String id) {
		sessionId = id;
		screenshotUrl = getDriverSessionUrl().append("screenshot").toString();
	}

	@Override
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public void setUdpInfo(String info) {
		screenshotUrl = info;
	}

	@Override
	public String getScreenshotUrl() {
		return screenshotUrl;
	}

	@Override
	public boolean isAlive() {

		try {
			final HttpURLConnection urlConn = (HttpURLConnection) getDriverServerUri().toURL().openConnection();
			urlConn.connect();

			final int response = urlConn.getResponseCode();
			urlConn.disconnect();

			return HttpURLConnection.HTTP_OK == response || HttpURLConnection.HTTP_CREATED == response;

		} catch (IOException e) {
			return false;
		}
	}

	private int getAgentPort() {

		int result = -1;
		HttpURLConnection urlConn = null;
		InputStream is = null;

		try {
			urlConn = (HttpURLConnection) getDriverServerUri().resolve("status").toURL().openConnection();
			urlConn.setRequestMethod("GET");
			urlConn.setRequestProperty("User-Agent", "ATS-EXECUTOR");

			int responseCode = urlConn.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				final ObjectMapper mapper = new ObjectMapper();
				is = urlConn.getInputStream();
				final JsonNode status = mapper.readTree(is);

				if(status.has("value")) {
					final JsonNode statusValue = status.get("value");
					if(statusValue.get("ready").asBoolean()) {
						if(statusValue.has("remoteAgent")) {
							final JsonNode agent = statusValue.get("remoteAgent");{
								result = agent.get("port").asInt();
							}
						}
					}
				}
			}

		} catch (IOException e) {

		}finally {
			if(is != null) {
				try {
					is.close();
				} catch (IOException e) {}
			}

			if(urlConn != null) {
				urlConn.disconnect();
			}
		}

		return result;
	}

	@Override
	public StringBuilder getDriverHostAndPort() {
		return new StringBuilder("http://").append(getDriverServerUri().getHost()).append(":").append(getDriverServerUri().getPort());
	}

	@Override
	public JsonNode toJson() {
		final ObjectNode data = new ObjectMapper().createObjectNode();
		data.put(SESSION_ID, sessionId);
		data.put("screenshotUrl", screenshotUrl);
		data.put("headless", headless);

		final ObjectNode node = new ObjectMapper().createObjectNode();
		node.set(DRIVER_INFO, data);

		return node;
	}

	@Override
	public String getId() {
		return driverId;
	}

	@Override
	public String getUuid() {
		return uuid;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setDriverServerUri(URI value) {
		this.driverServerUri = value;
	}

	@Override
	public URI getDriverServerUri(){
		if(driverServerUri == null) {
			try {
				return new URI("http://localhost:80/");
			} catch (URISyntaxException e) {}
		}
		return driverServerUri;
	}

	@Override
	public String getApplicationPath() {
		return applicationPath;
	}

	@Override
	public URI getDriverLoopback() {
		return null;
	}

	@Override
	public void close() {
		sendStop();
	}

	@Override
	public void quit() {
	}

	@Override
	public String getDriverVersion() {
		return driverVersion;
	}
}