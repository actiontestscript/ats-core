package com.ats.executor.drivers;

import java.net.MalformedURLException;
import java.net.URI;

import org.openqa.selenium.Capabilities;

import com.ats.driver.AtsRemoteWebDriver;
import com.ats.tools.logger.ExecutionLogger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class DriverInfoMobile implements IDriverInfo {

	protected String name;

	public DriverInfoMobile(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getId() {
		return "";
	}

	@Override
	public String getDriverVersion() {
		return "";
	}

	@Override
	public void setSessionId(String string) {
	}

	@Override
	public String getSessionId() { return ""; }

	@Override
	public StringBuilder getDriverSessionUrl() {
		return new StringBuilder();
	}

	@Override
	public URI getDriverServerUri() {
		return null;
	}

	@Override
	public URI getDriverLoopback() {
		return null;
	}

	@Override
	public String getApplicationPath() {
		return "";
	}

	@Override
	public StringBuilder getDriverHostAndPort() {
		return new StringBuilder();
	}

	@Override
	public boolean isAlive() {
		return true;
	}

	@Override
	public void close() {
	}

	@Override
	public void quit() {
	}

	@Override
	public void setUdpInfo(String info) {
	}

	@Override
	public String getScreenshotUrl() {
		return "";
	}

	@Override
	public void setHeadless(boolean value) {
	}

	@Override
	public boolean isHeadless() {
		return false;
	}

	@Override
	public JsonNode toJson() {
		final ObjectNode data = new ObjectMapper().createObjectNode();
		data.put(DriverInfo.SESSION_ID, "");
		data.put("name", name);
		data.put("screenshotUrl", "");
		data.put("headless", false);

		final ObjectNode node = new ObjectMapper().createObjectNode();
		node.set(DriverInfo.DRIVER_INFO, data);

		return node;
	}

	@Override
	public void sendLogsInfo(ExecutionLogger logger) {
	}

	@Override
	public String getUuid() {
		return "";
	}

	@Override
	public AtsRemoteWebDriver getAtsRemoteWebDriver(Capabilities cap) throws MalformedURLException {
		return null; // not used for grid
	}

	@Override
	public boolean isGrid() {
		return false; // not used for grid
	}
}