/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.drivers;

import java.awt.GraphicsEnvironment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.Channels;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ats.AtsSingleton;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.executor.gobblers.StreamGobblerError;
import com.ats.executor.gobblers.StreamGobblerInput;
import com.ats.generator.ATS;
import com.ats.tools.OperatingSystem;
import com.ats.tools.ReadableConsumerByteChannel;
import com.ats.tools.Utils;
import com.ats.tools.logger.ExecutionLogger;

public class DriverInfoLocal extends DriverInfo {

	private static final List<String> WEBDRIVER_NOTERROR_LIST =
			Collections.unmodifiableList(
					List.of(
							"CreatePlatformSocket() failed:",
							"bind() failed:",
							"Error: no DISPLAY environment variable specified",
							"*** You are running in headless mode.",
							"WebDriver BiDi listening on ws",
							"DevTools listening on ws",
							"JavaScript error:",
							"Dynamically enable window",
							"Unable to receive message from renderer",
							"WARNING"));

	private static final String WINDOWS_DRIVER_FILE_NAME = "windowsdriver.exe";
	private static final String LINUX_DRIVER_FILE_NAME = "linuxdriver";
	private static final String MACOS_DRIVER_FILE_NAME = "macosdriver";
	
	private static final String X11_ENABLED = System.getenv("X11_ENABLED");
	
	private final static Set<PosixFilePermission> EXEC_PERMISSIONS =
			Stream.of(
					PosixFilePermission.OWNER_READ,
					PosixFilePermission.OWNER_WRITE,
					PosixFilePermission.OWNER_EXECUTE,
					PosixFilePermission.GROUP_EXECUTE,
					PosixFilePermission.OTHERS_EXECUTE).collect(Collectors.toSet());

	private Process process;
	private File driverFile;

	private final String atsSystemDriverUrl = ATS.getAtsServer() + "/releases/ats-drivers/" + OperatingSystem.getOSName() + "/system/";
	private final static Pattern DRIVER_VERSION_PATTERN = Pattern.compile(".* (.*)");

	public DriverInfoLocal(ActionStatus status, Path driverFolderPath, String logLevel, ActionTestScript script) {
		super(Channel.DESKTOP, script);

		String driverFileName = WINDOWS_DRIVER_FILE_NAME;
		if(OperatingSystem.isUnix()) {
			driverFileName = LINUX_DRIVER_FILE_NAME;
		}else if(OperatingSystem.isMac()) {
			driverFileName = MACOS_DRIVER_FILE_NAME;
		}

		this.driverFile = driverFolderPath.resolve(driverFileName).toFile();

		final String projectDriverVersion = getAtsSystemDriverVersion(atsSystemDriverUrl);

		final boolean outboundTrafficBlocked = "false".equals(System.getProperty("outbound-traffic"));

		if(!driverFile.exists()) {

			if(projectDriverVersion == null) {
				status.setError(ActionStatus.CHANNEL_START_ERROR, "unable to launch driver process, driver file is missing : " + driverFile.getAbsolutePath());
				return;
			}

			donwloadAndInstallSystemDriver(outboundTrafficBlocked, driverFolderPath, atsSystemDriverUrl + projectDriverVersion + "." + OperatingSystem.getArchiveExtension());
		}

		if(driverFile.exists()){
			
			if(OperatingSystem.isUnix()) {
				try {
					Files.setPosixFilePermissions(driverFile.toPath(), EXEC_PERMISSIONS);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			loadDriverVersion(driverFile);

			if(projectDriverVersion != null && !"LATEST".equals(projectDriverVersion) && !projectDriverVersion.equals(getDriverVersion())){
				donwloadAndInstallSystemDriver(outboundTrafficBlocked, driverFolderPath, atsSystemDriverUrl + projectDriverVersion + "." + OperatingSystem.getArchiveExtension());
			}

			ArrayList<String> arguments = new ArrayList<>();
			
			if("true".equalsIgnoreCase(X11_ENABLED)) {

				System.out.println("------------------------------");
				System.out.println("    X11_ENABLED detected !");
				if(GraphicsEnvironment.getLocalGraphicsEnvironment().isHeadlessInstance()) {
					//final TestBound bound = AtsSingleton.getInstance().getApplicationBound();
					final int w = 1600;//(int) (bound.getWidth() + 20);
					final int h = 1200;//(int) (bound.getHeight() + 20);
					
					arguments.add("xvfb-run");
					arguments.add("-s");
					arguments.add("-screen 0 " + w + "x" + h + "x24");
					
					System.out.println("   [screen size: " + w + "x" + h + "]");
				}
				System.out.println("------------------------------");
			}
						
			arguments.add(driverFile.getAbsolutePath());
			arguments.add("--local");

			final ProcessBuilder builder = new ProcessBuilder(arguments);
			builder.redirectInput(Redirect.PIPE);
			builder.redirectError(Redirect.PIPE);

			script.getLogger().sendScriptInfo("start system driver process ...");
			
			try {

				process = builder.start();

				final StreamGobblerError errorGobbler = new StreamGobblerError(process.getErrorStream(), this);
				final StreamGobblerInput intputGobbler = new StreamGobblerInput(process.getInputStream(), this);

				intputGobbler.start();
				errorGobbler.start();

			} catch (IOException e1) {
				status.setError(ActionStatus.CHANNEL_START_ERROR, e1.getMessage());
				return;
			}

		}else{
			status.setError(ActionStatus.CHANNEL_START_ERROR, "unable to launch driver process, driver file is missing : " + driverFile.getAbsolutePath());
			return;
		}

		int maxTry = 40;
		while(port < 0 && maxTry > 0) {

			maxTry--;

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}

		if(port < 0) {
			status.setError(ActionStatus.CHANNEL_START_ERROR, "unable to get valid port for this driver (" + name + ")");
			return;
		}

		try {
			setDriverServerUri(new URI("http://localhost:" + port));
		} catch (URISyntaxException e) {}

		
		status.setNoError();
		Runtime.getRuntime().addShutdownHook(new TerminateSystemDriverProcess(this, script.getLogger()));
	}
	
	public static class TerminateSystemDriverProcess extends Thread {

		private DriverInfoLocal driver;
		private ExecutionLogger logger;
		
		public TerminateSystemDriverProcess(DriverInfoLocal driver, ExecutionLogger logger) {
			this.driver = driver;
			this.logger = logger;
		}

		@Override
		public void run() {
			logger.sendScriptInfo("terminate system driver process");
			driver.close();
		}
	}

	@Override
	public boolean isAlive() {
		return process != null && process.isAlive();
	}

	@Override
	public StringBuilder getDriverHostAndPort() {
		return new StringBuilder("http://localhost:").append(port);
	}

	public void outputError(String line) {
		if(WEBDRIVER_NOTERROR_LIST.stream().parallel().anyMatch(line::contains)) {
			script.getLogger().sendDriverWarning(line);
		}else {
			script.getLogger().sendDriverError(line);
		}
	}

	@Override
	public void close() {
		super.close();
		quit();
	}

	@Override
	public void quit() {
		if(process != null && process.isAlive()) {
			shutdownDriver();// prepare driver to shutdown
			shutdownDriver();// finalize driver to shutdown
			if(process != null && process.isAlive()) {
				terminateProcess();
			}
		}
	}

	private void terminateProcess() {
		
		process.descendants().forEach(p -> p.destroy());
		process.destroy();

		try {
			process.waitFor(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		process.destroyForcibly();
		process = null;
	}


	//--------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------

	@Override
	public URI getDriverLoopback(){
		try {
			return new URI("http://127.0.0.1:" + port);
		} catch (URISyntaxException e) {
			return null;
		}
	}

	private void loadDriverVersion(File file) {
		try {
			final ProcessBuilder pb = new ProcessBuilder(new String[] {file.getAbsolutePath(), "--version"});
			final Process process = pb.start();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line = reader.readLine();
			if(line != null) {
				final Matcher m = DRIVER_VERSION_PATTERN.matcher(line);
				if(m.find()) {
					setDriverVersion(m.group(1));
				}
			}
			reader.close();
			process.destroyForcibly();

		} catch (IOException e) {}
	}

	//--------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------

	public void unExpectedExit() {
		AtsSingleton.getInstance().setSystemDriver(null);
	}

	public void shutdownDriver() {
		sendShutdown();
	}

	@Override
	public void sendLogsInfo(ExecutionLogger logger) {
		logger.sendInfo("ATS system driver version", getDriverVersion());
		logger.sendInfo("ATS drivers folder", driverFile.getParent());
	}

	//--------------------------------------------------------------------------------------------------
	// System driver functions
	//--------------------------------------------------------------------------------------------------

	private static String getAtsSystemDriverVersion(String sysDriverUrl) {

		final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		String version = null;

		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			Document doc = db.parse(Paths.get("pom.xml").toFile());
			doc.getDocumentElement().normalize();

			final NodeList project = doc.getElementsByTagName("project");
			if (project.getLength() > 0) {
				final NodeList projectItems = project.item(0).getChildNodes();
				for (int i=0; i < projectItems.getLength(); i++) {
					if("properties".equals(projectItems.item(i).getNodeName())) {
						final NodeList properties = projectItems.item(i).getChildNodes();
						for (int j=0; j < properties.getLength(); j++) {
							if("ats.driver.version".equals(properties.item(j).getNodeName())) {
								version = properties.item(j).getTextContent();
								break;
							}
						}
					}
				}
			}
		} catch (Exception e) {}

		if("LATEST".equals(version)){
			version = Utils.getArtifactLastVersion(sysDriverUrl);
		}

		return version;
	}

	private static void donwloadAndInstallSystemDriver(boolean trafficBlocked, Path driverFolderPath, String systemDriverUrl) {

		try {
			if(!trafficBlocked) {
				Path folder = Files.createTempDirectory("ats_download");
				final File tmpFile = folder.resolve("system-driver").toAbsolutePath().toFile();

				if (tmpFile.exists()) {
					tmpFile.delete();
				}

				final HttpURLConnection connection = (HttpURLConnection) new URI(systemDriverUrl).toURL().openConnection();
				final int fileLength = connection.getContentLength();

				ReadableConsumerByteChannel rcbc = new ReadableConsumerByteChannel(
						Channels.newChannel(connection.getInputStream()),
						fileLength,
						(p) -> {});

				final FileOutputStream fosx = new FileOutputStream(tmpFile);
				fosx.getChannel().transferFrom(rcbc, 0, Long.MAX_VALUE);
				fosx.close();

				if (tmpFile.exists() && tmpFile.length() > 10000) {
					if(OperatingSystem.isUnix()) {
						try {
							final ProcessBuilder pb = new ProcessBuilder(new String[] {"tar", "-xzf", tmpFile.getAbsolutePath(), "-C", driverFolderPath.toFile().getAbsolutePath()});
							pb.start().waitFor();
						}catch(Exception e) {}

					}else if (OperatingSystem.isWindows()){
						unzipArchive(tmpFile, driverFolderPath);
					}
				}

				folder.toFile().delete();
			}

		} catch (IOException | URISyntaxException e) {}
	}

	private static void unzipArchive(File archive, Path destination) {
		try {
			unzipFolder(archive.toPath(), destination);
		} catch (IOException e) {
			e.printStackTrace();
		}
		archive.delete();
	}

	private static void unzipFolder(Path source, Path target) throws IOException {

		try (ZipInputStream zis = new ZipInputStream(new FileInputStream(source.toFile()))) {

			ZipEntry zipEntry = zis.getNextEntry();

			while (zipEntry != null) {

				boolean isDirectory = false;

				if (zipEntry.getName().endsWith(File.separator) || zipEntry.isDirectory()) {
					isDirectory = true;
				}

				Path newPath = zipSlipProtect(zipEntry, target);

				if (isDirectory) {
					Files.createDirectories(newPath);
				} else {

					if (newPath.getParent() != null) {
						if (Files.notExists(newPath.getParent())) {
							Files.createDirectories(newPath.getParent());
						}
					}
					Files.copy(zis, newPath, StandardCopyOption.REPLACE_EXISTING);
				}
				zipEntry = zis.getNextEntry();
			}
			zis.closeEntry();
		}
	}

	private static Path zipSlipProtect(ZipEntry zipEntry, Path targetDir) throws IOException {
		Path targetDirResolved = targetDir.resolve(zipEntry.getName());
		Path normalizePath = targetDirResolved.normalize();
		if (!normalizePath.startsWith(targetDir)) {
			throw new IOException("Bad zip entry: " + zipEntry.getName());
		}
		return normalizePath;
	}
}