package com.ats.executor.scripts;

public class AtsPythonReturn {
	
	private String[] logs;
	private String[] values;
	
	public AtsPythonReturn() {}

	public String[] getLogs() {
		return logs;
	}

	public void setLogs(String[] logs) {
		this.logs = logs;
	}

	public String[] getValues() {
		return values;
	}

	public void setValues(String[] values) {
		this.values = values;
	}
	

}