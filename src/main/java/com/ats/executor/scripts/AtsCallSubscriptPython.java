/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.scripts;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Context.Builder;
import org.graalvm.polyglot.Value;
import org.testng.annotations.Test;

import com.ats.AtsSingleton;
import com.ats.script.actions.ActionComment;

public class AtsCallSubscriptPython extends AtsCallSubscriptScript {

	public static final String PYTHON_LANGUAGE = "python";
	
	private static final String PY_CONSOLE_PREFIX = "ats-py-console: ";

	private final static String atsCode = "class ats_result:\r\n"
			+ "\tlogs=[]\r\n"
			+ "\tvalues=[]\r\n"
			+ "\tcomments=[]\r\n"
			+ "def ats_log(data):\r\n"
			+ "\tats_result.logs.append(data)\r\n"
			+ "def ats_comment(data):\r\n"
			+ "\tats_result.comments.append(data)\r\n"
			+ "def ats_return(data):\r\n"
			+ "\tats_result.values=data\r\n\r\n";

	@Test
	public void testMain(){
		
		List<Object> returnValues = new ArrayList<Object>();
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		final StringBuilder pyCode = 
				new StringBuilder(atsCode).
				append("def ats_parameter(index):\r\n\t").
				append(getParametersCode()).
				append("\r\n\tif(index<len(p)):\r\n\t\treturn p[index]\r\n\telse:\r\n\t\treturn ''\r\n\r\n").
				append(ATS_ITERATION).append("=").append(getIteration()).append("\r\n").
				append(ATS_ITERATIONS_COUNT).append("=").append(getIterationsCount()).append("\r\n").
				append(code).
				append("\r\n");

		try {
			
			final Builder builder = AtsSingleton.getInstance().getPythonBuilder();
			Context ctx = builder.out(out).build();
			
			//ctx.getBindings(PYTHON_LANGUAGE).putMember(ATS_ITERATION, getIteration());
			//ctx.getBindings(PYTHON_LANGUAGE).putMember(ATS_ITERATIONS_COUNT, getIterationsCount());

			ctx.eval(PYTHON_LANGUAGE, pyCode.toString());
			Value binding = ctx.getBindings(PYTHON_LANGUAGE);
									
			Value atsResult = binding.getMember("ats_result");
			Value atsReturnValues = atsResult.getMember("values");
			Value atsLogs = atsResult.getMember("logs");
			Value atsComments = atsResult.getMember("comments");
						
			if(atsReturnValues.isString() || atsReturnValues.isBoolean() || atsReturnValues.isNumber()) {
				returnValues = Arrays.asList(atsReturnValues.as(Object.class));
			}else {
				returnValues = new ArrayList<Object>(Arrays.asList(atsReturnValues.as(Object[].class)));
			}
			
			String[] logs = atsLogs.as(String[].class);
			for(int i=0; i<logs.length; i++) {
				final String log = logs[i];
				System.out.println(PY_CONSOLE_PREFIX + log);
				exec(0,new ActionComment(this, ActionComment.LOG_TYPE, clv(log)));
			}
			
			String[] comments = atsComments.as(String[].class);
			for(int i=0; i<comments.length; i++) {
				exec(0,new ActionComment(this, ActionComment.STEP_TYPE, clv(comments[i])));
			}

			System.out.println(out.toString());
			
			ctx.close();
			
		}catch(Exception e) {
			throw new AtsCallSubscriptException(e.getMessage());
		}

		returnValues(returnValues.toArray());
	}
}