/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.executor.results;

import java.util.ArrayList;
import java.util.List;

public class SuiteResultCollection {

	private List<SuiteResult> suites;
	private int suitesPassed = 0;
	private int suitesFailed = 0;
	private int suitesCount = 0;
	private int testsCount = 0;
	
	private long duration = 0;
	private long started = System.currentTimeMillis();
	
	private String status = SuiteResult.PASS;

	public void addSuiteResult(String name, int testsCount, int passed, int failed, int filtered, boolean nativeSuite, long started) {
		
		if(this.suites == null) {
			this.suites = new ArrayList<SuiteResult>();
		}
		
		final SuiteResult suite = new SuiteResult(name, failed, passed, filtered, nativeSuite, started);
		
		this.suites.add(suite);
		this.suites.sort((a, b) -> Boolean.compare(a.isNativeSuite(), b.isNativeSuite()));
		
		if(suite.isPassed()) {
			this.suitesPassed += 1;
		}else {
			this.status = SuiteResult.FAIL;
			this.suitesFailed += 1;
		}
		
		this.suitesCount += 1;
		this.testsCount += testsCount;
		this.duration += suite.getDuration();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SuiteResult> getSuites() {
		return suites;
	}

	public void setSuites(List<SuiteResult> suites) {
		this.suites = suites;
	}

	public int getSuitesPassed() {
		return suitesPassed;
	}

	public void setSuitesPassed(int suitesPassed) {
		this.suitesPassed = suitesPassed;
	}

	public int getSuitesFailed() {
		return suitesFailed;
	}

	public void setSuitesFailed(int suitesFailed) {
		this.suitesFailed = suitesFailed;
	}

	public int getSuitesCount() {
		return suitesCount;
	}

	public void setSuitesCount(int suitesCount) {
		this.suitesCount = suitesCount;
	}

	public int getTestsCount() {
		return testsCount;
	}

	public void setTestsCount(int testsCount) {
		this.testsCount = testsCount;
	}

	public long getStarted() {
		return started;
	}

	public void setStarted(long date) {
		this.started = date;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}
}