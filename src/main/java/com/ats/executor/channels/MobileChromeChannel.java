package com.ats.executor.channels;

import com.ats.executor.ActionTestScript;
import com.ats.executor.drivers.DriverManager;
import com.ats.executor.drivers.engines.IDriverEngine;
import com.ats.executor.drivers.engines.mobiles.MobileChromeDriverEngine;
import com.ats.script.actions.ActionChannelStart;

public class MobileChromeChannel extends MobileChannel {

	public MobileChromeChannel( ActionChannelStart action, ActionTestScript script, DriverManager driverManager, String testName, int testLine) {
		super(action, script, driverManager, testName, testLine);
	}

	public void setApplicationData(String type, String version, byte[] icon, String appName) {
		setApplicationData(type, version, icon);
		systemValues.setApplicationName(appName);
	}

	public IDriverEngine getDesktopDriverEngine() {
		return ((MobileChromeDriverEngine)getDriverEngine()).getMobileDesktopDriver();
	}
}