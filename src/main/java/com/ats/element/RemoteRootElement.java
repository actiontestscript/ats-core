package com.ats.element;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.RemoteWebElement;

public class RemoteRootElement extends RemoteWebElement {

	@Override
	public Dimension getSize() {
		return new Dimension(10, 10);
	}

	@Override
	public String getId() {
		return "root";
	}
}