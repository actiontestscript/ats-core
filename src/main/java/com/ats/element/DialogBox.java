package com.ats.element;

import org.openqa.selenium.Alert;

import com.ats.executor.ActionStatus;
import com.ats.executor.TestBound;
import com.ats.executor.drivers.engines.MobileDriverEngine;
import com.ats.executor.drivers.engines.SystemDriverEngine;
import com.ats.generator.variables.CalculatedProperty;

public class DialogBox {

	private Alert alert;

	protected int waitBox = 1000;

	public DialogBox(Alert alert) {
		this.alert = alert;
	}

	public DialogBox(SystemDriverEngine engine, TestBound dimension) { }

	public DialogBox(MobileDriverEngine engine) { }

	public DialogBox() {}

	public int getWaitBox() {
		return waitBox;
	}

	public String getTitle() {
		return getText();
	}

	public String getText() {
		return alert.getText();
	}

	public void dismiss(ActionStatus status) {
		alert.dismiss();
	}

	public void accept(ActionStatus status) {
		alert.accept();
	}

	public void defaultButton(ActionStatus status) {
		alert.accept();
	}

	public void clickButtonText(String text, ActionStatus status) { }

	public void clickButtonId(String id, ActionStatus status) { }

	public void clickButtonAtIndex(int index, ActionStatus status) { }

	public void sendKeys(String txt) {
		alert.sendKeys(txt);
	}

	public void sendKeys(String txt, String identifier) {
		alert.sendKeys(txt);
	}

	public void sendKeys(String txt, int index) {
		alert.sendKeys(txt);
	}

	public CalculatedProperty[] getAttributes() {
		return new CalculatedProperty[] {new CalculatedProperty("text", alert.getText())};
	}
}