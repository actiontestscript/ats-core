package com.ats.element;

import com.ats.executor.ActionStatus;
import com.ats.executor.drivers.engines.SapDriverEngine;
import com.ats.generator.variables.CalculatedProperty;
import com.google.common.collect.ObjectArrays;

public class SapDialogBox extends DialogBox {

	private final static String ACCEPT = "accept";
	private final static String DISMISS = "dismiss";

	private SapDriverEngine sapDriver;

	private String id;
	private String title;
	private String text;
	private String type;

	public SapDialogBox(SapDriverEngine driver, String id, String title, String text, String type) {
		super();
		this.sapDriver = driver;
		this.id = id;
		this.title = title;
		this.text = text;
		this.type = type;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void dismiss(ActionStatus status) {
		sapDriver.dialogBoxExecute(status, id, DISMISS);
	}

	@Override
	public void accept(ActionStatus status) {
		sapDriver.dialogBoxExecute(status, id, ACCEPT);
	}

	@Override
	public CalculatedProperty[] getAttributes() {
		final CalculatedProperty[] arr1 = super.getAttributes();
		final CalculatedProperty[] arr2 = new CalculatedProperty[] {
				new CalculatedProperty("title", title),
				new CalculatedProperty("type", type)};

		return ObjectArrays.concat(arr1, arr2, CalculatedProperty.class);
	}

	@Override
	public void clickButtonAtIndex(int index, ActionStatus status) {
		sapDriver.dialogBoxExecute(status, id, Integer.toString(index));
		/*
		status.setPassed(false);
		status.setMessage("Index not found, the maximum is " + (buttons.size()-1));
		 */
	}
}