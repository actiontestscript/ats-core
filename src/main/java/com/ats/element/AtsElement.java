/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.element;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.openqa.selenium.remote.RemoteWebElement;

import com.ats.element.test.TestElement;
import com.ats.executor.drivers.desktop.DesktopData;
import com.ats.executor.drivers.engines.WebDriverEngine;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;

@SuppressWarnings("unchecked")
public class AtsElement extends AtsBaseElement {

	private static final String IFRAME = "IFRAME";
	private static final String FRAME = "FRAME";
	public static final String SELECT = "SELECT";
	public static final String OPTION = "OPTION";
	public final static String BODY = "BODY";
	public final static String INPUT = "INPUT";
	public final static String TABLE = "TABLE";
	public final static String TD = "TD";
	public final static String TH = "TH";

	private RemoteWebElement element;
	private boolean numeric = false;

	private boolean password = false;
	private int numChildren = 0;

	private Double screenX = 0D;
	private Double screenY = 0D;

	private Double boundX = 0D;
	private Double boundY = 0D;

	private boolean visible = true;
	private boolean clickable = true;

	private boolean shadowRoot = false;

	private ArrayList<AtsElement> children;

	public AtsElement() {}

	public AtsElement(JsonNode node){
		jsonDeserialize(node);
	}

	public AtsElement(WebDriverEngine engine, List<Object> data, boolean colName) {

		setTag(data.get(1).toString());
		setWidth( getDataValue(data.get(6)));
		setHeight(getDataValue(data.get(7)));
		setX(getDataValue(data.get(8)));
		setY(getDataValue(data.get(9)));
		setAttributes((Map<String, String>) data.get(12));

		this.element = (RemoteWebElement) data.get(0);
		this.numeric = (boolean) data.get(2);
		this.password = (boolean) data.get(3);
		this.boundX = getDataValue(data.get(4));
		this.boundY = getDataValue(data.get(5));
		this.screenX = getDataValue(data.get(10));
		this.screenY = getDataValue(data.get(11));
		this.shadowRoot = (boolean) data.get(13);

		if(colName) {
			addAttribute(TestElement.ATS_COL_NAME, engine.getColName(this.element));
		}
	}

	public static boolean checkIframe(String value) {
		return IFRAME.equalsIgnoreCase(value) || FRAME.equalsIgnoreCase(value);
	}

	public static boolean checkBody(String value) {
		return BODY.equalsIgnoreCase(value);
	}

	public static boolean checkInput(String value) {
		return INPUT.equalsIgnoreCase(value);
	}
	
	public static boolean checkTD(String value) {
		return TD.equalsIgnoreCase(value);
	}

	private static Double getDataValue(Object data) {
		if(data == null) {
			return 0D;
		}
		return (Double)data;
	}

	public RemoteWebElement getElement() {
		return element;
	}

	public Double getScreenX() {
		return screenX;
	}

	public Double getScreenY() {
		return screenY;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isNumeric() {
		return numeric;
	}

	public Double getBoundX() {
		return boundX;
	}

	public void setBoundX(Double boundX) {
		this.boundX = boundX;
	}

	public Double getBoundY() {
		return boundY;
	}

	public void setBoundY(Double boundY) {
		this.boundY = boundY;
	}

	//----------------------------------------------------------------------------------------
	// Specifique attribute
	//----------------------------------------------------------------------------------------

	@Override
	public String getAttribute(String key) {
		final Object attr = super.getAttribute(key);
		if(attr instanceof ArrayList<?>) {
			return Arrays.toString(((ArrayList<String>) attr).toArray());
		}else {
			return (String)attr;
		}
	}

	//----------------------------------------------------------------------------------------
	// Desktop serialization
	//----------------------------------------------------------------------------------------

	public boolean isPassword() {
		return password;
	}

	public void setPassword(boolean value) {
		this.password = value;
	}

	public void setChildren(ArrayList<AtsElement> value) {
		this.children = value;
	}

	public ArrayList<AtsElement> getChildren() {
		return children;
	}

	public void setAttributes(ArrayList<DesktopData> value) {
		setAttributes(value.parallelStream().collect(Collectors.toMap(s -> s.getName(), s -> s.getValue())));
	}

	public ArrayList<DesktopData> getAttributes() {
		return null;
	}

	public boolean isClickable() {
		return clickable;
	}

	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	public int getNumChildren() {
		return numChildren;
	}

	public void setNumChildren(int value) {
		this.numChildren = value;
	}

	//----------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------

	public boolean isIframe() {
		return checkIframe(getTag());
	}

	public boolean isShadowRoot() {
		return shadowRoot;
	}

	// Getter  / Setter for serialization
	void setNumeric(boolean value) {this.numeric = value; }
	void setScreenX(Double value) {this.screenX = value; }
	void setScreenY(Double value) {this.screenY = value; }

	@Override
	public void jsonDeserialize(JsonNode node){
		super.jsonDeserialize(node);
		AtsElement defaultValue = new AtsElement();
		if(node instanceof NullNode || node.get("error") != null) {	return ; }
		setNumeric(JsonUtils.getJsonValue(node, "numeric", defaultValue.numeric, Boolean.class));
		setPassword(JsonUtils.getJsonValue(node, "password", defaultValue.password, Boolean.class));
		setNumChildren(JsonUtils.getJsonValue(node, "numChildren", defaultValue.numChildren, Integer.class));
		setScreenX(JsonUtils.getJsonValue(node, "screenX", defaultValue.screenX, Double.class));
		setScreenY(JsonUtils.getJsonValue(node, "screenY", defaultValue.screenY, Double.class));
		setBoundX(JsonUtils.getJsonValue(node, "boundX", defaultValue.boundX, Double.class));
		setBoundY(JsonUtils.getJsonValue(node, "boundY", defaultValue.boundY, Double.class));
		setVisible(JsonUtils.getJsonValue(node, "visible", defaultValue.visible, Boolean.class));
		setClickable(JsonUtils.getJsonValue(node, "clickable", defaultValue.clickable, Boolean.class));

		JsonNode childrenNode = node.get("children");
		if (childrenNode != null && childrenNode.isArray()) {
			ArrayList<AtsElement> childrenList = new ArrayList<>();
			for(JsonNode children : childrenNode){
				AtsElement element = new AtsElement(children);
				childrenList.add(element);
			}
			setChildren(childrenList);

		}
		else this.children = null;
	}
}