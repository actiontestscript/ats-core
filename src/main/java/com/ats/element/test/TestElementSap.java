package com.ats.element.test;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ats.element.AtsSapElement;
import com.ats.element.SearchedElement;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.engines.SapDriverEngine;

public class TestElementSap extends TestElement {

	public TestElementSap(ActionTestScript script, Channel channel, int actionMaxTry, Predicate<Integer> predicate, SearchedElement searchElement) {
		super(script, channel, actionMaxTry, predicate, searchElement);
	}

	private SapDriverEngine getSapEngine() {
		return (SapDriverEngine)engine;
	}

	@Override
	public String getAttribute(ActionStatus status, String name) {
		if (SapDriverEngine.SAP_NETWORKOPTION.equalsIgnoreCase(name)) {
			return getSapEngine().getNetworkOption();
		}
		return super.getAttribute(status, name);
	}

	@Override
	public String getNotFoundDescription() {

		final StringBuilder messageBuilder = new StringBuilder(super.getNotFoundDescription());

		if (getSelector().contains(AtsSapElement.ELEMENT_ID)) {
			String networkOption = getAttribute(null, SapDriverEngine.SAP_NETWORKOPTION);

			final String regExStr = AtsSapElement.ELEMENT_ID + ":([a-zA-Z]+\\[\\d+\\])"; //We are testing slow connection pattern

			final Pattern pattern = Pattern.compile(regExStr, Pattern.MULTILINE);
			final Matcher matcher = pattern.matcher(getSelector());

			if (matcher.find()) {
				//Slow connectionOption elementId pattern
				if (SapDriverEngine.SAP_NETWORKOPTION_FAST.equalsIgnoreCase(networkOption)) {
					messageBuilder.append(getNotFoundDescription_FastConn());
				}
			} else {
				//Fast connectionOption elementId pattern
				if (SapDriverEngine.SAP_NETWORKOPTION_SLOW.equalsIgnoreCase(networkOption)) {
					messageBuilder.append(getNotFoundDescription_SlowConn());
				}
			}
		}

		return messageBuilder.toString();
	}

	private String getNotFoundDescription_FastConn() {
		final StringBuilder builder = new StringBuilder("\nINFO: You are looking for an indexed elementId, while your connection is in \"Fast Connection\" and will therefore not produce indexed elementIds.\n");
		builder.append("This indicates that the script designer had a connection with the \"Slow connect\" network option.\n");
		builder.append("You can try setting your network option of your connection to \"Slow connection\".\n");
		builder.append("WARNING: This will modify the Ids created by SAP and therefore may impact your other scripts");

		return builder.toString();
	}

	private String getNotFoundDescription_SlowConn() {
		final StringBuilder builder = new StringBuilder("\nINFO: It is possible that the element was not found because your connection is configured in \"Slow Connection\", which produces indexed elements of form obj[idx], whereas your looking for an unindexed elementId.\n");
		builder.append("This indicates that the script designer had a connection with the \"Fast connect\" network option.\n");
		builder.append("You can try setting your network option of your connection to \"Fast connection\".\n");
		builder.append("WARNING: This will modify the Ids created by SAP and therefore may impact your other scripts");

		return builder.toString();
	}
}