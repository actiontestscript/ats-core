/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.parsers;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.ats.generator.GeneratorReport;
import com.ats.generator.events.ScriptProcessedNotifier;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.EnvironmentValue;
import com.ats.generator.variables.ParameterValue;
import com.ats.generator.variables.Variable;
import com.ats.script.AtsScript;
import com.ats.script.Project;
import com.ats.script.Script;
import com.ats.script.actions.ActionApi;
import com.ats.script.actions.ActionAssertCount;
import com.ats.script.actions.ActionAssertProperty;
import com.ats.script.actions.ActionAssertValue;
import com.ats.script.actions.ActionButton;
import com.ats.script.actions.ActionCallscript;
import com.ats.script.actions.ActionChannelClose;
import com.ats.script.actions.ActionChannelStart;
import com.ats.script.actions.ActionChannelSwitch;
import com.ats.script.actions.ActionComment;
import com.ats.script.actions.ActionGesturePress;
import com.ats.script.actions.ActionGestureTap;
import com.ats.script.actions.ActionGotoUrl;
import com.ats.script.actions.ActionMouse;
import com.ats.script.actions.ActionMouseDragDrop;
import com.ats.script.actions.ActionMouseKey;
import com.ats.script.actions.ActionMouseScroll;
import com.ats.script.actions.ActionMouseSwipe;
import com.ats.script.actions.ActionProperty;
import com.ats.script.actions.ActionPropertySet;
import com.ats.script.actions.ActionScripting;
import com.ats.script.actions.ActionSelect;
import com.ats.script.actions.ActionText;
import com.ats.script.actions.ActionWindowNew;
import com.ats.script.actions.ActionWindowResize;
import com.ats.script.actions.ActionWindowState;
import com.ats.script.actions.ActionWindowSwitch;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.script.actions.neoload.ActionNeoloadContainer;
import com.ats.script.actions.neoload.ActionNeoloadRecord;
import com.ats.script.actions.neoload.ActionNeoloadRun;
import com.ats.script.actions.performance.ActionPerformanceRecord;
import com.ats.script.actions.performance.ActionPerformanceStart;
import com.ats.script.actions.performance.octoperf.ActionOctoperfVirtualUser;
import com.ats.tools.Operators;

public class Lexer {

	private final Pattern ACTION_PATTERN = Pattern.compile("(.*)\\[(.*?)\\]", Pattern.CASE_INSENSITIVE);
	
	private final String FILE_TYPE = "#file";
	//private final String ELEMENT_TYPE = "#elem";
	private final String PARAMETERS_TYPE = "#params";
	private final String TABLE_TYPE = "#table";

	@SuppressWarnings("unused")
	private GeneratorReport report; //TODO create report with list of failed ATS to java conversions steps

	private Project projectData;

	private Charset charset = StandardCharsets.UTF_8;

	public int countScript = 0;

	private boolean isGenerator = true;

	public boolean isGenerator() {
		return isGenerator;
	}

	public Lexer(GeneratorReport report){
		this.isGenerator = false;
		this.report = report;
	}

	public Lexer(Project projectData, GeneratorReport report, String charset) {
		this.projectData = projectData;
		this.report = report;

		try {
			this.charset = Charset.forName(charset);
		} catch (IllegalArgumentException e) {}
	}

	public Lexer(Project projectData, GeneratorReport report, Charset charset) {
		this.projectData = projectData;
		this.report = report;
		this.charset = charset;
	}

	public void addScript(){
		countScript++;
	}

	public AtsScript loadScript(File f, ScriptProcessedNotifier notifier){
		final AtsScript script = getScript(f);
		notifier.scriptProcessed();
		return script;
	}

	public AtsScript loadScript(String content) {
		return new AtsScript(this, content, projectData);
	}

	public AtsScript loadScript(File f) {
		return getScript(f);
	}

	private AtsScript getScript(File f) {
		if (f.exists() && f.isFile()) {
			return new AtsScript(Script.ATS_EXTENSION, this, f, projectData, charset);
		}
		return null;
	}

	//---------------------------------------------------------------------------------------------------------------
	// Steps creation
	//---------------------------------------------------------------------------------------------------------------

	public void createAction(AtsScript script, String data, boolean disabled) {

		final ArrayList<String> dataArray = new ArrayList<>(Arrays.asList(data.split(ScriptParser.ATS_SEPARATOR)));

		if (dataArray.size() > 0) {

			final ExecuteOptions options = new ExecuteOptions();
			final String actionType = options.init(script, dataArray.remove(0).trim(), ACTION_PATTERN);
			final int stopPolicy = options.getStopPolicy();

			//-------------------------------------------------------------------------------------------
			// Start decision tree ...
			//-------------------------------------------------------------------------------------------

			if (ActionGesturePress.PREDICATE.test(actionType)) {

				//-----------------------
				// gesture press action
				//-----------------------

				String elementInfo = dataArray.remove(dataArray.size() - 1).trim();
				ArrayList<String> elements = new ArrayList<>(Arrays.asList(elementInfo));
				script.addAction(new ActionGesturePress(script, options, stopPolicy, dataArray, elements), disabled);

			} else if (Mouse.PREDICATE_OVER.test(actionType)) {

				//-----------------------
				// Mouse over action
				//-----------------------

				script.addAction(new ActionMouse(script, options, Mouse.OVER, stopPolicy, dataArray), disabled);

			} else if (Mouse.PREDICATE_DRAG_DROP.test(actionType)) {

				//-----------------------
				// Drag drop action
				//-----------------------

				script.addAction(new ActionMouseDragDrop(script, options, actionType, stopPolicy, dataArray), disabled);

			} else if (Mouse.PREDICATE_CLICK.test(actionType)) {

				//-----------------------
				// Mouse button action
				//-----------------------

				script.addAction(new ActionMouseKey(script, options, actionType, stopPolicy, dataArray), disabled);

			} else if (ActionChannelClose.PREDICATE.test(actionType)) {

				//-----------------------
				// Channel close action
				//-----------------------

				String cname = "";
				if (dataArray.size() > 0) {
					cname = dataArray.remove(0).trim();
				}
				script.addAction(new ActionChannelClose(script, options, cname), disabled);

			} else if(ActionPerformanceRecord.PREDICATE.test(actionType)) {

				if (dataArray.size() > 0) {
					script.addAction(new ActionPerformanceRecord(script, options, dataArray.remove(0).trim()), disabled);
				}

			}else if(ActionPerformanceStart.PREDICATE.test(actionType)){

				script.addAction(new ActionPerformanceStart(script, options, dataArray), disabled);

			}else if(ActionOctoperfVirtualUser.PREDICATE.test(actionType)){

				if(dataArray.size() > 0) {
					script.addAction(new ActionOctoperfVirtualUser(script, options, dataArray), disabled);
				}

			}else if(ActionNeoloadRun.PREDICATE.test(actionType)){

				//---------------------------------------------------------------------------------------------------
				//---------------------------------------------------------------------------------------------------
				// NEOLOAD is disable for the moment !!!!
				//---------------------------------------------------------------------------------------------------
				//---------------------------------------------------------------------------------------------------
				
				//-------------------------------
				// Neoload start and stop actions
				//-------------------------------

				/*String userData = null;
				if(dataArray.size() > 0) {
					userData = dataArray.remove(0).trim();
				}*/

				/*optionsFlat = optionsFlat.toLowerCase();

				if(ActionNeoloadStart.SCRIPT_LABEL.equals(actionType)) {
					script.addAction(new ActionNeoloadStart(script, optionsFlat, userData), disabled);
				}else {
					String userOptions = "";
					if(userData != null) {
						matcher = ACTION_PATTERN.matcher(userData);
						if (matcher.find()){
							userData = matcher.group(1).trim();
							userOptions = matcher.group(2).trim().toLowerCase();
						}
					}
					script.addAction(new ActionNeoloadStop(script, optionsFlat, userData, userOptions), disabled);
				}*/

			}else if(ActionComment.PREDICATE.test(actionType)) {

				script.addAction(new ActionComment(script, actionType, dataArray), disabled);

			} else if(ActionText.PREDICATE.test(actionType)) {

				//-----------------------
				// Text action
				//-----------------------

				String dataText = "";
				if(dataArray.size() > 0) {
					dataText = dataArray.remove(0).trim();
				}
				script.addAction(new ActionText(script, options, stopPolicy, dataText, dataArray), disabled);

			} else if(dataArray.size() > 0) {

				String dataOne = dataArray.remove(0).trim();

				if (ActionPropertySet.PREDICATE.test(actionType)) {

					String propertyValue = "";
					if(dataArray.size() > 0) {
						propertyValue = dataArray.remove(0).trim();
					}
					script.addAction(new ActionPropertySet(script, options, dataOne, propertyValue), disabled);

				}else if(ActionButton.PREDICATE.test(actionType)) {

					script.addAction(new ActionButton(script, options, dataOne), disabled);

				}else if (ActionGestureTap.PREDICATE.test(actionType)) {

					//-----------------------
					// Gesture tap action
					//-----------------------

					script.addAction(new ActionGestureTap(script, options, dataOne, stopPolicy, dataArray), disabled);

				} else if(ActionChannelSwitch.PREDICATE.test(actionType)) {

					//-----------------------
					// Channel switch
					//-----------------------

					script.addAction(new ActionChannelSwitch(script, options, dataOne), disabled);

				} else if(ActionChannelStart.PREDICATE.test(actionType)) {

					//-----------------------
					// Channel start action
					//-----------------------

					if (!dataArray.isEmpty()) {
						script.addAction(new ActionChannelStart(script, options, dataOne, new CalculatedValue(script, dataArray.removeFirst().trim()), dataArray), disabled);
					}

				} else if(ActionApi.PREDICATE.test(actionType)){

					//-----------------------
					// Api action
					//-----------------------

					String headerData = "";

					Matcher matcher = ACTION_PATTERN.matcher(dataOne);
					if (matcher.find()){
						dataOne = matcher.group(1).trim();
						headerData = matcher.group(2).trim();
					}
					script.addAction(new ActionApi(script, options, actionType, dataOne, headerData, dataArray), disabled);

				}else if(ActionWindowResize.PREDICATE.test(actionType)){

					//-----------------------
					// Window resize action
					//-----------------------

					script.addAction(new ActionWindowResize(script, options, dataOne), disabled);

				}else if(ActionWindowState.PREDICATE_STATE.test(actionType)){

					script.addAction(new ActionWindowState(script, options, dataOne), disabled);
					
				}else if(ActionWindowNew.PREDICATE.test(actionType)){

					script.addAction(new ActionWindowNew(script, options, dataOne, dataArray), disabled);

				}else if(ActionWindowSwitch.PREDICATE.test(actionType)){

					//-----------------------
					// Window action
					//-----------------------
					
					boolean regexp = false;
					
					String type = ActionWindowSwitch.SWITCH_INDEX;
					String dataValue = dataOne;
					if(dataArray.size() > 0){
						if(ActionWindowSwitch.SWITCH_INDEX.equalsIgnoreCase(dataOne) || ActionWindowSwitch.SWITCH_NAME.equalsIgnoreCase(dataOne) || ActionWindowSwitch.SWITCH_URL.equalsIgnoreCase(dataOne)) {
							type = dataOne.toLowerCase();
							dataValue = dataArray.get(0).trim();
						}
					}else {
						
						dataValue = "0";
						
						int separator = dataOne.indexOf("=~");
						if(separator == -1) {
							separator = dataOne.indexOf("=");
						}
						
						if(separator > 0) {
							String testType = dataOne.substring(0, separator).trim();
							if(ActionWindowSwitch.SWITCH_INDEX.equalsIgnoreCase(testType) || ActionWindowSwitch.SWITCH_NAME.equalsIgnoreCase(testType) || ActionWindowSwitch.SWITCH_URL.equalsIgnoreCase(testType)) {
								type = testType.toLowerCase();
								dataValue = dataOne.substring(separator + 1).trim();
								if(dataValue.startsWith("~")) {
									dataValue = dataValue.substring(1).trim();
									if(!testType.equals(ActionWindowSwitch.SWITCH_INDEX)) {
										regexp = true;
									}
								}
							}
						}else {
							dataValue = dataOne;
						}
					}
					
					script.addAction(new ActionWindowSwitch(script, options, new CalculatedValue(script, dataValue), type, regexp), disabled);
					
				}else if(ActionWindowState.PREDICATE_CLOSE.test(actionType)){
					
					script.addAction(new ActionWindowState(script, options, ActionWindowState.CLOSE), disabled);

				} else if (ActionScripting.PREDICATE.test(actionType)) {

					//-----------------------
					// Javascript action
					//-----------------------

					final String[] jsDataArray = dataOne.split(ScriptParser.ATS_ASSIGN_SEPARATOR);

					if (jsDataArray.length > 0) {

						Variable variable = null;
						final String jsCode = jsDataArray[0].trim();

						if(jsDataArray.length > 1) {
							variable = script.getVariable(jsDataArray[1].trim(), true);
						}

						script.addAction(new ActionScripting(script, options, stopPolicy, jsCode, variable, dataArray), disabled);
					}

				} else if(ActionProperty.PREDICATE.test(actionType)){

					//-----------------------
					// Get property action
					//-----------------------

					final String[] propertyArray = dataOne.split(ScriptParser.ATS_ASSIGN_SEPARATOR);

					if (propertyArray.length > 1) {
						final String propertyName = propertyArray[0].trim();
						final Variable variable = script.getVariable(propertyArray[1].trim(), true);
						script.addAction(new ActionProperty(script, options, stopPolicy, propertyName, variable, dataArray), disabled);
					}

				} else if(ActionSelect.PREDICATE.test(actionType)){

					//-----------------------
					// Select action
					//-----------------------

					script.addAction(new ActionSelect(script, options, dataOne, stopPolicy, dataArray), disabled);

				}else if(ActionCallscript.PREDICATE_OLD.test(actionType)){

					//-----------------------
					// Callscript action
					//-----------------------

					String[] parameters = new String[0];
					String[] returnValues = new String[0];
					String csvFilePath = null;

					if(dataOne.contains(ScriptParser.ATS_ASSIGN_SEPARATOR)) {
						final String[] callscriptData = dataOne.split(ScriptParser.ATS_ASSIGN_SEPARATOR);
						dataOne = callscriptData[0].trim();
						returnValues = callscriptData[1].split(",");
					}

					if (dataArray.size() > 0) {
						final String firstValue = dataArray.get(0).trim();
						if(firstValue.startsWith(ActionCallscript.ASSETS_PROTOCOLE) || firstValue.startsWith(ActionCallscript.FILE_PROTOCOLE) || firstValue.startsWith(ActionCallscript.HTTP_PROTOCOLE) || firstValue.startsWith(ActionCallscript.HTTPS_PROTOCOLE)) {
							csvFilePath = dataArray.remove(0).trim();
						}
					} else {

						Matcher matcher = ACTION_PATTERN.matcher(dataOne);
						if (matcher.find()){
							dataOne = matcher.group(1).trim();
							String parametersData = matcher.group(2);

							Matcher mv = CalculatedValue.PARAMETER_PATTERN.matcher(parametersData);
							while (mv.find()) {
								ParameterValue sp = new ParameterValue(mv);
								parametersData = parametersData.replace(sp.getReplace(), sp.getNoComma());
							}

							mv = CalculatedValue.ENV_PATTERN.matcher(parametersData);
							while (mv.find()) {
								final EnvironmentValue sp = new EnvironmentValue(mv);
								parametersData = parametersData.replace(sp.getReplace(), sp.getNoComma());
							}

							parameters = parametersData.split(",");
						}
					}
					
					options.checkLegacy(script);
					
					script.addAction(new ActionCallscript(script, options, dataOne, parameters, returnValues, csvFilePath, dataArray), disabled, data);

				}else if(ActionCallscript.PREDICATE.test(actionType)){

					//-----------------------
					// New Callscript action
					//-----------------------

					String[] parameters = new String[0];
					String[] returnValues = new String[0];
					String csvFilePath = null;

					ArrayList<String> newDataArray = new ArrayList<String>();
					
					if (dataArray.size() > 0) {
						
						for (String s : dataArray) {
							int idx = s.indexOf(ScriptParser.ATS_ASSIGN_SEPARATOR);
							if(idx > -1) {
								String rVal = s.substring(idx + 2);
								
								rVal = rVal.replaceFirst(ScriptParser.ATS_ASSIGN_SEPARATOR + ".*", "");
								returnValues = rVal.split(",");
								
								s = s.substring(0, idx);
								newDataArray.add(s.trim());
								s.split(ScriptParser.ATS_ASSIGN_SEPARATOR);
								
							}else {
								newDataArray.add(s.trim());
							}
						}

						String firstValue = newDataArray.get(0).trim();
						if(firstValue.startsWith(ActionCallscript.ASSETS_PROTOCOLE) || firstValue.startsWith(ActionCallscript.FILE_PROTOCOLE) || firstValue.startsWith(ActionCallscript.HTTP_PROTOCOLE) || firstValue.startsWith(ActionCallscript.HTTPS_PROTOCOLE)) {
							csvFilePath = firstValue;
						}

					} else {

						int idx = dataOne.indexOf(ScriptParser.ATS_ASSIGN_SEPARATOR);
						if(idx > -1) {
							final String rVal = dataOne.substring(idx + 2);
							returnValues = rVal.split(",");
							dataOne = dataOne.substring(0, idx).trim();
						}
												
						Matcher matcher = ACTION_PATTERN.matcher(dataOne);
						if (matcher.find()){
							dataOne = matcher.group(1).trim();
							String parametersData = matcher.group(2);
							
							Matcher mv = CalculatedValue.PARAMETER_PATTERN.matcher(parametersData);
							while (mv.find()) {
								ParameterValue sp = new ParameterValue(mv);
								parametersData = parametersData.replace(sp.getReplace(), sp.getNoComma());
							}

							mv = CalculatedValue.ENV_PATTERN.matcher(parametersData);
							while (mv.find()) {
								final EnvironmentValue sp = new EnvironmentValue(mv);
								parametersData = parametersData.replace(sp.getReplace(), sp.getNoComma());
							}

							parameters = parametersData.split(",");
						}
					}

					script.addAction(new ActionCallscript(script, options, dataOne, parameters, returnValues, csvFilePath, newDataArray), disabled, data);
					
				}else if(ActionCallscript.PREDICATE_NEW.test(actionType)){
					
					String[] parameters = new String[0];
					String[] returnValues = new String[0];
					String csvFilePath = null;
					
					if(dataOne.contains(ScriptParser.ATS_ASSIGN_SEPARATOR)) {
						final String[] callscriptData = dataOne.split(ScriptParser.ATS_ASSIGN_SEPARATOR);
						dataOne = callscriptData[0].trim();
						returnValues = StringUtils.stripAll(callscriptData[1].split(","));
					}
					
					String tableSplit = null;
					if (dataArray.size() > 1) {
						final String dataType = dataArray.remove(0).trim();
						if(FILE_TYPE.equals(dataType)) {
							csvFilePath = dataArray.remove(0).trim();
						}else if(TABLE_TYPE.equals(dataType)) {
							if(dataArray.size() > 1) {
								tableSplit = dataArray.remove(0).trim();
								parameters = new String[] {dataArray.remove(0).trim()};
							}
						}else if(PARAMETERS_TYPE.equals(dataType)) {
							parameters = new String[dataArray.size()];
							int i = 0;
							while(dataArray.size() > 0) {
								parameters[i] = dataArray.remove(0);
								i++;
							}
						}
					}
					
					script.addAction(new ActionCallscript(script, options, dataOne, parameters, returnValues, csvFilePath, dataArray, tableSplit), disabled, data);
					
				}else if(ActionGotoUrl.PREDICATE.test(actionType)){

					//-----------------------
					// Goto url action
					//-----------------------

					script.addAction(new ActionGotoUrl(script, options, stopPolicy, new CalculatedValue(script, dataOne)), disabled);

				}else if(ActionMouseScroll.PREDICATE.test(actionType)){

					//-----------------------
					// Mouse scroll action
					//-----------------------

					script.addAction(new ActionMouseScroll(script, options, dataOne, stopPolicy, dataArray), disabled);

				}else if(ActionMouseSwipe.PREDICATE.test(actionType)){

					script.addAction(new ActionMouseSwipe(script, options, actionType, dataOne, stopPolicy, dataArray), disabled);

				}else if(ActionAssertCount.PREDICATE.test(actionType)){

					//-----------------------
					// Assert count action
					//-----------------------

					script.addAction(new ActionAssertCount(script, options, stopPolicy, dataOne, dataArray), disabled);
					
				}else if(ActionAssertCount.PREDICATE_OCCURRENCE.test(actionType)){
					
					if(dataOne.length() > 1) {
						
						final String op = dataOne.substring(0, 2);
						
						if(Operators.COMPARAISON_TYPE.contains(op)) {
							final String calc = dataOne.substring(2).trim();
							script.addAction(new ActionAssertCount(script, options, stopPolicy, op, calc, dataArray), disabled);
						}
					}
					
				}else if(ActionAssertProperty.PREDICATE.test(actionType)){

					//-----------------------
					// Assert property action
					//-----------------------

					script.addAction(new ActionAssertProperty(script, options, stopPolicy, dataOne, dataArray), disabled);

				}else if(ActionAssertValue.PREDICATE.test(actionType)){

					//-----------------------
					// Assert value action
					//-----------------------

					script.addAction(new ActionAssertValue(script, options, stopPolicy, dataOne), disabled);

				}else if(ActionNeoloadContainer.PREDICATE.test(actionType)){

					//--------------------------
					// Neoload container action
					//--------------------------

					if(dataOne.length() > 0){
						script.addAction(new ActionNeoloadContainer(script, dataOne), disabled);
					}

				}else if(ActionNeoloadRecord.PREDICATE.test(actionType)){

					//--------------------------
					// Neoload record actions
					//--------------------------

					script.addAction(new ActionNeoloadRecord(script, dataOne), disabled);

				}
			}else {
				if(data.startsWith("<<<<<<<") || data.startsWith("=======") || data.startsWith(">>>>>>>")) {
					//TODO whe have a problem here ! need a way to show that git merge has failed in actions file
				}else {
					script.addAction(new ActionComment(script, data), disabled);
				}
			}
		}
	}
}