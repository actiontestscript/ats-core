/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ats.driver.AtsManager;

public class AntCompiler {

	public static void main(String[] args) throws ParserConfigurationException, Exception {
		if(args.length > 0) {
			
			String baseDir = ".";
			File buildFile = new File(args[0]);
			
			if("build".equals(args[0])){
				buildFile = Paths.get("build.xml").toAbsolutePath().toFile();
				if(buildFile.exists()) {
					buildFile.delete();
				}
			} else {
				buildFile = new File(args[0]);
			}
			
			if(!buildFile.exists()) {

				buildFile.deleteOnExit();
				buildFile.getParentFile().mkdirs();

				final FileWriter fw = new FileWriter(buildFile);
				fw.write("<project basedir=\"" + baseDir + "\" default=\"compile\">");
				fw.write("<copy todir=\"${basedir}/target/classes\"> ");
				fw.write("<fileset dir=\"${basedir}/src\" includes='assets/**'/>");
				fw.write("</copy>");
				
				fw.write("<copy todir=\"${basedir}/target/classes/assets/" + AtsManager.SCRIPTS_FOLDER + "\"> ");
				fw.write("<fileset dir=\"${basedir}/src/main\" includes='javascript/**'/>");
				fw.write("<fileset dir=\"${basedir}/src/main\" includes='python/**'/>");
				fw.write("<fileset dir=\"${basedir}/src/main\" includes='cs/**'/>");
				fw.write("</copy>");
								
				fw.write("<property name=\"lib.dir\" value=\"lib\"/>");
				fw.write("<path id=\"classpath\">");
				fw.write("<fileset dir=\"" + AtsManager.getAtsHomeFolder() + "/libs\" includes=\"**/*.jar\"/>");
				fw.write("</path>");
				fw.write("<target name=\"compile\">");
				fw.write("<mkdir dir=\"${basedir}/target/classes\"/>");
				fw.write("<mkdir dir=\"${basedir}/target/generated\"/>");
				
				fw.write("<copy todir=\"${basedir}/target/generated\"> ");
				fw.write("<fileset dir=\"${basedir}/src/main/java\" includes=\"**\"/>");
				fw.write("</copy>");
				
				fw.write("<javac srcdir=\"${basedir}/target/generated\" destdir=\"${basedir}/target/classes\" classpathref=\"classpath\"/>");
				fw.write("</target>");
				fw.write("</project>");

				fw.close();
			}

			build(buildFile);
		}
	}
	
	public static File createXmlFile(String targetFolderPath) throws IOException, ParserConfigurationException, TransformerException {
		
		final File tempXml = File.createTempFile("ant_", ".xml");
		tempXml.deleteOnExit();
		
		final DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		final Document writeXmlDocument = builder.newDocument();

		final Element project = writeXmlDocument.createElement("project");
		project.setAttribute("basedir", targetFolderPath);
		project.setAttribute("default", "compile");

		final Element copy = writeXmlDocument.createElement("copy");
		copy.setAttribute("todir", "classes");

		final Element fileset = writeXmlDocument.createElement("fileset");
		fileset.setAttribute("dir", "../src");
		fileset.setAttribute("includes", "assets/**");

		copy.appendChild(fileset);
		project.appendChild(copy);
		
		final Element copy2 = writeXmlDocument.createElement("copy");
		copy2.setAttribute("todir", "classes/assets/" + AtsManager.SCRIPTS_FOLDER);

		final Element fileset2 = writeXmlDocument.createElement("fileset");
		fileset2.setAttribute("dir", "../src/main");
		fileset2.setAttribute("includes", "javascript/**");
		
		final Element fileset3 = writeXmlDocument.createElement("fileset");
		fileset3.setAttribute("dir", "../src/main");
		fileset3.setAttribute("includes", "python/**");
		
		final Element fileset4 = writeXmlDocument.createElement("fileset");
		fileset4.setAttribute("dir", "../src/main");
		fileset4.setAttribute("includes", "cs/**");

		copy2.appendChild(fileset2);
		copy2.appendChild(fileset3);
		copy2.appendChild(fileset4);
		project.appendChild(copy2);
		
		final Element target = writeXmlDocument.createElement("target");
		target.setAttribute("name", "compile");

		Element mkdir = writeXmlDocument.createElement("mkdir");
		mkdir.setAttribute("dir", "classes");
		target.appendChild(mkdir);

		mkdir = writeXmlDocument.createElement("mkdir");
		mkdir.setAttribute("dir", "generated");
		target.appendChild(mkdir);

		final Element javac = writeXmlDocument.createElement("javac");
		javac.setAttribute("includeantruntime", "true");
		javac.setAttribute("srcdir", "generated");
		javac.setAttribute("destdir", "classes");

		target.appendChild(javac);

		project.appendChild(target);

		writeXmlDocument.appendChild(project);

		final Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(new DOMSource(writeXmlDocument),
				new StreamResult(new OutputStreamWriter(
						new FileOutputStream(tempXml),
						StandardCharsets.UTF_8)));
		
		return tempXml;
	}

	public static void build(File buildFile) {
		final Project p = new Project();
		p.setUserProperty("ant.file", buildFile.getAbsolutePath());
		p.init();

		ProjectHelper helper = ProjectHelper.getProjectHelper();
		p.addReference("ant.projectHelper", helper);
		helper.parse(p, buildFile);

		DefaultLogger consoleLogger = new DefaultLogger();
		consoleLogger.setErrorPrintStream(System.err);
		consoleLogger.setOutputPrintStream(System.out);
		consoleLogger.setMessageOutputLevel(Project.MSG_INFO);

		p.addBuildListener(consoleLogger);
		p.executeTarget(p.getDefaultTarget());
	}
}