package com.ats.generator.variables;

import com.ats.executor.ActionTestScript;

public class ProjectVariable {

	private ActionTestScript script;
	private String name;
	
	public ProjectVariable(ActionTestScript script, String name) {
		this.script = script;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return script.getProjectVariableValue(name);
	}
}