/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.variables.transform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.text.StringEscapeUtils;

import com.ats.executor.ActionTestScript;
import com.google.common.base.Joiner;

public class RegexpTransformer extends Transformer {

	private List<Integer> groups = new ArrayList<>(Arrays.asList(1));
	private String pattern = "(.*)";

	public RegexpTransformer() {} // Needed for serialization

	public RegexpTransformer(String pattern, int[] groups) {
		setPattern(pattern);

		this.groups = new ArrayList<>();
		for(int i : groups) {
			this.groups.add(i);
		}
	}

	public RegexpTransformer(String pattern, String groups) {
		setPattern(pattern);

		this.groups = new ArrayList<>();
		for(String s : groups.split("+")) {
			this.groups.add(getInt(s));
		}
	}

	public RegexpTransformer(String data) {

		int lastComa = data.lastIndexOf(",");

		try {

			final String[] groups = data.substring(lastComa + 1).split("\\+");
			final ArrayList<Integer> groupsNum = new ArrayList<>();

			for(String s : groups) {
				groupsNum.add(getInt(s));
			}

			if(groupsNum.size() > 0) {
				setGroups(groupsNum);
			}

		}catch (IndexOutOfBoundsException e) {
			setGroups(new ArrayList<>(Arrays.asList(1)));
		}

		try {
			setPattern(data.substring(0, lastComa).trim());
		}catch (IndexOutOfBoundsException e) {
			setPattern("(.*)");
		}
	}

	@Override
	public String getJavaCode() {
		final StringBuilder sb =
				new StringBuilder(", ").
				append(ActionTestScript.JAVA_REGEX_FUNCTION_NAME)
				.append("(\"")
				.append(StringEscapeUtils.escapeJava(pattern))
				.append("\", new int[]{")
				.append(Joiner.on(", ").join(groups))
				.append("})");
		return sb.toString();
	}

	@Override
	public String format(String data) {
		if(data.length() > 0) {
			try {
				final Pattern patternComp = Pattern.compile(pattern);
				final Matcher m = patternComp.matcher(data);

				if(m.find()) {
					final StringBuilder builder = new StringBuilder();
					for(int g : groups) {
						try {
							builder.append(m.group(g));
						}catch(IndexOutOfBoundsException e) {}
					}
					return builder.toString();
				}
			}catch(PatternSyntaxException e) {
				return "#REGEXP_ERROR# (Pattern syntax error) " + pattern;
			}
		}
		return "";
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public List<Integer> getGroups() {
		return groups;
	}

	public void setGroups(List<Integer> value) {
		this.groups = value;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}