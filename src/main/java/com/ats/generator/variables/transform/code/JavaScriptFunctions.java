/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator.variables.transform.code;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaScriptFunctions {

	private static final Pattern isEmptyPattern = Pattern.compile("isEmpty\\((.*)\\)");
	private static final Pattern isNotEmptyPattern = Pattern.compile("isNotEmpty\\((.*)\\)");
	
	 private static final String JS_CODE = """

			function isEmpty(p){ 
			    return p.length == 0; 
			}
			function isNotEmpty(p){
			    return p.length > 0;
			}

			console.log(%s); 

			""";
	
	public static String getCode(String value) {
		
		String code = value.replace("\"", "'");
		if(code.endsWith(";")) {
			code = code.substring(0, code.length()-1);
		}
		
		String newCode = code;
		
		Matcher m = isEmptyPattern.matcher(code);
		while(m.find()) {
			newCode = replaceData(newCode, m.group(1));
			code = code.replace(m.group(0), "");
			m = isEmptyPattern.matcher(code);
		}
		
		m = isNotEmptyPattern.matcher(code);
		while(m.find()) {
			newCode = replaceData(newCode, m.group(1));
			code = code.replace(m.group(0), "");
			m = isNotEmptyPattern.matcher(code);
		}
		
		return String.format(JS_CODE, newCode);
	}
	
	private static String replaceData(String code, String data) {
		
		if(data != null) {
			String replace = "''";
			if(data.length() > 0) {
				
				replace = data;
				
				if(!replace.startsWith("'")) {
					replace = "'" + replace;
				}
				
				if(!replace.endsWith("'")) {
					replace = replace + "'";
				}
			}
			code = code.replace("(" + data + ")", "(" + replace + ")");
		}
		return code;
	}
}
