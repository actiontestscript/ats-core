/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.generator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;

import com.ats.generator.events.ScriptProcessedEvent;
import com.ats.generator.events.ScriptProcessedNotifier;
import com.ats.generator.parsers.Lexer;
import com.ats.script.Project;
import com.ats.script.AtsScript;
import com.ats.tools.Utils;

public class Generator implements ScriptProcessedEvent{

	private GeneratorReport genReport;
	private Lexer lexer;
	private ArrayList<File> filesList;

	private Project project;

	private int remainingScripts = 0;

	public static void main(String[] args) throws TransformerException, IOException, ParserConfigurationException {

		ATS.logInfo("Java version : " + System.getProperty("java.version"));
		ATS.logInfo(StringUtils.repeat("-", 72));

		final ATS arguments = new ATS(args);
		arguments.parse();

		final Project projectData = Project.getProjectData(arguments.getProjectFolder(), arguments.getDestinationFolder(), arguments.getReportFolder());

		if(projectData.isValidated()) {

			final Generator generator = new Generator(projectData);
			final GeneratorReport report = generator.launch();

			ATS.logInfo(StringUtils.repeat("-", 72));
			ATS.logInfo("ATS Generator finished :");
			ATS.logInfo("- java files generated -> " + report.getGeneratedScriptsCount());
			ATS.logInfo("- ellapsed time -> " + report.getGenerationEllapsedTime() + " ms");
			ATS.logInfo(StringUtils.repeat("-", 72));

			if(arguments.isCompile()) {
				final File tempXml = AntCompiler.createXmlFile(projectData.getTargetFolderPath().toString());
				AntCompiler.build(tempXml);
			}

		}else {
			ATS.logInfo("no valid Ats project found at -> " + arguments.getProjectFolder());
		}
	}

	public static String findSuiteFile(String s) {
		if(Paths.get(s).toFile().exists()) {
			return s;
		}else {
			if(Paths.get(s + ".xml").toFile().exists()) {
				return s + ".xml";
			}else {
				s = "src/exec/" + s;
				if(Paths.get(s).toFile().exists()) {
					return s;
				}else {
					if(Paths.get(s + ".xml").toFile().exists()) {
						return s + ".xml";
					}
				}
			}
		}
		return null;
	}

	public Generator(String projectPath){
		this(new File(projectPath));
	}

	public Generator(File atsFile){
		this(Project.getProjectData(atsFile, null, null));
	}

	public Generator(Project project){

		if(init(project) && remainingScripts > 0){

			project.initFolders();
			genReport.startGenerator(remainingScripts);

			lexer = new Lexer(project, genReport, StandardCharsets.UTF_8);

		}else{
			ATS.logInfo("nothing to be done (no ATS files found !)");
		}
	}

	private boolean init(Project p) {
		if(p.isValidated()) {

			genReport = new GeneratorReport();
			project = p;

			filesList = project.getAtsScripts();
			remainingScripts = filesList.size();

			return true;
		}
		return false;
	}

	public GeneratorReport launch(){

		if(Files.exists(project.getJavaSourceFolder())){
			Utils.copyFiles(project.getJavaSourceFolder().toString(), project.getJavaDestinationFolder().toString(), true);
		}

		final Stream<File> stream = filesList.parallelStream();
		stream.forEach(this::generateJava);
		stream.close();

		genReport.endGenerator();

		filesList.clear();
		lexer = null;

		return genReport;
	}

	private void generateJava(File f){
		final AtsScript sc = lexer.loadScript(f, new ScriptProcessedNotifier(this));
		sc.generateJavaFile(project);
	}

	public ArrayList<AtsScript> getScripts(){
		final ArrayList<AtsScript> result = new ArrayList<>();
		for(File f : filesList) {
			result.add(lexer.loadScript(f, new ScriptProcessedNotifier(this)));
		}
		return result;
	}

	public Project getProject() {
		return project;
	}

	@Override
	public void scriptProcessed() {
		remainingScripts--;

		//int percent = (int)(10000-(double)remainingScripts/(double)totalScript*10000)/100;
		//log.info("Generator in progress : " + percent + " % done");
	}
}