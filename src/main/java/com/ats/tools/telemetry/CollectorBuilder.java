/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools.telemetry;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import io.opentelemetry.exporter.otlp.metrics.OtlpGrpcMetricExporter;
import io.opentelemetry.sdk.metrics.export.MetricExporter;

public class CollectorBuilder {
	
	private final static String OTEL_ENDPOINT = "OTEL_ENDPOINT";
	private final static String OTEL_RESOURCE_ATTRIBUTES = "OTEL_RESOURCE_ATTRIBUTES";
	
    private String endPointUrl;
    private Map<String,String> resources;
    private MetricExporter metricExporter;
    
    public CollectorBuilder withEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
        return this;
    }

    public CollectorBuilder withResources(Map<String,String> resources) {
        this.resources = resources;
        return this;
    }

    public CollectorBuilder withMetricExporter(MetricExporter metricExporter) {
        this.metricExporter = metricExporter;
        return this;
    }
    
    public static String getOpenTelemetryEndpoint() {
    	return System.getenv(OTEL_ENDPOINT);
    }
    
    public static String getOpenTelemetryResources() {
    	return System.getenv(OTEL_RESOURCE_ATTRIBUTES);
    }
        
    public MetricExporter getDefaultMetricExporter(String endPointUrl) {
        return OtlpGrpcMetricExporter.builder().setEndpoint(endPointUrl).build();
    }

    private String getEndPointUrl() {
        return endPointUrl;
    }
    
    public Collector build() {
        if(endPointUrl == null) {
            endPointUrl = getOpenTelemetryEndpoint();
        }

        if(metricExporter == null && endPointUrl != null) {
            metricExporter = getDefaultMetricExporter(endPointUrl);
        }
        if(resources == null) {
            resources = stringToMap(getOpenTelemetryResources());
        }

        return new Collector(resources, metricExporter);
    }

    private static Map stringToMap(String attributesString) 
    {   
        if(StringUtils.isAllBlank(attributesString)) {
            return Collections.emptyMap();
        }
        
        String[] values = attributesString.split(",");
        Map<String,String> map = new HashMap<>();
        for(String value:values) {
            String[] parts = value.split("=");
            if(parts.length > 1) {
                map.put(parts[0],parts[1]);
            }
        }
        return map;
    }
}
