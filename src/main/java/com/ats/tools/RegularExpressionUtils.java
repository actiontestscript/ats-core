/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressionUtils {

	private static final int checkInterval = 3000000;

	public static Matcher createMatcherWithTimeout(String stringToMatch, Pattern regularExpressionPattern, long timeout) {

		if(stringToMatch == null) {
			throw new RegexpTimeoutException("String data to match is null !");
		}

		if (timeout <= 0) {
            return regularExpressionPattern.matcher(stringToMatch);
        }

        return regularExpressionPattern.matcher(
        		new TimeoutRegexCharSequence(
        				stringToMatch,
        				timeout * 1000,
        				stringToMatch,
        				regularExpressionPattern.pattern(),
        				checkInterval));
    }

    private static class TimeoutRegexCharSequence implements CharSequence {

        private final CharSequence inner;
        private final long timeoutMillis;
        private final long timeoutTime;
        private final String stringToMatch;
        private final String regularExpression;
        private int checkInterval;
        private int attemps;

        TimeoutRegexCharSequence(CharSequence inner, long timeoutMillis, String stringToMatch, String regularExpression, int checkInterval) {
            super();

            this.inner = inner;
            this.timeoutMillis = timeoutMillis;
            this.stringToMatch = stringToMatch;
            this.regularExpression = regularExpression;
            this.timeoutTime = System.currentTimeMillis() + timeoutMillis;
            this.checkInterval = checkInterval;
            this.attemps = 0;
        }

        @Override
		public char charAt(int index) {
            if (this.attemps == this.checkInterval) {
                if (System.currentTimeMillis() > timeoutTime) {
                    throw new RegexpTimeoutException(regularExpression, stringToMatch, timeoutMillis);
                }
                this.attemps = 0;
            } else {
                this.attemps++;
            }

            return inner.charAt(index);
        }

        @Override
		public int length() {
            return inner.length();
        }

        @Override
		public CharSequence subSequence(int start, int end) {
            return new TimeoutRegexCharSequence(inner.subSequence(start, end), timeoutMillis, stringToMatch, regularExpression, checkInterval);
        }

        @Override
        public String toString() {
            return inner.toString();
        }
    }
}