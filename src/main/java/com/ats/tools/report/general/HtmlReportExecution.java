package com.ats.tools.report.general;

import com.ats.generator.ATS;
import com.ats.tools.ResourceContent;
import com.ats.tools.report.SuitesReportItem;
import com.ats.tools.report.models.HtmlReportProject;
import com.ats.tools.report.models.Suite;
import com.ats.tools.report.utils.FileUtils;
import com.ats.tools.report.utils.HtmlActionUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class HtmlReportExecution {

    private static final String REPORT_TYPE = "${reportType}";
    private static final String PROJECT_NAME = "${projectNameFormatted}";
    private static final String SUITS_LAUNCHED = "${suitsLaunched}";
    private static final String CREATED_AT = "${createdAt}";
    private static final String LOGO = "${logoImage}";
    private static final String PDF_REPORT_LINK = "${pdfReportLink}";
    private static final String ATS_TEST_SCRIPT_VERSION = "${atsTestScriptVersion}";
    public static final String PROJECT_NAME_HTML_TEMPLATE = "<a class='project-info-body-value hover-underline' style='color: rgba(56, 63, 79, 1);' href='ats://${projectUri}' target='_self'><div class='external-link-icon'></div>${projectNameUri}</a>";
    public static final String PDF_SUMMARY_CSS = "pdf-summary";
    public static final String DISPLAY_NONE_CSS = "display-none";
    public static final String COMMAND_LINE_EXECUTION_FILE_NAME = "command-line-execution.html";
    public static final String SUITE_EXECUTION_FILE_NAME = "ats-report.html";
    private final String mainTemplate;
    private final HtmlReportProject project;
    private final String outputPath;
    private final int devReportLevel;
    private final String projectLogo;
    private OutputStream fileWriter;
    private OutputStream validationFileWriter;
    private final boolean isValidationReport;
    private SuitesReportItem suiteItem;

    private static final String styles = ResourceContent.getHtmlReportCss();
    private static final String scripts = ResourceContent.getHtmlReportJavascript();
    private static final String STYLES_PLACEHOLDER = "${styles}";
    private static final String SCRIPTS_PLACEHOLDER = "${scripts}";

    public HtmlReportExecution(String mainTemplate, HtmlReportProject project, String outputPath, int devReportLevel, boolean isValidationReport, SuitesReportItem suiteItem) {
        this.mainTemplate = mainTemplate;
        this.project = project;
        this.outputPath = outputPath;
        this.devReportLevel = devReportLevel;
        this.projectLogo = FileUtils.getBase64LogoProperty(project, outputPath);
        this.isValidationReport = isValidationReport;
        this.suiteItem = suiteItem;
    }

    public String processMainExecutionsFile() {
        String result = mainTemplate.replace(REPORT_TYPE, HtmlActionUtils.getReportType(devReportLevel));
        result = result.replace(STYLES_PLACEHOLDER, styles);
        result = result.replace(SCRIPTS_PLACEHOLDER, scripts);
        result = result.replace(LOGO, projectLogo);
        result = result.replace(PROJECT_NAME, buildProjectName(project));
        result = result.replace(SUITS_LAUNCHED, String.valueOf(project.getSuites().size()));
        result = result.replace(PDF_REPORT_LINK, "summary.html");
        result = result.replace(CREATED_AT, String.valueOf(project.getStartedFormatted()));
        result = result.replace(ATS_TEST_SCRIPT_VERSION, ATS.getAtsVersion());
        File executionFile;
        Optional<Suite> commandLineSuite = this.project.getSuites().stream().filter(suite -> suite.getName().toLowerCase().contains("command line suite")).findFirst();
        boolean commandLineExecution = commandLineSuite.isPresent();
        this.suiteItem.setIsNoSuiteLaunch(commandLineExecution);


        if (suiteItem.isNoSuiteLaunch()) {
            result = result.replace(PDF_SUMMARY_CSS, DISPLAY_NONE_CSS);
            String outputFileName = COMMAND_LINE_EXECUTION_FILE_NAME;
            if (commandLineSuite.isPresent()) {
                outputFileName = commandLineSuite.get().getTests().get(0) + ".html";
            }
            executionFile = new File(outputPath + File.separator + outputFileName);
        } else {
            executionFile = new File(outputPath + File.separator + SUITE_EXECUTION_FILE_NAME);
        }

        writeFile(result, executionFile);

        if (isValidationReport) {
            File validationFile = new File(outputPath + File.separator + "validation-report.html");
            writeValidationFile(result, validationFile);
        }

        return executionFile.getPath();
    }

    private void writeValidationFile(String result, File validationFile) {
        try {
            validationFileWriter = Files.newOutputStream(Path.of(validationFile.getPath()));
            validationFileWriter.write(result.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private CharSequence buildProjectName(HtmlReportProject project) {
        return PROJECT_NAME_HTML_TEMPLATE.replace("${projectUri}", project.getProjectUuid()).replace("${projectNameUri}", project.getProjectName() + " " + project.getProjectVersion());
    }

    public void writeFile(String executionReport, File suiteFile) {
        try {
            fileWriter = Files.newOutputStream(Path.of(suiteFile.getPath()));
            fileWriter.write(executionReport.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    public void closeWriter() {
        try {
            fileWriter.write("</div></div></body></html>".getBytes());
            fileWriter.flush();
            fileWriter.close();
            if (isValidationReport) {
                validationFileWriter.write("</div></div></body></html>".getBytes());
                validationFileWriter.flush();
                validationFileWriter.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public OutputStream getFileWriter() {
        return fileWriter;
    }

    public OutputStream getValidationFileWriter() {
        return validationFileWriter;
    }
}
