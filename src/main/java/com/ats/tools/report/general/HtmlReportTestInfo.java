package com.ats.tools.report.general;

import com.ats.recorder.TestError;
import com.ats.script.Project;
import com.ats.tools.Utils;
import com.ats.tools.report.SuitesReportItem;
import com.ats.tools.report.models.TestInfo;
import com.ats.tools.report.utils.HtmlActionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.stream.Collectors;

public class HtmlReportTestInfo {
    private static final String TABLE_VALUE_TEMPLATE = "<div style='word-wrap: anywhere'>${valuePlaceholder}</div>";
    private static final String TABLE_NO_VALUE_TEMPLATE = "<div class='no-value' style='word-wrap: anywhere'>${valuePlaceholder}</div>";
    private static final String TEST_NAME = "${testName}";
    private static final String ACTIONS_COUNT = "${actionsCount}";
    private static final String EXECUTION_TIME_MIL = "${executionTime}";
    private static final String EXECUTION_TIME_SEC = "${executionTimeSec}";
    private static final String EXECUTION_TIME_MIN = "${executionTimeMin}";
    private static final String EXECUTION_TIME_HRS = "${executionTimeHrs}";
    private static final String ID_HIDDEN_PLACEHOLDER_MIN = "${idHiddenPlaceHolderMin}";
    private static final String ID_HIDDEN_PLACEHOLDER_SEC = "${idHiddenPlaceHolderSec}";
    private static final String ID_HIDDEN_PLACEHOLDER_MIL = "${idHiddenPlaceHolderMil}";
    private static final String ID_HIDDEN_PLACEHOLDER_HRS = "${idHiddenPlaceHolderHrs}";
    private static final String ERROR_TEXT_COLOR_PLACEHOLDER = "${error-text-color-placeholder}";
    private static final String EXECUTION_ERROR = "${executionError}";
    private static final String ERROR_ACTION_URL = "${errorActionUrl}";
    private static final String ERROR_ACTION_ID = "${errorActionId}";
    private static final String REPORT_SUMMARY = "${reportSummary}";
    private static final String AUTHOR = "${author}";
    private static final String STARTED = "${started}";
    private static final String FINISHED = "${finished}";
    private static final String TEST_ID = "${testId}";
    private static final String EXTERNAL_ID = "${externalId}";
    private static final String TEST_DESCRIPTION = "${testDescription}";
    private static final String PREREQUISITES = "${prerequisites}";
    private static final String GROUPS = "${groups}";
    public static final String EXECUTE_BUTTON_TEMPLATE = "${executeButton}";
    public static final String EXECUTE_BUTTON_HTML_TEMPLATE = "<a  style='color: rgba(104, 112, 125, 1); text-decoration: none; display: flex; align-items: center;' href='ats://${scriptNameUri}' target='_self'><div class='run-script-icon'></div>${buttonLabel}</a>";
    private static final String TEST_CASE_ID = "${testCaseId}";
    private static final String TEST_TITLE = "${testTitle}";
    private static final String TEST_EXECUTION_STATUS_NAME_CLASS = "test-execution-status-name";
    private static final String BLOCKING_ERROR_LABEL_PLACEHOLDER = "${blocking-error-label}";
    private static final String EXECUTION_ERROR_TEMPLATE = "Line: ${executionErrorLine}, Error: ${executionErrorText}";
    public static final String GROUP_NAME_HTML_TEMPLATE = "<a class='item-with-frame' style='text-decoration: none; color: rgba(56, 63, 79, 1); display: flex;' href='ats://${groupUri}' target='_self'><div class='external-link-icon'></div>${groupName}</a>";
    public static final String EXTERNAL_LINK_HTML_TEMPLATE = "<a class='hover-underline gen-margin-right-10' style='color: rgba(56, 63, 79, 1); display: flex;' href='${externalResourceUri}' target='_blank'><div class='external-link-icon'></div>${externalResource}</a>";
    public static final String EXECUTION_ERROR_HTML_TEMPLATE = "<div class='test-execution-status'>\n" +
            "                <div class='test-execution-status-name' title='Go to failed action' onclick=\"navigateToElementById('${errorActionId}', this, true)\"><div class='hover-underline div-with-flex-center'><div class='down-arrow-icon'></div>Execution error</div></div>\n" +
            "                <div class='test-execution-status-value right-border-radius' style='justify-content: space-between;'><a style='text-decoration: none; display: flex; align-items: center;' id='errorUrl' href='ats://${errorActionUrl}' target='_self'><div class='external-link-icon'></div><div class='hover-underline ${error-text-color-placeholder}' title='${executionError}'>${executionError}</div></a></div>" +
            "            </div>";

    public static final String REPORT_SUMMARY_HTML_TEMPLATE = "<div class='test-execution-status' >\n" +
            "                <div class='test-execution-status-name test-execution-status-name-blue'>Report summary</div>\n" +
            "                <div class='test-execution-status-value test-execution-status-value-blue right-border-radius'>" +
            "                   <div class='data-body-1-cell-cell-action-comment'><iframe class='action-comment-iframe' loading='lazy' srcdoc='${reportSummary}'></iframe></div>\n" +
            "                </div>\n" +
            "            </div>";

    public static final String TEST_NAME_HTML_TEMPLATE = "<a style='color: rgb(56 63 74 / 75%); text-decoration: none;" +
            "display: flex; align-items: center;' href='ats://${testNameUri}' target='_self'><div class='external-link-icon'></div><div class='test-name-text hover-underline' title='${testNameTitle}'>${testName}</div></a>";

    private static final String REPORT_HTML_LINK_PLACE_HOLDER = "${htmlReportLink}";

    private static final String REPORT_EXECUTION_RESULT_HTML_TEMPLATE = "<div class='${executionResult}'></div>\n";
    private static final String REPORT_EXECUTION_RESULT_HTML_TEMPLATE_PLACE_HOLDER = "${executionResulPlaceholder}";
    private static final String EXECUTION_ERROR_HTML_PLACEHOLDER = "${executionErrorTemplate}";
    private static final String EXECUTION_SUMMARY_HTML_PLACEHOLDER = "${executionSummaryTemplate}";
    private static final String ACTION_TYPE_CHART = "${actionTypeChart}";
    private static final String COMMAND_LINE_SUITE_NAME = "command line suite";
    private static final String MODIFIED_AT_PLACEHOLDER = "${modifiedAt}";
    private static final String PROFILE_PICTURE_PLACEHOLDER = "${profilePic}";
    private static final String AUTHOR_HEADER = "Author";
    private static final String MODIFIED_BY_HEADER = "Modifier";
    private static final String MODIFIED_AT_HEADER = "Modified";
    private static final String MODIFIED_AT_HEADER_CSS = "modified-at-header";
    private static final String CREATED_AT_HEADER = "Created";
    private static final String DISPLAY_NONE_CSS = "display-none";
    private static final String PROFILE_PIC_CSS = "profile-pic";
    private String template;
    private String result;

    public HtmlReportTestInfo(TestInfo testInfo, String template, String htmlReportPath, String suiteName, SuitesReportItem suiteItem) {

        String groups = testInfo.getGroups().stream().filter(group -> !StringUtils.isEmpty(group)).toList().isEmpty() ?
                "" : testInfo.getGroups().stream().map(group -> buildGroupNameWithLink(testInfo, group))
                .collect(Collectors.joining());

        boolean isCommandLineExecution = suiteItem.getName().toLowerCase().contains(COMMAND_LINE_SUITE_NAME) || suiteName.toLowerCase().contains(COMMAND_LINE_SUITE_NAME);

        this.result = template.replace(STARTED, formatValue(testInfo.getStartedFormatted()));
        if (StringUtils.isNoneEmpty(testInfo.getModifiedBy())) {
            String profilePic = getUserProfilePictureFromProjectProperties(testInfo.getSystemUser());
            this.result = result.replace(AUTHOR_HEADER, MODIFIED_BY_HEADER);
            this.result = result.replace(AUTHOR, formatValue(testInfo.getModifiedBy()));
            this.result = result.replace(MODIFIED_AT_PLACEHOLDER, formatValue(testInfo.getModifiedAt()));
            this.result = result.replace(MODIFIED_AT_HEADER_CSS + " " + DISPLAY_NONE_CSS, MODIFIED_AT_HEADER_CSS);
            if (StringUtils.isNoneEmpty(profilePic)) {
                this.result = result.replace(PROFILE_PICTURE_PLACEHOLDER, profilePic);
                this.result = result.replace(PROFILE_PIC_CSS + " " + DISPLAY_NONE_CSS, PROFILE_PIC_CSS);
            }
        } else {
            this.result = result.replace(AUTHOR, formatValue(testInfo.getAuthor()));
            String profilePic = getUserProfilePictureFromProjectProperties(testInfo.getAuthor());
            if (StringUtils.isNoneEmpty(profilePic)) {
                this.result = result.replace(PROFILE_PICTURE_PLACEHOLDER, profilePic);
                this.result = result.replace(PROFILE_PIC_CSS + " " + DISPLAY_NONE_CSS, PROFILE_PIC_CSS);
            }
            if (testInfo.getCreatedAt() != null) {
                this.result = result.replace(MODIFIED_AT_PLACEHOLDER, formatValue(testInfo.getCreatedAt()));
                this.result = result.replace(MODIFIED_AT_HEADER, CREATED_AT_HEADER);
                this.result = result.replace(MODIFIED_AT_HEADER_CSS + " " + DISPLAY_NONE_CSS, MODIFIED_AT_HEADER_CSS);
            }
        }
        this.result = result.replace(FINISHED, formatValue(testInfo.getFinishedFormated()));
        this.result = result.replace(TEST_ID, formatValue(testInfo.getTestId()));
        this.result = result.replace(EXTERNAL_ID, formatValue(testInfo.getExternalId()));
        this.result = result.replace(TEST_DESCRIPTION, formatValue(testInfo.getDescription()));
        this.result = result.replace(PREREQUISITES, formatValue(testInfo.getPrerequisite()));
        this.result = result.replace(GROUPS, formatValue(groups));
        this.result = result.replace(TEST_NAME, buildTestNameLink(testInfo));
        this.result = StringUtils.replace(result, TEST_CASE_ID, testInfo.getTestName() + " " + suiteName);
        this.result = result.replace(REPORT_HTML_LINK_PLACE_HOLDER, buildReportsLinks(htmlReportPath, testInfo.getTestName(), suiteName));
        this.result = result.replace(EXECUTE_BUTTON_TEMPLATE, buildExecuteButton(testInfo, suiteName));
        this.result = result.replace(TEST_TITLE, testInfo.getTestName());

        if (isCommandLineExecution) {
            this.result = result.replace("test-case-body collapsed", "test-case-body");
            this.result = result.replace("actions-container collapsed", "actions-container");
            this.result = result.replaceAll("display-toggle-icon-right", "display-toggle-icon-down");
            this.result = result.replace("elementNodeListOf.item(0).click();", "");
        }

        if (testInfo.getSummary() != null) {
            this.result = result.replace(ACTIONS_COUNT, testInfo.getSummary().getActions());
            if (Long.parseLong(testInfo.getSummary().getDuration()) < 1000) {
                this.result = result.replace(EXECUTION_TIME_MIL, Long.parseLong(testInfo.getSummary().getDuration()) % 1000 + " ms");
                this.result = result.replace(ID_HIDDEN_PLACEHOLDER_MIN, "hidden");
                this.result = result.replace(ID_HIDDEN_PLACEHOLDER_SEC, "hidden");
                this.result = result.replace(ID_HIDDEN_PLACEHOLDER_HRS, "hidden");
            } else {
                this.result = result.replace(ID_HIDDEN_PLACEHOLDER_MIL, "hidden");
                this.result = result.replace(EXECUTION_TIME_SEC,  (Long.parseLong(testInfo.getSummary().getDuration()) / 1000) % 60 + " sec");
                this.result = result.replace(EXECUTION_TIME_MIN, ((Long.parseLong(testInfo.getSummary().getDuration()) / 1000) % 3600) / 60 + " min");
                if (Long.parseLong(testInfo.getSummary().getDuration()) > 3600000) {
                    this.result = result.replace(EXECUTION_TIME_HRS, Long.parseLong(testInfo.getSummary().getDuration()) / 3600000 + " hr");
                } else {
                    this.result = result.replace(ID_HIDDEN_PLACEHOLDER_HRS, "hidden");
                }
            }
            String formattedExecutionResult = "";
            if (testInfo.getSummary().getStatus().equals("1")) {
                formattedExecutionResult = REPORT_EXECUTION_RESULT_HTML_TEMPLATE.replace("${executionResult}", "result-pass-icon");
            } else {
                formattedExecutionResult = REPORT_EXECUTION_RESULT_HTML_TEMPLATE.replace("${executionResult}", "result-failed-icon");
            }
            this.result = result.replace(REPORT_EXECUTION_RESULT_HTML_TEMPLATE_PLACE_HOLDER, formattedExecutionResult);

            if (StringUtils.isNoneEmpty(testInfo.getSummary().getData())) {
                this.result = result.replace(EXECUTION_SUMMARY_HTML_PLACEHOLDER,
                        REPORT_SUMMARY_HTML_TEMPLATE.replace(REPORT_SUMMARY, testInfo.getSummary().getData().replaceAll("'", "&apos;")));
            } else {
                this.result = result.replace(EXECUTION_SUMMARY_HTML_PLACEHOLDER, "");
            }

            if (testInfo.getSummary().getTestErrors() != null && !testInfo.getSummary().getTestErrors().isEmpty()) {
                String uniqueErrorsBlockId = UUID.randomUUID().toString();
                String testErrorsNonBlocking = wrapNonBlockingErrors(testInfo.getSummary().getTestErrors().stream().filter(testError -> !TestError.TestErrorStatus.FAIL_STOP.equals(testError.getTestErrorStatus())).map((TestError testError) -> buildExecutionErrorElement(testError, testInfo)).collect(Collectors.joining()), uniqueErrorsBlockId);
                String testErrorsBlocking = buildExecutionErrorElement(testInfo.getSummary().getTestErrors().stream().filter(testError -> TestError.TestErrorStatus.FAIL_STOP.equals(testError.getTestErrorStatus())).findFirst().orElse(null), testInfo);
                this.result = result.replace(EXECUTION_ERROR_HTML_PLACEHOLDER, testErrorsNonBlocking + " " + testErrorsBlocking);
                this.result = result.replace(ERROR_ACTION_URL, HtmlActionUtils.getActionUrl(testInfo, testInfo.getSummary().getErrorScriptName(), testInfo.getSummary().getErrorLine()));
            } else {
                this.result = result.replace(EXECUTION_ERROR_HTML_PLACEHOLDER, "");
            }
        } else {
            this.result = result.replace(EXECUTION_SUMMARY_HTML_PLACEHOLDER, "");
            this.result = result.replace(EXECUTION_ERROR_HTML_PLACEHOLDER, "");
            this.result = result.replace(ACTIONS_COUNT, "No value");
            this.result = result.replace(EXECUTION_TIME_MIL, "No value");
            this.result = result.replace(REPORT_EXECUTION_RESULT_HTML_TEMPLATE_PLACE_HOLDER, "No value");
        }
    }

    private String getUserProfilePictureFromProjectProperties(String userName) {
        NodeList users = null;
        try {
            users = Project.getProjectProperties().getElementsByTagName("user");
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }
        String userImage = "";
        for (int i = 0; i < users.getLength(); i++) {
            boolean contains = users.item(i).getAttributes().getNamedItem("systemUser").getNodeValue().contains(userName);
            if (contains) {
                userImage = users.item(i).getAttributes().getNamedItem("pic").getNodeValue();
            }
        }
        return userImage;
    }

    private String wrapNonBlockingErrors(String nonBlockingErrors, String uniqueErrorsBlockId) {
        String nonBlockingErrorsWrapped = "<div class='non-blocking-errors-group'>\n" +
                "    <div class='frame-header frame-header-errors-group'>\n" +
                "        <div class='display-toggle-icon-right'\n" +
                "             onclick='toggleElement(&quot;${uniqueErrorsGroupId}&quot;, this, &quot;errors-group&quot;)'></div>\n" +
                "        Non-blocking actions failed\n" +
                "    </div>\n" +
                "    <div id=${uniqueErrorsGroupId} class='non-blocking-errors-items collapsed'>" + nonBlockingErrors + "</div>\n" +
                "</div>";
        nonBlockingErrorsWrapped = StringUtils.replace(nonBlockingErrorsWrapped, "${uniqueErrorsGroupId}", uniqueErrorsBlockId);
        return StringUtils.isEmpty(nonBlockingErrors) ? "" : nonBlockingErrorsWrapped;
    }

    @NotNull
    private String buildExecutionErrorElement(TestError testError, TestInfo testInfo) {
        if (testError == null) {
            return "";
        }
        String result = StringUtils.replace(EXECUTION_ERROR_HTML_TEMPLATE, EXECUTION_ERROR, buildExecutionErrorLine(testError));
        result = result.replace(ERROR_ACTION_ID, testInfo.getTestName() + ":" + testInfo.getSummary().getTestErrors().indexOf(testError));
        result = result.replace(ERROR_ACTION_URL, HtmlActionUtils.getActionUrl(testInfo, testError.getScript(), String.valueOf(testError.getLine())));
        result = result.replace(ERROR_TEXT_COLOR_PLACEHOLDER, TestError.TestErrorStatus.FAIL_PASS.equals(testError.getTestErrorStatus()) ? "error-text-brown" : "error-text-red");

        if (TestError.TestErrorStatus.FAIL_STOP.equals(testError.getTestErrorStatus())) {
            return result;
        } else if (TestError.TestErrorStatus.FAIL_PASS.equals(testError.getTestErrorStatus())) {
            return result.replace(TEST_EXECUTION_STATUS_NAME_CLASS, TEST_EXECUTION_STATUS_NAME_CLASS + " soft-orange-background-color").replace("Execution error", "Optional action").replace("this, true", "this, false");
        } else {
            return result.replace("Execution error", "Failed action").replace("this, true", "this, false");
        }
    }

    @NotNull
    private static String buildReportsLinks(String htmlReportLink, String testName, String suiteName) {
        String atsvLink = ""; // EXTERNAL_LINK_HTML_TEMPLATE.replace("${externalResourceUri}", suiteName + File.separator + testName + ".atsv").replace("${externalResource}", "ATSV");
        String atsvHtmlLink = EXTERNAL_LINK_HTML_TEMPLATE.replace("${externalResourceUri}", suiteName + File.separator + testName + "_atsv.html").replace("${externalResource}", "ATSV_HTML");
        if (htmlReportLink != null) {
            String path = suiteName + File.separator + testName + "_xml" + File.separator + "ats-test-report.html";
            htmlReportLink = EXTERNAL_LINK_HTML_TEMPLATE.replace("${externalResourceUri}", path).replace("${externalResource}", "HTML");
        } else {
            htmlReportLink = "";
        }

        return atsvLink + atsvHtmlLink + htmlReportLink;
    }

    @NotNull
    private static String buildGroupNameWithLink(TestInfo testInfo, String group) {
        return GROUP_NAME_HTML_TEMPLATE.replace("${groupUri}", testInfo.getProject().getProjectId() + "/group/" + group).replace("${groupName}", group);
    }

    private String buildTestNameLink(TestInfo testInfo) {
        return TEST_NAME_HTML_TEMPLATE.replace("${testNameUri}", testInfo.getProject().getProjectId() + "/script/" + testInfo.getTestName()).replace("${testName}", testInfo.getTestName()).replace("${testNameTitle}", testInfo.getTestName());
    }

    private String buildExecutionErrorLine(TestError testError) {
        return EXECUTION_ERROR_TEMPLATE
                .replace("${executionErrorLine}", String.valueOf(testError.getLine()))
                .replace("${executionErrorText}", testError.getMessage());
    }

    private String formatValue(String value) {
        return StringUtils.isEmpty(value) ?
                TABLE_NO_VALUE_TEMPLATE.replace("${valuePlaceholder}", "No value") :
                TABLE_VALUE_TEMPLATE.replace("${valuePlaceholder}", value);
    }

    private String buildExecuteButton(TestInfo testInfo, String suiteName) {
        return EXECUTE_BUTTON_HTML_TEMPLATE.replace("${scriptNameUri}", testInfo.getProject().getProjectId() + "/run-script/" + testInfo.getTestName() + "/" + suiteName).replace("${buttonLabel}", "Execute");
    }

    public String getResult() {
        return result;
    }
}
