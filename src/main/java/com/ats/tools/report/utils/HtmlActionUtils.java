package com.ats.tools.report.utils;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TestInfo;

public class HtmlActionUtils {

    public static String buildSearchedElementHeader(Action action) {
        switch (action.getError()) {
            case "-1":
                return "<div>Searched element</div><div class='searched-element-error'>element_not_found #-1</div>";
            case "-2":
                return "<div>Searched element</div><div class='searched-element-error'>element_not_visible #-2</div>";
            case "-3":
                return "<div>Searched element</div><div class='searched-element-error'>element_not_interactable #-3</div>";
            default:
                return "Searched element";
        }
    }

    public static String buildSearchedElementData(Action action) {
        return "<div style='margin-right: 5px;'>"+action.getActionElement().getCriterias()+ "<div style='font-size: 12px; color: rgba(104, 112, 125, 0.88); margin-top: 6px;'> <div>Elements found: "+ action.getActionElement().getFoundElements() + "</div><div> Search duration: " +action.getActionElement().getSearchDuration() +" ms</div></div></div>";
    }

    public static String getActionUrl(Action action, TestInfo testInfo) {
        return testInfo.getProject().getProjectId() + "/script/" + action.getScript() + "/" + action.getLine();
    }

    public static String getActionUrl(TestInfo testInfo, String scriptName, String line) {
        return testInfo.getProject().getProjectId() + "/script/" + scriptName + "/" + line;
    }

    public static String getReportType(int devReportLevel) {
        return switch (devReportLevel) {
            case 1 -> "Execution report";
            case 2 -> "Detailed execution report";
            case 3 -> "Detailed execution report with screenshots";
            default -> "Not available";
        };
    }

}
