package com.ats.tools.report.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HtmlReportActionApiRequest {
    private String method;
    private String type;
    private boolean cached;
    private int port;
    private List<Map.Entry<String, String>> headers;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isCached() {
        return cached;
    }

    public void setCached(boolean cached) {
        this.cached = cached;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Map<String, String> getHeaders() {
        Map<String, String> result = new HashMap<>();
        this.headers.forEach(stringStringEntry -> result.put(stringStringEntry.getKey(), stringStringEntry.getValue()));
        return result;
    }

    public void setHeaders(List<Map.Entry<String, String>> headers) {
        this.headers = headers;
    }


}
