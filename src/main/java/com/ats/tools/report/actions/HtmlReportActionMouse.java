package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;
import com.ats.tools.report.utils.HtmlActionUtils;

public class HtmlReportActionMouse {
    private static final String MOUSE_ACTION_HEADER = TemplateConstants.HEADER_ONE;
    private static final String MOUSE_ACTION = TemplateConstants.VALUE_ONE;
    private static final String ELEMENT_HEADER = TemplateConstants.HEADER_TWO;
    private static final String ELEMENT = TemplateConstants.VALUE_TWO;

    private String template;
    private String result;

    public HtmlReportActionMouse(String template, Action action, boolean isScroll) {
        this.template = template;
        this.result = template.replace(ELEMENT_HEADER, HtmlActionUtils.buildSearchedElementHeader(action));
        this.result = result.replace(MOUSE_ACTION_HEADER, "Mouse action type");
        if (action.getActionElement() != null) {
            this.result = result.replace(ELEMENT, HtmlActionUtils.buildSearchedElementData(action));

            if (isScroll) {
                this.result = result.replace(MOUSE_ACTION, (action.getValue().contains("-") ? "Scroll down " : "Scroll up "));
            } else if (action.getValue().contains("undefined")) {
                this.result = result.replace(MOUSE_ACTION, "Swipe");

            } else {
                this.result = result.replace(MOUSE_ACTION, action.getValue());
            }
        } else {
            this.result = result.replace(MOUSE_ACTION, "");
            this.result = result.replace(ELEMENT, "");
        }
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
