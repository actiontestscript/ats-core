package com.ats.tools.report.actions;

import com.ats.tools.Utils;
import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;

public class HtmlReportActionComment {

    private static final String COMMENT_HEADER = TemplateConstants.HEADER_ONE;
    private static final String COMMENT_VALUE = TemplateConstants.VALUE_ONE;
    private static final String BODY_STYLE_NAME_STANDARD = "'data-body-1-cell-cell'";
    private static final String BODY_STYLE_NAME_ACTION_COMMENT = "'data-body-1-cell-cell-action-comment'";

    private static final String ACTION_COMMENT_HTML_TEMPLATE = "<iframe class='action-comment-iframe' loading='lazy' srcdoc='${actionCommentValue}'></iframe>";
    private String template;
    private String result;

    public HtmlReportActionComment(String template, Action action) {
        this.template = template;
        this.result = template.replace(COMMENT_HEADER, "Comment");
        this.result = result.replace(COMMENT_VALUE, ACTION_COMMENT_HTML_TEMPLATE.replace("${actionCommentValue}", Utils.escapeHTML(action.getData())));
        this.result = result.replace(BODY_STYLE_NAME_STANDARD, BODY_STYLE_NAME_ACTION_COMMENT);
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
