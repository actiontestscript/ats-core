package com.ats.tools.report.actions;

import java.util.Map;
import java.util.stream.Collectors;

public class HtmlReportActionKeyValueTemplate {
    private static final String DATA_KEY_VALUE_MAIN_TEMPLATE = "<div style='display: flex;flex-direction: ${direction};margin-right: 10px;'>${dataProperties}</div>";
    private static final String DATA_KEY_VALUE_TEMPLATE =
            "<div style='display: flex;'>\n" +
                    "        <div class='data-action-property-value-header' style='display: flex;'>\n" +
                    "            ${dataKey}\n" +
                    "        </div>\n" +
                    "        <div style='display: flex;width: auto; overflow-wrap: anywhere; margin-right: 10px;'>\n" +
                    "            ${dataValue}\n" +
                    "        </div>\n" +
                    "    </div>";

    private static final String DATA_KEY_VALUE_INLINE_TEMPLATE = "<span class='inline-key-value-item'><span class='inline-key-value-item-header'>${dataKey}</span>${dataValue}</span>";

    public static String buildEntireKeyValueString(Map<String, String> keyValues, boolean isColumnDirection) {
        String items = keyValues.entrySet().stream().map((Map.Entry<String, String> values) -> buildKeyValueString(values, true)).collect(Collectors.joining());
        if (isColumnDirection) {
            return DATA_KEY_VALUE_MAIN_TEMPLATE.replace("${dataProperties}", items).replace("${direction}", "column");
        } else {
            return DATA_KEY_VALUE_MAIN_TEMPLATE.replace("${dataProperties}", items).replace("${direction}", "row");
        }
    }

    public static String buildEntireKeyValueInLineString(Map<String, String> keyValues) {
        return keyValues.entrySet().stream().map((Map.Entry<String, String> stringStringMap) -> buildKeyValueString(stringStringMap, true)).collect(Collectors.joining());
    }

    private static String buildKeyValueString(Map.Entry<String, String> values, boolean isInLine) {
        return isInLine ? processString(DATA_KEY_VALUE_INLINE_TEMPLATE, values) : processString(DATA_KEY_VALUE_TEMPLATE, values);
    }

    private static String processString(String template, Map.Entry<String, String> values) {
        return template.replace("${dataKey}", values.getKey() + ":").replace("${dataValue}", values.getValue().equals("NO_DATA_AVAILABLE") ? "Ø" : values.getValue());
    }

}
