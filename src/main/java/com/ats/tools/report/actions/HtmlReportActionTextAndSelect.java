package com.ats.tools.report.actions;

import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.TemplateConstants;
import com.ats.tools.report.utils.HtmlActionUtils;

public class HtmlReportActionTextAndSelect {
    private static final String ACTION_TEXT_HEADER = TemplateConstants.HEADER_ONE;
    private static final String ACTION_TEXT = TemplateConstants.VALUE_ONE;
    private static final String ELEMENT_HEADER = TemplateConstants.HEADER_TWO;
    private static final String ELEMENT = TemplateConstants.VALUE_TWO;

    private String template;
    private String result;

    public HtmlReportActionTextAndSelect(String template, Action action, boolean isSelect) {
        this.template = template;
        this.result = template.replace(ELEMENT_HEADER, HtmlActionUtils.buildSearchedElementHeader(action));
        if (action.getActionElement() != null) {
            this.result = result.replace(ELEMENT, HtmlActionUtils.buildSearchedElementData(action));
            if (isSelect) {
                this.result = result.replace(ACTION_TEXT_HEADER, "Selected option");
            } else {
                this.result = result.replace(ACTION_TEXT_HEADER, "Entered text");
            }
            this.result = result.replace(ACTION_TEXT, action.getValue());
        } else {
            this.result = result.replace(ELEMENT, "");
            this.result = result.replace(ACTION_TEXT, "");
            if (isSelect) {
                this.result = result.replace(ACTION_TEXT_HEADER, "Selected option");
            } else {
                this.result = result.replace(ACTION_TEXT_HEADER, "Entered text");
            }

        }
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
