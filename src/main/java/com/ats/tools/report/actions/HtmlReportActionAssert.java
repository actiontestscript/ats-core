package com.ats.tools.report.actions;

import com.ats.tools.Operators;
import com.ats.tools.Utils;
import com.ats.tools.logger.levels.AtsLogger;
import com.ats.tools.report.models.Action;
import com.ats.tools.report.models.OperatorsWithIcons;
import com.ats.tools.report.utils.HtmlActionUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

public class HtmlReportActionAssert {

    private static final String SEARCHED_ELEMENT = "${searchedElement}";
    private static final String PROPERTY_NAME = "${propertyName}";
    private static final String OPERATION = "${operation}";
    private static final String ASSERT_PROPERTY_HTML_TEMPLATE = "<div>${actual}</div>${operator}<div>${expected}</div>";

    private String template;
    private String result;

    public static enum ActionAssertOperation {
        ASSER_VALUE, ASSERT_COUNT, ASSERT_PROPERTY
    }

    public HtmlReportActionAssert(String template, Action action, ActionAssertOperation actionAssertOperation) {
        ObjectMapper objectMapper = new ObjectMapper();
        HtmlReportActionAssertCondition htmlReportActionAssertCondition = null;
        try {
            if (actionAssertOperation.equals(ActionAssertOperation.ASSERT_PROPERTY) && StringUtils.isNoneEmpty(action.getValue())) {
                htmlReportActionAssertCondition = objectMapper.readValue(action.getValue(), HtmlReportActionAssertCondition.class);
                htmlReportActionAssertCondition.setActual(action.getData());
            }
        } catch (JsonProcessingException e) {
            AtsLogger.printLog("cannot parse assert property data.");
        }
        this.template = template;

        this.result = template.replace(OPERATION, buildOperationTemplate(htmlReportActionAssertCondition, actionAssertOperation, action));
        if (action.getActionElement() != null) {
            this.result = result.replace(SEARCHED_ELEMENT, HtmlActionUtils.buildSearchedElementData(action));
        }

        if (htmlReportActionAssertCondition != null) {
            this.result = result.replace(PROPERTY_NAME, htmlReportActionAssertCondition.getName());
        } else {
            this.result = result.replace(PROPERTY_NAME, "");
            this.result = result.replace(SEARCHED_ELEMENT, "");
        }

    }

    private CharSequence buildOperationTemplate(HtmlReportActionAssertCondition htmlReportActionAssertCondition, ActionAssertOperation actionAssertOperation, Action action) {
        if (actionAssertOperation.equals(ActionAssertOperation.ASSERT_COUNT) || actionAssertOperation.equals(ActionAssertOperation.ASSER_VALUE)) {
            return buildCheckOccurrenceAndValueData(action);
        } else {
            return htmlReportActionAssertCondition == null ? "" : buildCheckPropertyData(htmlReportActionAssertCondition);
        }
    }

    private static String buildCheckOccurrenceAndValueData(Action action) {
        if (StringUtils.isEmpty(action.getValue()) && StringUtils.isEmpty(action.getData())) {
            return "";
        }

        String expected = "Ø";
        String[] operatorValueArray = action.getData().split(" ");
        if (operatorValueArray.length > 1) {
            expected = operatorValueArray[1].equals("NO_DATA_AVAILABLE") ? "Ø" : Utils.escapeMathSymbolsOnce(action.getData());
        }
        String operator = OperatorsWithIcons.getIconForOperator(Operators.getJavaCode(operatorValueArray[0]));
        String actual = action.getValue();

        return ASSERT_PROPERTY_HTML_TEMPLATE.replace("${expected}", expected).replace("${actual}", actual).replace("${operator}", operator);
    }

    private static String buildCheckPropertyData(HtmlReportActionAssertCondition htmlReportActionAssertCondition) {
        String operator = OperatorsWithIcons.getIconForOperator(htmlReportActionAssertCondition.getOperator());
        String actual = StringUtils.isEmpty(htmlReportActionAssertCondition.getActual()) || htmlReportActionAssertCondition.getActual().equals("NO_DATA_AVAILABLE") ? "Ø" : htmlReportActionAssertCondition.getActual();
        String expected = htmlReportActionAssertCondition.getExpected();

        return ASSERT_PROPERTY_HTML_TEMPLATE.replace("${expected}", expected).replace("${actual}", actual).replace("${operator}", operator);
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResult() {
        return result;
    }
}
