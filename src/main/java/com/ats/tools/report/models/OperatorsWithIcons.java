package com.ats.tools.report.models;

import com.ats.tools.Operators;

import java.util.HashMap;
import java.util.Map;

public class OperatorsWithIcons {

    private static final Map<String, String> operatorsWithIcons;

    static {
        operatorsWithIcons = new HashMap<>();
        operatorsWithIcons.put("Operators.EQUAL", "<div class='complex-operator'><div class='equals-icon'></div></div>");
        operatorsWithIcons.put("Operators.LOWER", "<div class='complex-operator'><div class='lower-icon'></div></div>");
        operatorsWithIcons.put("Operators.DIFFERENT", "<div class='complex-operator'><div class='lower-icon'></div><div class='greater-icon'></div></div>");
        operatorsWithIcons.put("Operators.GREATER", "<div class='complex-operator'><div class='greater-icon'></div></div>");
        operatorsWithIcons.put("Operators.GREATER_EQUAL", "<div class='complex-operator'><div class='greater-icon'></div><div class='equals-icon'></div></div>");
        operatorsWithIcons.put("Operators.LOWER_EQUAL", "<div class='complex-operator'><div class='lower-icon'></div><div class='equals-icon'></div></div>");
        operatorsWithIcons.put("Operators.REGEXP", "<div class='complex-operator'><div class='regexp-icon'></div></div>");
        operatorsWithIcons.put("ARROW", "<div class='complex-operator'><div class='arrow-icon'></div></div>");
    }

    public static String getIconForOperator(Operators operator) {
        return operatorsWithIcons.get(operator.getType());
    }

    public static String getIconForOperator(String operator) {
        return operatorsWithIcons.get(operator);
    }


}
