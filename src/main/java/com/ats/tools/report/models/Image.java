package com.ats.tools.report.models;

import javax.xml.stream.XMLStreamReader;

public class Image {

    private int height;
    private int width;
    private String id;
    private String src;
    private String type;
    private static final String apiIcon = "iVBORw0KGgoAAAANSUhEUgAAAMoAAACYCAYAAAClI3ZUAAAPUElEQVR4Ae2d308UWRbHb7fQ7ayt0UHdZEezIBt/BRhRHxzNimSMqw86PrAv68Pu+k/tmzMv7osmq2My7pidCCaOPqgYICgkIkYyuyMgBHCWbkWmvre8RVFdP251VWF39feTEJCuqq6W+61zz7nnnpMRLvT3959Yt27dX7PZ7Inl5eVmQUj6eWyM9cfG9+sdHR3XnC9m7P8wBNLc0NDw9Sef/OZEoVAQhY2bRGNjoyAk7SwuLopicVFMT0+Jt6XS+NLSUndnZ+e4et0SytDQ0IFMJnO7aeu2zVu2fCoIqVdmZl6L6anJWcPCdLe1tcHKmEKBJcnlcv07dv5+8/r16wUh9Q4szMTLF7OlUqkTliWLXxr+iLQkFAkhJtACNAFXBP/ODAwMnDesyb9adv1BEEJW8/LlCzE/N9edNeZh55uatgpCSDmFDRvFhwhw9vP8+k8EIaScwsaN+HYCPsqBfD4vCCHlYHnEiAY3ZwUhJBAKhRANKBRCNKBQCNGAQiFEAwqFEA0oFEI0oFAI0YBCIUQDCoUQDSgUQjRoEKTmmfjplZibXxA7fvdbsWnjBt/jisWS2LZ1i+9xpBwKpYYpFt+Kvh8fiuGRMet3PedOGoLZXnZs392Hon9wRP4MkRw53C7279kliB6cetUgEMj9B4Pi0j+vrRIJuHX7Xtnxz55PWCIBc/NvjOPui0uXr4vJ6VlBgqFQagwM8stXv5NCgWDcXsdrdmB1PK915TvP18kKnHrVGBABBrgfTusRePzAiGht3uk6ZSMmFEqNAWdc5xinVQk8pxR83XqGU68ao7Njj4gbOPetzTsE8YZCqTEQAo57ioQIGPGHQqlBTnUfEXHR2rKDYWINKJSaJCPiwi1yRsqhM1+DuK2VKOBvwI9BFAtgxR5rLcMjz12Pn/jpZ7liz4iXP5nBwcHl3Xv2CVKd9A8+NQbypBXtwvfJ6RnXYzvb94iuY4dcX4Ngrn77g2uoGOLatLFg/Tufz4muoweZ5vKB0ZEntCjVDCxB391HWsfu39PiKRIAIfSc+9JYrLxZNt1yW2uZnHotLvSckaIh9FGqmsmpGe1jdSJXEAusjg4QTrFE/0VBoVQxugPVzBouaB2rfBcSDgolBYTxJZBiT8JDodQZOikwpBwKJQU8G5/QPhbhYBIeCqWKyef0Gs3CSmAtRAd7ZnFc718PUChVDBYOdf0PLEIGpdPffzCgLShExxgaXoELjlWOWmC0iwAbrdxST7y2+Jo7Igc8rUlry2eromFw+Lc10elXcMGxBsBTHeFfO1hlv/9gqOxYtcUXe1FwTj6XE3MLbwwr8j/fnK6uo4e0w8v1CoVSgxw53OEqFAUE49xL7wWyhymSYOij1CC6ItBBlTAi/lAoNYZb8YgoyG3DD+O7XlqhUGqM4ZFngdGtsKC4BK2KPxRKjTE5lUwdron/6oWN6xU682sAIk7YVwLfIsga5PONRmj2U5k277ZFFxusglbiEdo9e/q4/FntQwl+z82CeEOhJAyEcfXb/2hPlyAqc9fhzzJ72JkW39mxV37HmojXNc+e/qO1UCl3PLbvNo4fdT0Wwus6dpCRrwA49UoYnYJ1fue6+Q4QCzZhweo4we+cgx7hZFgNO/g3ilSgVjEsGPGHQkkY7BSsFIjEa5oFMZzq/kJcvHBOWgUMfGkdjpbvcsSi5akTR6R1wXFHDreJi3/5itVXQsCpV8IkvUvQ3OJ7MvC41pad8otUBi0KIRpQKIRoQKEQogGFEhJEsKqx+Y4ZVn7Fyo8JQWdeE3StutV7zxqI1dLeTaXW27f44p5QwI4br+KDG7c0gEhufH/H9TUIBoXnnG0T8HTHOkjUPeoI52JTFURp3+2oVvu9EiSxHwVrLSQ63LiliV/rNjzRb/z7jnyKqyJ0zid8FCAIpL7gesqCKYH4TbNwPFbvdQveEX9oUQLAFlq/TVJOYAGS9BOQx+VVe7j8XnLGwuI5TsEiAotCZ94Hc6fg81DnJO1M64oEYGU/TNUV4g2F4kOUPK1qIQ2foRqgUDwIs++82oHPRKJBoXgABz0tqGZBpHIoFBdgScL4ArWAX5cuEgzDwy7EWbzBDayHmK0azHURVeQuyae+mkoytb4yKBQHbt2n4kJt73UWtFOo/SdJOeAQIoVSGRTKGgDLgd2EXgJRYL0DAxmr/GYJ1FFBqgP6KA4wqOMstIDrIZUkSCR2ZLPRY4flTsQ4YeffyqFQXPBrGhoGrNJDJJUWbsBedxSGiAOs6HPaVTkUigsqoTBq+2gzkTFadRO3whBhQb4XEySjQaF4ALFcvPBVxf3WzTJBe0VUMA2rNLERU60Lfz4tLSTzvaJBZz4AlAZCxXdEosLkfcXpD0BwyNnSzSPTDR4QfWhRNFClgS70nNE+Z/+eVhEXsAa6jX2kFTHukyKJFwolIeIuUarr6+A4TrPih0IJQbGkX/E97sGq6ycxUzgZKJQQoNWbLnG3UWBbho8LhRKCMFYCvRPjRNdSRA1pE3coFE2QJxVmX0fcCY662cwqV4zEC8PDAQS1nvYClVviKuyA/ST6bSNKH0LZY1VRTiktUCg+YMCh4kkl++DVZqk41lPC7tsHqt4XzsWaCqdk0eDUywUMsstXbgaWBAqi7+5DERVYhihbkiHYS5evs8hERCgUFyCQOHY44hp+NcGCgGCjnG8Hok3brs21hEJxgDl+nEUl0HG3ksGOQY2WdnGWPxp+mo5iGR8D+igOkmj8A7HAuUcaTJDPElQqNQpJNzVKMxSKg3yuMZFqj6rpKXKwWls+k30T7Q725JS5Z3549FliRfTo0FcOheJApbWHKaMaBtXx92Pg1hyV6EEfxQWktUfdLFVtxLGJrJ6hUFyAVTlyqF2kBXMTGavaR4FC8QAbttIyp4c1Yep9NCgUH7CiXetA7ExjiQ6F4gMiVLVe4odFJeKBQglA16rA+UcdrrhrcdmBaMNUh0GUiw58PDA8HAAGGga/X7gY6yJdRw9ZgxJTnbDFKPwwRdhuVXVBdRhkD/iVXlXnkHhgazoNkNZyq/e+XF23gyc8BqNXIYe5+QVx+erNSAuIsAoQoZszbnbUelomYojk1IkvZPUYEh02O9UEg/Tsn44bQnlpbcgyV9j9B6Is9JDLRRIK3scrYiXD2Ic7pAWD9YJ12bZ1s9i/exejXDFDoYSgtWWn/Ko2zOkhp1lJQmeeEA0oFEI0oFAI0YBCIUQDCiVhouaLccGwOqBQEiZKNArrNOySVR1QKAmDdZBKeqxAIGlIykwLXEdZA5Cyj0VBFIzQKWS3besW7TYPZG2gUNYIrJSzZ0ntwqkXIRpQKIRoQKEQogGFQogGFAohGlAohGhAoRCiQV2to9jbxWGl3Gu13KutnNnvvbwt9uT0rNWMFK9X2+5CWdN4ZEyu9rN0UWXUjVBQBBtFshWonNh17FDZcaqYtheqTpY9h6vv7gNLXD3nTlZVfhYErD4PxIIkS+aPhaduhOLsODU8+ryiCooQkmrJUE3bb1H4W1V9cVqOJKrz1xt1IxRVQV4NGjxp0UHXbyoC6wELoUBxib4fH8mfIRZYpWqZZkHA9gZI6nPh/nrOnpSvIYeM1qQy6kIoGOAqGREVSpR1wRM4aM5u92OQ3AhxqWlWkND8wP1Y4s3l5CD2yzDGe6L8kXlPq6dPuJa9SRB+xu9wXTwYZCuLjr3a92JWflldzV8+XEqlD++/wbonPHA2bSq4+m5poi6EYi9Eh+nS8OiY/MNjcOAPHcYqmBupKu8hr7r1uvVIgeiQkm+/H9QSu9V7r2zqhMF69vRxmWUMH8SelYwHA77wWfFlfx3F89RA97sXda7C3gVM/h8aFsr+nm73niZSHx7GHxNPfoBicvhDwqoownbLnZx6bf1cye5DDFo1MGEVMH1TAxeDz34/eGLf+P6OJRI8tdWT2ww6/KDdf96JClrYp6R2iwZReLXHUxUq7VbHee9pI/UWxf60VDW5ULhuZfo15uuUq4GI6czw02cyFAwwqMJON9S15LnGVAtF9dT9YNADiFrdz63b96xzu44dtEqqIsrWPzhqNWZVJVZhHQCe7kGbvuzlWFESFpUl8RCxV7f08sNM3+1L+aBQ9wLS3HU49ULpH3gqv+OP29psVnZUc3AMBnN+/srVycVr6NHuRmdHeEce94BBLd93YcX5Vmsw9p/xXQ1ks13eXtt77zN+l5c/V7rBy24ZUW1SfRYM/lV+nBEddDYhMnvHFKx7sYQyRaHUJBhoygLASbVHhTDALKfcmM/rRoOC6g0Hoeb6QeFa+9PZKQYILmpoWv2/uF3f/gCYm1soOxcNYeuNVAvFPsfGwFNTEydeayrO8HDUiioQat/dR9a1pFWStYlLVthZgd8rVLQpKcIGNOqRVAvF7qjaB54CAzBoTSXO9nT2avj2nvNuUxb7+zoddogevetBpWkpuL66LqyLM9ysgC9FUiwUiMRyVg3fBAPTyf0HA1bLBJ01lbjvTw3OZ+Mvy14399hvt9YqYB3VdGv46Zg1jZTTIEcfU51IGCKA6rP33X0oQ80Qj8oLU3CB0iS1QrGvnXgttsFBVoNFrakkCaJbKlSNga8WEb0GNnLREMJVESj4N0D5N2r6Jq/dvNKCAp/lH19fkZ/Py5fBa6pVBCwUghbOVBe23F4hlesoKmwKzDCu+/RBPbUVSa8DmMmUK63rpDiN6R/CxOo+MHCVcHDfF3rOWGFoc5poDuSVNnUF67PYrx0ULJCpLcb5sCzOcyAYhKPZSmIFdtz6CEDIKuVE1weyWx6Iw+s8XBsWAkLYVNig5aSrc4C5lYB+iR123PpIYDCGX4MpaE2DKqkfxppjwXCHIyEaUCiEaEChEKIBhUKIBhQKIRpQKIRoQKEQogGFQogGFAohGlAohGhAoRCiAYVCiAYUCiEaUCiEaAChvHj3lgWcCXGjWFwUmUzmcXZ5ebl3fmFeEELKWVws4tvjrKGWbxYoFEJcmZ6eFO/fv7+ebW9v7/3/L7/0zsykt8ofIZUwM/NaLL17N97R0XFNOvMNDQ1/n556NVssFgUhBFOuRcOaTM2uW7euG//OqBeGhoYOGNOw201bt2/esoXFBUj9AksCkSy/f9/d1tb2GL/L2A948uRJ89LS0m3DwjQ3NW0T+fXrrWLQhKQZRH7n5+fFmzf4etPX2Nj4t3379o2r1zNuJw0MDJw3vuELVuZzQUj6eWE47b3ZbPYb+O3OF38FHcwyY9L+BP4AAAAASUVORK5CYII=";

    public Image(XMLStreamReader reader) {
        for (int i = 0; i < reader.getAttributeCount(); i++) {
            String attributeName = reader.getAttributeName(i).getLocalPart();
            switch (attributeName) {
                case "height": {
                    this.height = Integer.parseInt(reader.getAttributeValue("", attributeName));
                    break;
                }
                case "width": {
                    this.width = Integer.parseInt(reader.getAttributeValue("", attributeName));
                    break;
                }
                case "id": {
                    this.id = reader.getAttributeValue("", attributeName);
                    break;
                }
                case "src": {
                    this.src = reader.getAttributeValue("", attributeName);
                }
                case "type": {
                    this.type = reader.getAttributeValue("", attributeName);
                    break;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Image{" +
                "height=" + height +
                ", width=" + width +
                ", id='" + id + '\'' +
                ", src='" + src + '\'' +
                ", type='" + type + '\'' +
                '}';
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id != null) {
            this.id = id;
        }
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        if (src != null) {
            this.src = src;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void useApiIcon() {
        this.src = apiIcon;
        this.type = "png";
    }
}