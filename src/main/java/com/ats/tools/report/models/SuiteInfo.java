package com.ats.tools.report.models;

public class SuiteInfo {
    private String name;
    private int testsFailed;
    private int testsPassed;
    private int testsFiltered;
    private boolean nativeSuite;
    private long duration;
    private long started;
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTestsFailed() {
        return testsFailed;
    }

    public void setTestsFailed(int testsFailed) {
        this.testsFailed = testsFailed;
    }

    public int getTestsPassed() {
        return testsPassed;
    }

    public void setTestsPassed(int testsPassed) {
        this.testsPassed = testsPassed;
    }

    public int getTestsFiltered() {
        return testsFiltered;
    }

    public void setTestsFiltered(int testsFiltered) {
        this.testsFiltered = testsFiltered;
    }

    public boolean isNativeSuite() {
        return nativeSuite;
    }

    public void setNativeSuite(boolean nativeSuite) {
        this.nativeSuite = nativeSuite;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getStarted() {
        return started;
    }

    public void setStarted(long started) {
        this.started = started;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
