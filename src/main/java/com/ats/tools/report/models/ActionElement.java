package com.ats.tools.report.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActionElement {
    private String tag;
    private String criterias;
    private int foundElements;
    private int searchDuration;
    private Bound bound;
    private static final String ELEMENT_INDEX_TEMPLATE = "<div class=\"index-icon\">${elementIndex}</div>";

    @Override
    public String toString() {
        return "Element{" +
                "tag='" + tag + '\'' +
                ", criterias='" + criterias + '\'' +
                ", foundElements=" + foundElements +
                ", searchDuration=" + searchDuration +
                ", bound=" + bound +
                '}';
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCriterias() {
        String pattern = "\\[(-?\\d+)\\]$";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(this.criterias);

        if (m.find()) {
            String indexFull = m.group();
            String index = m.group(1);
            String criteriaWithIndex = criterias.replace(indexFull, ELEMENT_INDEX_TEMPLATE.replace("${elementIndex}", index));
            criteriaWithIndex = "<div style='display: flex; align-items: center;'><div>" + criteriaWithIndex.replace("<div ", "</div><div ") + "</div>";
            return criteriaWithIndex;
        } else {
            return criterias;
        }
    }

    public void setCriterias(String criterias) {
        this.criterias = criterias;
    }

    public int getFoundElements() {
        return foundElements;
    }

    public void setFoundElements(int foundElements) {
        this.foundElements = foundElements;
    }

    public int getSearchDuration() {
        return searchDuration;
    }

    public void setSearchDuration(int searchDuration) {
        this.searchDuration = searchDuration;
    }

    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }
}
