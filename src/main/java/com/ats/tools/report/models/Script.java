package com.ats.tools.report.models;

public class Script {
    private String name;
    private String suite;
    private String duration;
    private boolean passed;
    private String actions;

    public Script() {
    }

    public Script(String name, String suite, String duration, boolean passed, String actions) {
        this.name = name;
        this.suite = suite;
        this.duration = duration;
        this.passed = passed;
        this.actions = actions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }
}
