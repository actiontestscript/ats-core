package com.ats.tools.report.models;

public class TemplateConstants {

    public static final String HEADER_ONE = "${header1}";
    public static final String HEADER_TWO = "${header2}";
    public static final String HEADER_THREE = "${header3}";
    public static final String HEADER_FOUR = "${header4}";
    public static final String HEADER_FIVE = "${header5}";

    public static final String VALUE_ONE = "${value1}";
    public static final String VALUE_TWO = "${value2}";
    public static final String VALUE_THREE = "${value3}";
    public static final String VALUE_FOUR = "${value4}";
    public static final String VALUE_FIVE = "${value5}";

}
