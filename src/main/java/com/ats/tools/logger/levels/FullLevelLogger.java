package com.ats.tools.logger.levels;

import java.io.PrintStream;

public class FullLevelLogger extends WarningLevelLogger {

	public FullLevelLogger(PrintStream out, String type, String level) {
		super(out, type, level);
	}

	@Override
	public void driverOutput(String value) {
		out.println(logType.getDriverOutput() + value);
	}
}