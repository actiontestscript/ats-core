/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools.logger.levels;

import com.ats.script.actions.Action;
import com.ats.tools.logger.ExecutionLogger;

@SuppressWarnings("serial")
public class AtsFailError extends AssertionError {

	private String info;
	private String details;

	public AtsFailError(Action action, String testName, int line, String info, String details, ExecutionLogger logger) {
		super(action.getClass().getSimpleName() + " (" + testName + ":" + line + ")" + ExecutionLogger.RIGHT_ARROW_LOG + details);
		this.info = info + " (Executed action :" + action.getClass().getSimpleName() + ")";
		this.details = details;
	}

	public AtsFailError(String actionName, String testName, int line, String info, String details, ExecutionLogger logger) {
		super(actionName + " (" + testName + ":" + line + ")" + ExecutionLogger.RIGHT_ARROW_LOG + details);
		this.info = actionName + " (" + line + ")";
		this.details = info;
	}

	public String getInfo() {
		return info;
	}

	public String getFullMessage(ExecutionLogger logger) {
		return info + ExecutionLogger.RIGHT_ARROW_LOG + details;
	}
}