package com.ats.tools.logger.levels;

import java.io.PrintStream;

public class ErrorLevelLogger extends AtsLogger {

	public ErrorLevelLogger(PrintStream out, String type, String level) {
		super(out, type, level);
	}

	@Override
	public void error(String message, String value) {
		printError(message, value);
	}

	@Override
	public void driverOutput(String value) {
	}
}