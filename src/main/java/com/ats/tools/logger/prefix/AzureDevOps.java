package com.ats.tools.logger.prefix;

import com.ats.executor.ScriptStatus;
import com.ats.tools.logger.ExecutionLogger;

public class AzureDevOps implements ILogType{

	@Override
	public String getLoggerData() {
		return "";
	}

	@Override
	public String getScriptData(String value) {
		return "";
	}
	
	@Override
	public String getTestData(String value) {
		if(value != null) {
			if(value.startsWith(ScriptStatus.TEST_STARTED)) {
				return "##[group]";
			}else if(value.startsWith(ScriptStatus.TEST_TERMINATED)) {
				return value + "\n##[endgroup]";
			}
		}
		return "";
	}

	@Override
	public String getErrorData() {
		return "##[error]";
	}

	@Override
	public String getFailData(String actionName, String testName, int line, String info, String details) {

		final StringBuilder sb =
				new StringBuilder("##vso[task.logissue type=warning;sourcepath=")
				.append(testName)
				.append(";linenumber=")
				.append(line)
				.append(";columnnumber=1;code=-1;]")
				.append(actionName)
				.append(ExecutionLogger.RIGHT_ARROW_LOG)
				.append(info)
				.append(ExecutionLogger.RIGHT_ARROW_LOG)
				.append(details);

		return sb.toString();
	}

	@Override
	public String getInfoData() {
		return "##[debug]";
	}

	@Override
	public String getWarningData() {
		return "##[warning]";
	}

	@Override
	public String getActionData() {
		return "##[command]";
	}

	@Override
	public String getCommentData(String message) {
		return message;
	}

	@Override
	public String getDriverLog() {
		return "driver:";
	}

	@Override
	public String getDriverWarning() {
		return "##[warning]driver:";
	}

	@Override
	public String getDriverError() {
		return "##[error]driver:";
	}

	@Override
	public String getDriverOutput() {
		return "##[debug]driver:";
	}

	@Override
	public String getSuiteData() {
		return "##[section]";
	}
}