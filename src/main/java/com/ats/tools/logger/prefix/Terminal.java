package com.ats.tools.logger.prefix;

import com.ats.tools.logger.ExecutionLogger;

public class Terminal implements ILogType{

	protected static String ERROR;
	protected static String FAILED;
	protected static String EXEC;
	protected static String SUITE;
	protected static String TEST;
	protected static String LOGGER;
	protected static String INFO;
	protected static String WARNING;
	protected static String ACTION;
	protected static String COMMENT;
	protected static String DRIVER_INFO;
	protected static String DRIVER_WARNING;
	protected static String DRIVER_ERROR;
	protected static String DRIVER_OUTPUT;

	static {
		String ATS = "ATS-";
		ERROR = ATS + "ERROR";
		FAILED = ATS + "FAILED";
		EXEC = ATS + "EXEC";
		SUITE = ATS + "SUITE";
		TEST = ATS + "TEST";
		LOGGER = ATS + "LOGGER";
		INFO = ATS + "INFO";
		WARNING = ATS + "WARNING";
		ACTION = ATS + "ACTION";
		COMMENT = ATS + "COMMENT";
		DRIVER_INFO = ATS + "DRIVER-INFO";
		DRIVER_WARNING = ATS + "DRIVER-WARNING";
		DRIVER_ERROR = ATS + "DRIVER-ERROR";
		DRIVER_OUTPUT = ATS + "DRIVER-OUTPUT";
	}

	protected String getBracketLog(String log) {
		return "[" + log + "] ";
	}

	@Override
	public String getErrorData() {
		return getBracketLog(ERROR);
	}

	@Override
	public String getFailData(String actionName, String testName, int line, String info, String details) {
		final StringBuilder sb =
				new StringBuilder(getBracketLog(FAILED))
				.append(actionName)
				.append(" (")
				.append(testName)
				.append(":")
				.append(line)
				.append(")")
				.append(ExecutionLogger.RIGHT_ARROW_LOG)
				.append(info);
		return sb.toString();
	}

	@Override
	public String getScriptData(String value) {
		return getBracketLog(EXEC);
	}
	
	@Override
	public String getTestData(String value) {
		return getBracketLog(TEST);
	}

	@Override
	public String getLoggerData() {
		return getBracketLog(LOGGER);
	}

	@Override
	public String getInfoData() {
		return getBracketLog(INFO);
	}

	@Override
	public String getWarningData() {
		return getBracketLog(WARNING);
	}

	@Override
	public String getActionData() {
		return getBracketLog(ACTION);
	}

	@Override
	public String getCommentData(String message) {
		final StringBuilder sb =
				new StringBuilder(getBracketLog(COMMENT))
				.append(" <![CDATA[\n")
				.append(message)
				.append("\n]]>");
		return sb.toString();
	}

	@Override
	public String getDriverLog() {
		return getBracketLog(DRIVER_INFO);
	}

	@Override
	public String getDriverWarning() {
		return getBracketLog(DRIVER_WARNING);
	}

	@Override
	public String getDriverError() {
		return getBracketLog(DRIVER_ERROR);
	}

	@Override
	public String getDriverOutput() {
		return getBracketLog(DRIVER_OUTPUT);
	}

	@Override
	public String getSuiteData() {
		return getBracketLog(SUITE);
	}
}