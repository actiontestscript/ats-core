/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools.wait;

import com.ats.executor.channels.Channel;
import com.ats.executor.drivers.engines.WebDriverEngine;

public class WaitGuiReadyWithValues implements IWaitGuiReady{

	private int waitEnterText = 0;
	private int waitSearch = 0;
	private int waitMouseMove = 0;
	private int waitSwitchWindow = 0;
	private int waitGotoUrl = 0;

	public WaitGuiReadyWithValues(int waitEnterText, int waitGotoUrl, int waitMouseMove, int waitSearch, int waitSwitchWindow) {
		this.waitEnterText = waitEnterText;
		this.waitGotoUrl = waitGotoUrl;
		this.waitMouseMove = waitMouseMove;
		this.waitSearch = waitSearch;
		this.waitSwitchWindow = waitSwitchWindow;
	}

	@Override
	public void waitBeforeSearchElement(Channel channel, WebDriverEngine engine) {
		channel.sleep(waitSearch);
	}

	@Override
	public void waitBeforeMouseMoveToElement(Channel channel, WebDriverEngine engine) {
		channel.sleep(waitMouseMove);
	}

	@Override
	public void waitBeforeSwitchWindow(Channel channel, WebDriverEngine engine) {
		channel.sleep(waitSwitchWindow);
	}

	@Override
	public void waitBeforeEnterText(Channel channel, WebDriverEngine engine) {
		channel.sleep(waitEnterText);
	}

	@Override
	public void waitBeforeGotoUrl(Channel channel, WebDriverEngine engine) {
		channel.sleep(waitGotoUrl);
	}

	@Override
	public String toString() {
		return this.getClass().getCanonicalName();
	}
}