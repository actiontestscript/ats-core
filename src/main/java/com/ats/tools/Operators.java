/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.tools;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;

import com.ats.AtsSingleton;
import com.ats.element.AtsBaseElement;
import com.ats.generator.variables.CalculatedValue;

public final class Operators {

	public static final String REGEXP = "=~";
	public static final String DIFFERENT = "<>";
	public static final String LOWER_EQUAL = "<=";
	public static final String GREATER_EQUAL = ">=";

	public static final String EQUAL = "=";
	public static final String LOWER = "<";
	public static final String GREATER = ">";
	
	public static final String EQUAL_STRICT = "==";
	public static final String LOWER_STRICT = "<<";
	public static final String GREATER_STRICT = ">>";
	public static final String NOT_EQUAL = "!=";
	
	public static final List<String> COMPARAISON_TYPE = Arrays.asList(EQUAL_STRICT, NOT_EQUAL, LOWER_EQUAL, GREATER_EQUAL, LOWER_STRICT, GREATER_STRICT);

	public static final Pattern REGEXP_PATTERN = Pattern.compile("(.*)" + REGEXP + "(.*)");
	public static final Pattern EQUAL_PATTERN = Pattern.compile("(.*)" + EQUAL + "(.*)");

	private static final Trie trieOperator = Trie.builder().ignoreOverlaps().addKeywords(REGEXP).addKeywords(DIFFERENT).addKeyword(GREATER_EQUAL).addKeyword(LOWER_EQUAL).addKeyword(EQUAL).addKeyword(LOWER).addKeyword(GREATER).build();
	private static final String numCompareError = "cannot be compared as number with";
	private static final int regexTimeOut = AtsSingleton.getInstance().getRegexTimeOut();

	private Pattern regexpPattern;
	private String type = EQUAL;

	public Operators() {}

	public Operators(String value) {
		setType(value);
	}

	public Predicate<AtsBaseElement> getPredicate(Predicate<AtsBaseElement> predicate, String name, CalculatedValue value) {
		if(REGEXP.equals(getType())){
			return predicate.and(p -> safeRegexpMatch(p.getAttribute(name)));
		}else {
			return predicate.and(p -> textEquals(p.getAttribute(name), value.getCalculated()));
		}
	}

	public void updatePattern(String value) {
		try {
			regexpPattern = Pattern.compile(value);
		}catch(PatternSyntaxException e) {
			regexpPattern = Pattern.compile(".*");
		}
	}

	public String check(String data, String calculated) {
        switch (getType()) {
            case REGEXP -> {
                if (!safeRegexpMatch(data)) {
                       return "does not match regex pattern";
				}
            }
            case DIFFERENT -> {
                if (textEquals(data, calculated)) {
                    return "is not different than";
                }
            }
            case GREATER -> {
                try {
                    if (getDoubleNumValue(data) <= getDoubleNumValue(calculated)) {
                        return "is not greater than";
                    }
                } catch (NumberFormatException e) {
                    return numCompareError;
                }
            }
            case LOWER -> {
                try {
                    if (getDoubleNumValue(data) >= getDoubleNumValue(calculated)) {
                        return "is not lower than";
                    }
                } catch (NumberFormatException e) {
                    return numCompareError;
                }
            }
            case GREATER_EQUAL -> {
                try {
                    if (getDoubleNumValue(data) < getDoubleNumValue(calculated)) {
                        return "is not greater or equals to";
                    }
                } catch (NumberFormatException e) {
                    return numCompareError;
                }
            }
            case LOWER_EQUAL -> {
                try {
                    if (getDoubleNumValue(data) > getDoubleNumValue(calculated)) {
                        return "is not lower or equals to";
                    }
                } catch (NumberFormatException e) {
                    return numCompareError;
                }
            }
            case null, default -> {
                if (!textEquals(data, calculated)) {
                    return "is not equals to";
                }
            }
        }

		return null;
	}

	private static double getDoubleNumValue(String s) {
		return Double.parseDouble(s.replaceAll(",", ".").replaceAll(" ", ""));
	}

	public boolean textEquals(String data, String calculated){
		if(data == null) {
			return false;
		}
		return data.equals(calculated);
	}

	public boolean safeRegexpMatch(String data){//TODO send detailed error to user
		try {
			return RegularExpressionUtils.createMatcherWithTimeout(data, regexpPattern, regexTimeOut).matches();
		}catch(RegexpTimeoutException e) {
			return false;
		}
	}

	public static String getJavaCode(String op) {
		final String code = Operators.class.getSimpleName() + ".";

		switch (op) {
		case LOWER:
			return code + "LOWER";
		case GREATER:
			return code + "GREATER";
		case DIFFERENT:
			return code + "DIFFERENT";
		case LOWER_EQUAL:
			return code + "LOWER_EQUAL";
		case GREATER_EQUAL:
			return code + "GREATER_EQUAL";
		case REGEXP:
			return code + "REGEXP";
		default:
			return code + "EQUAL";
		}
	}

	public String getJavaCode() {
		return getJavaCode(type);
	}

	public boolean isRegexp() {
		return REGEXP.equals(type);
	}

	public String[] initData(String data) {
		final Optional<Emit> opt = trieOperator.parseText(data).stream().findFirst();
		if(opt.isPresent()) {
			final Emit emit = opt.get();
			setType(emit.getKeyword());
			return new String[] {data.substring(0, emit.getStart()).trim(), data.substring(emit.getEnd()+1).trim()};
		}

		return new String[] {data.trim(), ""};
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}