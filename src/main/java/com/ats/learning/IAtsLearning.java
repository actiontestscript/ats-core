package com.ats.learning;

import com.ats.element.SearchedElement;
import com.ats.element.test.TestElement;
import com.ats.executor.drivers.engines.IDriverEngine;

public interface IAtsLearning {
	void saveAtsLearning(TestElement testElement, IDriverEngine engine, SearchedElement searchedElement);
	void terminate();
}
