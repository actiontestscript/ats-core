/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script;

import java.beans.Transient;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.ats.recorder.TestError;
import org.testng.ITest;
import org.testng.collections.Maps;

import com.ats.AtsSingleton;
import com.ats.crypto.Passwords;
import com.ats.executor.channels.Channel;
import com.ats.generator.variables.CalculatedValue;
import com.ats.generator.variables.EnvironmentValue;
import com.ats.generator.variables.ScriptValue;
import com.ats.generator.variables.Variable;
import com.ats.generator.variables.parameter.ParameterList;
import com.ats.generator.variables.transform.DateTransformer;
import com.ats.generator.variables.transform.TimeTransformer;
import com.ats.generator.variables.transform.Transformer;
import com.ats.recorder.ShadowScript;
import com.ats.script.actions.Action;
import com.ats.tools.logger.ExecutionLogger;
import com.google.common.collect.ImmutableMap;

public class Script implements ITest {

	public static final Pattern OBJECT_PATTERN = Pattern.compile("(.*)\\[(.*)\\]", Pattern.CASE_INSENSITIVE);
	public final static Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
	public final static String ATS_EXTENSION = "ats";
	public final static String ATS_FILE_EXTENSION = "." + ATS_EXTENSION;
	public final static String ATS_VISUAL_EXTENSION = "atsv";
	public final static String ATS_VISUAL_FILE_EXTENSION = "." + ATS_VISUAL_EXTENSION;
	public final static String ATS_VISUAL_FOLDER = "visual";
	public final static String SCRIPT_LOG = "SCRIPT";

	//------------------------------------------------------------------------------------------------------
	// Ats Script special values
	//------------------------------------------------------------------------------------------------------

	protected static final String TEST_NAME = "ATS_TEST_NAME";

	//-------------------------------------------------------------------------------------
	// instances
	//-------------------------------------------------------------------------------------

	protected ScriptHeader scriptHeader = new ScriptHeader();
	private ParameterList parameterList;
	private List<Variable> variables = new ArrayList<>();
	private ArrayList<List<Variable>> iterVariables = new ArrayList<>();
	private ArrayList<CalculatedValue> returns;

	protected String csvAbsoluteFilePath;
	protected int iteration = 0;
	protected int iterationsCount = 0;

	protected long started = System.currentTimeMillis();

	private Map<String, String> testExecutionVariables;
	private Map<String, String> ATSEnvVariables;

	protected ExecutionLogger logger = new ExecutionLogger();
	protected Passwords passwords;

	private String testName = "";

	private ShadowScript shadowScript;
	private List<String> summary;
	
	private Action lastAction;

	// Constructor
	public Script() {
	}

	//constructor with parameters
	public Script(ExecutionLogger logger) {
		setTopLogger(logger);
	}

	public String getPassword(String name) {
		if(passwords == null) {
			return "";
		}
		return passwords.getPassword(name);
	}

	public long getTimeLine() {
		return System.currentTimeMillis() - started;
	}

	public long getStarted() {
		return started;
	}

	protected void initShadowScript() {
		shadowScript = new ShadowScript();
		summary = new ArrayList<>();
	}

	protected void initShadowScript(ShadowScript script) {
		shadowScript = script;
	}

	protected void initShadowScript(ScriptHeader header, String suiteName) {
		shadowScript = new ShadowScript(header, suiteName);
		summary = new ArrayList<>();
	}

	public ShadowScript getShadowScript() {
		return shadowScript;
	}

	public void addShadowAction(Action action) {
		shadowScript.addAction(action, getTimeLine());
	}

	public void addShadowActionError(Action action, int line, String errorText, TestError.TestErrorStatus testErrorStatus) {
		shadowScript.setError(action, getTimeLine(), line, errorText, testErrorStatus);
	}

	public void summaryStack(String value) {
		if(summary == null) {
			summary = new ArrayList<>();
		}
		summary.add(value);
	}

	public String getSummaryText() {
		if(summary != null) {
			return String.join("<br>", summary);
		}
		return "";
	}
	
	public void setLastAction(Action action) {
		this.lastAction = action;
	}

	//---------------------------------------------------------------------------------------------------

	public void setTopLogger(ExecutionLogger value) {
		if (value != null) {
			this.logger = value;
		}
	}

	@Transient
	public ExecutionLogger getLogger() {
		return logger;
	}

	//---------------------------------------------------------------------------------------------------

	public void sleep(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
		}
	}

	protected void setTestExecutionVariables(Map<String, String> params) {
		this.testExecutionVariables = params;
	}

	public Map<String, String> getTestExecutionVariables() {
		if (testExecutionVariables == null) {
			return new HashMap<>();
		}
		return testExecutionVariables;
	}

	public Map<String, String> getParametersMap() {
		if(parameterList != null) {
			return parameterList.getMap();
		}
		return Maps.newHashMap(ImmutableMap.of());
	}

	public Map<String, String> getVariablesMap() {
		if(variables != null) {
			return variables.stream().collect(Collectors.toMap(e -> e.getName(), e -> e.getCalculatedValue()));
		}
		return Maps.newHashMap(ImmutableMap.of());
	}

	public Channel getChannel(String name) {
		return AtsSingleton.getInstance().getChannel(name);
	}

	//-------------------------------------------------------------------------------------------------
	//  getters and setters for serialization
	//-------------------------------------------------------------------------------------------------

	public List<Variable> getAllIterVariables(){

		List<Variable> vars = new ArrayList<>();
		iterVariables.forEach(vars::addAll);

		return vars;
	}

	public void setIterVariables(List<Variable> data){
		setIterVariables(iteration, data);
	}

	public void setIterVariables(int iterNum, List<Variable> data){
		if(data != null && iterVariables != null && iterNum-1 < iterVariables.size()) {
			iterVariables.set(iterNum - 1, data);
		}
	}

	public List<Variable> getIterVariables(int iterNum){
		if(iterVariables != null && iterNum-1 < iterVariables.size()){
			return iterVariables.get(iterNum-1);
		}
		return null;
	}

	public List<Variable> getVariables()
	{
		return variables;
	}

	public void setVariables(List<Variable> data) {
		this.variables = data;
	}

	public ParameterList getParameterList() {
		return parameterList;
	}

	public void setParameterList(ParameterList data) {
		this.parameterList = data;
	}

	public CalculatedValue[] getReturns() {
		if (returns != null) {
			return returns.toArray(new CalculatedValue[returns.size()]);
		}
		return null;
	}

	public void setReturns(CalculatedValue[] data) {
		this.returns = new ArrayList<>(Arrays.asList(data));
	}

	//-------------------------------------------------------------------------------------------------
	// variables
	//-------------------------------------------------------------------------------------------------

	public String getGlobalVariableValue(String varPath) {
		String value = AtsSingleton.getInstance().getGlobalVariableValue(varPath);
		if(value == null) {
			getLogger().sendWarning("Unable to find global variable", varPath);
			value = getParameterValue(varPath);
		}
		return value;
	}

	public boolean checkVariableExists(String name) {
		for (Variable variable : getVariables()) {
			if (variable.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public Variable getVariable(String name, boolean noCalculation) {

		Variable foundVar = getVariable(name);

		if (foundVar == null) {
			foundVar = createVariable(name, new CalculatedValue(this, ""), null, false);
		}

		if (noCalculation) {
			foundVar.getValue().setData("");
			foundVar.setCalculation(false);
		}

		return foundVar;
	}

	public Variable getVariable(String name) {
		final Variable[] vars = variables.toArray(new Variable[variables.size()]);
		for(int ind = vars.length-1 ; ind >= 0 ; ind--){
			final Variable var = vars[ind];
			if(var != null && name.equals(var.getName())){
				return var;
			}
		}
		return null;
	}

	public Variable addVariable(String name, CalculatedValue value, Transformer transformer, boolean fixed) {
		Variable foundVar = getVariable(name);
		if (foundVar == null) {
			foundVar = createVariable(name, value, transformer, fixed);
		} else {
			foundVar.setValue(value);
			foundVar.setTransformation(transformer);
		}
		return foundVar;
	}

	public Variable createVariable(String name, CalculatedValue value, Transformer transformer) {
		return createVariable(name, value, transformer, false);
	}
	
	public Variable createVariable(String name, CalculatedValue value, Transformer transformer, boolean fixed) {
		final Variable newVar = new Variable(name, value, transformer, fixed);
		variables.add(newVar);
		return newVar;
	}

	public String getVariableValue(String variableName) {
		return getVariable(variableName, false).getCalculatedValue();
	}

	//-------------------------------------------------------------------------------------------------
	// parameters
	//-------------------------------------------------------------------------------------------------

	public String[] getParameters() {
		if (parameterList == null) {
			return new String[0];
		}
		return parameterList.getParameters();
	}

	public ScriptValue getParameter(String name) {
		return new ScriptValue(getParameterValue(name, ""));
	}
	
	public ScriptValue ats_parameter(int index) {
		return getParameter(index);
	}
	
	public ScriptValue getParameter(int index) {
		return new ScriptValue(getParameterValue(index, ""));
	}

	public String getParameterValue(String name) {
		return getParameterValue(name, "");
	}

	public String getParameterValue(String name, String defaultValue) {

		if (parameterList == null) {
			return defaultValue;
		}

		try {
			int index = Integer.parseInt(name);
			return getParameterValue(index, defaultValue);
		} catch (NumberFormatException e) {}

		return parameterList.getParameterValue(name, defaultValue);
	}

	public String getParameterValue(int index) {
		return getParameterValue(index, "");
	}

	public String getParameterValue(int index, String defaultValue) {
		if (parameterList == null) {
			return defaultValue;
		}
		return parameterList.getParameterValue(index, defaultValue);
	}

	//-------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------

	public String getSpecialValue(String key) {
		return scriptHeader.getData(key);
	}

	//-------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------

	public String getPropertyVariableValue(String file, String name) {
		
		try (InputStream input = this.getClass().getClassLoader().getResourceAsStream("assets/data/" + file.replace(".", "/") + ".properties")) {

            Properties prop = new Properties();

            if (input == null) {
            	getLogger().sendWarning("Unable to find properties file", file);
                return "";
            }

            prop.load(input);
            
            final String value = prop.getProperty(name);

            if(value != null) {
            	return value;
            }

        } catch (IOException ex) {}
		
		getLogger().sendWarning("Unable to find property key", name);
		return "";
	}
	
	public String getProjectVariableValue(String name) {
		String value = AtsSingleton.getInstance().getProjectVariableValue(name);
		if (value == null) {
			getLogger().sendWarning("Unable to find project variable", name);
			return "";
		}

		return value;
	}

	public void setEnvironnementValue(String name, String value) {
		try {
			System.setProperty(name, value);
		}catch (Exception e) {}
	}

	public String getEnvironmentValue(String name, String defaultValue) {

		// commandline Parameter preemption
		String value = System.getProperty(name);
		if (value != null) {
			return value;
		}

		// system env value preemption
		value = System.getenv(name);
		if(value != null) {
			return value;
		}

		// test execution
		if (testExecutionVariables != null) {
			value = testExecutionVariables.get(name);
			if (value != null) {

				Matcher mv = CalculatedValue.ENV_PATTERN.matcher(value);
				while (mv.find()) {
					final EnvironmentValue sp = new EnvironmentValue(mv);
					value = value.replace(sp.getReplace(), getEnvironmentValue(sp.getValue(), sp.getDefaultValue()));

					mv = CalculatedValue.ENV_PATTERN.matcher(value);
				}

				return value;
			}
		}

		//specific env variable
		StringBuilder ATS_name = new StringBuilder("ATS_")
				.append(name.replaceAll("\\.",""));

		value = System.getenv(ATS_name.toString());
		if (value != null) {
			return value;
		}

		//Global ATS env Variable
		if (ATSEnvVariables == null) {
			ATSEnvVariables = extractEnvOptions();
		}

		if (ATSEnvVariables != null && !ATSEnvVariables.isEmpty()) {
			value = ATSEnvVariables.get(name.replaceAll("\\-", ""));
			if (value != null) {
				return value;
			}
		}

		return defaultValue;
	}

	private static Map<String, String> extractEnvOptions() {

		final String atsOptions = System.getenv("ATS_OPTIONS");

		if (atsOptions != null) {
			Map<String, String> envOptions = Maps.newHashMap();
			List<String> listArgs = Arrays.asList(atsOptions.split(" "));

			for (String arg : listArgs) {

				String paramValue = arg;

				if (arg.startsWith("-D")) {
					paramValue = arg.substring(2);
				}

				final String firstArg = paramValue.toLowerCase(Locale.ROOT);

				final int equalPos = firstArg.indexOf("=");

				if (equalPos == -1) {
					final String argName = firstArg.replaceAll("\\-", "");
					envOptions.put(argName, "1");
				} else {
					final String argName = firstArg.substring(0, equalPos).replaceAll("\\-", "");
					final String argValue = firstArg.substring(equalPos + 1).trim();
					envOptions.put(argName, argValue);
				}

			}
			return envOptions;
		}

		return null;
	}


	public String getUuidValue() {
		return UUID.randomUUID().toString();
	}

	public String getTodayValue() {
		return DateTransformer.getTodayValue();
	}

	public String getNowValue() {
		return TimeTransformer.getNowValue();
	}

	public int getIteration() {
		return iteration;
	}

	public int getIterationsCount() {
		return iterationsCount;
	}
	
	public long getLastActionDuration() {
		if(lastAction != null) {
			return lastAction.getExecutionDuration();
		}
		return 0L;
	}

	public String getCsvFilePath() {
		return csvAbsoluteFilePath;
	}

	public File getCsvFile() {
		return new File(csvAbsoluteFilePath);
	}

	public File getAssetsFile(String relativePath) {
		if (!relativePath.startsWith("/")) {
			relativePath = "/" + relativePath;
		}
		relativePath = Project.ASSETS_FOLDER + relativePath;

		final URL url = getClass().getClassLoader().getResource(relativePath);
		if (url != null) {
			try {
				return Paths.get(url.toURI()).toFile();
			} catch (URISyntaxException e) {
			}
		}
		return null;
	}

	public String getAssetsFilePath(String relativePath) {
		final File f = getAssetsFile(relativePath);
		if (f != null) {
			return f.getAbsolutePath();
		}
		return "";
	}

	public String getAssetsUrl(String relativePath) {
		final URL url = getClass().getClassLoader().getResource(relativePath);
		if (url != null) {
			return "file://" + url.getPath();
		}
		return "";
	}

	@Override
	public String getTestName() {
		return testName;
	}

	protected void setTestName(String value) {
		this.testName = value;
	}
}