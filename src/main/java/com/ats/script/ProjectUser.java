package com.ats.script;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class ProjectUser {

	private String systemName;
	private String name;
	private String email;
	private byte[] pic;
	
	public ProjectUser(String name, String systemName, String email, String b64pic) {
		this.name = name;
		this.systemName = systemName;
		this.email = email;
		this.pic = Base64.getDecoder().decode(new String(b64pic).getBytes(StandardCharsets.UTF_8));
	}
	
	public byte[] getPic(String value) {
		if(value != null && (value.equals(name) || value.equals(systemName) || value.equals(email))) {
			return pic;
		}
		return null;
	}
}