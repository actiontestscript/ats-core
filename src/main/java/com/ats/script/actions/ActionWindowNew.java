/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;

import org.openqa.selenium.WindowType;

import com.ats.executor.channels.Channel;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.logger.ExecutionLogger;

public class ActionWindowNew extends ActionWindow {

	private static final String SCRIPT_NEW_LABEL = SCRIPT_LABEL + "new";
	public static final Predicate<String> PREDICATE = g -> SCRIPT_NEW_LABEL.equals(g);

	private WindowType type = WindowType.TAB;
	private CalculatedValue url;

	public ActionWindowNew() {}

	public ActionWindowNew(Script script, ExecuteOptions options, String type, ArrayList<String> data) {
		super(script, options);
		setType(type);
		if(data.size() > 0) {
			setUrl(new CalculatedValue(script, data.get(0).trim()));
		}
	}

	public ActionWindowNew(Script script, ExecuteOptions options, String type, CalculatedValue url) {
		super(script, options);
		setType(type);
		setUrl(url);
	}

	public ActionWindowNew(Script script, ExecuteOptions options, String type) {
		super(script, options);
		setType(type);
	}

	@Override
	public StringBuilder getJavaCode() {

		final StringBuilder codeBuilder = 
				super.getJavaCode()
				.append("\"")
				.append(type.toString())
				.append("\"");

		if(url != null) {
			codeBuilder.append(", ").append(url.getJavaCode());
		}

		return codeBuilder.append(")");
	}

	@Override
	public String exec(Channel channel, ExecutionLogger logger) {
		channel.newWindow(status, type, url);
		return type.toString();
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getType() {
		return type.toString();
	}

	public void setType(String value) {
		if(WindowType.TAB.toString().equalsIgnoreCase(value)) {
			this.type = WindowType.TAB;
		}else if(WindowType.WINDOW.toString().equalsIgnoreCase(value)) {
			this.type = WindowType.WINDOW;
		}
	}

	public CalculatedValue getUrl() {
		return url;
	}

	public void setUrl(CalculatedValue url) {
		this.url = url;
	}
}