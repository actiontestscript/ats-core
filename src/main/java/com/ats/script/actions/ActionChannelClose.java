/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.function.Predicate;

import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;

public class ActionChannelClose extends ActionChannelExist {

	public static final String SCRIPT_CLOSE_LABEL = SCRIPT_LABEL + "close";

	public static final String NO_STOP_LABEL = "nostop";

	public static final Predicate<String> PREDICATE = g -> SCRIPT_CLOSE_LABEL.equals(g);

	private boolean keepRunning = false;

	public ActionChannelClose() {}

	public ActionChannelClose(Script script, ExecuteOptions options, String name) {
		super(script, options, name);
		setKeepRunning(options.hasNoStopLabel());
	}

	public ActionChannelClose(Script script, ExecuteOptions options, String name, boolean run) {
		super(script, options, name);
		setKeepRunning(run);
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		super.execute(ts, testName, testLine, tryNum);
		getCurrentChannel().closeChannel(status, ts, testName, testLine, getName());
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public boolean isKeepRunning() {
		return keepRunning;
	}

	public void setKeepRunning(boolean keepRunning) {
		this.keepRunning = keepRunning;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {
		return super.getJavaCode().append("\"").append(getName()).append("\", ").append(keepRunning).append(")");
	}

	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".Close");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}
}