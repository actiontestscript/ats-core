/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ats.element.SearchedElement;
import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.script.AtsScript;
import com.ats.tools.Operators;
import com.ats.tools.Utils;
import com.google.gson.JsonObject;

public class ActionAssertCount extends ActionExecuteElement {

	public static final String SCRIPT_LABEL = "check-count";
	public static final String SCRIPT_OCCURRENCE_LABEL = "check-occurrence";
	
	public static final Predicate<String> PREDICATE = s -> SCRIPT_LABEL.equals(s);
	public static final Predicate<String> PREDICATE_OCCURRENCE = s -> SCRIPT_OCCURRENCE_LABEL.equals(s);

	private final Pattern COUNT_PATTERN = Pattern.compile("(\\d+) ?(\\-?\\+?=?!?)");

	private int value = 1;
	private String operator = Operators.EQUAL;

	public ActionAssertCount() {}

	public ActionAssertCount(AtsScript script, ExecuteOptions options, int stopPolicy, String data, ArrayList<String> objectArray) {
		super(script, options, stopPolicy, objectArray);

		final Matcher m = COUNT_PATTERN.matcher(data);
		if(m.matches()) {

			setValue(Utils.string2Int(m.group(1).trim(), 1));

			if(m.groupCount() > 1) {
				switch (m.group(2).trim()) {
				case "+":
					setOperator(Operators.GREATER_EQUAL);
					break;
				case "-":
					setOperator(Operators.LOWER_EQUAL);
					break;
				case "!":
					setOperator(Operators.DIFFERENT);
					break;
				}
			}
		}
	}
	
	public ActionAssertCount(AtsScript script, ExecuteOptions options, int stopPolicy, String operator, String calculate, ArrayList<String> objectArray) {
		super(script, options, stopPolicy, objectArray);
		
		
		
		
	}
	
	public ActionAssertCount(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element, String operator, int value) {
		super(script, options, stopPolicy, maxTry, delay, element);
		setOperator(operator);
		setValue(value);
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {
		StringBuilder codeBuilder = super.getJavaCode();
		codeBuilder.append(", ")
		.append(Operators.getJavaCode(operator))
		.append(", ")
		.append(value)
		.append(")");
		return codeBuilder;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		if(getCondition().isPassed()) {
			super.execute(ts, testName, testLine, operator, value, tryNum);
		}
	}

	@Override
	public void terminateExecution(ActionTestScript ts) {
		getTestElement().checkOccurrences(ts, status, operator, value);
		status.getElement().setFoundElements(null);
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("operator", operator);
		data.addProperty("occurrences", value);
		return super.getActionLogs(scriptName, scriptLine, data);
	}
	
	public static StringBuilder getAtsCodeStr(String subFolder) {
		return new StringBuilder().append("callscript -> ").append(subFolder).append(".AssertCount");
		// return new StringBuilder().append(SCRIPT_LABEL).append(ExecutionLogger.RIGHT_ARROW_LOG);
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}