/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.function.Predicate;

import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;

import com.ats.AtsSingleton;
import com.ats.data.Rectangle;
import com.ats.driver.AtsManager;
import com.ats.element.SearchedElement;
import com.ats.element.test.TestElement;
import com.ats.element.test.TestElementDialog;
import com.ats.element.test.TestElementImage;
import com.ats.element.test.TestElementRecord;
import com.ats.element.test.TestElementRoot;
import com.ats.element.test.TestElementSap;
import com.ats.element.test.TestElementSystem;
import com.ats.executor.ActionStatus;
import com.ats.executor.ActionTestScript;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.Operators;
import com.ats.tools.logger.MessageCode;
import com.google.gson.JsonObject;

public class ActionExecuteElement extends ActionExecuteElementAbstract {

	private SearchedElement searchElement;
	protected TestElement testElement;

	private boolean async;

	private int actionMaxTry;

	public ActionExecuteElement() {}

	public ActionExecuteElement(Script script, ExecuteOptions options, int stopPolicy, ArrayList<String> element) {
		super(script, options, stopPolicy);
		if (element != null && !element.isEmpty()) {
			setSearchElement(new SearchedElement(script, element));
		}
	}

	public ActionExecuteElement(Script script, ExecuteOptions options, int stopPolicy, int maxTry, int delay, SearchedElement element) {
		super(script, options, stopPolicy, maxTry, delay);
		setSearchElement(element);
	}

	public int getActionMaxTry() {
		return actionMaxTry;
	}

	public TestElement getTestElement() {
		return testElement;
	}

	private void setTestElement(TestElement element) {
		if (testElement != null) {
			testElement.dispose();
		}
		testElement = element;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StringBuilder getJavaCode() {
		final StringBuilder codeBuilder = super.getJavaCode();
		if(searchElement == null){
			codeBuilder.append("null");
		}else {
			codeBuilder.append(searchElement.getJavaCode());
		}
		return codeBuilder;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	public void execute(ActionTestScript ts, String testName, int testLine) {
		execute(ts, testName, testLine, 0);
	}

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {
		try {
			executeNoStale(ts, testName, testLine, tryNum);
		} catch(JavascriptException e) {
			status.setException(ActionStatus.JAVASCRIPT_ERROR, e);
			ts.getTopScript().getLogger().sendError(status.getErrorType(), e.getMessage());
			ts.getRecorder().update(status.getCode(), status.getDuration(), status.getMessage());
		}
	}

	private void executeNoStale(ActionTestScript ts, String testName, int testLine, int tryNum) {
		int maxTry = 0;
		while(maxTry < AtsManager.getMaxStaleOrJavaScriptError()) {
			try {
				execute(ts, testName, testLine, Operators.GREATER, 0, maxTry + tryNum);
				return;
			} catch(StaleElementReferenceException e) {
				ts.getTopScript().getLogger().sendWarning("stale reference error", "try again : " + maxTry);
				status.setException(ActionStatus.WEB_DRIVER_ERROR, e);
			}

			AtsSingleton.getInstance().getCurrentChannel().sleep(500);
			setTestElement(null);
			maxTry++;
		}

		throw new WebDriverException("execute element error : " + status.getFailMessage());
	}

	public void execute(ActionTestScript ts, String testName, int testLine, String operator, int value, int tryNum) {

		super.execute(ts, testName, testLine, tryNum);

		/*if(ts.getParameterList() != null) {
			paramElement = ts.getParameterList().getElement();
		}*/

		if (status.isPassed()) {

			final int delay = getDelay();
			if(delay > 0) {
				ts.getTopScript().getLogger().sendInfo("delay before action", delay + "s");
				getCurrentChannel().sleep(delay * 1000);
			}

			actionMaxTry = AtsSingleton.getInstance().getMaxTry() + getMaxTry();
			if (actionMaxTry < 1) {
				actionMaxTry = 1;
			}

			if(testElement == null) {

				final Predicate<Integer> predicate = getPredicate(operator, value);

				if(searchElement == null) {
					setTestElement(new TestElementRoot(ts, getCurrentChannel(), predicate));
				}else {

					int trySearch = 0;

					//ts.setParamElement(paramElement);

					// TODO : à refacto
					if(searchElement.isDialog()) {
						while (trySearch < actionMaxTry) {

							setTestElement(new TestElementDialog(ts, getCurrentChannel(), actionMaxTry, searchElement, predicate));

							if(testElement.isValidated()) {
								trySearch = actionMaxTry;
							}else {
								trySearch++;
								getCurrentChannel().sendWarningLog(MessageCode.OBJECT_TRY_SEARCH, MessageCode.ELEMENT_NOT_FOUND_MESSAGE, actionMaxTry - trySearch);
								getCurrentChannel().progressiveWait(trySearch);
							}
						}
					} else {

						if (searchElement.isSysComp()) {
							while (trySearch < actionMaxTry) {

								if (getCurrentChannel().isSap()) {
									setTestElement(new TestElementSap(ts, getCurrentChannel(), actionMaxTry, predicate, searchElement));
								} else {
									setTestElement(new TestElementSystem(ts, getCurrentChannel(), actionMaxTry, predicate, searchElement));
								}

								if (testElement.isValidated()) {
									trySearch = actionMaxTry;
								} else {
									trySearch++;
									getCurrentChannel().sendWarningLog(MessageCode.OBJECT_TRY_SEARCH, MessageCode.ELEMENT_NOT_FOUND_MESSAGE, actionMaxTry - trySearch);
									getCurrentChannel().progressiveWait(trySearch);
								}
							}
						} else {

							SearchedElement parent = searchElement.getParent();
							if (parent != null && parent.isScrollable()) {
								TestElement parentElement = new TestElement(ts, getCurrentChannel(), actionMaxTry, predicate, parent);
								Rectangle parentRect = parentElement.getFoundElement().getRectangle();

								int tryScrollSearch = 0;
								int tryScrollSearchMax = AtsSingleton.getInstance().getMaxTryScroll();

								while (tryScrollSearch < tryScrollSearchMax) {

									if (tryScrollSearch > 0) {
										parentElement.swipe(-200);
										getCurrentChannel().sleep(2000);
									}

									if (searchElement.isImageSearch()) {
										setTestElement(new TestElementImage(ts, getCurrentChannel(), 1, predicate, searchElement));
									}else if(searchElement.isRecordSearch()) {
										setTestElement(new TestElementRecord(ts, getCurrentChannel(), 1, predicate, searchElement));
									}else if(getCurrentChannel().isSap()) {
										setTestElement(new TestElementSap(ts, getCurrentChannel(), 1, predicate, searchElement));
									} else {
										setTestElement(new TestElement(ts, getCurrentChannel(), 1, predicate, searchElement));
									}

									if (testElement.isValidated() && parentRect.contains(testElement.getFoundElement().getRectangle())) {
										tryScrollSearch = tryScrollSearchMax;
									} else {
										parentElement = new TestElement(ts, getCurrentChannel(), actionMaxTry, predicate, parent);
										tryScrollSearch++;
									}
								}

							} else {
								while (trySearch < actionMaxTry) {

									if (searchElement.isImageSearch()) {
										setTestElement(new TestElementImage(ts, getCurrentChannel(), actionMaxTry, predicate, searchElement));
									}else if(searchElement.isRecordSearch()) {
										setTestElement(new TestElementRecord(ts, getCurrentChannel(), actionMaxTry, predicate, searchElement));
									}else if(getCurrentChannel().isSap()) {
										setTestElement(new TestElementSap(ts, getCurrentChannel(), actionMaxTry, predicate, searchElement));
									} else {
										setTestElement(new TestElement(ts, getCurrentChannel(), actionMaxTry, predicate, searchElement));
									}

									if (testElement.isValidated()) {
										trySearch = actionMaxTry;
									} else {
										trySearch++;
										getCurrentChannel().tryAndWait(MessageCode.OBJECT_TRY_SEARCH, MessageCode.ELEMENT_NOT_FOUND_MESSAGE, actionMaxTry, actionMaxTry - trySearch);
									}
								}
							}
						}
					}
				}

				status.setElement(testElement);
				status.setSearchDuration(testElement.getTotalSearchDuration());
				status.setData(testElement.getCount());

				AtsSingleton.getInstance().sleep(getDelay());
				asyncExec(ts);

			}else {
				terminateExecution(ts);
			}
		}
	}

	protected boolean getLoadAttributes() {
		if(searchElement != null) {
			return searchElement.getLoadAttributes();
		}
		return false;
	}

	private Predicate<Integer> getPredicate(String operator, int value) {
		switch (operator) {
		case Operators.DIFFERENT :
			return p -> p != value;
		case Operators.GREATER :
			return p -> p > value;
		case Operators.GREATER_EQUAL :
			return p -> p >= value;
		case Operators.LOWER :
			return p -> p < value;
		case Operators.LOWER_EQUAL :
			return p -> p <= value;
		}
		return p -> p == value;
	}

	private void asyncExec(ActionTestScript ts) {
		if(!async) {
			terminateExecution(ts);
		}
	}

	public void terminateExecution(ActionTestScript ts) {
		int error = 0;

		if(testElement.isValidated()) {
			status.setPassed(true);
		}else {
			status.setError(ActionStatus.OBJECT_NOT_FOUND, testElement.getNotFoundDescription());
			error = ActionStatus.OBJECT_NOT_FOUND;
			saveAppSource();
		}

		terminateExecution(ts, error);
	}

	protected void terminateExecution(ActionTestScript ts, int error) {
		status.endDuration();
		testElement.terminateExecution(status, ts, error, status.getDuration());
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("searchDuration", status.getSearchDuration());
		data.addProperty("delay", getDelay());
		if(status.getElement() != null) {
			data.addProperty("occurrences", status.getElement().getCount());
		}
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	@Override
	public ArrayList<String> getKeywords() {
		final ArrayList<String> keywords = super.getKeywords();
		if(searchElement != null) {
			keywords.addAll(searchElement.getKeywords());
		}
		return keywords;
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public boolean isAsync() {
		return async;
	}

	public void setAsync(boolean value) {
		this.async = value;
	}

	public SearchedElement getSearchElement() {
		return searchElement;
	}

	public void setSearchElement(SearchedElement value) {
		this.searchElement = value;
	}
}