package com.ats.script.actions;

public interface IActionStoppable {
	public boolean isStop();
}
