/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.script.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Predicate;

import com.ats.executor.ActionTestScript;
import com.ats.generator.variables.ApiData;
import com.ats.generator.variables.CalculatedProperty;
import com.ats.generator.variables.CalculatedValue;
import com.ats.script.Script;
import com.ats.script.actions.condition.ExecuteOptions;
import com.ats.tools.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ActionApi extends ActionCondition {

	private static final String SCRIPT_LABEL = "api-";
	public static final Predicate<String> PREDICATE = g -> g.startsWith(SCRIPT_LABEL);

	private static final int SCRIPT_LABEL_LENGTH = SCRIPT_LABEL.length();

	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String PUT = "PUT";
	public static final String PATCH = "PATCH";
	public static final String DELETE = "DELETE";

	public static final String SOAP = "SOAP";
	public static final String REST = "REST";

	public static final String JSON_DATA = "JSON_DATA";
	public static final String XML_DATA = "XML_DATA";

	private CalculatedValue method;
	private boolean useCache = true;

	private List<ApiData> headers;
	private List<ApiData> data;

	private String type = GET;

	private int port = -1;

	public ActionApi() {}

	public ActionApi(Script script, ExecuteOptions options, String type, String method, String headerData, ArrayList<String> data) {
		super(script, options);

		setType(type.substring(SCRIPT_LABEL_LENGTH));
		setMethod(new CalculatedValue(script, method));

		if(headerData.length() > 0) {
			this.headers = new ArrayList<>();
			Arrays.stream(headerData.split(",")).forEach(s -> addHeader(s));
		}

		if(data.size() > 0) {
			this.data = new ArrayList<>();
			for(int i=0; i<data.size(); i++) {

				String dataValue = data.get(i).trim();
				String key = null;

				if(Utils.isXmlLike(Utils.unescapeAts(dataValue))) {
					key = XML_DATA;
				}else {
					final String jsonString = Utils.checkJsonData(Utils.unescapeAts(dataValue));
					if(jsonString != null) {
						key = JSON_DATA;
						dataValue = jsonString;
					}
				}

				if(!JSON_DATA.equals(key) && !XML_DATA.equals(key) && dataValue.contains("=")) {
					String[] arr = dataValue.split("=");
					key = arr[0].trim();
					dataValue = Utils.unescapeAts(arr[1].trim());
				}

				this.data.add(new ApiData(key, new CalculatedValue(script, dataValue)));
			}
		}

		this.useCache = options.hasCache();
		this.port = options.getPort();
	}

	public ActionApi(Script script, ExecuteOptions options, String type, CalculatedValue method, List<ApiData> data) {
		this(script, options, type, -1, true, method, data, new ApiData[0]);
	}

	public ActionApi(Script script, ExecuteOptions options, String type, int port, boolean cache, CalculatedValue method, List<ApiData> data, ApiData ... headerData) {
		super(script, options);
		setType(type);
		setPort(port);
		setUseCache(cache);
		setMethod(method);
		setData(data);
		setHeaders(new ArrayList<>(Arrays.asList(headerData)));
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	// Code Generator
	//---------------------------------------------------------------------------------------------------------------------------------

	private void addHeader(String s) {
		if(headers == null) {
			headers = new ArrayList<ApiData>();
		}
		final ApiData prop = new ApiData(script, s);
		headers.add(prop);
	}

	@Override
	public StringBuilder getJavaCode() {

		StringBuilder codeBuilder = super.getJavaCode();
		codeBuilder.append("\"")
		.append(type)
		.append("\", ")
		.append(port)
		.append(", ")
		.append(useCache)
		.append(", ")
		.append(method.getJavaCode())
		.append(", ");

		if(data == null){
			codeBuilder.append("null");
		}else {
			final StringJoiner sj = new StringJoiner(", ");

			codeBuilder.append(List.class.getCanonicalName()).append(".of(");
			for (ApiData p : data) {
				sj.add(p.getJavaCode());
			}
			codeBuilder.append(sj.toString()).append(")");
		}

		if(headers != null && headers.size() > 0){

			codeBuilder.append(", ");

			StringJoiner joiner = new StringJoiner(", ");
			for (CalculatedProperty head : headers){
				joiner.add(head.getJavaCode());
			}

			codeBuilder.append(joiner.toString());
		}

		codeBuilder.append(")");
		return codeBuilder;
	}

	//---------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void execute(ActionTestScript ts, String testName, int testLine, int tryNum) {

		super.execute(ts, testName, testLine, tryNum);

		if(isPassed()) {

			method.uncrypt(ts);

			if(headers != null) {
				headers.stream().forEach(h -> h.getValue().uncrypt(ts));
			}

			getCurrentChannel().api(status, this);
			status.endDuration();

			final JsonObject value = new JsonObject();
			value.addProperty("method", method.getSafeCalculated());
			value.addProperty("type", type);
			value.addProperty("cached", useCache);
			value.addProperty("port", port);

			final JsonArray headersValue = new JsonArray();
			for (ApiData header : headers) {
				headersValue.add(header.getJson());
			}

			value.add("headers", headersValue);

			if(data != null && data.size() > 0) {
				final StringJoiner join = new StringJoiner("|");
				for (CalculatedProperty cp : data) {
					cp.getValue().uncrypt(ts);
					join.add(cp.getValue().getSafeCalculated());
				}
				ts.getRecorder().update(value.toString(), join.toString());
			}else {
				ts.getRecorder().update(value.toString());
			}
		}
	}

	@Override
	public StringBuilder getActionLogs(String scriptName, int scriptLine, JsonObject data) {
		data.addProperty("type", type);
		data.addProperty("method", method.getCalculated());

		final Object statusData = status.getData();
		if(statusData == null) {
			data.addProperty(status.getMessage(), "");
		}else {
			data.addProperty(status.getMessage(), statusData.toString());
		}
		return super.getActionLogs(scriptName, scriptLine, data);
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public CalculatedValue getMethod() {
		return method;
	}

	public void setMethod(CalculatedValue endPoint) {
		this.method = endPoint;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if(POST.equalsIgnoreCase(type) || PUT.equalsIgnoreCase(type) || DELETE.equalsIgnoreCase(type) || SOAP.equalsIgnoreCase(type) || PATCH.equalsIgnoreCase(type)) {
			this.type = type.toUpperCase();
		}else {
			this.type = GET;
		}
	}

	public List<ApiData> getHeaders() {
		return headers;
	}

	public void setHeaders(List<ApiData> data) {
		this.headers = data;
	}

	public List<ApiData> getData() {
		return data;
	}

	public void setData(List<ApiData> value) {
		this.data = value;
	}

	public boolean isUseCache() {
		return useCache;
	}

	public void setUseCache(boolean value) {
		this.useCache = value;
	}
}