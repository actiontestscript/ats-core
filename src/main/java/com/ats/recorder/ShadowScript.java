package com.ats.recorder;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ats.script.ScriptHeader;
import com.ats.script.actions.Action;

public class ShadowScript {

	private ScriptHeader header;
	private String suiteName;
	private List<Action> actions;
	private String errorText;
	private String errorStatus;
	private int errorLine = 0;

	public ShadowScript() {
		this.actions = new ArrayList<>();
	}

	public ShadowScript(ScriptHeader header, String suiteName) {
		this.header = header;
		this.suiteName = suiteName;
		this.actions = new ArrayList<>();
	}

	public void addAction(Action action, long timeLine) {
		if(actions.indexOf(action) == -1) {
			action.setTimeline(timeLine);
			actions.add(action);
		}
	}

	public void setError(Action action, long timeLine, int line, String errorText, TestError.TestErrorStatus testErrorStatus) {
		this.errorText = errorText;
		this.errorLine = line;
		this.errorStatus = testErrorStatus.toString();
		addAction(action, timeLine);
	}

	public List<Action> getActions(){
		return actions;
	}

	public Element getScriptXmlResult(String summaryText){

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		Element atsRoot = null;

		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document document = docBuilder.newDocument();

			atsRoot = document.createElement("ats");
			document.appendChild(atsRoot);

			final Element scriptElem = document.createElement("script");
			atsRoot.appendChild(scriptElem);

			scriptElem.setAttribute("testId", header.getId());
			scriptElem.setAttribute("testName", header.getQualifiedName());
			scriptElem.setAttribute("externalId", header.getExternalId());
			scriptElem.setAttribute("cpuSpeed", "");
			scriptElem.setAttribute("cpuCount", "");
			scriptElem.setAttribute("totalMemory", "");
			scriptElem.setAttribute("osInfo", "");

			Element description = document.createElement("description");
			description.setTextContent(header.getDescription());
			scriptElem.appendChild(description);

			Element author = document.createElement("author");
			author.setTextContent(header.getAuthor());
			scriptElem.appendChild(author);
			
			Element memo = document.createElement("memo");
			memo.setTextContent(header.getMemo());
			scriptElem.appendChild(memo);
			
			Element modifiedAt = document.createElement("modifiedAt");
			modifiedAt.setTextContent(OffsetDateTime.ofInstant(header.getModifiedAt().toInstant(), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
			scriptElem.appendChild(modifiedAt);
			
			Element modifiedBy = document.createElement("modifiedBy");
			modifiedBy.setTextContent(header.getModifiedBy());
			scriptElem.appendChild(modifiedBy);

			Element prerequisite = document.createElement("prerequisite");
			prerequisite.setTextContent(header.getPrerequisite());
			scriptElem.appendChild(prerequisite);

			final long t = System.currentTimeMillis();
			Element started = document.createElement("started");
			started.setTextContent(String.valueOf(t));
			scriptElem.appendChild(started);

			Element startedFormated = document.createElement("startedFormated");
			startedFormated.setTextContent(OffsetDateTime.ofInstant(Instant.ofEpochMilli(t), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd 'at' HH:mm:ss")));
			scriptElem.appendChild(startedFormated);

			Element quality = document.createElement("quality");
			quality.setTextContent("");
			scriptElem.appendChild(quality);

			Element groups = document.createElement("groups");
			for (String gr : header.getGroups()) {
				Element group = document.createElement("group");
				group.setTextContent(gr);
				groups.appendChild(group);
			}
			scriptElem.appendChild(groups);


			Element project = document.createElement("project");

			Element projectId = document.createElement("id");
			projectId.setTextContent(header.getProjectUuid());
			project.appendChild(projectId);

			Element projectName = document.createElement("name");
			projectName.setTextContent(header.getProjectId());
			project.appendChild(projectName);

			scriptElem.appendChild(project);

			Element actionsList = document.createElement("actions");
			atsRoot.appendChild(actionsList);

			int index = 0;
			for(Action action : actions) {
				actionsList.appendChild(action.getXmlElement(document, header, index, errorText));

				if(!action.isPassed()) {
					errorText = action.getStatus().getFailMessage();
					errorLine = action.getLine();
				}

				index++;
			}

			final Element summaryElement = document.createElement("summary");
			summaryElement.setAttribute("actions", String.valueOf(actions.size()));

			summaryElement.setAttribute("suiteName", suiteName);
			summaryElement.setAttribute("testName", header.getQualifiedName());

			if(summaryText.length() > 0) {
				final Element data = document.createElement("data");
				data.setTextContent(summaryText);
				summaryElement.appendChild(data);
			}

			if(errorText == null) {
				summaryElement.setAttribute("status", "1");
			}else {
				summaryElement.setAttribute("status", "0");
				Element errors = document.createElement("errors");
				Element error = document.createElement("error");
				error.setAttribute("line", String.valueOf(errorLine));
				error.setAttribute("script", header.getQualifiedName());
				error.setAttribute("testErrorStatus", errorStatus);
				error.setTextContent(errorText);
				errors.appendChild(error);
				summaryElement.appendChild(errors);
			}

			scriptElem.appendChild(summaryElement);

		} catch (ParserConfigurationException e) {
			return (getEmptyResultXml(header.getQualifiedName()));
		}

		return atsRoot;
	}

	private static Element getEmptyResultXml(String scriptName) {
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Element atsRoot = null;

		try {

			final DocumentBuilder builder = factory.newDocumentBuilder();
			final Document document = builder.newDocument();

			atsRoot = document.createElement("ats");
			document.appendChild(atsRoot);

			//------------------------------------------------------------------------------------------------------
			// script header
			//------------------------------------------------------------------------------------------------------
			final Element script = document.createElement("script");

			atsRoot.appendChild(script);

			script.setAttribute("testId", "");
			script.setAttribute("testName", scriptName);

			script.setAttribute("cpuSpeed", "");
			script.setAttribute("cpuCount", "");
			script.setAttribute("totalMemory", "");
			script.setAttribute("osInfo", "");

			Element description = document.createElement("description");
			description.setTextContent("This script is empty");
			script.appendChild(description);

			Element author = document.createElement("author");
			author.setTextContent("");
			script.appendChild(author);

			Element prerequisite = document.createElement("prerequisite");
			prerequisite.setTextContent("");
			script.appendChild(prerequisite);

			final long start = System.currentTimeMillis();
			Element started = document.createElement("started");
			started.setTextContent(String.valueOf(start));
			script.appendChild(started);

			Element startedFormated = document.createElement("startedFormated");
			startedFormated.setTextContent(OffsetDateTime.ofInstant(Instant.ofEpochMilli(start), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd 'at' HH:mm:ss")));
			script.appendChild(startedFormated);

			Element groups = document.createElement("groups");
			groups.setTextContent("");
			script.appendChild(groups);

			Element quality = document.createElement("quality");
			quality.setTextContent("");
			script.appendChild(quality);

			Element summary = document.createElement("summary");

			summary.setAttribute("actions", "0");
			summary.setAttribute("status", "0");
			summary.setAttribute("suiteName", "N/A");
			summary.setAttribute("testName", scriptName);

			Element error = document.createElement("error");
			error.setAttribute("line", "0");
			error.setAttribute("script", scriptName);
			error.setTextContent("Unknown error");
			summary.appendChild(error);

			script.appendChild(summary);

		}catch(ParserConfigurationException e) {

		}

		return atsRoot;
	}
}
