/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.recorder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class TestError {
	private String script;
	private int line = 0;
	private String message;
	private TestErrorStatus testErrorStatus;

	public TestErrorStatus getTestErrorStatus() {
		return testErrorStatus;
	}

	public void setTestErrorStatus(TestErrorStatus testErrorStatus) {
		this.testErrorStatus = testErrorStatus;
	}

	public enum TestErrorStatus {
		FAIL_CONTINUE,FAIL_STOP,FAIL_PASS,
	}

	public TestError() {}

	public TestError(List<TestError> errors) {
		ObjectMapper objectMapper = new ObjectMapper();
		this.script = errors.getFirst().getScript();
		this.line = 0;
		try {
			this.message = objectMapper.writeValueAsString(errors);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	public TestError(String script, int line, String message) {
		this.script = script;
		this.line = line;
		this.message = message;
	}

	public TestError(String script, int line, String message, TestErrorStatus executionStatus) {
		this.script = script;
		this.line = line;
		this.message = message;
		this.testErrorStatus = executionStatus;
	}

	@JsonIgnore
	public Object getScriptLine() {
		final StringBuilder sb = new StringBuilder(script).append(":").append(line);
		return sb.toString();
	}

	//--------------------------------------------------------
	// getters and setters for serialization
	//--------------------------------------------------------

	public String getScript() {
		return script;
	}

	public void setScript(String value) {
		this.script = value;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}