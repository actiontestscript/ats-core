package com.ats.recorder;

import com.ats.executor.ScriptStatus;
import com.ats.executor.drivers.IDriverInfo;

public class RecorderSummaryData {

    private boolean passed = false;
    private int actions = -1;
    private int status = -1;
    private String suiteName = null;
    private String testName = null;
    private String data = null;
    private TestError error = null;
    private IDriverInfo driverInfo = null;

	public RecorderSummaryData(ScriptStatus st) {
		setPassed(st.isPassed());
		setActions(st.getActions());
		setSuiteName(st.getSuiteName());
		setTestName(st.getTestName());
	}

	public IDriverInfo getDriverInfo() {
		return driverInfo;
	}

	public void setDriverInfo(IDriverInfo driverInfo) {
		this.driverInfo = driverInfo;
	}

	public boolean isPassed() {
		return passed;
	}
	public void setPassed(boolean passed) {
		this.passed = passed;
	}
	public int getActions() {
		return actions;
	}
	public void setActions(int actions) {
		this.actions = actions;
	}
	public String getSuiteName() {
		return suiteName;
	}
	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public TestError getError() {
		return error;
	}
	public void setError(TestError error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
