const element = arguments[0];
var attrib, textValue, result = {};

Array.prototype.slice.call(element.attributes).forEach(function(attrib) {
	if(attrib.name != 'value'){
		result[attrib.name] = attrib.value;
	}
});

textValue = element.textContent;
if(textValue){
	result['text'] = textValue.trim().replace(/[\u00A0|\u202F]/g, ' ').replace(/\|/g, '##tmp-pipe-char##').replace(/[\t|\n|\r|\u8203|\u200B|\u200C|\u200D|\uFEFF|\u2028|\u2029|\uB200]/g, '').replace(/\p{Cf}/gu, '').replace(/##tmp-pipe-char##/g, '|');
}

if(element.value){
	result['value'] = element.value.replace(/[\u00A0|\u202F]/g, ' ').replace(/\|/g, '##tmp-pipe-char##').replace(/[\t|\n|\r|\u8203|\u200B|\u200C|\u200D|\uFEFF|\u2028|\u2029|\uB200]/g, '').replace(/\p{Cf}/gu, '').replace(/##tmp-pipe-char##/g, '|');
	if(element.tagName == 'INPUT'){
		if(element.type == 'radio' || element.type == 'checkbox'){
			if(element.checked == true){
				result['checked'] = 'true';
			}else{
				result['checked'] = 'false';
			}			
		}
	}
};