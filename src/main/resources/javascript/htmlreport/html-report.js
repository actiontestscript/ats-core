let isPopupOpened = false;
let isPlaylistNameInWidgetTruncated = false;
let playlistsData = null;
let currentAction = null;
let currentTestCase = null;
let currentPlaylist = null;
let topBegin = 94;

class WidgetManager {
	constructor() {
        this.currentAction = null;
        this.currentTestCase = null;
        this.currentPlaylist = null;
    }

	initCurrentPlaylist(playlist) {
	    let playlistSection = playlist.querySelector(".playlist-info-name-text");
		if (playlistSection) {
            const playlistName = document.querySelector(".current-playlist-name");
            playlistName.innerText = playlistSection.innerText;
            playlistName.parentElement.title = playlistSection.innerText;
        }
	}

	initCurrentSection(testCaseSection) {
	    let testCaseTextSection = testCaseSection.querySelector(".test-name-text");
		if (testCaseTextSection) {
			const parties = testCaseTextSection.innerText.split(".");
			const header = parties.slice(0, -1).join(".");
			const testCase = parties[parties.length-1];

			const currentSectionHeader = document.querySelector(".current-section-header");
			currentSectionHeader.innerText = header;
			currentSectionHeader.title = testCaseTextSection.innerText;

			const playlistInfoName = currentPlaylist.querySelector(".playlist-info-name-text");
			if (playlistInfoName) {
				currentSectionHeader.parentElement.title = testCaseTextSection.innerText + " " + playlistInfoName.innerText;

			}
			const currentSection = document.querySelector("#current-section");
			currentSection.innerText = testCase;
			currentSection.title = testCaseTextSection.innerText;
		}
	}
}

const widgetManager = new WidgetManager();

function processWidowResize() {
    let minimalWindowWidth = 1858;
    let startLeftPosition = 1608;
    let fixedDiv = document.getElementsByClassName('navigation-widget-container')[0];
    let windowWidth = window.innerWidth;

    if (fixedDiv) {
        if (windowWidth < minimalWindowWidth) {
            fixedDiv.style.left = `${startLeftPosition - (minimalWindowWidth - windowWidth + 35)}px`;
        } else {
            fixedDiv.style.left = 'calc(50% + 650px)';
        }
    }
}

processWidowResize();
window.addEventListener('resize', processWidowResize);

function applyVerticalStyle(imageSrc, imageModal, isRemove) {
    if (isRemove) {
        if (imageSrc.includes('video')) {
            imageModal.childNodes[3].classList.remove('image-vertical');
        } else {
            imageModal.childNodes[1].classList.remove('image-vertical');
        }
    } else {
        if (imageSrc.includes('video')) {
            imageModal.childNodes[3].classList.add('image-vertical');
        } else {
            imageModal.childNodes[1].classList.add('image-vertical');
        }
    }
}

function toggleElement(elementId, switchElement, type) {
    let element = document.getElementById(elementId);
    let baseClassName;
    let currentPlaylistId = null;
    let currentTestcaseId = null;
    let currentActionId = null;
    let contentDiv = null;
    let currentSection = null;
    let scrollOffset = 10;

    if (type === 'actions') {
        baseClassName = 'actions-container';
        currentSection = switchElement.parentElement;
        contentDiv = switchElement.parentElement.nextElementSibling;
        currentActionId = contentDiv.id;
        let currentTestcaseElement = contentDiv.parentElement;
        currentTestcaseId = currentTestcaseElement.id;
        currentPlaylistId = currentTestcaseElement.parentElement.parentElement.parentElement.parentElement.parentElement.id;
    } else if (type === 'testCase') {
        baseClassName = 'test-case-body';
        activateTestCaseGraph(switchElement);
        currentSection = switchElement.parentElement.parentElement.parentElement.parentElement.parentElement;
        let testcaseHeader = switchElement.parentElement.parentElement;
        testcaseHeader.style.border = "3px solid rgba(211, 211, 211, 1)";

        contentDiv = switchElement.parentElement.parentElement.nextElementSibling;
        currentTestcaseId = contentDiv.id;
        currentPlaylistId = contentDiv.parentElement.parentElement.parentElement.parentElement.parentElement.id;
        currentTestCase = currentSection;
        widgetManager.initCurrentSection(currentTestCase);
    } else if (type === 'suite') {
        baseClassName = 'playlist-data-general';
        currentSection = switchElement.parentElement.parentElement.parentElement;
        currentTestCase = null;
        let playlistHeader = switchElement.parentElement.parentElement;
        playlistHeader.style.border = "3px solid #6ea1ff";
        contentDiv = playlistHeader.nextElementSibling;
        currentPlaylistId = contentDiv.id;
        let playlist = playlistHeader.parentElement;

        if (playlist === null) {
            return;
        }

        if (!playlist.classList.contains('chart-rendered'))  {
            buildChartTypesRadar(playlist, playlistsData);
            buildChartTests(playlist, playlistsData);
            playlist.classList.add('chart-rendered');
        }
        currentPlaylist = playlist;
        widgetManager.initCurrentPlaylist(currentPlaylist);
    } else if (type === 'errors-group') {
        baseClassName = 'non-blocking-errors-items';
    } else {
        console.error('Unknown type:', type);
        return;
    }

    if (baseClassName !== 'non-blocking-errors-items') {
        const divs = document.querySelectorAll('div.display-toggle-icon-down');
        divs.forEach(toggleDiv => {
            const onClickContent = toggleDiv.getAttribute('onclick');
            const match = onClickContent.match(/"([^"]+)"/g);
            const lastArgument = match[match.length - 1].replace(/"/g, '');
            let dataDiv = null;
            if (lastArgument === 'suite') {
                dataDiv = toggleDiv.parentElement.parentElement.nextElementSibling;
                toggleDiv.parentElement.parentElement.style.border = "";
            } else if (lastArgument ===  'testCase') {
                dataDiv = toggleDiv.parentElement.parentElement.nextElementSibling;
                toggleDiv.parentElement.parentElement.style.border = "";
            } else if (lastArgument === 'actions') {
                dataDiv = toggleDiv.parentElement.nextElementSibling;
            }
            if (dataDiv) {
                if (currentPlaylistId !== dataDiv.id &&
                    currentTestcaseId !== dataDiv.id &&
                    currentActionId !== dataDiv.id) {
                        dataDiv.className = dataDiv.classList[0] + ' collapsed';
                        toggleDiv.className = 'display-toggle-icon-right';
                }
            }
        });
    }
    toggleSection(baseClassName, element, switchElement);

    if (!currentSection) return;
    window.scrollTo({
        top: currentSection.getBoundingClientRect().top - topBegin + window.scrollY - scrollOffset,
        behavior: "smooth"
    });
}

function toggleSection(baseClassName, element, switchElement) {
    element.className = element.className === `${baseClassName} collapsed` ? baseClassName : `${baseClassName} collapsed`;
    switchElement.className = switchElement.className === 'display-toggle-icon-right' ? 'display-toggle-icon-down' : 'display-toggle-icon-right';
}

function activateTestCaseGraph(switchElement) {
    let container = switchElement.parentElement.parentElement.parentElement.querySelector('.charts-container-chart-element.chart-duration');
    if (container === null) {
        console.log("container is null");
        return;
    }
    if (!container.classList.contains('chart-rendered'))  {
        buildChartDuration(container);
        const chartType = switchElement.parentElement.parentElement.parentElement.querySelector('.charts-container-chart-element.chart-types');

        const baseElement = chartType.parentElement.parentElement.parentElement.parentElement.parentElement;
        const data = [
            baseElement.querySelectorAll('.atl-navigation').length,
            baseElement.querySelectorAll('.atl-calls').length,
            baseElement.querySelectorAll('.atl-user-actions').length,
            baseElement.querySelectorAll('.atl-assert').length,
            baseElement.querySelectorAll('.atl-technical').length,
            baseElement.querySelectorAll('.atl-others').length
        ];
        buildChartTypes(chartType, data);

        container.classList.add('chart-rendered');
    }
}

function toggleActionsView(toggleButton) {
    toggleButton.parentElement.childNodes[1].click();
}

function toggleCollapsedElementsWithNavigationWidget(elementId) {
    let testCaseElement = document.getElementById(elementId);
    let actionsSectionElement = document.getElementById(elementId.replace('test-data', 'actions-container'));

    let suiteElement = testCaseElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    Array.from(suiteElement.getElementsByClassName('test-case-body')).forEach(el => el.classList.add('collapsed'));
    Array.from(suiteElement.getElementsByClassName('display-toggle-icon-down')).forEach(el => el.className = 'display-toggle-icon-right');

    Array.from(testCaseElement.parentElement.getElementsByClassName('display-toggle-icon-right')).forEach(el => el.className = 'display-toggle-icon-down');

    testCaseElement.className = 'test-case-body';
    actionsSectionElement.className = 'actions-container';

    let switchElement = testCaseElement.previousElementSibling.querySelector("div").querySelector("div");
    activateTestCaseGraph(switchElement);
}

function applyTruncationToScriptNamesInAction() {
    document.querySelectorAll('.script-name-text').forEach(element => {
        if (element.parentElement.parentElement.parentElement.parentElement.classList.contains("status-line-non-functional")) {
            if (element.clientWidth > 1210) {
                element.classList.add('truncated-text-1210');
            }
        } else {
            if (element.clientWidth > 790) {
                element.classList.add('truncated-text-790');
            }
        }
    });
}

function applyTruncationToSuiteName() {
    document.querySelectorAll('.playlist-info-name-text').forEach(element => {
        if (element.clientWidth > 460) {
            element.classList.add('truncated-text-460');
        }
    });
}

function applyTruncationToTestName() {
    document.querySelectorAll('.test-name-text').forEach(element => {
        if (element.clientWidth > 670) {
            element.classList.add('truncated-text-670');
        }
    });
}

function flashElement(element) {
    element.classList.add('flash');
    setTimeout(() => {
        element.classList.remove('flash');
    }, 250);
    setTimeout(() => {
        element.classList.remove('flash');
    }, 250);
}

function navigateToNextAndPrevSuiteById(button, isNext) {
    let id = button.parentElement.childNodes[3].title;
    const containers = Array.from(document.querySelectorAll('.playlist-info-header'));
    const currentIndex = containers.findIndex(div => div.id === id);
    if (currentIndex === -1) {
        console.log('Element not found');
        return;
    }
    const prevElement = containers[(currentIndex - 1 + containers.length) % containers.length];
    const nextElement = containers[(currentIndex + 1) % containers.length];

    if (isNext) {
        let suiteElementToggle = nextElement.getElementsByClassName('display-toggle-icon-right')[0];
        if (suiteElementToggle) {
            suiteElementToggle.click();
        }
        navigateToElementByTestId(nextElement.id, true);
    } else {
        let suiteElementToggle = prevElement.getElementsByClassName('display-toggle-icon-right')[0];
        if (suiteElementToggle) {
            suiteElementToggle.click();
        }
        navigateToElementByTestId(prevElement.id, true);
    }
}

function drawChartDonut(container, prcValues, labels) {
    if (!container) return null;

    const options = {
        series: prcValues,
        height: 110,
        width: 230,
        parentHeightOffset: 0,
        offsetX: 0,
        offsetY: 30,
        zoom: {
            enabled: false
        },
        toolbar: {
            show: false
        },
        grid: {
            padding: {
                top: 0,
                right: 0,
                bottom: -40,
                left: 0
            }
        },
        markers: {
            size: 2
        },
        labels: labels,
        chart: {
            type: 'donut'
        },
        plotOptions: {
            pie: {
                donut: {
                    size: '40%'
                }
            }
        },
        colors: ['#9dbf3f', '#3B5761', '#ecf0df'],
        tooltip: {
            enabled: true,
            shared: false,
            style: {
                fontSize: '10px',
                fontFamily: 'Arial',
                colors: ['#080808']
            },
            x: {
                show: false,
            },
            y: {
                show: true,
                formatter: function(value) {
                    return value + '%';
                }
            },
            theme: 'dark',
            fillSeriesColor: false
        },
        dataLabels: {
            style: {
                fontSize: '10px',
                fontFamily: 'Arial',
                colors: ['#080808']
            }
        },
        legend: {
            position: 'top',
            horizontalAlign: 'center',
            fontSize: '10px',
            fontFamily: 'Arial'
        }
    };

    const chart = new ApexCharts(container, options);
    chart.render();

    return chart;
}

function drawChartRadar(canvas, valuesRadar) {
    const categories = ['Navigation','Callscript','User action','Assertion','Technical','Others'];
    options = {
        chart: {
            type: 'radar',
            height: 243,
            width: 270,
            parentHeightOffset: 0,
            offsetX: -2,
            offsetY: -20,
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            }
        },
        grid: {
            padding: {
                top: -15,
                right: -15,
                bottom: -30,
                left: 0
            }
        },
        markers: {
            size: 3,
            colors: ['#FF5733'],
            strokeColors: '#ffffff',
            strokeWidth: 2,
            hover: {
                size: 5
            }
        },
        xaxis: {
            categories: categories,
            labels: {
                style: {
                    fontFamily: 'Arial, serif',
                    fontSize: '9px',
                    fontWeight: 'normal',
                }
            }
        },
        yaxis: {
            show: false
        },
        tooltip: {
            enabled: true,
            shared: false,
            style: {
              fontFamily: 'Arial, serif',
              fontSize: '9px',
              fontWeight: 'normal',
            },
            x: {
                show: false
            },
            y: {
                title: {
                    formatter: function(seriesName, { dataPointIndex }) {
                        return categories[dataPointIndex];
                    }
                },
                formatter: function(value) {
                    return value;
                }
            }
        },
        dataLabels: {
            enabled: false
        },
        series: [{ name: 'My Data', data: valuesRadar }]
    };

    const chart = new ApexCharts(canvas, options);
    chart.render();
}

function buildChartTests(playlist, playlistData) {
    if (playlist === null || playlistData.length === 0) return;

    let playlistDataStruct;
    const nameElement = playlist.querySelector('.playlist-info-name-text');
    if (nameElement === null) {
        playlistDataStruct = playlistData[0];
    } else {
        playlistDataStruct = playlistData.find(playlist => playlist.name === nameElement.innerHTML);
    };

    const values = [playlistDataStruct.passed, playlistDataStruct.failed, playlistDataStruct.filtered];
    const total = values.reduce((sum, num) => sum + num, 0);
    const percentages = total > 0 ? values.map(value => Math.round((value / total) * 100)) : values.map(() => 0);

    const labels = ['Passed', 'Failed', 'Filtered'];
    drawChartDonut(playlist.querySelector('.chart-tests'), percentages, labels);
    return values;
}

function buildChartDuration(container) {
    let dataElements = Array.from(container.parentElement.parentElement.parentElement.parentElement.querySelectorAll('.header'));
    let actionLabels = [];
    const peaks = dataElements.map((el, index) => {
        let actionLabelElement = el.querySelector('.action-name');
        actionLabels.push(actionLabelElement ? actionLabelElement.textContent.trim() : null);
        let dataArray = el.classList[1].split("-");
        return Number(dataArray[dataArray.length - 1] /1000 + index / '1e6').toFixed(2);
    });

    const peaksWithElements = dataElements.map((el, index) => {
        let dataArray = el.classList[1].split("-");
        return {"value": Number(dataArray[dataArray.length - 1] + index / '1e6'), "element": el};
    });

    let cumul = 0;
    const xAxisValues = peaks.map(v => Math.ceil(cumul += +v));

    let step = Math.max(Math.floor(peaks.length / 5), 1);
    var options = {
        chart: {
            type: 'line',
            height: 120,
            width: 230,
            parentHeightOffset: -15,
            offsetX: -9,
            offsetY: 0,
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            },
            grid: {
                padding: {
                    top: -20,
                    right: 3,
                    bottom: -30,
                    left: 5
                }
            },
            events: {
                click: function(event, chartContext, config) {
                    const dataPointIndex = config.dataPointIndex;
                    if (dataPointIndex !== -1) {
                        const targetElement = peaksWithElements[dataPointIndex].element;

                        let toggleElement = targetElement.parentElement.parentElement.parentElement.parentElement.querySelector('.display-toggle-icon-right');
                        if (toggleElement) {
                            toggleElement.click();
                        }
                        targetElement.scrollIntoView({ behavior: 'smooth', block: 'center' });

                        let statusLine = targetElement.querySelector("div");
                        highlightElement(statusLine);
                    }
                }
            }
        },
        series: [{
            name: '',
            data: peaks
        }],
        dataLabels: {
            enabled: false,
            formatter: function(value, {dataPointIndex}) {
                return actionLabels[dataPointIndex];
            }
        },
        xaxis: {
            categories: xAxisValues,
            tickAmount:step,
            labels: {
                show: true,
                formatter: function (value) {
                  return value + ' s';
                },
                style: {
                    fontSize: 10
                },
            },
        },
        yaxis: {
            labels: {
                style: {
                    fontSize: 10
                },
            },
            min: 0,
            max: Math.max(...peaks.map((Number)))
        },
        tooltip: {
            enabled: true,
            shared: false,
            style: {
              fontSize: '10px'
            },
            x: {
                show: false
            },
            y: {
                formatter: function(value, {dataPointIndex}) {
                    return actionLabels[dataPointIndex] + ' - ' + Math.round(peaksWithElements[dataPointIndex].value / 10) + ' ms';
                }
            }
        },
        stroke: {
            width: 1
        },
        colors: ['#00BAEC'],
        markers: {
            size: 2,
            colors: ['#FFA41B'],
            strokeWidth: 1,
            hover: {
              size: 2
            }
        }
    };
    var chart = new ApexCharts(container,options);
    chart.render();
}

function highlightElement(element) {
    let color = getComputedStyle(element).getPropertyValue('border-left-color');

    if (currentAction) {
        currentAction.style.border = "";
    }

    element.style.boxShadow = "0 0 10px 5px " + color;
    setTimeout(() => {
        element.style.boxShadow = "";
    }, 2000);

    element.style.border = "3px solid " + color;
    currentAction = element;
}

function _highlightElement(element) {
    element.style.transform = "scale(1.1)";
    element.style.transition = "transform 0.3s";
    element.style.transition = "background-color 3s";
    element.style.backgroundColor = "#0cfa6f";
    setTimeout(() => {
        element.style.backgroundColor = "";
        element.style.transform = "scale(1)";
    }, 3000);
}

function buildChartTypes(container, dataTypes) {
    const values = dataTypes;
    const categories = ["Navigation", "Callscript", "User action", "Assertion", "Technical", "Others"];
    const colors = ["#1BBC9B", "#3498DB", "#72B500", "#EE6AB9", "#6E4AD3", "#7D7D7D"];

    options = {
        chart: {
            type: 'bar',
            height: 112,
            width: 230,
            parentHeightOffset: -15,
            offsetX: -9,
            offsetY: 0,
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            }
        },
        plotOptions: {
            bar: {
                distributed: true
            }
        },
        xaxis: {
            categories,
            labels: {
                show: false
            },
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
        },
        yaxis: {
          labels: {
            style: {
              fontSize: '10px',
              fontFamily: 'Arial'
            }
          }
        },
        tooltip: {
            enabled: true,
            shared: false,
            style: {
              fontSize: '10px'
            },
            x: {
                show: false
            },
            y: {
                title: {
                    formatter: () => ''
                },
                formatter: function(value, {dataPointIndex}) {
                    return categories[dataPointIndex] + ' - ' + value;
                }
            }
        },
        grid: {
            padding: {
                top: -20,
                right: 3,
                bottom: -10,
                left: 5
            }
        },
        dataLabels: {
            enabled: false
        },
        legend: {
            position: 'right',
            fontSize: '10px',
            fontFamily: 'Arial',
            offsetY: -25,
            itemMargin: {
                horizontal: 0,
                vertical: 1
            }
        },
        colors,
            series: [{
                data: values
            }]
        };

    const chart = new ApexCharts(container, options);
    chart.render();
}

function buildChartTypesRadar(playlist, playlistData) {
    if (playlist === null || playlistData.length === 0) return;

    let playlistDataStruct;
    const nameElement = playlist.querySelector('.playlist-info-name-text');
    if (nameElement === null) {
        playlistDataStruct = playlistData[0];
    } else {
        playlistDataStruct = playlistData.find(playlist => playlist.name === nameElement.innerHTML);
    };

    let valuesRadar = playlistDataStruct.actions.map(action => action.value);
    const canvas = playlist.querySelector('.charts-container-chart-element.chart-types-suite');
    drawChartRadar(canvas, valuesRadar);
}

function buildTopChartActions() {
    const categories = ['Navigation', 'Calls', 'UserAction', 'Assertion', 'Technical', 'Others'];
    const valuesRadar = new Array(categories.length).fill(0);
    for (const playlist of playlistsData) {
        for (const action of playlist.actions) {
            const index = categories.indexOf(action.name);
            if (index !== -1) {
                valuesRadar[index] += action.value;
            }
        }
    }
    const canvas = document.querySelector('.charts-container-chart-element.chart-actions-report');
    drawChartRadar(canvas, valuesRadar);
}

function buildTopChartTests() {
    const values = new Array(2).fill(0);
    let totalActions = 0;
    for (const playlist of playlistsData) {
        values[0] += playlist.passed;
        values[1] += playlist.failed;
        totalActions += playlist.nbActions;
    }
    const total = values.reduce((sum, num) => sum + num, 0);
    const percentages = total > 0 ? values.map(value => Math.round((value / total) * 100)) : values.map(() => 0);

    document.querySelector('#executed-tests').innerText = total;
    document.querySelector('#executed-tests-passed').innerText = values[0];
    document.querySelector('#executed-tests-failed').innerText = values[1];
    document.querySelector('#executed-tests-actions').innerText = totalActions;

    const canvas = document.querySelector('.charts-container-chart-element.chart-tests-report');
    const labels = ['Passed', 'Failed'];
    drawChartDonut(canvas, percentages, labels)
}

function buildTopChartPlaylists() {
    let playlists = Array.from(document.querySelectorAll('.playlist'));
    let values = [0, 0];

    playlists.forEach(playlist => {
        const resultPass = playlist.querySelector('.result-pass-icon-transparent');
        if (resultPass === null) {
            values[1] = values[1] + 1;
        } else {
            values[0] = values[0] + 1;
        }
    });
    const totalDuration = playlistsData.reduce((total, playlist) => total + playlist.durationInSecond, 0);
    const total = values.reduce((sum, num) => sum + num, 0);
    const percentages = total > 0 ? values.map(value => Math.round((value / total) * 100)) : values.map(() => 0);

    document.querySelector('#executed-playlists').innerText = total;
    document.querySelector('#executed-playlists-passed').innerText = values[0];
    document.querySelector('#executed-playlists-failed').innerText = values[1];
    document.querySelector('#executed-playlists-total').innerText = formatDuration(totalDuration);

    const canvas = document.querySelector('.charts-container-chart-element.chart-results-report');
    const labels = ['Passed', 'Failed'];
    drawChartDonut(canvas, percentages, labels)
}

function formatDuration(durationInSecond) {
    let hours = Math.floor(durationInSecond / 3600);
    let minutes = Math.floor((durationInSecond % 3600) / 60);
    let seconds = durationInSecond % 60;

    hours = String(hours).padStart(2, '0');
    minutes = String(minutes).padStart(2, '0');
    seconds = String(seconds).padStart(2, '0');

    return `${hours}:${minutes}:${seconds}`;
}

function applyHeightToIframes() {
    let iFrames = document.getElementsByTagName('iFrame');
    if (iFrames) {
        try {
            Array.from(iFrames).forEach(element => {
                if (element) {
                    element.style.height = element.contentWindow.document.body.scrollHeight + 40 + 'px';
                    let iframeDocument = element.contentDocument || element.contentWindow.document;
                    let style = iframeDocument.createElement('style');
                    style.textContent = "body { font-family: 'Arial', sans-serif; }";
                    iframeDocument.head.appendChild(style);
                }
            });
        } catch(error) {
            console.error("function applyHeightToIframes, erreur :", error);
        }
    }
}

function unwrapFirstPlayList() {
    let elementNodeListOf = document.querySelectorAll('.display-toggle-icon-right');
    if (elementNodeListOf.length > 0) {
        elementNodeListOf.item(0).click();
        activateTestCaseGraph(elementNodeListOf.item(0));
    } else {
        let switchDown = document.querySelector('.display-toggle-icon-down');
        activateTestCaseGraph(switchDown);
    }
}

function displayTopGRaph() {
    if (playlistsData && playlistsData.length > 1) {
        buildTopChartPlaylists();
        buildTopChartTests();
        buildTopChartActions();
    } else {
        const divTopTest = document.querySelector('.suits-container.executed-tests');
        const divTopResults = document.querySelector('.suits-container.executed-playlists');
        const divTopActions = document.querySelector('.charts-container-chart-test.actions');
        if (divTopTest && divTopActions && divTopResults) {
            divTopTest.style.display = 'none';
            divTopActions.style.display = 'none';
            divTopResults.style.display = 'none';
        }
    }
}

function navigateToNextPlaylist() {
    if (currentPlaylist) {
        if (!playlistsData || playlistsData.length === 1) return;
        currentPlaylist = currentPlaylist.nextElementSibling;
        if (!currentPlaylist || currentPlaylist.id == 'playlist-data') {
            currentPlaylist = document.getElementsByClassName('playlist')[0];
        }
    } else {
        currentPlaylist = document.getElementsByClassName('playlist')[0];
    }
    navigateToPlaylist(currentPlaylist);
}

function navigateToPrevPlaylist() {
    if (currentPlaylist) {
        if (!playlistsData || playlistsData.length === 1) return;
        currentPlaylist = currentPlaylist.previousElementSibling;
        if (!currentPlaylist) {
            let playlists = document.getElementsByClassName('playlist');
            currentPlaylist = playlists[playlists.length - 1];
        }
        if (!playlistsData || playlistsData.length === 1) return;
    } else {
        let playlists = document.getElementsByClassName('playlist');
        currentPlaylist = playlists[playlists.length - 1];
    }
    navigateToPlaylist(currentPlaylist);
}

function navigateToPlaylist(playlist) {
    if (!playlist) {
        return;
    } else {
        widgetManager.initCurrentPlaylist(playlist);
    }

    currentTestCase = playlist.getElementsByClassName('general-layout')[0];

    let switchElement = playlist.querySelector('.display-toggle-icon-down, .display-toggle-icon-right');
    let dataElement = playlist.querySelector('.playlist-data-general');
    if (switchElement && dataElement) {
        toggleElement(dataElement.id, switchElement, "suite");
    }
}

function navigateToNextTestcase() {
    if (currentPlaylist) {
        if (currentTestCase) {
            currentTestCase = currentTestCase.nextElementSibling;
            if (!currentTestCase) {
                currentTestCase = currentPlaylist.getElementsByClassName('general-layout')[0];
            }
        } else {
            currentTestCase = currentPlaylist.getElementsByClassName('general-layout')[0];
        }
    }
    navigateToTestcase(currentTestCase);
}

function navigateToPrevTestcase() {
    if (currentPlaylist) {
        if (currentTestCase) {
            currentTestCase = currentTestCase.previousElementSibling;
            if (!currentTestCase) {
                let testCases = currentPlaylist.getElementsByClassName('general-layout');
                currentTestCase = testCases[testCases.length - 1];
            }
        } else {
            currentTestCase = currentPlaylist.getElementsByClassName('general-layout')[0];
        }
    }
    navigateToTestcase(currentTestCase);
}

function navigateToTestcase(currentTestCase) {
    if (!currentTestCase) {
        return;
    }

    widgetManager.initCurrentSection(currentTestCase);
    let switchElement = currentTestCase.querySelector('.display-toggle-icon-down, .display-toggle-icon-right');
    let dataElement = currentTestCase.querySelector('.test-case-body');
    if (switchElement && dataElement) {
        toggleElement(dataElement.id, switchElement, "testCase");
    }
}

function selectAction(actionContainer) {
    console.log(actionContainer);
    highlightElement(actionContainer);
}


function navigateToElement(elementId, isSuite) {
    let element = null;
    console.log("elementId: ", elementId);
    elementId = elementId.replaceAll(',', '.');
    if (isSuite) {
        element = document.getElementById(elementId).parentElement;
    } else {
        element = document.getElementById(elementId);
    }

    if (element) {
        window.scrollTo({
            top: element.getBoundingClientRect().top - topBegin + window.scrollY - 10,
            behavior: "smooth"
        });
    }
}

function navigateToElementByTestId(testId, isSuite) {
    testId = testId.replaceAll(',', '.');
    if (!isSuite) {
        toggleCollapsedElementsWithNavigationWidget(testId + '-test-data');
    }
    document.getElementById(testId).scrollIntoView({
        behavior: 'instant',
        block: 'start',
    });
}

function navigateToElementById(elementIdWithIndex, errorElement, isBlockingError) {
    let errorParentElement = isBlockingError ? errorElement.parentElement.parentElement : errorElement.parentElement.parentElement.parentElement.parentElement.parentElement;

    let toggleButtonElement = errorParentElement.querySelectorAll(".action-button-text")[0];
    let iconRightElement = toggleButtonElement.parentElement.querySelector('.display-toggle-icon-right');

    if (iconRightElement) {
        toggleActionsView(toggleButtonElement);
    }

    let elementById;
    let split = elementIdWithIndex.split(':');
    let elementId = split[0];
    let elementIndex = split[1];

    let elementsById = Array.from(errorParentElement.querySelectorAll(`.error.${CSS.escape(elementId)}`));
    if (elementsById.length > 1) {
        elementById = elementsById[elementIndex];
    } else {
        elementById = elementsById[0];
    }

    elementById.scrollIntoView({
        behavior: 'instant',
        block: 'start',
    });

    let statusLine = elementById;
    flashElement(statusLine);
    setTimeout(() => {
        flashElement(statusLine);
    }, 500);
}

function isSingleExec() {
    const elements = document.querySelectorAll(".general-report-layout");
    return !playlistsData || (playlistsData.length === 1 && elements.length === 1);
}

window.onload = function () {
    if (document) {
        let jsonData = document.getElementById("playlist-data");
        if (jsonData) {
            playlistsData = JSON.parse(jsonData.textContent);
            displayTopGRaph();
            unwrapFirstPlayList();
            if (isSingleExec()) {
                let widget = document.querySelector('.project-info-body-item');
                widget.style.display = 'none';
                navigateToNextTestcase();
            }
        }

        applyTruncationToScriptNamesInAction();
        applyTruncationToTestName();
        applyTruncationToSuiteName();
        applyHeightToIframes();

    }
    let loader = document.getElementById("loader");
    if (loader) {
        document.getElementById("loader").style.display = "none";
    }
}
