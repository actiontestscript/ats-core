var parent=arguments[0], result=[];
const tag=arguments[1], attributes=arguments[2], attributesLen=arguments[3];	
const scX=(window.outerWidth-window.innerWidth)/2+window.screenX+0.0001, scY=window.outerHeight-window.innerHeight+window.screenY+0.0001;

if(parent == null){
	parent = window.document;
};

const elts=parent.getElementsByTagName(tag);
const eltsLength=elts.length;

function addElement(e, t, a){
	let r = e.getBoundingClientRect();
	result[result.length] = [e, t, e.getAttribute('inputmode')=='numeric',e.getAttribute('type')=='password', r.x+0.0001, r.y+0.0001, r.width+0.0001, r.height+0.0001, r.left+0.0001, r.top+0.0001, scX, scY, a, false];
};

if(attributesLen == 0){

	for(let h = 0; h < eltsLength; h++){
		var e = elts[h];
		addElement(e, e.tagName, {});
	}

}else{

	function loop(e){

		let a={};
		const t=e.tagName;
		
		attributes.forEach(function (attributeName) {
			if(attributeName == 'text'){
				if(e.textContent){
					a['text'] = e.textContent.trim().replace(/\u00A0/g, ' ').replace(/\|/g, '##tmp-pipe-char##').replace(/[\t|\n|\r|\u8203|\u200B|\u200C|\u200D|\uFEFF|\u2028|\u2029|\uB200|\u2060]/g, '').replace(/\p{Cf}/gu, '').replace(/##tmp-pipe-char##/g, '|');
				}
			}else{
				const type = e.type;
				if(attributeName == 'checked' && t == 'INPUT' && (type == 'radio' || type == 'checkbox')){
					if(e.checked == true){
						a['checked'] = 'true';
					}else{
						a['checked'] = 'false';
					}		
				}else{
					if(e.hasAttribute(attributeName)){
						a[attributeName] = e.getAttribute(attributeName);
					}else{
						var prop = e[attributeName];
						if(prop != null && prop != 'function'){
							a[attributeName] = prop.toString();
						}else{
							a[attributeName] = window.getComputedStyle(e,null).getPropertyValue(attributeName);
						}
					}
				}
			}
		});

		if(Object.keys(a).length == attributesLen){
			addElement(e, t, a);
		}
	};
	
	Array.prototype.forEach.call(elts, loop);
};