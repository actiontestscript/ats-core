
// constante or init
let ellapseAction = 1500;
let ellapseImg = ellapseAction;
let popUpDuration = ((ellapseAction * 0.9) / ellapseAction);
let mouseMotionTime = 1.5;
let mouseAnimationDuration = 2000;
let minActionDuration = 300;
let enterTxtTime = 1500;

let focusTime = 2;
let num_act = 0;
let currentIndex = 0;
let playing = false;
let selectedAction = false;
let offsetBorder = 0;
let stopAction = false;
let previousAnimationIndex = -1;
let timeOrDate = true; // true for Date
let currentTime = 0;
let duration = 0;
let previousIndex = 0;
let fullScreenAsk = true;
let fullScreen = false;
let fullInitialised = false;
let addActsData = {};
let currentChannel = "";
let activeChannel = {};
let channelInfo = {};
let sequential = true; //le déroulement du test de facon séquentiel ou timestamp
let minPrevAct = -1;
let minOuterY = 0;
let mousePosition = { x: 0, y: 0 };
let PrevMousePosition = { x: 0, y: 0 };
let animationFinished = true;
let pos = { vPos: 0, hPos: 0, height: 0, width: 0, scale: { x: 0, y: 0 } };
let imgStock = {};
let imgStockLoaded = 0;
let dataLoaded = false;

let numDataInitialized = 0;

let NBComment = 0;
let NumSection = -1;

const lang = ["en_US", "fr_FR"];
let langInd = 0; // en_US
const NagAction = ["back", "refresh", "next"];
const actionToSub = ["ActionMouseKey",
    "ActionMouseDragDrop",
    "ActionMouse",
    "ActionMouseScroll",
    "ActionGotoUrl"];

const abstractAct = ["ActionCallscript"];

const centerActDisplay = ["ActionAssertValue", "ActionAssertCount", "ActionComment", "ActionAssertProperty", "ActionProperty", "ActionApi", "ActionCallscript"];
const operator = ["= ", "==", "<=", ">=", "<>", "=~", "< ", "> "];
let boundScreen = { x: 0, y: 0, width: 0, height: 0 }
let appChannel = {};
let offset = { chrome: { x: 0, y: -7 }, firefox: { x: 0, y: 0 }, msedge: { x: -25, y: 35 } };
const iconAnim = {
    ActionMouseKey: {
        default: ["mouse"],
        click_right: ["mouse", "mouse_select_right"],
        click: ["mouse", "mouse_select_left"],
        click_double: ["mouse", "mouse_double_click", "mouse", "mouse", "mouse_double_click"]
    },
    ActionMouseDragDrop: {
        default: ["mouse"],
        drap: ["mouse", "drag_start"],
        drop: ["mouse", "drag_drop"]
    },
    ActionMouse: {
        default: ["mouse"],
        over: ["mouse", "mouse_over"],
        swipe: ["mouse", "mouse_swipe"]
    },
    ActionMouseScroll: {
        default: ["mouse", "mouse_select_scroll_up_down", "mouse", "mouse_select_scroll_up_down"],
        up: ["mouse", "mouse_select_scroll_up", "mouse", "mouse_select_scroll_up"],
        down: ["mouse", "mouse_select_scroll_down", "mouse", "mouse_select_scroll_down"]
    },
    ActionMouseSwipe: {
        default: ["mouse", "mouse_swipe"]
    },
    ActionChannelStart: { default: ["layers_new"] },
    ActionChannelClose: { default: ["layers_close"] },
    ActionWindowSwitch: { default: ["switch_windows"] },
    ActionChannelSwitch: { default: ["layers_go"] },
    ActionWindowResize: { default: ["application_resize"] },
    ActionWindowState: {
        default: ["application_close"],
        close: ["application_close"],
        restore: ["application_restore"],
        maximize: ["application_maximize"],
        reduce: ["application_reduce"]
    },
    ActionGotoUrl: {
        default: ["address_bar"],
        back: ["breadcrumb_select"],
        refresh: ["breadcrumb_select_perrent"],
        next: ["breadcrumb_select_current"]
    },
    ActionText: {
        default: ["textfield"],
        extra: "extra_action_txt"
    },
    ActionAssertCount: { default: ["tick"] },
    ActionAssertProperty: { default: ["check_property"] },
    ActionProperty: { default: ["attributes_display"] },
    ActionAssertValue: { default: ["check_value"] },
    ActionScripting: { default: ["scripting"] },
    ActionPropertySet: { default: ["attributes_set"] },
    ActionSelect: { default: ["combo_box_light_blue"] },
    ActionComment: { default: ["comment_step"] },
    ActionApi: {
        default: ["network_clouds"],
        rest: ["network_clouds_rest"],
        soap: ["network_clouds_soap"]
    }
}

const actionName = {
    ActionMouseKey: { en_US: "Mouse click", fr_FR: "Clic de souris" }, //
    ActionMouseDragDrop: { en_US: "Drag and drop", fr_FR: "Glisser dÃ©poser" }, // DRAG_DROP
    ActionMouse: { en_US: "Mouse", fr_FR: "" }, //
    ActionMouseScroll: { en_US: "Mouse scroll", fr_FR: "Molette de souris" }, // SCROLL
    ActionMouseSwipe: { en_US: "", fr_FR: "" }, //
    ActionChannelStart: { en_US: "Start channel", fr_FR: "DÃ©marrer un canal" }, // START_CHANNEL
    ActionChannelSwitch: { en_US: "Switch channel", fr_FR: "Changer de canal" }, // SWITCH_CHANNEL
    ActionChannelClose: { en_US: "Close channel", fr_FR: "Fermer un canal" }, // CLOSE_CHANNEL
    ActionWindowSwitch: { en_US: "Switch window", fr_FR: "Changer de fenÃªtre" }, // SWITCH_WINDOW
    ActionWindowResize: { en_US: "Size and location", fr_FR: "Taille et position" }, // RESIZE_WINDOW
    ActionWindowState: { en_US: "State window", fr_FR: "État de la fenÃªtre" }, // 
    ActionGotoUrl: { en_US: "Go to URL", fr_FR: "Redirection URL" }, // GOTO_URL
    ActionText: { en_US: "Enter Text", fr_FR: "Saisie clavier" }, // ENTER_TEXT
    ActionAssertCount: { en_US: "Check occurences", fr_FR: "VÃ©rifier les occurrences" }, // ASSERT_OCCURRENCES
    ActionAssertProperty: { en_US: "Check property", fr_FR: "VÃ©rifier la propriÃ©tÃ©" }, // ASSERT_PROPERTY
    ActionProperty: { en_US: "Get Property", fr_FR: "récupération de propriÃ©tÃ©" }, // PROPERTY
    ActionPropertySet: { en_US: "Set Property", fr_FR: "modification de propriÃ©tÃ©" }, // PROPERTY
    ActionAssertValue: { en_US: "Check value", fr_FR: "VÃ©rifier les valeurs" }, // ASSERT_VALUE
    ActionScripting: { en_US: "Execute script", fr_FR: "ExÃ©cution de script" }, // SCRIPTING
    ActionSelect: { en_US: "Select", fr_FR: "SÃ©lectionner" }, // SELECT
    ActionComment: { en_US: "Comment", fr_FR: "Commentaire" }, // COMMENT
    ActionApi: { en_US: "Web API", fr_FR: "API Web" } // WEB_API
}

let popUpTL = gsap.timeline();
let animTL = gsap.timeline();
let visiblePopUp = 0;// graphical element
let imgToDisplay = document.getElementById("displayer");
let PrevImgToDisplay = document.getElementById("displayer_prev");
let minPU = document.getElementById("miniaturesPU");
let minImgBox = minPU.firstElementChild;
let overPlayer = document.getElementById("overlay_player");
let focus = document.getElementById("focus");
let mouse = document.getElementById("mouseCont");
mouse.lastElementChild.firstElementChild.src = icons["actions_32"].mouse;
mouse.lastElementChild.lastElementChild.src = icons["actions_32"].mouse;
let arrow = document.getElementById("arrowPointer");
arrow.src = icons["actions_24"].focus_arrow;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function play_pause() {

    scriptClose();
    hiddeTestsInfos();
    stopAction = false;
    if (playing) {
        //console.info("pause");
        playing = false;
        let playButton = document.getElementById("play_pause");
        playButton.src = icons["player_32"].play;
    } else {
        //console.info("play")
        playing = true;
        globalUpdate();
        play();
    }
}

const play = async function () {

    let playButton = document.getElementById("play_pause");
    playButton.src = icons["player_32"].pause;

    //console.debug("start");

    while ((currentIndex < num_act) && playing) {

        const action = actions[currentIndex];

        if (action.Channel != null) {
            boundScreen = action.Channel.bound;
        }

        //console.log("playAction : " + action.index);

        await playAction(action);

        //console.log("End playAction : " + action.index);

        if (!selectedAction && currentIndex < (num_act - 1)) {
            currentIndex++;
        } else if (currentIndex == (num_act - 1)) {
            playing = false;
        } else {
            selectedAction = false;
        }

    }

    if (!playing) {
        //console.info("stop in actions loop");
    }

    playButton.src = icons["player_32"].play;

    //console.debug("stopAction : " + stopAction);
    if (stopAction) {
        initProgress();
        stopAction = false;
    }

    playing = false;
    //console.debug("end");

};

const playAction = async (action) => {

    let numDisplayedImg = imgStock[action.index].numImages;

    let subAction = getSubAction(action.type);

    if (numDisplayedImg == 0 || (centerActDisplay.includes(subAction) && typeof (action.element) == "undefined")) {

        showImg(action, 0, true);
        updateActScroll(action.index);
        updateProgress(action.index + 1);
        let overText = document.getElementById("overText");
        overText.style.display = "flex";
        await sleep(ellapseAction);
        overText.style.display = "none";

    } else {

        ellapseImg = ellapseAction / numDisplayedImg;

        ellapseImg = ellapseImg < minActionDuration ? minActionDuration : ellapseImg;

        let subAction = getSubAction(action.type);
        //console.debug(subAction);

        let animInd = getIndImgAnim(action);
        //console.log(numDisplayedImg);
        //console.debug("animInd : " + animInd);
        animInd = animInd <= numDisplayedImg - 1 ? animInd : numDisplayedImg - 1;
        //console.debug("animInd : " + animInd);

        if (action.element != null && !action.element.tag.includes("root")) {
            //console.log("calc pos");
            pos = posElemCalc(action);
        }

        for (let image_index = 0; image_index < numDisplayedImg; image_index++) {

            animationFinished = false;

            if (!selectedAction && playing) {
                showImg(action, image_index, true);
            } else {
                //console.debug("stop in action");
                break;
            }

            if (image_index == 0) {
                // overlay  Check
                showOverlay(action);
                updateActScroll(action.index);
                updateProgress(action.index + 1);
            }

            if (action.element != null) {
            /* //console.debug("el not null" + (!action.element.tag.includes("root")) + (action.element.foundElements == 1 || action.error == 0 || centerActDisplay.includes(subAction)));
             */if ((!action.element.tag.includes("root")) && (action.element.foundElements == 1 || action.error == 0 || centerActDisplay.includes(subAction))) {
                    animTL.clear(true);
                    //console.debug("1 element : " + image_index);
                    if (image_index <= animInd) {
                        // focus ?
                        if (image_index == animInd - 1 || animInd == 0) {
                            //console.debug("show tracking");
                            await tracking(pos);
                            if (image_index != animInd) {
                                //console.debug("nextImage before Focus")
                                animTL.call(nextImage);
                            }
                        } else if (image_index != animInd) {
                            endAnimation();
                        }

                        if (image_index == animInd) {
                            // element ?
                            //console.debug("show focus")
                            showFocus(pos, action);
                            specificAnimation(action, pos);
                        }

                    } else {
                        endAnimation();
                    }

                } else if (action.element.tag.includes("root")) {
                    pos.height = 0; pos.width = 0;
                    //console.log("root");
                    if (image_index <= animInd) {

                        if (image_index == animInd - 1 || animInd == 0) {
                            //console.debug("show tracking");
                            await tracking(pos);
                            if (image_index != animInd) {
                                //console.debug("nextImage before Focus")
                                animTL.call(nextImage);
                            }
                        } else if (image_index != animInd) {
                            endAnimation();
                        }
                        if (image_index == animInd) {
                            // element ?
                            //console.debug("show focus")
                            showFocus(pos, action);
                            specificAnimation(action, pos);
                        }

                    } else {
                        endAnimation();
                    }

                }

            } else {
                endAnimation();
            }

            await animationEnded();
        }

    }

};

function specificAnimation(action, pos) {

    //console.debug("specific animation");

    let subAction = getSubAction(action.type);

    let mousePosX = pos.vPos + (pos.width / 2) + 2;
    let mousePosY = pos.hPos + (pos.height / 2) + 2;

    if (subAction.includes("Mouse")) {

        //console.debug(subAction);

        mouse.style.visibility = "visible";
        mouse.style.opacity = "100%";

        animTL.set(mouse, { left: mousePosX, top: mousePosY, opacity: 1, display: "flex" }, ">");
        if (action.element.extra != null) {
            let clickPosX = pos.vPos + (pos.scale.x * action.element.extra.hVal);
            let clickPosY = pos.hPos + (pos.scale.y * action.element.extra.vVal);
            animTL.to(mouse, 1, { left: clickPosX, top: clickPosY }, ">");
        }

        animTL.call(mouseAnimation, [action]);

    } else if (subAction == "ActionText") {

        //console.debug(subAction);

        let extra = document.createElement("img");
        extra.classList.add("extraActionTxt");
        extra.src = icons["actions_32"][iconAnim[subAction].extra];
        focus.lastElementChild.appendChild(extra);
        animTL.to(focus, 2, { opacity: 1, display: "flex" }, ">");
        animTL.to(focusRect, 0.5, { opacity: 1, onStart: enterText, onStartParams: [action] }, "<");
    } else {

        animTL.call(endAnimation);
    }

}

function getIndImgAnim(action) {

    const subAction = getSubAction(action.type);
    let ind = 0;

    switch (subAction) {
        case 'ActionAssertCount':
        case 'ActionAssertProperty':
        case 'ActionProperty':
        case 'ActionPropertySet':
        case 'ActionAssertValue':
        case 'ActionScripting':
            ind = 0;
            break;
        //case 'ActionMouseKey':
        /*             if (imgStock[action.index].numImages == 4) {
                        ind = 2;
                        break;
                    } */
        case 'ActionMouseDragDrop':
        case 'ActionText':
        case 'ActionSelect':
            ind = 1;
            break;
        case 'ActionMouseScroll':
        case 'ActionMouse':
        case 'ActionMouseKey':
            ind = 2;
            break;

        default:
            break;

    }

    return ind;
}

function showOverlay(action, force = false) {

    if (force) {
        overPlayer.style.visibility = "visible";
        return true;
    }

    if (action != null) {
        let subAction = getSubAction(action.type);

        if (centerActDisplay.includes(subAction)) {
            if (typeof (action.element) == "undefined") {
                overPlayer.style.visibility = "visible";
                return true;
            }
        }

    }
    overPlayer.style.visibility = "hidden";
    return false;
}

function forceOverlay() {
    showOverlay(null, true);
}

const tracking = function (pos) {

    arrow.parentElement.style.visibility = "visible";
    animTL.to(arrow.parentElement, 0, { opacity: 1, display: "flex" })
    animTL.to(arrow.parentElement, 1.5, { left: (pos.vPos - arrow.width - 15), top: ((pos.hPos + pos.height / 2 + 2) - (arrow.height / 2)) });

}

const nextImage = async function () {
    //console.log("next image : " + getTime());
    //await sleep(200);
    animationFinished = true;

}

function showFocus(pos, action) {

    let focus = document.getElementById("focus");
    let subAction = getSubAction(action.type);

    focus.firstElementChild.firstElementChild.src = icons["actions_32"][selectSubIcon(subAction, action.value)];
    focus.firstElementChild.lastElementChild.innerHTML = actionName[subAction][lang[langInd]];

    animTL.to(focus, { left: pos.vPos, top: pos.hPos - 30 }, ">")

    let focusRect = focus.querySelector("#focusRect");
    //console.debug(!action.element.tag.includes("root"));
    if (!action.element.tag.includes("root")) {
        focusRect.style.height = pos.height + "px";
        focusRect.style.width = pos.width + "px";
        animTL.to(focusRect, { opacity: 1 });

    }

    animTL.to(focus, 0.150, { opacity: 1, display: "flex" }, "<");

}

function posElemCalc(action) {

    const displayer = document.getElementById("displayer");
    const VB = document.getElementById("videoBox");
    let scale = focusELementData(displayer.getBoundingClientRect());

    let marginLeft = displayer.getBoundingClientRect().x - VB.getBoundingClientRect().x;
    let marginTop = displayer.getBoundingClientRect().y - VB.getBoundingClientRect().y;

    let element = action.element;

    ////console.warn(scale);

    ////console.warn(marginLeft + ";" + marginTop);

    // to improve
    let tmpOffset = { x: 0, y: 0 };

    if (typeof (offset[appChannel[action.Channel.name]]) != "undefined") {
        tmpOffset.x = offset[appChannel[action.Channel.name]].x;
        tmpOffset.y = offset[appChannel[action.Channel.name]].y;
    }

    let leftPos = (marginLeft + (scale.x * (element.bound.x + tmpOffset.x))) - 2;
    let topPos = ((marginTop + (scale.y * (element.bound.y + tmpOffset.y)))) - 2;

    let height = (element.bound.height * scale.y) + 4;
    let width = (element.bound.width * scale.x) + 4;

    ////console.warn(leftPos + ";" + topPos + " || " + width + ";" + height);

    leftPos = leftPos < 0 ? 0 : leftPos;
    topPos = topPos < 0 ? 0 : topPos;

    return { vPos: leftPos, hPos: topPos, width: width, height: height, scale: scale };

}

function getSubAction(type) {

    if (type == null) {
        return "subAction Available";
    }

    return type.substring(type.lastIndexOf(".") + 1, type.length);
}

const animationEnded = async function () {

    //console.log("ellapse : " + ellapseImg);
    await sleep(ellapseImg);

    while (!animationFinished) {
        //console.debug("looping");
        await sleep(10);
    }

    //console.log("end animation");

}

const endAnimation = async function () {
    //console.debug("end animation");

    await sleep(1000);

    animTL.to(focus, 0, { opacity: 0, display: "none" }, ">");
    animTL.to(arrow.parentElement, 0, { opacity: 0, display: "none" }, "<");
    animTL.to(mouse, 0, { opacity: 0, display: "none" }, "<");
    mouse.lastElementChild.style.visibility = "hidden";

    let focusRect = focus.querySelector("#focusRect");
    animTL.to(focusRect, { opacity: 0 }, "<");

    focusRect.style.height = "0px";
    focusRect.style.width = "0px";

    animationFinished = true;
}

function updateProgress(index) {

    let progress = document.getElementById("progression");
    let dotTime = document.getElementById("dotTime");
    let avancement = (index) * 100 / num_act;
    let calPos = ((document.getElementById("progress_box").offsetWidth * avancement) / 100) - 9;
    let dotPos = ((calPos * 100) / (document.getElementById("progress_box").offsetWidth))

    ////console.log(calPos + ";" + dotPos + "%");

    if (index >= num_act) {
        index -= 1;
    }
    currentTime = actions[index].timeLine;

    timeUpdate();

    gsap.to(progress, ((ellapseImg * 0.8) / 1000), { width: "" + avancement + "%" });
    gsap.to(dotTime, ((ellapseImg * 0.8) / 1000), { left: dotPos + "%" }, "<");

    updateChannelData(actions[index]);

}

function updateText(index) {
    document.getElementById("actionTxt").innerHTML = (index) + "/" + (num_act - 1);
    document.getElementById("actionsCount").innerHTML = (index) + "/" + (num_act - 1);
}

const initProgress = async () => {

    let progress = document.getElementById("progression");
    ////console.debug("index : " + index + ", nbActions : " + num_act);
    gsap.to(progress, ((ellapseAction * 0.8) / 1000), { width: "0%" });

    let playButton = document.getElementById("play_pause");
    playButton.src = icons["player_32"].play;
    showOverlay(true);

    currentIndex = 0;

    allAPUToStock();
    globalUpdate();

}

function stopPlaying() {

    scriptClose();
    //console.debug("STOP");
    playing = false;
    stopAction = true;
    currentIndex = 0;
    animTL.progress(1);

    if (!playing) {
        initProgress();
    }

}

function step(diff) {

    scriptClose();
    hiddeTestsInfos();
    //console.debug(diff);
    selectedAction = true;

    allAPUToStock();
    if ((currentIndex == 0 && diff == -1) || (currentIndex == num_act && diff == 1)) {
        return;
    }
    currentIndex += diff;
    //console.debug("step");
    globalUpdate();
}

function selectAction(index, indImg = 0) {

    scriptClose();
    hiddeTestsInfos();

    //console.debug("selectAction");
    if (index < imgStockLoaded) {

        selectedAction = true;
        if (currentIndex != 0 || index != 0) {
            allAPUToStock();
        }

        showOverlay(actions[index]);
        currentIndex = Number(index);
        globalUpdate(indImg);
    } else {
        updateActScroll(index);
        forceOverlay();
        gsap.fromTo("#overText", 5, { opacity: 1 }, { opacity: 0 });

    }
}

function globalUpdate(indImg = 0) {
    //console.log("global update : " + currentIndex);

    let action = actions[currentIndex];

    showImg(action, indImg);
    updateProgress(currentIndex);
    updateActScroll(currentIndex);

    selectedAction = false;

}

function videoBoxSizeSelection(reduce) {

    let videoSection = document.getElementsByClassName("videoSection")[0];

    if (reduce) {
        let playerBound = document.getElementById("player").getBoundingClientRect();
        let scrollWidth = document.getElementById("actionsScroller").getBoundingClientRect().width;
        videoSection.style.maxHeight = (playerBound.height - minOuterY) + "px";
        videoSection.style.width = ((playerBound.width - 16) - (scrollWidth + 32)) + "px";
    } else {
        videoSection.style = "";
    }
}

function updateActScroll(index) {

    ////console.log("update act scroll");
    let subAction = getSubAction(actions[index].type);
    if (!abstractAct.includes(subAction)) {
        let actionScroller = document.getElementById("actionsScroller");
        scrollStyle = getComputedStyle(actionScroller);
        if (fullScreen && scrollStyle.display === "none" &&
            (previousAnimationIndex != index || !playing)) {
            popUpAnimation(index);
        } else {
            visiblePopUp = 0;
        }

        let animTime = 0.5;

        let tmp = document.getElementById("scrollerDataActions");
        const act = tmp.querySelector("#S_Act_" + String(index));

        let tl = gsap.timeline();

        let prevAct = tmp.querySelector("#S_Act_" + previousAnimationIndex);

        let OSPrevAct = 4;

        if ((previousAnimationIndex < parseInt(index)) && (prevAct !== null) && (selectedAction)) {
            OSPrevAct = prevAct.lastElementChild.clientHeight + 4;
        }

        if (prevAct != null && (previousAnimationIndex != parseInt(index))) {
            prevAct.classList.remove("selected");
            let content = prevAct.lastElementChild;

            if (content.style.height != "0px") {
                tl.addLabel("start");
                //tl.to(content, animTime, { height: "0px", paddingTop: "0px", paddingBottom: "0px", paddingLeft: "8px", paddingRight: "8px", marginTop: "0px", ease: Power0.easeNone });
                tl.to(content, animTime, { height: "0px", ease: Power0.easeNone });
                tl.to(content, animTime * 0.5, { marginTop: "0px", ease: Power0.easeNone }, ">");
            }

            act.classList.add("selected");

            content = act.lastElementChild;
            let contentSize = getDatacontSize(content);

            tl.to(content, animTime, { height: contentSize + "px", marginTop: "6px", ease: Power0.easeNone }, "start");
            tl.to(content, 0.5, { opacity: 1, }, "<+0.25");

            const aboveAct = tmp.querySelector("#S_Act_" + (index - 2));

            if (aboveAct != null) {
                if (index > 1) {
                    tl.to(tmp, animTime, { scrollTo: (aboveAct.offsetTop + aboveAct.offsetHeight - OSPrevAct), ease: Power0.easeNone }, "start");
                } else {
                    tl.to(tmp, animTime, { scrollTo: 0, ease: Power0.easeNone }, "start");
                }
            }
        }

        //console.debug(previousAnimationIndex + " : " + index);

        previousAnimationIndex = index;

    }

}

function showImg(action, image_index, next = false) {

    updateChannelData(action);

    const type = imgStock[action.index].imageType;
    const imageData = imgStock[action.index].imagesPool[image_index];

    const prevDim = imgToDisplay.getBoundingClientRect();

    PrevImgToDisplay.src = imgToDisplay.src;

    let subAction = getSubAction(actions[action.index].type);

    let dimHeight = 400;
    let dimWidth = 600;

    if (prevDim.height > dimHeight) {
        dimHeight = prevDim.height;
    }

    if (prevDim.width > dimWidth) {
        dimWidth = prevDim.width;
    }


    if (imgStock[action.index].numImages == 0 || ("ActionApi".includes(subAction) && typeof (action.element) == "undefined")) {

        imgToDisplay.src = icons["actions_32"].black_bg;
        imgToDisplay.style = "";
        imgToDisplay.style.height = dimHeight + "px";
        imgToDisplay.style.width = dimWidth + "px";

    } else {
        if (next) {
            gsap.to(PrevImgToDisplay, 0, { opacity: 1 });
        } else {
            gsap.to(PrevImgToDisplay, 0, { opacity: 0 });
        }

        imgToDisplay.src = "data:image/" + type + ";base64," + imageData;

        imgToDisplay.style = "";

        const newDim = imgToDisplay.getBoundingClientRect();

        let dimHeight = 400;
        let dimWidth = 600;

        console.log(prevDim.width + ";" + prevDim.height);
        console.log(newDim.width + ";" + newDim.height);

        if (prevDim.height > newDim.height) {
            dimHeight = prevDim.height;
        } else {
            if (newDim.height > dimHeight) {
                dimHeight = newDim.height;
            }
        }

        if (prevDim.width > newDim.width) {
            dimWidth = prevDim.width;
        } else {
            if (newDim.width > dimWidth) {
                dimWidth = newDim.width;
            }
        }

        console.log(prevDim.width + ";" + prevDim.height);
        console.log(newDim.width + ";" + newDim.height);

        imgToDisplay.style.height = dimHeight + "px";
        imgToDisplay.style.height = dimWidth + "px";

    }

    if (next) {
        gsap.to(PrevImgToDisplay, 0.5, { opacity: 0 });
        gsap.to(imgToDisplay, 0.5, { opacity: 1 });
    } else {
        gsap.to(imgToDisplay, { opacity: 1 });
    }

    showOverlay(action);

    updateText(action.index);
}

function animation(index) {

    let actionScroller = document.getElementById("actionsScroller");
    scrollStyle = getComputedStyle(actionScroller);
    if (fullScreen && scrollStyle.display === "none" &&
        (previousAnimationIndex != index || !playing)) {
        popUpAnimation(index);
    } else {
        visiblePopUp = 0;
    }

    previousAnimationIndex = index;

}

function popUpAnimation(index) {

    let popUp = document.getElementById("popUp_" + index);

    let style = popUp.currentStyle || window.getComputedStyle(popUp);
    let marginTop = parseFloat(style.marginTop.substring(0, style.marginTop.length - 2));
    let subAction = getSubAction(actions[index].type);
    if (centerActDisplay.includes(subAction)) {

        let positionX = (popUp.parentElement.getBoundingClientRect().width - popUp.offsetWidth) / 2 + (2 / 100 * popUp.parentElement.getBoundingClientRect().width);
        let positionY = popUp.parentElement.getBoundingClientRect().height / 2 - (popUp.offsetHeight + marginTop) + popUp.offsetHeight / 2;
        popUpTL.to(popUp, { duration: popUpDuration, x: -positionX, y: -positionY });
    } else {
        popUpTL.to(popUp, { duration: popUpDuration, x: 0 });
    }

    visiblePopUp++;
    popUpHandler(index);

    popUp.classList.add("centerPopUp");

}

function getTimeS() {
    var timing = Math.round((new Date).getTime() / 1000);
    return timing;
}

function getTime() {
    return (new Date).getTime();
}

function enterText(action) {

    const regex = /(\$key\(\w+\))/ig;
    let defValue = action.value.replaceAll(regex, '\t');

    let touchShowTime = enterTxtTime / defValue.length;

    showText(defValue, touchShowTime);

    let gsapAfter = "<+=" + String(((defValue.length + 1) * touchShowTime / 1000));
    animTL.to(focusRect, { opacity: 0 }, gsapAfter);

}

const showText = async function (text, time) {

    let insideRect = document.getElementById("insideRect");
    insideRect.style.opacity = "1";
    for (let i = 0; i < text.length; i++) {
        insideRect.innerText = text.substring(0, i + 1);
        await sleep(time);
    }

    if (text.length < 2) {
        await sleep(1000);
    }

    focus.lastElementChild.removeChild(focus.lastElementChild.firstElementChild);

    insideRect.innerText = "";
    insideRect.style.opacity = "0";

    endAnimation();

}

function pausing() {
    //console.log()
}

function stepper(num) {

    //console.debug(getTime() + " : " + num);
}

const mouseAnimation = async function (action) {

    let subAction = getSubAction(action.type);

    ////console.debug("mouse animation, action :" + action.value + "\nsubAction : " + subAction);
    ////console.debug("include mouse : " + subAction);
    let iconsTxt = selectSubIcons(subAction, action.value);
    let timeSleeping = mouseAnimationDuration / iconsTxt.length;
    mouse.lastElementChild.style.visibility = "visible";

    if (action.value == "over") {
        mouse.lastElementChild.lastElementChild.src = icons["actions_32"][iconsTxt[0]];
        let anim = document.getElementById("mouseAnimCont");
        focusRect = document.getElementById("focusRect");

        let sideBaseTRI = 20;
        let heightTRI = (Math.sqrt(3) / 2) * sideBaseTRI;
        let durationOver = Math.round(Math.round(mouseAnimationDuration / 1000) / 3);
        animTL.to(anim, durationOver, { x: sideBaseTRI, ease: "none" });
        animTL.to(anim, durationOver, { x: 0, ease: "none" }, ">");
        animTL.to(anim, durationOver / 2, { y: heightTRI, repeat: 1, ease: "none", yoyo: true }, "<");
        animTL.to(anim, durationOver, { x: 0, y: 0 }, ">");
        animTL.to(mouse, { scale: 1 }, ">");
        animTL.to("#insideRect", { opacity: 0 }, "<");
        animTL.call(endAnimation);

    } else {

        for (let index = 0; index < iconsTxt.length; index++) {
            mouse.lastElementChild.lastElementChild.src = icons["actions_32"][iconsTxt[index]];
            await sleep(timeSleeping);
        }
        endAnimation();
    }

}

function focusELementData(VBB) {

    let scaleX = VBB.width / boundScreen.width;
    let scaleY = VBB.height / boundScreen.height;
    return { x: scaleX, y: scaleY };
}

function popUpHandler(ind) {

    let newPopUp = document.getElementById("popUp_" + ind);
    let formerPopUp;
    let style = newPopUp.currentStyle || window.getComputedStyle(newPopUp);
    let marginTop = parseFloat(style.marginTop.substring(0, style.marginTop.length - 2));
    let borderBT = parseFloat(style.borderBottomWidth.substring(0, style.borderBottomWidth.length - 2));
    let moveUp = newPopUp.offsetHeight + (marginTop * 2);
    //console.debug("data : " + marginTop + ";" + newPopUp.offsetHeight + ";" + moveUp);
    let FPUbound;
    if (previousAnimationIndex != ind) {
        //console.debug("visiblePopUp : " + visiblePopUp);
        for (let index = ind - visiblePopUp; index < ind; index++) {
            if (index >= 0) {
                formerPopUp = document.getElementById("popUp_" + index);
                if (formerPopUp != null) {
                    formerPopUp.classList.remove("centerPopUp");
                    FPUbound = formerPopUp.getBoundingClientRect();
                    if (formerPopUp.getBoundingClientRect().bottom > formerPopUp.parentElement.getBoundingClientRect().top) {

                        if (((FPUbound.bottom + marginTop + borderBT) != formerPopUp.parentElement.getBoundingClientRect().bottom
                            && index == previousAnimationIndex)) {
                            //console.info("ex center")

                            let centerMoveUp = moveUp - (formerPopUp.parentElement.offsetHeight - (FPUbound.y + FPUbound.height + marginTop));
                            popUpTL.to(formerPopUp, { duration: popUpDuration, x: "=" + 0, y: '-=' + centerMoveUp }, "<");

                        } else {
                            //console.debug("normalPopUp")
                            popUpTL.to(formerPopUp, { duration: popUpDuration, y: '-=' + moveUp }, "<");
                        }
                    } else {
                        //console.debug("to stock : " + formerPopUp.firstElementChild.firstElementChild.firstElementChild.innerHTML);
                        APUToStock(formerPopUp);
                        visiblePopUp--;
                    }
                }
            }
        }
        ////console.warn(popUpVisible);
    }

}

function allAPUToStock() {

    visiblePopUp = 0;
    ////console.info("allToStock");
    for (let index = 0; index < num_act; index++) {
        let popUpToInit = document.getElementById("popUp_" + index);
        if (popUpToInit != null) {
            APUToStock(popUpToInit);
        } else {
            //console.debug("popUp doesn't exist");
        }
    }

}

function APUToStock(popUpToInit) {
    gsap.set(popUpToInit, { duration: 1, x: popUpToInit.parentElement.offsetWidth, y: 0 });
}

function actionDataSelection(action) {

    ////console.debug("actionDataSelection")
    let data = {};

    let subAction = getSubAction(action.type);

    switch (subAction) {
        case 'ActionMouseKey':
            data["Type"] = emptyVal(action.value);
            break;
        case 'ActionMouseScroll':
            let tmpValue = "";
            if (action.value === "0") {
                tmpValue = "automatic scroll";
            } else {
                tmpValue = emptyVal(action.value);
            }
            data["delta (+/-)"] = tmpValue;
            break;
        case 'ActionChannelStart':
            data["Name"] = action.Channel.name;
            channelFragmentData(data, action.data);
            boundData(data, action.Channel.bound);
            appChannel[action.Channel.name] = data.app;
            break;
        case 'ActionChannelClose':
            data["Name"] = action.Channel.name;
            break;
        case 'ActionWindowSwitch':
            data["To windows"] = emptyVal(action.value);
            break;
        case 'ActionChannelSwitch':
            data["From"] = action.Channel.name;
            data["To"] = emptyVal(action.value);
            break;
        case 'ActionWindowResize':
            data["Name"] = action.Channel.name;
            boundData(data, action.Channel.bound);
            resizeFormat(data, action.value.split(","));
            break;
        case 'ActionWindowState':
            data["type"] = emptyVal(action.value);
            //initActiveChannel(subAction, action);
            break;
        case 'ActionGotoUrl':
            if (NagAction.includes(action.value)) {
                data["action"] = emptyVal(action.value);
            } else {
                data["url"] = emptyVal(action.value);
            }
            break;
        case 'ActionText':
            data["text"] = emptyVal(action.value);
            break;
        case 'ActionAssertCount':
            data["Expected elements number"] = action.data.substring(action.data.lastIndexOf(" ") + 1, action.data.length);
            data["founds elements"] = emptyVal(action.value);
            break;
        case 'ActionAssertProperty':
            data["verification"] = parseACTAP(action);
            break;
        case 'ActionProperty':
            data["recover"] = "'" + action.value +
                "' propertie value of element is : " + action.data;
            break;
        case 'ActionPropertySet':
            data["modification"] = "change '" + action.value +
                "' propertie value of element to : " + action.data;
            break;
        case 'ActionAssertValue':
            data["verification"] = parseACTAV(action);
            break;
        case 'ActionScripting':
            data["value"] = emptyVal(action.value);
            break;
        case 'ActionSelect':
            data["selected value"] = emptyVal(action.data);
            break;
        case 'ActionComment':
            data["value"] = emptyVal(action.value);
            data["data"] = emptyVal(action.data);
            NBComment++;
            break;
        case 'ActionApi':
            data["data"] = emptyVal(action.data);
            data["value"] = emptyVal(action.value);
            break;
        case 'ActionMouseDragDrop':
            data["action"] = emptyVal(action.value);
            break;
        case 'ActionMouse':
            data["Type"] = emptyVal(action.value);
            break;
        default:
            data["default"] = "Not available Data";
            break;
    }

    ////console.log(data);
    if (action.element != null) {
        elementFragmentData(data, action.element);
    }

    return data;
}

function initActiveChannel(subAction, action) {

    ////console.log("initactivechannel : " + action.index + ";" + subAction);
    activeChannel[action.index] = {};

    activeChannel[action.index].name = action.Channel.name;

    switch (subAction) {
        case "ActionChannelStart":
            activeChannel[action.index].bound = action.Channel.bound;
            break;
        case "ActionWindowResize":
            let boundValue = action.value.split(",");
            activeChannel[action.index].bound = { x: boundValue[0], y: boundValue[1], width: boundValue[2], height: boundValue[3] }
            break;
        case "ActionChannelSwitch":

            let previousFound = false;
            let previous;
            let ind = action.index;
            while (!previousFound && ind >= 0) {

                if (activeChannel[ind] != null && activeChannel.name == action.value) {
                    previous = activeChannel[ind].bound;
                    previousFound = true;
                } else {
                    ind--;
                }
            }
            activeChannel[action.index + 1] = {};
            activeChannel[action.index + 1].bound = previous;
            activeChannel[action.index + 1].name = action.value;

            break;
        case "ActionChannelClose":
            break;
        case "ActionChannelState":

            break;

        default:
            break;

    }
    activeChannel[action.index].bound = action.Channel.bound;

}

function formatActionInfo(datas) {

    let formatedDatas = [];
    for (let dataTitle in datas) {
        let line = document.createElement("div");
        line.classList.add("act_Infos");
        let title = document.createElement("div");
        title.classList.add("title");
        let info = document.createElement("div");
        info.classList.add("dataInfos");
        title.innerHTML = dataTitle + " : ";
        info.innerHTML = datas[dataTitle];
        line.appendChild(title);
        line.appendChild(info);
        formatedDatas.push(line);
    }

    ////console.debug(formatedDatas);

    return formatedDatas;

}

function elementFragmentData(data, datas) {

    for (item in datas) {
        if (item == "extra") {
            if (datas[item]["hPos"] != null) {
                data["click position"] = "Hpos(" + datas[item]["hVos"] + "): " + datas[item]["hPos"] +
                    ", Vpos(" + datas[item]["vVos"] + "): " + datas[item]["vPos"];
            }
        } else if (item != "bound") {
            data[item] = datas[item];
        }
    }

}

function emptyVal(data) {

    let modifData = data;

    if (data.length = 0 || data === "" || data === "&empty;") {
        modifData = "<div style='opacity: 0.5;'><I>empty</I></div>";
    }

    return modifData;
}

function parseACTAP(action) {

    let parsedInfo = "";

    let locProp = action.value.substring(0, action.value.indexOf(" "));

    parsedInfo = "<b>Property verify : </b>" + locProp +
        "<br><b>Value found : </b>" + emptyVal(action.data) +
        "<br><b>Expected Value : </b>" + action.value;

    return parsedInfo;

}

function parseACTAV(action) {

    let parsedInfo = "";

    let comparator = action.data.substring(0, action.data.indexOf(" "))
    let champs2 = action.data.substring(action.data.indexOf(" "), action.data.length);

    parsedInfo = emptyVal(action.value) + " " + comparator +
        " " + emptyVal(champs2);

    return parsedInfo;

}

function boundData(data, datas) {

    data["location"] = datas.x + ", " + datas.y;
    data["size"] = datas.width + ", " + datas.height;
}

function resizeFormat(data, resizeData) {

    data.location = "from : (" + data.location + ") " +
        "\nto (" + resizeData[0] + ", " + resizeData[1] + ")";

    data.size = "from : (" + data.size + ") " +
        "\nto (" + resizeData[2] + ", " + resizeData[3] + ")";

}

function channelFragmentData(data, datas) {

    let tmpData = datas.replaceAll("\\", "/").replaceAll('\'', "\"");
    let JSONData = JSON.parse(tmpData);

    data["app"] = JSONData.app;
    data["appVersion"] = JSONData.appVersion;
    data["OS"] = JSONData.os;

}

function timeDateSwitcher() {
    timeOrDate = !timeOrDate;
    timeUpdate();
}

function timeUpdate() {

    let tmpCT = timeOrDate ? test.started + currentTime : currentTime;

    let cTDiv = document.getElementById("currentTime");
    cTDiv.innerHTML = timeFormat(new Date(tmpCT));

    let tTDiv = document.getElementById("totalTime");

    let tmpTT = timeOrDate ? test.started + duration : duration;

    tTDiv.innerHTML = timeFormat(new Date(tmpTT));
}

function timeFormat(epoch, unity = false, around = false) {

    let d = getFormatNum(epoch.getUTCDay() - 4, 2);
    let h = getFormatNum(epoch.getUTCHours(), 2);
    let m = getFormatNum(epoch.getUTCMinutes(), 2);
    let s = getFormatNum(epoch.getUTCSeconds(), 2);
    let ms = getFormatNum(epoch.getUTCMilliseconds(), 3);
    let time = "";

    s = around ? Math.round(parseFloat(s + "." + ms)) : s;

    time += d.length != 0 ? (unity ? d + " day(s) " : d + ":") : "";
    time += h.length != 0 ? (unity ? h + " hour(s) " : h + ":") : "";
    time += m.length != 0 ? (unity ? m + " minute(s) " : m + ":") : "00:";
    time += s.length != 0 ? (unity ? s + " second(s) " : s + ".") : "00.";

    if (!around) {
        time += ms.length != 0 ? (unity ? ms + " milisecond(s) " : ms) : "000";
    }
    return time;

}

function getFormatNum(num, digit) {

    if (num > 0) {
        return num.toLocaleString('fr-FR', { minimumIntegerDigits: digit, useGrouping: false });
    } else {
        return "";
    }

}

function initBanner() {

    ////console.debug("initBanner");
    let testName = document.getElementById("testName");
    let scriptName = document.getElementById("scriptName");

    testName.innerHTML = test.suiteName;
    scriptName.innerHTML = test.testName;

    initTestInfo();

    timeOrDate = false;
    timeUpdate();
}

function initTestInfo() {
    let testID = document.getElementById("testID");
    let externalID = document.getElementById("externalID");
    let author = document.getElementById("testAuthor");
    let startedTime = document.getElementById("testStarted");
    let testDuration = document.getElementById("testDuration");
    let testDescript = document.getElementById("testDesc");
    let testReq = document.getElementById("testRequirement");
    let testGroups = document.getElementById("testGroups");

    let time = new Date(test.started);

    testID.innerHTML = emptyVal(test.testId);
    externalID.innerHTML = emptyVal(test.externalId);
    author.innerHTML = test.author;
    startedTime.innerHTML = time.toLocaleDateString() + " at " + time.toLocaleTimeString();
    testDuration.innerHTML = timeFormat(new Date(duration), true, true);
    testDescript.innerHTML = emptyVal(test.description);
    testReq.innerHTML = emptyVal(test.prerequisite);
    test.groups = typeof (test.groups) != "undefined" ? test.groups : "";
    testGroups.innerHTML = emptyVal(test.groups);

    let infos = document.getElementById("testInfos");
    let insideInfos = infos.cloneNode(true);

    insideInfos.id = "insideInfos";
    insideInfos.className = "inside_Infos";
    insideInfos.style = "";

    overPlayer.prepend(insideInfos);
    forceOverlay();

    let compagnyPic = document.getElementById("CompagnyPic");

    if (typeof (customsImg) != "undefined" && typeof (customsImg.defaultWM) != "undefined") {
        compagnyPic.src = customsImg.watermark != null ? customsImg.watermark : customsImg.defaultWM;
    }
    ////console.debug("end init Data");
}

function hiddeTestsInfos() {
    overPlayer.firstElementChild.style.display = "none";
}

function switchFull() {

    let player = document.getElementById("player");

    if (fullScreenAsk) {
        if (player.requestFullscreen) {
            player.requestFullscreen();
        }
    } else {
        if (document.exitFullscreen && fullScreen) {
            document.exitFullscreen();
        }
    }

    fullScreenAsk = !fullScreenAsk;

}

// create Element
function createActionsElements() {
    ////console.log("create Actions Elements :")
    for (let action_index = 0; action_index < num_act; action_index++) {
        createMark(action_index);
        let subAction = getSubAction(actions[action_index].type);
        if (!abstractAct.includes(subAction)) {
            createActionsElement(actions[action_index]);
        }
    }

    cleanTpl();

}

function createMark(index) {
    const mark_box = document.getElementById("progress_box");
    const marker = document.createElement("div");
    marker.id = "mark_" + index;
    marker.className = "marker";
    let posLeft = ((index + 1) * 100 / (num_act)) - ((1 * 100 / (num_act)));
    ////console.debug("posLeft (" + index + ") : " + posLeft);
    marker.style.left = posLeft + "%";

    marker.onclick = function () { selectAction(index); };
    mark_box.appendChild(marker);

    let subAction = getSubAction(actions[index].type);

    if (subAction === "ActionComment") {
        sectionMark(index, posLeft, mark_box);
        NumSection = index;
    }

    addActsData[index] = NumSection;
}

function sectionMark(index, posLeft, mark) {

    const foncSection = document.createElement("div");
    foncSection.id = "FSection_" + index;
    foncSection.className = "fonctional_section";
    foncSection.style.left = posLeft + "%";
    const TTSection = document.createElement("span");
    TTSection.className = "tooltip_section";

    const sectionText = document.createElement("div");

    TTSection.appendChild(sectionText);
    const titleTT = document.createElement("div");
    titleTT.classList.add("TT_title");
    titleTT.innerHTML = "Comment";
    const textTT = document.createElement("div");
    textTT.classList.add("TT_text");
    textTT.innerHTML = actions[index].data;

    TTSection.appendChild(titleTT);
    TTSection.appendChild(textTT);

    foncSection.appendChild(TTSection);

    foncSection.onmouseover = function () { sectionHovering(this); };
    TTSection.onmouseout = function () { sectionOuting(this); };

    mark.appendChild(foncSection);

}

function selectSubIcon(subAction, value) {

    let iconTxt;

    if (subAction === "ActionMouseScroll") {
        let scrollValue = parseInt(value);
        if (scrollValue == 0) {
            iconTxt = iconAnim[subAction].default[iconAnim[subAction].default.length - 1];
        } else if (scrollValue > 0) {
            iconTxt = iconAnim[subAction].down[iconAnim[subAction].down.length - 1];
        } else {
            iconTxt = iconAnim[subAction].up[iconAnim[subAction].up.length - 1];
        }
    } else {
        value = value.replaceAll("-", "_");
        if (value.length != 0 && typeof (iconAnim[subAction][value]) != "undefined") {
            iconTxt = iconAnim[subAction][value][iconAnim[subAction][value].length - 1];
        } else {
            iconTxt = iconAnim[subAction].default[iconAnim[subAction].default.length - 1];
        }
    }
    return iconTxt;
}

function selectSubIcons(subAction, value) {

    let iconsTxt = [];

    if (subAction === "ActionMouseScroll") {
        let scrollValue = parseInt(value);
        if (scrollValue == 0) {
            iconsTxt = iconAnim[subAction].default;
        } else if (scrollValue > 0) {
            iconsTxt = iconAnim[subAction].down;
        } else {
            iconsTxt = iconAnim[subAction].up;
        }
    } else {
        value = value.replaceAll("-", "_");
        if (typeof (iconAnim[subAction][value]) != "undefined") {
            iconsTxt = iconAnim[subAction][value];
        } else {
            iconsTxt = iconAnim[subAction].default;
        }
    }
    return iconsTxt;
}

function createActionsElement(action) {

    ////console.debug("action : "+ action.index);

    const scrollActionsZone = document.getElementById("scrollerDataActions");
    const scrollActTmp = document.getElementById("scrollActionTmp");
    let newActTmp = scrollActTmp.cloneNode(true);

    const popUpBox = document.getElementById("insideActionsListBox");
    const popUpTemplate = document.getElementById("popUpTest");
    let newPopUp = popUpTemplate.cloneNode(true);

    let subAction = getSubAction(action.type);

    newActTmp.id = "S_Act_" + action.index;
    newActTmp.className = "scroll_act";
    if (subAction === "ActionComment") {
        newActTmp.classList.add("comment");
    }

    newPopUp.id = "popUp_" + action.index;
    newPopUp.className = "pop_Up";
    newPopUp.style = "right: 2% ; bottom : 0px";

    if (action.error) {
        if (action.stop) {
            newActTmp.classList.add("error");
            newPopUp.classList.add("error");
            newActTmp.firstElementChild.lastElementChild.firstElementChild.src = icons["actions_20"].stop_fail;
        } else {
            newActTmp.classList.add("default");
            newPopUp.classList.add("default");
            newActTmp.firstElementChild.lastElementChild.firstElementChild.src = icons["actions_20"].continu_fail;
        }
    } else {
        newActTmp.firstElementChild.lastElementChild.firstElementChild.src = icons["actions_20"].continu;
    }

    let iconTxt = selectSubIcon(subAction, action.value);

    let ActionsElement = [newActTmp, newPopUp];

    ActionsElement.forEach(function (element) {
        let header = element.firstElementChild;
        let ind = header.querySelector("#index");

        let iconImg = header.querySelector("#icon").firstElementChild; //header.div[icon].img    
        let title = header.querySelector("#title"); //header.title

        ind.innerHTML = action.index;
        iconImg.src = icons["actions_24"][iconTxt];
        title.innerHTML = actionName[subAction][lang[langInd]];
        let data = element.lastElementChild;

        formatActionInfo(actionDataSelection(action)).forEach(function (item) {
            data.appendChild(item);
        })

    });

    let data = newActTmp.lastElementChild;
    data.style.height = "0px";
    data.style.padding = "0px";
    data.style.opacity = "0";
    newActTmp.addEventListener("click", function () {
        selectAction(Number(action.index));
    });

    popUpBox.appendChild(newPopUp);
    scrollActionsZone.appendChild(newActTmp);

}

function cleanTpl() {
    const scrollActTmp = document.getElementById("scrollActionTmp");

    let tmp = scrollActTmp.parentElement;
    tmp.removeChild(tmp.querySelector("#scrollActionTmp"));

}

// Initialisation
const initData = async function () {

    num_act = Object.keys(actions).length;

    await createActionsElements();

    initLoading();

    endInit();
}

function initLoading() {
    duration = actions[num_act - 1].timeLine + actions[num_act - 1].duration;

    initProgress();
    initBanner();

}

function endInit() {

    ////console.debug("init scrollBar");
    let firstAction = -1;

    for (index = 0; index < num_act; index++) {
        const act = document.getElementById("S_Act_" + index);
        if (act != null) {
            firstAction = index
            break;
        }
    }

    if (firstAction != -1) {

        const act = document.getElementById("S_Act_" + firstAction);

        act.classList.add("selected");

        let content = act.lastElementChild;
        let contentSize = getDatacontSize(content);
        content.style.opacity = "1";
        gsap.to(content, 0.5, { height: contentSize + "px", marginTop: "6px" });
    }


}

function getDatacontSize(cont) {

    let contentSize = 0;
    for (const child of cont.children) {
        contentSize += child.clientHeight;
    }

    contentSize += 6 * (cont.children.length - 1)

    return contentSize;

}

function updateChannelData(action) {

    currentChannel = action.Channel.name;
    boundScreen = action.Channel.bound;

    let scrollChan = document.getElementById("SDCName");
    scrollChan.innerHTML = currentChannel;
    let insideCurChan = document.getElementById("insideSDCName");
    insideCurChan.innerHTML = currentChannel;

    updateSection(action.index - 1);

}

function updateSection(index) {

    //    console.debug("action inde : " + index);

    let tempInd = index < 0 ? 0 : index;

    let scrollerSection = document.getElementById("scrollerSection");
    let insideSection = document.getElementById("insideSection");

    let sections = [scrollerSection, insideSection]

    sections.forEach(function (el) {
		if (addActsData[tempInd] == -1) {
            el.firstElementChild.innerHTML = "";
            gsap.to(el.firstElementChild, 0.5, { width: "0px" });
            gsap.to(el.children[1], 0.5, { width: "0px" });
			
			el.lastElementChild.innerHTML = "Script start";
        } else {
            el.firstElementChild.innerHTML = addActsData[tempInd];
            gsap.to(el.firstElementChild, 0.5, { width: "fit-content" });
            gsap.to(el.children[1], 0.5, { width: "16px" });
			
			let xhtml = actions[addActsData[tempInd]].data;
			let xdiv = document.createElement("div");
			xdiv.innerHTML = xhtml;
			let xdata = xdiv.textContent || xdiv.innerText || "";
			if(xdata.length > 40){
				xdata = xdata.substring(0, 40);
			}
			el.lastElementChild.innerHTML = xdata;
        }
    })

}

// Event 
const el = document.getElementById('player');
el.addEventListener('fullscreenchange', fullscreenchanged);

function fullscreenchanged(event) {

    let btnFull = document.getElementById("fullscreen_btn");
    let burgerMenu = document.getElementById("scrollerBurger");
    let info = document.getElementsByClassName("scriptInfos")[0];
    if (document.fullscreenElement) {
        //console.log(`Element: ${document.fullscreenElement.id} entered fullscreen mode.`);
        btnFull.src = icons["player_32"].reduce_hover;
        burgerMenu.src = icons["player_32"].steps_list_dark;
        info.src = icons["player_32"].info_light;
        el.classList.add("full");
        setStateHeader(true);
        setStateInside(true);
        fullScreen = true;
    } else {
        //console.log('Leaving fullscreen mode.');
        btnFull.src = icons["player_32"].full_screen;
        burgerMenu.src = icons["player_32"].steps_list_dark;
        info.src = icons["player_32"].info_dark;

        el.classList.remove("full");
        reduceVideoElement(false);
        setStateHeader(false);
        setStateInside(false);
        fullScreen = false;

    }

    allAPUToStock();

    globalUpdate();

};

function reduceVideoElement(full) {

    let player = document.getElementById("player");
    //console.log("reduce");
    if (full) {
        player.classList.add("fullReduce");
        videoBoxSizeSelection(true);
    } else {
        player.classList.remove("fullReduce");
        videoBoxSizeSelection(false);
    }
}

function scrollOpen() {

    //console.debug("click open scrolls");

    let actionScroller = document.getElementById("actionsScroller");
    scrollStyle = getComputedStyle(actionScroller);
    if (fullScreen && scrollStyle.display === "none") {

        let player = document.getElementById("player");
        let burgerMenu = document.getElementById("scrollerBurger");

        burgerMenu.src = icons["player_32"].steps_list;

        reduceVideoElement(true);
        setStateHeader(false);

        setATSControls(false);
        //setStateScroll(true);
        setStateInside(false);
    }

}

function scrollClose() {

    let burgerMenu = document.getElementById("scrollerBurger");

    burgerMenu.src = icons["player_32"].steps_list;

    reduceVideoElement(false);
    setStateHeader(true);
    setATSControls(true);
    //setStateScroll(false);
    setStateInside(true);

}

function scriptOpen() {
    ////console.debug("openScriptInfos");
    let overScript = document.getElementsByClassName("scriptDataOverlay")[0];
    let overScriptP = document.getElementsByClassName("scriptDataOverlay")[1];
    let scriptInfos = document.getElementById("testInfos");

    overScript.style.display = "flex";
    overScriptP.style.display = "flex";
    scriptInfos.style.display = "flex";
}

function scriptClose() {
    ////console.debug("CloseScriptInfos");
    let overScript = document.getElementsByClassName("scriptDataOverlay")[0];
    let overScriptP = document.getElementsByClassName("scriptDataOverlay")[1];

    let scriptInfos = document.getElementById("testInfos");

    overScript.style.display = "none";
    overScriptP.style.display = "none";
    scriptInfos.style = "";

}

function setStateScroll(visible) {
    let actionScroller = document.getElementById("actionsScroller");
    actionScroller.style.display = visible ? "flex" : "none";

}

function setATSControls(inside) {

    let btnPrev = document.getElementById("prev_btn");
    let btnNext = document.getElementById("next_btn");

    let dark = inside ? "" : "_dark";

    btnPrev.firstElementChild.src = icons["player_32"]["arrow_left" + dark];
    btnNext.lastElementChild.src = icons["player_32"]["arrow_right" + dark];

}

const setStateInside = async function (visible) {
    let inside = document.getElementById("insideActionsListBox");

    let insideChan = document.getElementById("insideChannel");
    let insideSect = document.getElementById("insideSection");

    await sleep(200);
    let style = document.getElementById("videoBox").currentStyle || window.getComputedStyle(document.getElementById("videoBox"));
    let height = parseFloat(style.height.substring(0, style.height.length - 2));

    inside.style.height = height - inside.parentElement.offsetHeight + "px";
    inside.style.visibility = visible ? "visible" : "hidden";
    insideChan.style.visibility = visible ? "visible" : "hidden";

    insideSect.style.visibility = visible ? "visible" : "hidden";



}

function setStateHeader(inside) {

    let header = document.getElementById("pBanner");

    if (inside) {
        header.classList.remove("_out");
        header.lastElementChild.style.display = "flex";
    } else {
        header.classList.add("_out");
        header.lastElementChild.style.display = "none";
    }

}

const TLMouseMove = async function (e) {
    var x = e.clientX;
    var y = e.clientY;

    var BPD = document.getElementById("progress_box");
    let BP = BPD.getBoundingClientRect();
    var PRP = BPD.parentElement.offsetTop;

    let curH = parseFloat((y - BP.y));

    if (curH > BPD.offsetHeight) {
        e.stopImmediatePropagation();
        return;
    }

    minPU.style.bottom = (PRP - 40) + "px";
    minPU.style.display = "flex";

    let curPourPos = parseFloat((x - BP.x) * 100 / BP.width);

    const actPourTL = 100 / num_act;
    let actNumber = 0;
    let actNumMod = 0;
    if (sequential) {
        actNumber = Math.trunc(curPourPos / actPourTL);
        actNumMod = curPourPos % actPourTL;
    }

    actNumber = actNumber > num_act ? num_act : actNumber;
    actNumber = actNumber < 0 ? 0 : actNumber;
    let overAction = actions[actNumber];

    if (minPrevAct != actNumber) {
        while (minImgBox.firstChild) {
            minImgBox.removeChild(minImgBox.firstChild);
        }

        if (actNumber < imgStockLoaded) {
            let img, frame;
            const type = imgStock[overAction.index].imageType;
            for (let ind = 0; ind < imgStock[overAction.index].numImages; ind++) {
                frame = document.createElement("div");
                frame.classList.add("min_Img_Frame");
                img = document.createElement("img");
                img.classList.add("minImg");
                const imageData = imgStock[overAction.index].imagesPool[ind];
                img.src = "data:image/" + type + ";base64," + imageData;
                img.onclick = function () { selectAction(actNumber, ind); };
                frame.appendChild(img);
                minImgBox.appendChild(frame);
            }

            minPrevAct = actNumber;
        }

        let minTxtDiv = minPU.lastElementChild;
        const timeStart = overAction.timeLine;
        let temp = timeOrDate ? test.started + timeStart : timeStart;
        minTxtDiv.innerHTML = actNumber + "." + overAction.type.substring(overAction.type.lastIndexOf(".") + 1, overAction.type.length) + "<br>" +
            timeFormat(new Date(temp));

        let progBoxWidth = document.getElementById("progress_box").offsetWidth;
        let posMarker = document.getElementById("mark_" + actNumber).offsetLeft;

        await sleep(5);
        let tmpPos = posMarker - (minPU.offsetWidth / 2);
        if (tmpPos > 0 && (tmpPos + (minPU.offsetWidth) + 16) < (progBoxWidth)) {
            minPU.style.left = (tmpPos + 16) + "px";
        } else if (tmpPos < 0) {
            minPU.style.left = 16 + "px";
        } else if ((tmpPos + (minPU.offsetWidth) + 16) > (progBoxWidth)) {
            minPU.style.left = (progBoxWidth - minPU.offsetWidth + 16) + "px";
        } else {
            minPU.style.left = ((progBoxWidth / 2) - (minPU.offsetWidth / 2) + "px");
        }
    }

}

function TLMouseClick(e) {
    var x = e.clientX;
    var BPD = document.getElementById("progress_box");
    let BP = BPD.getBoundingClientRect();

    let curPourPos = parseFloat((x - BP.x) * 100 / BP.width);
    const actPourTL = 100 / num_act;
    let actNumber = 0;
    let actNumMod = 0;
    if (sequential) {
        actNumber = Math.trunc(curPourPos / actPourTL);
        actNumMod = curPourPos % actPourTL;
    }

    selectAction(actNumber);

}

function str(el) {
    if (!el) return "null"
    return el.id || el.className;
}

function TLMouseOut(e) {

    if (minPU.style.display === "flex") {
        if (str(e.target) === "progress_box" && (str(e.relatedTarget) != "miniaturesPU")) {
            minPU.style.display = "none";
        } else if (str(e.target).includes("min") && (str(e.relatedTarget) != "progress_box") &&
            !str(e.relatedTarget).includes("min")) {
            minPU.style.display = "none";

        }
    }
}

function hovering(ele) {

    let eleID = str(ele);
    ////console.debug(eleID);
    let imgHover = "";
    switch (eleID) {
        case "play_pause":
            imgHover = playing ? icons["player_32"].pause_hover : icons["player_32"].play_hover;
            break;
        case "stop_btn":
            imgHover = icons["player_32"].stop_hover;
            break;
        case "fullscreen_btn":
            imgHover = fullScreen ? icons["player_32"].reduce_hover_1 : icons["player_32"].full_screen_hover;
            break;
        default:
            break;
    }

    ele.src = imgHover;
}

function outing(ele) {

    let eleID = str(ele);
    ////console.debug(eleID);
    let img = "";
    switch (eleID) {
        case "play_pause":
            img = playing ? icons["player_32"].pause : icons["player_32"].play;
            break;
        case "stop_btn":
            img = icons["player_32"].stop;
            break;
        case "fullscreen_btn":
            img = fullScreen ? icons["player_32"].reduce_hover : icons["player_32"].full_screen;
            break;
        default:
            break;
    }

    ele.src = img;
}

function sectionHovering(el) {

    let TTSections = document.getElementsByClassName("tooltip_section");

    if (TTSections.length > 0) {

        for (let index = 0; index < TTSections.length; index++) {
            TTSections.item(index).style.display = "";
        }
    }

    el.firstElementChild.style.display = "flex";

}

function sectionOuting(el) {

    el.style.display = "";

}

// Running
//console.debug("loaded at : " + new Date(Date.now()).toLocaleString())

function InterfaceInitialisation() {

    let btnPlay = document.getElementById("play_pause");
    let btnStop = document.getElementById("stop_btn");
    let btnPrev = document.getElementById("prev_btn");
    let btnNext = document.getElementById("next_btn");
    let btnFull = document.getElementById("fullscreen_btn");
    let btnActF = document.getElementById("actionsBurgerImg");
    let SHTImg = document.getElementById("scrollerBurger");
    let SHCruise = document.getElementById("cruise_btn");
    let chanIcn = document.getElementById("channel_icn");
    let sectionIcn = document.getElementById("section_icn");
    let insSectionIcn = document.getElementById("inside_section_icn");
    let InsChanIcn = document.getElementById("inside_channel_icn");
    let scriptInfosIcn = document.getElementsByClassName("scriptInfos")[0];

    setStateHeader(false);

    chanIcn.src = icons["player_32"].status;
    sectionIcn.src = icons["actions_16"].document_comment;
    insSectionIcn.src = icons["actions_16"].document_comment;
    InsChanIcn.src = icons["player_32"].status;

    minPU.style.display = "none";

    btnPlay.src = icons["player_32"].play;
    btnStop.src = icons["player_32"].stop;
    btnFull.src = icons["player_32"].full_screen;
    btnPrev.firstElementChild.src = icons["player_32"].arrow_left;
    btnNext.lastElementChild.src = icons["player_32"].arrow_right;
    btnActF.src = icons["player_32"].steps_list;
    SHTImg.src = icons["player_32"].steps_list_dark;
    SHCruise.src = icons["player_32"].cross_dark;
    scriptInfosIcn.src = icons["player_32"].info_dark;

    mouse.firstElementChild.src = icons["actions_32"].cursor;

    minOuterY = document.getElementById("pBanner").offsetHeight +
        document.getElementsByClassName("ATS_controls")[0].offsetHeight;

}

InterfaceInitialisation();

window.addEventListener('load', function () {

});

const playerLoader = async function () {
    let allLoaded = false;
    let imgZero = false;
    let DLV = document.getElementById("dataLoadedValue");
    let ILV = document.getElementById("imageLoadedValue");

    ////console.log("loading data and Images");

    while (!allLoaded) {

        await sleep(50);
        //console.debug("loading");

        if (dataLoaded) {

            if (imgStockLoaded >= 1 && !imgZero) {
                //console.debug("dataLoaded");
                DLV.innerHTML = "Loaded";
                ILV.innerHTML = "Loading";
                initData();
                let sect = document.getElementsByClassName("videoSection")[0];
                sect.style.width = "";
                imgZero = true;
                // 2 elements graphiques créés dans le dataSelection. peux être Optimisé.
                await sleep(100);
                NBComment = NBComment / 2;
            } else {
                //console.debug("loading Image, NBComment : " + NBComment);
                ILV.innerHTML = "Loading   " + imgStockLoaded + "/" + test.actions + " ( " +
                    Math.round((imgStockLoaded * 100) / test.actions) + "% )";
            }

            if (imgStockLoaded >= (test.actions - NBComment)) {
                //console.debug("ALL DATA LOADED!!!!!!");
                allLoaded = true;
                ILV.innerHTML = "Loaded";
                focus.style.visibility = "";
                gsap.to(ILV.parentElement.parentElement, 2,
                    { x: -(ILV.parentElement.parentElement.offsetWidth + 50), delay: 1, ease: Power0.none });
                let overText = document.getElementById("overText");
                overText.style.display = "none";
                overText.innerHTML = "No viable image<br>for this action"

            }

        } else {
            //console.debug("loading data in progress");
        }

    }

    //console.log("end loading");

}

playerLoader();